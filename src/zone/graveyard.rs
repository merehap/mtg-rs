use std::collections::BTreeMap;

use card::card::Card;
use player::player_id::PlayerID;
use zone::card_zone::CardZone;

///
/// The Graveyard is the zone where destroyed permanents and used cards go.
///
pub struct Graveyard {
    owner_id: PlayerID,
    cards: BTreeMap<GraveyardCardID, Card>,
    next_id: usize,
}

impl Graveyard {
    pub fn new(owner_id: PlayerID) -> Graveyard {
        Graveyard{owner_id, cards: BTreeMap::new(), next_id: 0}
    }

    pub fn cards(&self) -> Vec<(&GraveyardCardID, &Card)> {
        self.cards.iter().collect()
    }

    pub fn size(&self) -> usize {
        self.cards.len()
    }

    pub fn get(&self, card_id: GraveyardCardID) -> Option<&Card> {
        self.cards.get(&card_id)
    }

    pub fn add(&mut self, card: Card) -> &Card {
        let id = self.next_id;
        self.next_id += 1;
        self.cards.insert(GraveyardCardID{owner_id: self.owner_id, id}, card);
        &self.cards[&GraveyardCardID{owner_id: self.owner_id, id}]
    }

    pub fn add_all(&mut self, cards: Vec<Card>) -> Vec<&Card> {
        let mut ids = Vec::new();

        for card in cards {
            ids.push(self.next_id);
            self.add(card);
        }

        let mut result = Vec::new();
        for id in ids {
            result.push(&self.cards[&GraveyardCardID{owner_id: self.owner_id, id}]);
        }

        result
    }

    pub fn remove(&mut self, card_id: GraveyardCardID) -> Option<Card> {
        self.cards.remove(&card_id)
    }
}

impl CardZone<GraveyardCardID> for Graveyard {
    fn cards_by_id(&self) -> BTreeMap<GraveyardCardID, &Card> {
        self.cards.iter()
            .map(|(&k, v)| (k, v))
            .collect()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct GraveyardCardID {
    owner_id: PlayerID,
    id: usize,
}

impl GraveyardCardID {
    pub fn owner_id(&self) -> PlayerID {
        self.owner_id
    }
}
