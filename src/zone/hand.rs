use std::collections::BTreeMap;
use std::fmt;

use card::card::Card;
use player::player_id::PlayerID;
use zone::card_zone::CardZone;

///
/// The Hand is the zone where Cards exist before they have been played to
/// the Battlefield or expended to the Graveyard.
///
pub struct Hand {
    owner_id: PlayerID,
    cards: BTreeMap<HandCardID, Card>,
    maximum_size: Option<usize>,
    next_hand_card_id: usize,
}

impl Hand {
    pub fn new(owner_id: PlayerID) -> Hand {
        Hand {
            owner_id,
            cards: BTreeMap::new(),
            maximum_size: Some(7),
            next_hand_card_id: 0,
        }
    }

    pub fn cards(&self) -> Vec<(&HandCardID, &Card)> {
        self.cards.iter().collect()
    }

    pub fn maximum_size(&self) -> Option<usize> {
        self.maximum_size
    }

    pub fn size(&self) -> usize {
        self.cards.len()
    }

    pub fn add(&mut self, card: Card) -> &Card {
        let id = self.next_hand_card_id;
        self.next_hand_card_id += 1;

        self.cards.insert(HandCardID {owner_id: self.owner_id, id}, card);
        &self.cards[&HandCardID {owner_id: self.owner_id, id}]
    }

    pub fn add_all(&mut self, cards: Vec<Card>) -> Vec<&Card> {
        let mut ids = Vec::new();

        for card in cards {
            ids.push(self.next_hand_card_id);
            self.add(card);
        }

        let mut result = Vec::new();
        for id in ids {
            result.push(&self.cards[&HandCardID{owner_id: self.owner_id, id}]);
        }

        result
    }

    pub fn get(&self, card_id: HandCardID) -> Option<&Card> {
        self.cards.get(&card_id)
    }

    pub fn remove(&mut self, card_id: HandCardID) -> Option<Card> {
        self.cards.remove(&card_id)
    }
}

impl CardZone<HandCardID> for Hand {
    fn cards_by_id(&self) -> BTreeMap<HandCardID, &Card> {
        self.cards.iter()
            .map(|(&k, v)| (k, v))
            .collect()
    }
}

impl fmt::Display for Hand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (_, card) in self.cards() {
            write!(f, "\"{}\" ", card.format_display_name()).unwrap();
        }

        Ok(())
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct HandCardID {
    owner_id: PlayerID,
    id: usize,
}

impl HandCardID {
    pub fn owner_id(&self) -> PlayerID {
        self.owner_id
    }
}
