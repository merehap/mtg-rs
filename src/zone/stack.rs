use std::collections::BTreeMap;
use std::fmt;

use card::card::Card;
use effect::effect::Effect;
use ephemeral::ephemeral_card::EphemeralCard;
use ephemeral::ephemeral_face::EphemeralFace;
use ephemeral::ephemeral_layout::SplitID;
use face::face::Face;
use location::location::CardLocation;
use permanent::face::permanent_face::PermanentFace;
use player::player_id::PlayerID;
use zone::zoned_face_id::ZonedFaceID;

///
/// The Stack is the zone where Spells and StackAbilities are stored
/// before they are resolved.
///
pub struct Stack {
    stack: BTreeMap<StackObjectID, StackObject>,
    // Use isize so that we can count *down*.
    // BTreeMap.iter().first() always returns the lowest element,
    // so counting down allows us to easily be able to get the "top" of the stack.
    next_id: isize,
}

impl Stack {
    pub fn new() -> Stack {
        Stack {
            stack: BTreeMap::new(),
            next_id: 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.stack.is_empty()
    }

    pub fn push(
        &mut self,
        owner_id: PlayerID,
        controller_id: PlayerID,
        object_type: StackObjectType,
        ephemeral_effects: Vec<Effect>,
        is_split_second: IsSplitSecond,
    ) -> StackObjectID {

        let id = StackObjectID {id: self.next_id};
        self.stack.insert(
            id,
            StackObject {
                id,
                owner_id,
                controller_id,
                object_type,
                is_split_second,
                ephemeral_effects,
            },
        );

        self.next_id -= 1;

        id
    }

    pub fn get_mut_or_panic(&mut self, id: StackObjectID) -> &mut StackObject {
        self.stack.get_mut(&id)
            .expect("StackObjectID is not present on the stack.")
    }

    pub fn pop(&mut self) -> Option<StackObject> {
        let stack_object_id = self.stack.iter()
            .next()
            .map(|(i, _)| i.clone())?;

        self.stack.remove(&stack_object_id)
    }

    pub fn to_vec(&self) -> Vec<StackObject> {
        self.stack.clone().into_iter()
            .map(|(_, o)| o)
            .collect()
    }
}

impl fmt::Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (_, stack_object) in self.stack.iter() {
            write!(f, "\"{}\", ", stack_object).expect("Bad fmt write.");
        }

        Ok(())
    }
}

///
/// A StackItem is any type of object that is allowed on the Stack.
///
#[derive(Clone)]
pub struct StackObject {
    pub id: StackObjectID,
    pub owner_id: PlayerID,
    pub controller_id: PlayerID,
    pub object_type: StackObjectType,
    pub is_split_second: IsSplitSecond,
    pub ephemeral_effects: Vec<Effect>,
}

impl StackObject {
    pub fn name(&self) -> String {
        self.object_type.name()
    }
}

impl fmt::Display for StackObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.object_type.fmt(f)
    }
}

#[derive(Clone)]
pub enum StackObjectType {
    Spell {
        card_view: Box<CardView>,
        overridden_success_destination: Option<CardLocation>,
        overridden_failure_destination: Option<CardLocation>,
        was_kicked: bool,
    },
    SpellCopy,
    Ability,
    ObjectTriggeredAbility {
        trigger_object_controller_id: PlayerID,
    }
}

impl StackObjectType {
    pub fn name(&self) -> String {
        match self {
            StackObjectType::Spell {card_view, ..} =>
                card_view.view_face()
                    .maybe_name()
                    .map(|n| n.clone().0)
                    .expect("StackObjects must have a name"),
            _ => unimplemented!("Name is not yet implemented for non-spells."),
        }
    }
}

impl fmt::Display for StackObjectType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            StackObjectType::Spell {..} => write!(
                f,
                "{}",
                self.name()
            ),
            _ => unimplemented!(),
        }
    }
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Debug)]
pub struct CardView {
    card: Card,
    split_id: Option<SplitID>,
}

impl CardView {
    pub fn new(card: Card, zoned_face_id: ZonedFaceID) -> CardView {
        CardView {card, split_id: zoned_face_id.split_id()}
    }
    pub fn view_face(&self) -> Box<&Face> {
        match self.view_face_type() {
            FaceType::Permanent(face) => Box::new(face.as_face()),
            FaceType::Ephemeral(face) => Box::new(face.as_face()),
        }
    }

    pub fn view_face_type(&self) -> FaceType {
        CardView::lookup_face_type_by_split(&self.card, self.split_id)
    }

    pub fn lookup_face_type(card: &Card, zoned_face_id: ZonedFaceID) -> FaceType {
        CardView::lookup_face_type_by_split(card, zoned_face_id.split_id())
    }

    fn lookup_face_type_by_split<'a>(
        card: &'a Card, split_id: Option<SplitID>) -> FaceType<'a> {

        match (card, split_id) {
            (Card::Permanent(_), Some(_)) =>
                panic!("Can't specify a split ID for a permanent card."),
            (Card::Permanent(permanent), None) =>
                FaceType::Permanent(permanent.primary_face()),
            (Card::Ephemeral(ephemeral), _) =>
                FaceType::Ephemeral(
                    CardView::get_face_from_ephemeral(ephemeral, split_id)),
        }
    }

    fn get_face_from_ephemeral<'a>(
        ephemeral: &'a EphemeralCard,
        maybe_split_id: Option<SplitID>,
    ) -> Box<&'a EphemeralFace> {

        use ephemeral::ephemeral_layout::EphemeralLayout::*;
        match (&ephemeral.layout, maybe_split_id) {
            (Regular(regular), None) => Box::new(regular),
            (Fuse(fused), None) => Box::new(fused),
            (Split(_, _), Some(split_id)) | (Fuse(_), Some(split_id)) => {
                Box::new(*ephemeral.faces().get(&Some(split_id))
                    .expect("SplitID should have been found.")
                    .as_ref())
            },
            (_, _) => panic!(
                "Bad combination of layout type and SplitID {:?}", maybe_split_id),
        }
    }

    pub fn card(&self) -> &Card {
        &self.card
    }

    pub fn into_card(self) -> Card {
        self.card
    }
}

pub enum FaceType<'a> {
    Permanent(Box<&'a PermanentFace>),
    Ephemeral(Box<&'a EphemeralFace>),
}

///
/// IsSplitSecond indicates if a StackItem will resolve prior to other
/// StackItems being placed on the Stack.
///
///
#[derive(Clone)]
pub enum IsSplitSecond {
    SplitSecond,
    NotSplitSecond,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub struct StackObjectID {
    id: isize,
}
