use ephemeral::ephemeral_layout::SplitID;
use zone::zoned_card_id::ZonedCardID;

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Copy, Debug)]
pub struct ZonedFaceID {
    zoned_card_id: ZonedCardID,
    split_id: Option<SplitID>,
}

impl ZonedFaceID {
    pub fn new(zoned_card_id: ZonedCardID, split_id: Option<SplitID>) -> ZonedFaceID {
        ZonedFaceID { zoned_card_id, split_id }
    }

    pub fn default_face(zoned_card_id: ZonedCardID) -> ZonedFaceID {
        ZonedFaceID { zoned_card_id, split_id: None}
    }

    pub fn zoned_card_id(&self) -> ZonedCardID {
        self.zoned_card_id
    }

    pub fn split_id(&self) -> Option<SplitID> {
        self.split_id
    }
}