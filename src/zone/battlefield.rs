use std::collections::{BTreeSet, BTreeMap};

use permanent::face::permanent_face::PermanentFace;
use permanent::flags::Flags;
use permanent::permanent::Permanent;
use permanent::permanent_id::PermanentID;
use permanent::permanent_layout::PermanentLayout;
use player::player_id::PlayerID;
use script::condition::permanent_condition::PermanentCondition;

///
/// A Battlefield is the per-player zone that holds all permanents in the game.
///
pub struct Battlefield {
    // TODO: Better data structure?
    sides: BTreeMap<PlayerID, Side>,
    next_permanent_id: usize,
    next_face_down_id: usize,
}

impl Battlefield {
    pub fn new() -> Battlefield {
        let mut sides = BTreeMap::new();
        sides.insert(PlayerID::Player0, Side::new());
        sides.insert(PlayerID::Player1, Side::new());

        Battlefield {
            sides,
            next_permanent_id: 0,
            next_face_down_id: 0,
        }
    }

    pub fn lookup_controller_id(
        &self,
        permanent_id: PermanentID,
    ) -> Option<PlayerID> {

        for (player_id, side) in &self.sides {
            if side.permanents.contains_key(&permanent_id) {
                return Some(*player_id);
            }
        }

        None
    }

    pub fn foreach_permanent<F>(&self, player_id: PlayerID, f: F) where
        F: FnMut(&Permanent) {

        self.side(player_id).permanents.values()
            .for_each(f)
    }

    pub fn foreach_permanent_mut<F>(&mut self, player_id: PlayerID, f: F) where
        F: FnMut(&mut Permanent) {

        self.side_mut(player_id).permanents.values_mut()
            .for_each(f)
    }

    pub fn foreach_selected_permanent<F>(
        &self, player_id: PlayerID, permanent_ids: Vec<PermanentID>, f: F) where
        F: FnMut(&Permanent) {

        self.side(player_id).permanents.values()
            .filter(|perm| permanent_ids.contains(&perm.id()))
            .for_each(f);
    }

    pub fn foreach_selected_permanent_mut<F>(
        &mut self, player_id: PlayerID, permanent_ids: Vec<PermanentID>, f: F) where
        F: FnMut(&mut Permanent) {

        self.side_mut(player_id).permanents.values_mut()
            .filter(|perm| permanent_ids.contains(&perm.id()))
            .for_each(f)
    }

    pub fn foreach_permanent_both_players_mut<F>(&mut self, f: F) where
        F: Fn(&mut Permanent) {

        for perm in self.side_mut(PlayerID::Player0).permanents.values_mut() {
            f(perm)
        }

        for perm in self.side_mut(PlayerID::Player1).permanents.values_mut() {
            f(perm)
        }
    }

    pub fn matching_permanents(&self, condition: PermanentCondition) -> Vec<PermanentID> {
        let check = condition.compile();

        let mut ids = Vec::new();
        for perm in self.side(PlayerID::Player0).permanents() {
            if check(perm) {
                ids.push(perm.id());
            }
        }

        for perm in self.side(PlayerID::Player1).permanents() {
            if check(perm) {
                ids.push(perm.id());
            }
        }

        ids
    }

    pub fn add_permanent(
        &mut self,
        owner_id: PlayerID,
        controller_id: PlayerID,
        permanent_layout: PermanentLayout) -> &Permanent {

        let flags = permanent_layout.primary_face()
            .map(|pl| Flags::from_permanent_face(Box::new(*pl)))
            .unwrap_or(Flags::initial());

        let permanent = Permanent::new(
            PermanentID::from_usize(self.next_permanent_id),
            owner_id,
            permanent_layout,
            flags,
        );

        let permanent_id = permanent.id();
        self.side_mut(controller_id).add(permanent);

        self.next_permanent_id += 1;

        &self.side(controller_id).permanents[&permanent_id]
    }

    pub fn get(&self, permanent_id: PermanentID) -> Option<&Permanent> {
        if let Some(permanent) = self.side(PlayerID::Player0).permanents.get(&permanent_id) {
            return Some(permanent);
        }

        if let Some(permanent) = self.side(PlayerID::Player1).permanents.get(&permanent_id) {
            return Some(permanent);
        }

        None
    }

    pub fn get_mut(&mut self, permanent_id: PermanentID) -> Option<&mut Permanent> {
        for side in self.sides_mut() {
            if let Some(permanent) = side.permanents.get_mut(&permanent_id) {
                return Some(permanent);
            }
        }

        None
    }

    pub fn get_multiple(&self, permanent_ids: BTreeSet<PermanentID>) -> Vec<&Permanent> {
        permanent_ids.into_iter()
            .filter_map(|id| self.get(id))
            .collect()
    }

    pub fn get_mut_for_player(
        &mut self,
        controller_id: PlayerID,
        permanent_id: PermanentID) -> Option<&mut Permanent> {

        self.side_mut(controller_id).permanents.get_mut(&permanent_id)
    }

    pub fn remove(&mut self, permanent_id: PermanentID) -> Option<Permanent> {
        if let Some(permanent) = self.side_mut(PlayerID::Player0).permanents.remove(&permanent_id) {
            return Some(permanent);
        }

        if let Some(permanent) = self.side_mut(PlayerID::Player1).permanents.remove(&permanent_id) {
            return Some(permanent);
        }

        None
    }

    pub fn change_controller(&mut self, new_controller_id: PlayerID, permanent_id: PermanentID) {
        let current_controller_id = self.controller_id(permanent_id);
        match current_controller_id {
            Some(current) if current != new_controller_id => {
                let perm = self.remove(permanent_id).expect("PermID should have already been checked.");
                self.side_mut(new_controller_id).add(perm);
            },
            _ => {},
        }
    }

    pub fn controller_id(&self, permanent_id: PermanentID) -> Option<PlayerID> {
        for (&player_id, side) in &self.sides {
            if side.permanents.contains_key(&permanent_id) {
                return Some(player_id);
            }
        }

        None
    }

    pub fn side(&self, player_id: PlayerID) -> &Side {
        self.sides.get(&player_id).expect("Bad Player ID???")
    }

    pub fn side_mut(&mut self, player_id: PlayerID) -> &mut Side {
        self.sides.get_mut(&player_id).expect("Bad Player ID???")
    }

    fn sides(&self) -> Vec<&Side> {
        self.sides.values().collect()
    }

    fn sides_mut(&mut self) -> Vec<&mut Side> {
        self.sides.values_mut().collect()
    }
}

pub struct Side {
    permanents: BTreeMap<PermanentID, Permanent>,
}

impl Side {
    fn new() -> Side {
        Side {permanents: BTreeMap::new()}
    }

    pub fn add(&mut self, permanent: Permanent) {
        let permanent_id = permanent.id();
        self.permanents.insert(permanent_id, permanent);
    }

    pub fn permanents(&self) -> Vec<&Permanent> {
        self.permanents.values().collect()
    }

    pub fn permanents_mut(&mut self) -> Vec<&mut Permanent> {
        self.permanents.values_mut().collect()
    }

    pub fn matching_permanents<F>(&self, f: F) -> Vec<PermanentID>
        where F: Fn(&Permanent) -> bool {

        self.permanents.values()
            .filter(|perm| f(*perm))
            .map(|perm| perm.id())
            .collect()
    }

    pub fn non_land_permanents(&self) -> Vec<&Permanent> {
        self.permanents.values()
            .filter(|perm| !perm.effective_face().is_land())
            .collect()
    }

    pub fn lands_with_no_other_permanent_type(&self) -> Vec<&Permanent> {
        self.permanents.values()
            .filter(|perm| perm.effective_face().is_land() &&
                !perm.effective_face().has_any_non_land_type())
            .collect()
    }
}
