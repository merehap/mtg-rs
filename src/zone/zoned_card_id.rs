use player::player_id::PlayerID;
use zone::exile::ExileCardID;
use zone::graveyard::GraveyardCardID;
use zone::hand::HandCardID;
use zone::library::LibraryCardID;

#[derive(PartialOrd, Ord, PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum ZonedCardID {
    Library(LibraryCardID),
    Hand(HandCardID),
    Graveyard(GraveyardCardID),
    Exile(ExileCardID),
}

impl ZonedCardID {
    pub fn owner_id(&self) -> PlayerID {
        match self {
            ZonedCardID::Library(id) => id.owner_id(),
            ZonedCardID::Hand(id) => id.owner_id(),
            ZonedCardID::Graveyard(id) => id.owner_id(),
            ZonedCardID::Exile(id) => id.owner_id(),
        }
    }
}