use std::collections::{BTreeSet, BTreeMap};
use std::hash::Hash;

use card::card::Card;
use script::condition::card_condition::CardCondition;

pub trait CardZone<ID: Ord + Hash> {
    fn cards_by_id(&self) -> BTreeMap<ID, &Card>;

    fn matching(&self, card_condition: CardCondition) -> BTreeSet<ID> {
        let check = card_condition.compile();

        let mut ids = BTreeSet::new();
        for (id, card) in self.cards_by_id() {
            if check(card) {
                ids.insert(id);
            }
        }

        ids
    }
}