use std::collections::BTreeMap;

use rand::prelude::*;

use card::card::Card;
use player::player_id::PlayerID;
use util::positive_integer::PositiveInteger;
use zone::card_zone::CardZone;

///
/// The Library is the zone where Cards are stored before they are used.
///
pub struct Library {
    owner_id: PlayerID,
    cards: Vec<Card>,
    is_top_card_exposed: IsExposed,
    initial_count: usize,
}

impl Library {
    pub fn fixed_order(owner_id: PlayerID, cards: Vec<Card>) -> Library {
        Library {
            owner_id,
            initial_count: cards.len(),
            cards,
            is_top_card_exposed: IsExposed::NotExposed,
        }
    }

    pub fn randomized_order(owner_id: PlayerID, mut cards: Vec<Card>) -> Library {
        Library::shuffle_items(&mut cards);
        Library::fixed_order(owner_id, cards)
    }

    pub fn shuffle(&mut self) {
        Library::shuffle_items(&mut self.cards);
    }

    fn shuffle_items<T>(cards: &mut Vec<T>) {
        let mut rng = rand::thread_rng();
        for i in 0..cards.len() {
            let other_index = rng.gen_range(0, cards.len());
            cards.swap(i, other_index)
        }
    }

    pub fn cards(&self) -> Vec<(LibraryCardID, &Card)> {
        self.cards_by_id().iter()
            .map(|(&k, &v)| (k, v))
            .collect()
    }

    pub fn size(&self) -> usize {
        self.cards.len()
    }

    pub fn add(&mut self, location: LibraryLocation, card: Card) -> &Card {
        let index = self.insertion_index_from_location(location);
        self.cards.insert(index, card);
        &self.cards[index]
    }

    pub fn add_all(
        &mut self, location: LibraryLocation, cards: Vec<Card>) -> Vec<&Card> {

        let count = cards.len();
        let index = self.insertion_index_from_location(location);
        for card in cards {
            self.cards.insert(index, card);
        }

        self.cards[index..index+count].iter()
            .rev()
            .collect()
    }

    pub fn get(&self, location: LibraryLocation) -> Option<&Card> {
        let index = self.index_from_location(location)?;
        self.cards.get(index)
    }

    pub fn remove(&mut self, location: LibraryLocation) -> Option<Card> {
        let index = self.index_from_location(location)?;
        Some(self.cards.remove(index))
    }

    // The returned index may be invalid if you remove cards after calling this.
    fn index_from_location(&self, location: LibraryLocation) -> Option<usize> {
        let len = self.cards.len() as isize;
        let index = match location {
            LibraryLocation::Top => len - 1,
            LibraryLocation::Bottom => 0,
            LibraryLocation::UnderTop(count) => len - count.to_isize() - 1,
        };

        if index >= 0 && index < len {
            Some(index as usize)
        } else {
            None
        }
    }

    fn insertion_index_from_location(&self, location: LibraryLocation) -> usize {
        let len = self.cards.len();
        match location {
            LibraryLocation::Top => len,
            LibraryLocation::Bottom => 0,
            LibraryLocation::UnderTop(count) =>
                len.checked_sub(count.to_usize()).unwrap_or(0),
        }
    }
}

impl CardZone<LibraryCardID> for Library {
    fn cards_by_id(&self) -> BTreeMap<LibraryCardID, &Card> {
        let mut cards_by_id = BTreeMap::new();
        if let Some(card) = self.cards.get(0) {
            let card_id = LibraryCardID {
                owner_id: self.owner_id,
                location: LibraryLocation::Top,
            };
            cards_by_id.insert(card_id, card);
        }

        for i in (1..self.cards.len()).filter_map(PositiveInteger::from_usize) {
            let card_id = LibraryCardID {
                owner_id: self.owner_id,
                location: LibraryLocation::UnderTop(i),
            };
            cards_by_id.insert(card_id, &self.cards[i.to_usize()]);
        }

        cards_by_id
    }
}

///
/// IsExposed indicates whether a given Card is revealed to all players or not.
///
enum IsExposed {
    Exposed,
    NotExposed,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct LibraryCardID {
    owner_id: PlayerID,
    location: LibraryLocation,
}

impl LibraryCardID {
    pub fn owner_id(&self) -> PlayerID {
        self.owner_id
    }

    pub fn location(&self) -> LibraryLocation {
        self.location
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum LibraryLocation {
    Top,
    Bottom,
    UnderTop(PositiveInteger),
}
