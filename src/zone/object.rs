use card::card;
use permanent::permanent;
use player::player_id::PlayerID;
use player::player_state;
use zone::stack;

pub enum Object<'a> {
    Card {
        owner_id: PlayerID,
        card: &'a card::Card,
    },
    Permanent {
        controller_id: PlayerID,
        permanent: &'a permanent::Permanent,
    },
    // Players are technically not objects,
    // but it is much more convenient to treat them that way.
    Player(&'a player_state::PlayerState),
    StackObject(&'a stack::StackObject),
}

impl <'a> Object<'a> {
    pub fn owner_id(&self) -> PlayerID {
        match self {
            Object::Card {owner_id, ..} => *owner_id,
            Object::Permanent{permanent, ..} => permanent.owner_id(),
            Object::Player(player) => player.id,
            Object::StackObject(stack_object) => stack_object.owner_id,
        }
    }

    pub fn controller_id(&self) -> PlayerID {
        match self {
            // Cards don't technically have a controller_id,
            // but if the game asks for them, an owner_id must be given.
            Object::Card {owner_id, ..} => *owner_id,
            Object::Permanent {controller_id, ..} => *controller_id,
            Object::Player(player) => player.id,
            Object::StackObject(stack_object) => stack_object.controller_id,
        }
    }
}
