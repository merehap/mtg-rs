use std::collections::BTreeMap;

use card::card::Card;
use player::player_id::PlayerID;
use zone::card_zone::CardZone;

///
/// Exile is the zone where cards go when they have been removed from the game.
///
pub struct Exile {
    owner_id: PlayerID,
    cards: BTreeMap<ExileCardID, Card>,
    next_id: usize,
}

impl Exile {
    pub fn new(owner_id: PlayerID) -> Exile {
        Exile {
            owner_id,
            cards: BTreeMap::new(),
            next_id: 0,
        }
    }

    pub fn get(&self, card_id: ExileCardID) -> Option<&Card> {
        self.cards.get(&card_id)
    }

    pub fn add(&mut self, card: Card) -> &Card {
        let id = self.next_id;
        self.next_id += 1;
        self.cards.insert(ExileCardID {owner_id: self.owner_id, id}, card);
        &self.cards[&ExileCardID {owner_id: self.owner_id, id}]
    }

    pub fn add_all(&mut self, cards: Vec<Card>) -> Vec<&Card> {
        let mut ids = Vec::new();

        for card in cards {
            ids.push(self.next_id);
            self.add(card);
        }

        let mut result = Vec::new();
        for id in ids {
            result.push(&self.cards[&ExileCardID {owner_id: self.owner_id, id}]);
        }

        result
    }

    pub fn remove(&mut self, card_id: ExileCardID) -> Option<Card> {
        self.cards.remove(&card_id)
    }
}

impl CardZone<ExileCardID> for Exile {
    fn cards_by_id(&self) -> BTreeMap<ExileCardID, &Card> {
        self.cards.iter()
            .map(|(k, v)| (*k, v))
            .collect()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct ExileCardID {
    owner_id: PlayerID,
    id: usize,
}

impl ExileCardID {
    pub fn owner_id(&self) -> PlayerID {
        self.owner_id
    }
}
