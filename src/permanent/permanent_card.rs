use cost::cost::Cost;
use permanent::face::costed_permanent_face::CostedPermanentFace;
use permanent::face::costless_permanent_face::CostlessPermanentFace;
use permanent::face::land_face::LandFace;
use permanent::face::permanent_face::PermanentFace;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentCard {
    /// A Permanent Card that only has a single Face.
    RegularPermanent(CostedPermanentFace),
    /// A Permanent Card that only has a single Face, which is a Land.
    Land(LandFace),
    /// A Permanent Card that has a front Face and a back Face.
    DoubleFaced(DoubleFaced),
    /// A Permanent Card that has a front Face that is a Land and a back Face.
    DoubleFacedLand(DoubleFacedLand),
    /// A Permanent Card that has a top Face and a bottom Face.
    Flip(Flip),
}

impl PermanentCard {
    pub fn maybe_casting_cost(&self) -> Option<&Cost> {
        self.primary_face().maybe_casting_cost()
    }

    pub fn is_land(&self) -> bool {
        self.primary_face().is_land()
    }

    pub fn is_artifact(&self) -> bool {
        self.primary_face().is_artifact()
    }

    pub fn is_creature(&self) -> bool {
        self.primary_face().is_creature()
    }

    pub fn is_enchantment(&self) -> bool {
        self.primary_face().is_enchantment()
    }

    pub fn is_planeswalker(&self) -> bool {
        self.primary_face().is_planeswalker()
    }

    pub fn primary_face(&self) -> Box<&PermanentFace> {
        use self::PermanentCard::*;
        match self {
            RegularPermanent(face) => Box::new(face),
            Land(face) => Box::new(face),
            DoubleFaced(self::DoubleFaced{front, ..}) => Box::new(front),
            DoubleFacedLand(self::DoubleFacedLand{front, ..}) => Box::new(front),
            Flip(self::Flip{top, ..}) => Box::new(top),
        }
    }

    pub fn faces(&self) -> Vec<Box<&PermanentFace>> {
        let mut faces: Vec<Box<&PermanentFace>> = Vec::new();

        use self::PermanentCard::*;
        match self {
            RegularPermanent(face) => faces.push(Box::new(face)),
            Land(face) => faces.push(Box::new(face)),
            DoubleFaced(self::DoubleFaced{front, back}) => {
                faces.push(Box::new(front));
                faces.push(Box::new(back));
            },
            DoubleFacedLand(self::DoubleFacedLand{front, back}) => {
                faces.push(Box::new(front));
                faces.push(Box::new(back));
            },
            Flip(self::Flip{top, bottom}) => {
                faces.push(Box::new(top));
                faces.push(Box::new(bottom));
            },
        }

        faces
    }

    pub fn converted_mana_cost(&self) -> usize {
        (*self.primary_face()).converted_mana_cost()
    }

    pub fn format_display_name(&self) -> String {
        self.primary_face().name_or_unnamed()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct DoubleFaced {
    pub front: CostedPermanentFace,
    pub back: CostlessPermanentFace,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct DoubleFacedLand {
    pub front: LandFace,
    pub back: CostlessPermanentFace,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Flip {
    pub top: CostedPermanentFace,
    pub bottom: CostlessPermanentFace,
}
