use std::collections::BTreeSet;

use ability::permanent_abilities::{PermanentAbilities, PermanentAbilitiesBuilder};
use cost::cost::Cost;
use mana::color::Color;
use face::face::{Face, Rarity, Name};
use permanent::face::permanent_face;
use permanent::face::permanent_face::{PermanentFace, NonLandPermanentTypes, IsSnow, IsLegendary};
use permanent::permanent_layout::PermanentLayout;
use permanent::permanent_type::creature::{Creature, Power, Toughness};
use permanent::permanent_type::land::Land;
use script::count::Count;
use player::player_id::PlayerID;
use game::Game;

pub struct EffectiveFace {
    pub maybe_name: Option<Name>,
    pub colors: BTreeSet<Color>,
    pub is_snow: permanent_face::IsSnow,
    pub maybe_casting_cost: Option<Cost>,
    pub maybe_land: Option<Land>,
    pub non_land_permanent_types: NonLandPermanentTypes,
    pub is_legendary: IsLegendary,
    pub maybe_rarity: Option<Rarity>,

    pub permanent_abilities: PermanentAbilities,
    pub graveyard_permanent_abilities: PermanentAbilities,
    pub kicker_permanent_abilities: PermanentAbilities,
}

impl EffectiveFace {
    pub fn from_permanent_layout(permanent_type: &PermanentLayout) -> EffectiveFace {
        match permanent_type.primary_face() {
            Some(primary_face) => EffectiveFace {
                maybe_name: (*primary_face).maybe_name().cloned(),
                colors: (*primary_face).colors().clone(),
                is_snow: (*primary_face).is_snow(),
                maybe_casting_cost: (*primary_face).maybe_casting_cost().cloned(),
                maybe_land: (*primary_face).maybe_land().cloned(),
                non_land_permanent_types: (*primary_face).non_land_permanent_types().clone(),
                is_legendary: (*primary_face).is_legendary(),
                maybe_rarity: (*primary_face).maybe_rarity(),

                permanent_abilities: primary_face.permanent_abilities().clone(),
                graveyard_permanent_abilities: primary_face.graveyard_permanent_abilities().clone(),
                kicker_permanent_abilities: primary_face.kicker_permanent_abilities().clone(),
            },
            // Face down cards have fixed base stats.
            None => EffectiveFace {
                maybe_name: None,
                colors: BTreeSet::new(),
                is_snow: IsSnow::NotSnow,
                maybe_casting_cost: None,
                maybe_land: None,
                non_land_permanent_types:
                NonLandPermanentTypes::creature_only(Creature {
                    creature_types: BTreeSet::new(),
                    power: Power(Count::Fixed(2)),
                    toughness: Toughness(Count::Fixed(2)),
                }),
                is_legendary: IsLegendary::NonLegendary,
                maybe_rarity: None,

                permanent_abilities: PermanentAbilitiesBuilder::new().build(),
                graveyard_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
                kicker_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
            }
        }
    }

    pub fn maybe_name(&self) -> &Option<Name> {
        &self.maybe_name
    }

    pub fn power_toughness(&self, controller_id: PlayerID, game: &Game) -> Option<(isize, isize)> {
        let creature = self.non_land_permanent_types.creature()?;
        Some((creature.power.0.to_isize(controller_id, &game), creature.toughness.0.to_isize(controller_id, &game)))
    }

    pub fn power(&self, controller_id: PlayerID, game: &Game) -> Option<isize> {
        self.power_toughness(controller_id, game).map(|(p, _)| p)
    }

    pub fn toughness(&self, controller_id: PlayerID, game: &Game) -> Option<isize> {
        self.power_toughness(controller_id, game).map(|(_, t)| t)
    }
}

impl Face for EffectiveFace {
    fn maybe_name(&self) -> Option<&Name> {
        self.maybe_name.as_ref()
    }

    fn colors(&self) -> BTreeSet<Color> {
        self.colors.clone()
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        self.maybe_casting_cost.as_ref()
    }
}

impl PermanentFace for EffectiveFace {
    fn is_snow(&self) -> IsSnow {
        self.is_snow
    }

    fn maybe_land(&self) -> Option<&Land> {
        self.maybe_land.as_ref()
    }

    fn non_land_permanent_types(&self) -> &NonLandPermanentTypes {
        &self.non_land_permanent_types
    }

    fn is_legendary(&self) -> IsLegendary {
        self.is_legendary
    }

    fn maybe_rarity(&self) -> Option<Rarity> {
        self.maybe_rarity
    }

    fn permanent_abilities(&self) -> &PermanentAbilities {
        &self.permanent_abilities
    }

    fn graveyard_permanent_abilities(&self) -> &PermanentAbilities {
        &self.graveyard_permanent_abilities
    }

    fn kicker_permanent_abilities(&self) -> &PermanentAbilities {
        &self.kicker_permanent_abilities
    }
}
