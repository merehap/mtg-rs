use ability::permanent_abilities::PermanentAbilities;
use face::face::{Face, AsFace, Rarity};
use permanent::permanent_type::artifact::Artifact;
use permanent::permanent_type::creature::Creature;
use permanent::permanent_type::enchantment::Enchantment;
use permanent::permanent_type::land::Land;
use permanent::permanent_type::planeswalker::Planeswalker;

pub trait PermanentFace: Face + AsFace {
    fn is_snow(&self) -> IsSnow;
    fn maybe_land(&self) -> Option<&Land>;
    fn non_land_permanent_types(&self) -> &NonLandPermanentTypes;
    fn is_legendary(&self) -> IsLegendary;
    fn maybe_rarity(&self) -> Option<Rarity>;
    fn permanent_abilities(&self) -> &PermanentAbilities;
    fn graveyard_permanent_abilities(&self) -> &PermanentAbilities;
    fn kicker_permanent_abilities(&self) -> &PermanentAbilities;

    fn is_artifact(&self) -> bool {
        self.non_land_permanent_types().artifact.is_some()
    }

    fn is_creature(&self) -> bool {
        self.non_land_permanent_types().creature.is_some()
    }

    fn is_enchantment(&self) -> bool {
        self.non_land_permanent_types().enchantment.is_some()
    }

    fn is_land(&self) -> bool {
        self.maybe_land().is_some()
    }

    fn is_planeswalker(&self) -> bool {
        self.non_land_permanent_types().planeswalker.is_some()
    }

    fn has_any_non_land_type(&self) -> bool {
        self.non_land_permanent_types().any_present()
    }
}

///
/// A PermanentFaceType represents the different types of Permanents
/// along with their associated data.
///
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct NonLandPermanentTypes {
    pub artifact: Option<Artifact>,
    pub creature: Option<Creature>,
    pub enchantment: Option<Enchantment>,
    pub planeswalker: Option<Planeswalker>,
}

impl NonLandPermanentTypes {
    pub fn none() -> NonLandPermanentTypes {
        NonLandPermanentTypes {
            artifact: None,
            creature: None,
            enchantment: None,
            planeswalker: None,
        }
    }

    pub fn creature_only(creature: Creature) -> NonLandPermanentTypes {
        let mut result = NonLandPermanentTypes::none();
        result.creature = Some(creature);
        result
    }

    pub fn any_present(&self) -> bool {
        *self != NonLandPermanentTypes::none()
    }

    pub fn artifact(&self) -> Option<&Artifact> {
        self.artifact.as_ref()
    }

    pub fn creature(&self) -> Option<&Creature> {
        self.creature.as_ref()
    }

    pub fn enchantment(&self) -> Option<&Enchantment> {
        self.enchantment.as_ref()
    }

    pub fn planeswalker(&self) -> Option<&Planeswalker> {
        self.planeswalker.as_ref()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum IsSnow {
    Snow,
    NotSnow,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum IsLegendary {
    Legendary,
    NonLegendary,
}

