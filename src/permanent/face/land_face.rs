use std::collections::BTreeSet;
use std::iter::FromIterator;

use ability::permanent_abilities::{PermanentAbilities, PermanentAbilitiesBuilder};
use cost::cost::Cost;
use face::face::{Face, Name, Rarity, Expansion, CardNumber, ArtistName, FlavorText};
use permanent::face::permanent_face::{PermanentFace, NonLandPermanentTypes, IsSnow, IsLegendary};
use permanent::permanent_type::land::{Land, LandType, BasicLandType};
use mana::color::Color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct LandFace {
    pub name: Name,
    pub expansion: Expansion,
    pub card_number: CardNumber,
    pub rarity: Rarity,
    pub artist_name: ArtistName,
    pub flavor_text: Option<FlavorText>,

    pub is_snow: IsSnow,
    pub is_legendary: IsLegendary,

    pub land: Land,
    pub non_land_permanent_types: NonLandPermanentTypes,
    pub permanent_abilities: PermanentAbilities,
    pub graveyard_permanent_abilities: PermanentAbilities,
    pub kicker_permanent_abilities: PermanentAbilities,
}

impl LandFace {
    pub fn basic(
        name: &str,
        expansion: Expansion,
        card_number: CardNumber,
        artist_name: ArtistName,
        basic_land_type: BasicLandType,
    ) -> LandFace {

        LandFace {
            name: Name(name.to_string()),
            expansion,
            card_number,
            artist_name,
            flavor_text: None,
            rarity: Rarity::Land,
            is_snow: IsSnow::NotSnow,
            is_legendary: IsLegendary::NonLegendary,
            land: Land {
                land_type: LandType::Basic {
                    basic_land_types: BTreeSet::from_iter(vec![basic_land_type].into_iter())
                },
            },
            non_land_permanent_types: NonLandPermanentTypes::none(),
            permanent_abilities: PermanentAbilitiesBuilder::new().build(),
            graveyard_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
            kicker_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
        }
    }
}

impl Face for LandFace {
    fn maybe_name(&self) -> Option<&Name> {
        Some(&self.name)
    }

    fn colors(&self) -> BTreeSet<Color> {
        BTreeSet::new()
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        None
    }
}

impl PermanentFace for LandFace {
    fn is_snow(&self) -> IsSnow {
        self.is_snow
    }

    fn maybe_land(&self) -> Option<&Land> {
        Some(&self.land)
    }

    fn non_land_permanent_types(&self) -> &NonLandPermanentTypes {
        &self.non_land_permanent_types
    }

    fn is_legendary(&self) -> IsLegendary {
        self.is_legendary
    }

    fn maybe_rarity(&self) -> Option<Rarity> {
        Some(self.rarity)
    }

    fn permanent_abilities(&self) -> &PermanentAbilities {
        &self.permanent_abilities
    }

    fn graveyard_permanent_abilities(&self) -> &PermanentAbilities {
        &self.graveyard_permanent_abilities
    }

    fn kicker_permanent_abilities(&self) -> &PermanentAbilities {
        &self.kicker_permanent_abilities
    }
}
