use std::collections::BTreeSet;

use ability::permanent_abilities::PermanentAbilities;
use cost::cost::Cost;
use face::face::{CostedFace, Face, Name, Expansion, CardNumber, Rarity, ArtistName, FlavorText};
use permanent::face::permanent_face::{PermanentFace, NonLandPermanentTypes, IsSnow, IsLegendary};
use permanent::permanent_type::land::Land;
use mana::color::Color;

///
/// A CostedPermanentFace is a Face of a Permanent that has a ManaCost or
/// a Permanent Card.
///
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct CostedPermanentFace {
    pub name: Name,
    pub expansion: Expansion,
    pub card_number: CardNumber,
    pub rarity: Rarity,
    pub artist_name: ArtistName,
    pub flavor_text: Option<FlavorText>,

    pub casting_cost: Cost,
    pub is_snow: IsSnow,
    pub is_legendary: IsLegendary,

    pub non_land_permanent_types: NonLandPermanentTypes,
    pub permanent_abilities: PermanentAbilities,
    pub graveyard_permanent_abilities: PermanentAbilities,
    pub kicker_permanent_abilities: PermanentAbilities,
}

impl Face for CostedPermanentFace {
    fn maybe_name(&self) -> Option<&Name> {
        Some(&self.name)
    }

    fn colors(&self) -> BTreeSet<Color> {
        self.casting_cost.mana().to_colors()
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        Some(&self.casting_cost)
    }
}

impl PermanentFace for CostedPermanentFace {
    fn is_snow(&self) -> IsSnow {
        self.is_snow
    }

    fn maybe_land(&self) -> Option<&Land> {
        None
    }

    fn non_land_permanent_types(&self) -> &NonLandPermanentTypes {
        &self.non_land_permanent_types
    }

    fn is_legendary(&self) -> IsLegendary {
        self.is_legendary
    }

    fn maybe_rarity(&self) -> Option<Rarity> {
        Some(self.rarity)
    }

    fn permanent_abilities(&self) -> &PermanentAbilities {
        &self.permanent_abilities
    }

    fn graveyard_permanent_abilities(&self) -> &PermanentAbilities {
        &self.graveyard_permanent_abilities
    }

    fn kicker_permanent_abilities(&self) -> &PermanentAbilities {
        &self.kicker_permanent_abilities
    }
}

impl CostedFace for CostedPermanentFace {
    fn casting_cost(&self) -> &Cost {
        &self.casting_cost
    }
}
