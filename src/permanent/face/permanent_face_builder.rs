use ability::activated_ability_builder::ActivatedAbilityBuilder;
use ability::global_triggered_ability::GlobalTriggeredAbility;
use ability::permanent_abilities::{PermanentAbilities, PermanentAbilitiesBuilder};
use ability::permanent_kicker::PermanentKicker;
use ability::static_ability::StaticAbility;
use ability::triggered_ability::TriggeredAbility;
use cost::cost::Cost;
use cost::cost_builder::CostBuilder;
use effect::effect_template::EffectTemplate;
use effect::effect_type::global_effect_type::ContinuousGlobalEffectType;
use effect::effect_type::permanent_effect_type::Keyword;
use face::face::{Name, Expansion, CardNumber, Rarity, ArtistName, FlavorText};
use mana::mana_cost::ManaCost;
use permanent::face::costed_permanent_face::CostedPermanentFace;
use permanent::face::land_face::LandFace;
use permanent::face::permanent_face::{NonLandPermanentTypes, IsSnow, IsLegendary};
use permanent::permanent_card::PermanentCard;
use permanent::permanent_type::artifact::{Artifact, ArtifactType};
use permanent::permanent_type::creature::Creature;
use permanent::permanent_type::enchantment::{Enchantment, EnchantmentType};
use permanent::permanent_type::land::Land;
use permanent::permanent_type::planeswalker::Planeswalker;
use util::positive_integer::PositiveInteger;

pub struct PermanentFaceBuilder {
    name: Option<Name>,
    expansion: Option<Expansion>,
    card_number: Option<CardNumber>,
    rarity: Option<Rarity>,
    artist_name: Option<ArtistName>,

    flavor_text: Option<Option<FlavorText>>,

    casting_cost: Option<Cost>,
    is_snow: Option<IsSnow>,
    is_legendary: Option<IsLegendary>,

    land: Option<Land>,

    artifact: Option<Artifact>,
    creature: Option<Creature>,
    enchantment: Option<Enchantment>,
    planeswalker: Option<Planeswalker>,

    permanent_abilities_builder: PermanentAbilitiesBuilder,
    graveyard_abilities: PermanentAbilities,

    kicker: Option<PermanentKicker>,
    kicker_abilities: PermanentAbilities,
}

impl PermanentFaceBuilder {
    pub fn new() -> PermanentFaceBuilder {
        PermanentFaceBuilder {
            name: None,
            expansion: None,
            card_number:  None,
            rarity:  None,
            artist_name:  None,
            flavor_text: None,

            casting_cost: None,
            is_snow: None,
            is_legendary: None,

            land: None,

            artifact: None,
            creature: None,
            enchantment: None,
            planeswalker: None,

            permanent_abilities_builder: PermanentAbilitiesBuilder::new(),
            graveyard_abilities: PermanentAbilitiesBuilder::new().build(),

            kicker: None,
            kicker_abilities: PermanentAbilitiesBuilder::new().build(),
        }
    }

    pub fn name(mut self, name: &str) -> PermanentFaceBuilder {
        self.name = Some(Name(name.to_string()));
        self
    }

    pub fn expansion(mut self, expansion: Expansion) -> PermanentFaceBuilder {
        self.expansion = Some(expansion);
        self
    }

    pub fn card_number(mut self, card_number: usize) -> PermanentFaceBuilder {
        self.card_number = Some(CardNumber::new(
            PositiveInteger::from_usize(card_number).expect("Card number must be positive")));
        self
    }

    pub fn rarity(mut self, rarity: Rarity) -> PermanentFaceBuilder {
        self.rarity = Some(rarity);
        self
    }

    pub fn artist_name(mut self, artist_name: &str) -> PermanentFaceBuilder {
        self.artist_name = Some(
            ArtistName::from_str(artist_name).expect("Bad artist name.")
        );
        self
    }

    pub fn flavor_text(mut self, flavor_text: &str) -> PermanentFaceBuilder {
        self.flavor_text = Some(Some(FlavorText::new(flavor_text)));
        self
    }

    pub fn no_flavor_text(mut self) -> PermanentFaceBuilder {
        self.flavor_text = Some(None);
        self
    }

    pub fn mana_only(mut self, casting_cost: Cost) -> PermanentFaceBuilder {
        self.casting_cost = Some(casting_cost);
        self
    }

    pub fn casting_cost(mut self, casting_cost: &str) -> PermanentFaceBuilder {
        self.casting_cost = Some(Cost::mana_only(
            ManaCost::try_from_str(casting_cost).expect("Bad casting cost.")));
        self
    }

    pub fn snow(mut self) -> PermanentFaceBuilder {
        self.is_snow = Some(IsSnow::Snow);
        self
    }

    pub fn not_snow(mut self) -> PermanentFaceBuilder {
        self.is_snow = Some(IsSnow::NotSnow);
        self
    }

    pub fn legendary(mut self) -> PermanentFaceBuilder {
        self.is_legendary = Some(IsLegendary::Legendary);
        self
    }

    pub fn non_legendary(mut self) -> PermanentFaceBuilder {
        self.is_legendary = Some(IsLegendary::NonLegendary);
        self
    }

    pub fn land(mut self, land: Land) -> PermanentFaceBuilder {
        self.land = Some(land);
        self
    }

    pub fn artifact(mut self, artifact_type: ArtifactType) -> PermanentFaceBuilder {
        self.artifact = Some(Artifact {artifact_type});
        self
    }

    pub fn creature(mut self, text: &str) -> PermanentFaceBuilder {
        let creature = Creature::from_str(text)
            .unwrap_or_else(|_| panic!("Bad creature type for {:?}", self.name));
        self.creature = Some(creature);
        self
    }

    pub fn complex_creature(mut self, creature: Creature) -> PermanentFaceBuilder {
        self.creature = Some(creature);
        self
    }

    pub fn enchantment(mut self, enchantment_type: EnchantmentType) -> PermanentFaceBuilder {
        self.enchantment = Some(Enchantment {enchantment_type});
        self
    }

    pub fn planeswalker(mut self, planeswalker: Planeswalker) -> PermanentFaceBuilder {
        self.planeswalker = Some(planeswalker);
        self
    }

    pub fn kicker(mut self, mana_cost: &str) -> PermanentFaceBuilder {
        self.kicker = Some(PermanentKicker::Regular(
            CostBuilder::new().mana(mana_cost).into_cost()));
        self
    }

    pub fn cost_kicker(mut self, cost_builder: CostBuilder) -> PermanentFaceBuilder {
        self.kicker = Some(PermanentKicker::Regular(cost_builder.into_cost()));
        self
    }

    pub fn enter_the_battlefield_ability(mut self, value: EffectTemplate) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.enter_the_battlefield_ability(value);
        self
    }

    pub fn enter_the_graveyard_ability(mut self, value: EffectTemplate) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.enter_the_graveyard_ability(value);
        self
    }

    pub fn static_ability(mut self, value: StaticAbility) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.static_ability(value);
        self
    }

    pub fn keyword(mut self, value: Keyword) -> PermanentFaceBuilder {
        self.permanent_abilities_builder = self.permanent_abilities_builder.keyword(value);
        self
    }

    pub fn activated_ability(mut self, builder: ActivatedAbilityBuilder) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.activated_ability(builder.into_ability());
        self
    }

    pub fn activated_mana_ability(mut self, builder: ActivatedAbilityBuilder) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.activated_mana_ability(builder.into_mana_ability());
        self
    }

    pub fn global_static_ability(mut self, value: ContinuousGlobalEffectType) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.global_static_ability(value);
        self
    }

    pub fn global_triggered_ability(mut self, value: GlobalTriggeredAbility) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.global_triggered_ability(value);
        self
    }

    pub fn triggered_ability(mut self, value: TriggeredAbility) -> PermanentFaceBuilder {
        self.permanent_abilities_builder =
            self.permanent_abilities_builder.triggered_ability(value);
        self
    }

    pub fn graveyard_abilities(mut self, value: PermanentAbilities) -> PermanentFaceBuilder {
        self.graveyard_abilities = value;
        self
    }

    pub fn kicker_abilities(mut self, value: PermanentAbilities) -> PermanentFaceBuilder {
        self.kicker_abilities = value;
        self
    }

    pub fn into_regular_permanent(self) -> PermanentCard {
        PermanentCard::RegularPermanent(self.into_costed_permanent_face())
    }

    pub fn into_land_permanent(self) -> PermanentCard {
        PermanentCard::Land(self.into_land_face())
    }

    pub fn into_costed_permanent_face(self) -> CostedPermanentFace {
        if self.artifact.is_none() && self.creature.is_none() &&
            self.enchantment.is_none() && self.planeswalker.is_none() {
            panic!("No PermanentType was specified for this CostedPermanentFace.");
        }

        if self.land.is_some() {
            panic!("Land must not be specified for a CostedPermanentFace.");
        }

        CostedPermanentFace {
            name: self.name
                .expect("Name is required for CostedPermanentFaces."),
            expansion: self.expansion
                .expect("Expansion is required for CostedPermanentFaces."),
            card_number: self.card_number
                .expect("CardNumber is required for CostedPermanentFaces."),
            rarity: self.rarity
                .expect("Rarity is required for CostedPermanentFaces."),
            artist_name: self.artist_name
                .expect("ArtistName is required for CostedPermanentFaces."),
            flavor_text: self.flavor_text
                .expect("FlavorText must be explicitly set or unset for CostedPermanentFaces."),

            casting_cost: self.casting_cost
                .expect("CastingCost is required for CostedPermanentFaces."),
            is_snow: self.is_snow
                .expect("IsSnow must be explicitly set or unset for CostedPermanentFaces."),
            is_legendary: self.is_legendary
                .expect("IsLegendary must be explicitly set or unset for CostedPermanentFaces."),

            non_land_permanent_types: NonLandPermanentTypes {
                artifact: self.artifact,
                creature: self.creature,
                enchantment: self.enchantment,
                planeswalker: self.planeswalker,
            },

            permanent_abilities: self.permanent_abilities_builder.build(),
            graveyard_permanent_abilities: self.graveyard_abilities,
            kicker_permanent_abilities: self.kicker_abilities,
        }
    }

    pub fn into_land_face(self) -> LandFace {
        if self.casting_cost.is_some() {
            panic!("Casting cost must not be set for a land");
        }

        LandFace {
            name: self.name
                .expect("Name is required for LandFaces."),
            expansion: self.expansion
                .expect("Expansion is required for LandFaces."),
            card_number: self.card_number
                .expect("CardNumber is required for LandFaces."),
            rarity: self.rarity
                .expect("Rarity is required for LandFaces."),
            artist_name: self.artist_name
                .expect("ArtistName is required for LandFaces."),
            flavor_text: self.flavor_text
                .expect("FlavorText must be explicitly set or unset for LandFaces."),

            is_snow: self.is_snow
                .expect("IsSnow must be explicitly set or unset for LandFaces."),
            is_legendary: self.is_legendary
                .expect("IsLegendary must be explicitly set or unset for LandFaces."),

            land: self.land
                .expect("Land is required for LandFaces."),
            non_land_permanent_types: NonLandPermanentTypes {
                artifact: self.artifact,
                creature: self.creature,
                enchantment: self.enchantment,
                planeswalker: self.planeswalker,
            },

            permanent_abilities: self.permanent_abilities_builder.build(),
            graveyard_permanent_abilities: self.graveyard_abilities,
            kicker_permanent_abilities: self.kicker_abilities,
        }
    }
}

