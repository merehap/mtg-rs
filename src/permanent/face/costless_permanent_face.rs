use std::collections::BTreeSet;

use ability::permanent_abilities::PermanentAbilities;
use cost::cost::Cost;
use face::face::{Face, Name, Rarity};
use permanent::face::permanent_face::{PermanentFace, NonLandPermanentTypes, IsSnow, IsLegendary};
use permanent::permanent_type::land::Land;
use mana::color::Color;

///
/// A CostlessPermanentFace is the Face of a Permanent that doesn't have a ManaCost.
/// Tokens and the flip sides of double-faced and meld cards do not have a cost
/// (despite having a CMC), so they need to be represented differently from
/// normal Permanents.
///
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct CostlessPermanentFace {
    pub name: Name,
    pub colors: BTreeSet<Color>,
    pub is_snow: IsSnow,
    pub land: Option<Land>,
    pub non_land_permanent_types: NonLandPermanentTypes,
    pub is_legendary: IsLegendary,

    pub permanent_abilities: PermanentAbilities,
    pub graveyard_permanent_abilities: PermanentAbilities,
    pub kicker_permanent_abilities: PermanentAbilities,
}

impl Face for CostlessPermanentFace {
    fn maybe_name(&self) -> Option<&Name> {
        Some(&self.name)
    }

    fn colors(&self) -> BTreeSet<Color> {
        self.colors.clone()
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        None
    }
}

impl PermanentFace for CostlessPermanentFace {
    fn is_snow(&self) -> IsSnow {
        self.is_snow
    }

    fn maybe_land(&self) -> Option<&Land> {
        self.land.as_ref()
    }

    fn non_land_permanent_types(&self) -> &NonLandPermanentTypes {
        &self.non_land_permanent_types
    }

    fn is_legendary(&self) -> IsLegendary {
        self.is_legendary
    }

    fn maybe_rarity(&self) -> Option<Rarity> {
        None
    }

    fn permanent_abilities(&self) -> &PermanentAbilities {
        &self.permanent_abilities
    }

    fn graveyard_permanent_abilities(&self) -> &PermanentAbilities {
        &self.graveyard_permanent_abilities
    }

    fn kicker_permanent_abilities(&self) -> &PermanentAbilities {
        &self.kicker_permanent_abilities
    }
}
