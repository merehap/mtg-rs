#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct PermanentID {
    id: usize,
}

impl PermanentID {
    pub fn from_usize(id: usize) -> PermanentID {
        PermanentID {id}
    }
}
