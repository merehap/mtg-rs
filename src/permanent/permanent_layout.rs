use permanent::face::costed_permanent_face::CostedPermanentFace;
use permanent::face::costless_permanent_face::CostlessPermanentFace;
use permanent::face::land_face::LandFace;
use permanent::face::permanent_face::PermanentFace;
use permanent::permanent_card;
use permanent::permanent_card::{PermanentCard, DoubleFaced, DoubleFacedLand, Flip};

#[allow(clippy::large_enum_variant)]
#[derive(Clone)]
pub enum PermanentLayout {
    Regular(CostedPermanentFace),
    Land(LandFace),
    DoubleFaced(DoubleFaced),
    DoubleFacedLand(DoubleFacedLand),
    Flip(Flip),
    Melded {
        active: CostlessPermanentFace,
        top: CostedPermanentFace,
        bottom: CostedPermanentFace,
    },
    FaceDown{front: PermanentCard, is_manifested: IsManifested},
    Token(CostlessPermanentFace),
    CostedToken(CostedPermanentFace),
}

impl PermanentLayout {
    pub fn primary_face(&self) -> Option<Box<&PermanentFace>> {
        use self::PermanentLayout::*;
        match self {
            Regular(face) => Some(Box::new(face)),
            Land(face) => Some(Box::new(face)),
            DoubleFaced(permanent_card::DoubleFaced{front, ..}) => Some(Box::new(front)),
            DoubleFacedLand(permanent_card::DoubleFacedLand{front, ..}) => Some(Box::new(front)),
            Flip(permanent_card::Flip{top, ..}) => Some(Box::new(top)),
            Melded{active, ..} => Some(Box::new(active)),
            FaceDown{..} => None,
            Token(face) => Some(Box::new(face)),
            CostedToken(face) => Some(Box::new(face)),
        }
    }
}

#[derive(Clone)]
pub enum IsManifested {
    Manifested,
    NotManifested,
}
