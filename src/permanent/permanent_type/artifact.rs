#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Artifact {
    pub artifact_type: ArtifactType
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ArtifactType {
    Regular,
    Clue,
    // TODO: Encode constraints for what can equip the current Equipment.
    Equipment,
    Fortification,
}

