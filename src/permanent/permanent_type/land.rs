use std::collections::BTreeSet;

use mana::color::Color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Land {
    pub land_type: LandType,
}

impl Land {
    pub fn regular_non_basic() -> Land {
        Land {
            land_type: LandType::NonBasic {
                basic_land_types: BTreeSet::new(),
                non_basic_land_type: NonBasicLandType::Regular,
            }
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum LandType {
    Basic{basic_land_types: BTreeSet<BasicLandType>},
    // TODO: Is there a way to have multiple non-basic land types?
    // Or for a basic land to acquire a non-basic land type?
    NonBasic{basic_land_types: BTreeSet<BasicLandType>, non_basic_land_type: NonBasicLandType},
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum BasicLandType {
    Plains,
    Island,
    Swamp,
    Mountain,
    Forest,
}

impl BasicLandType {
    pub fn mana_color(self) -> Color {
        use self::BasicLandType::*;
        match self {
            Plains => Color::White,
            Island => Color::Blue,
            Swamp => Color::Black,
            Mountain => Color::Red,
            Forest => Color::Green,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum NonBasicLandType {
    Regular,
    Desert,
    Gate,
    Lair,
    Locus,
    Urzas,
    UrzasPowerPlant,
    UrzasMine,
    UrzasTower,
}
