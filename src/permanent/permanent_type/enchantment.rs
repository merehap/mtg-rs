use ability::permanent_abilities::PermanentAbilities;
use effect::effect_template::EffectTemplate;
use mana::mana_produced::ManaProduced;
use script::condition::permanent_condition::PermanentCondition;

// TODO: Move World down the the appropriate level.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Enchantment {
    pub enchantment_type: EnchantmentType,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EnchantmentType {
    Regular,
    Aura {
        attachment_target_condition: PermanentCondition,
        aura_triggered_abilities: Vec<AuraTriggeredAbility>,
        enchanted_permanent_abilities: PermanentAbilities,
    },
    Curse,
    Saga,
    Shrine,
    // The rules call World a supertype rather than subtype, but that seems
    // like a pointless distinction, and it's simpler to represent as a subtype.
    World,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct AuraTriggeredAbility {
    pub trigger_condition: AuraTriggerCondition,
    // TODO: Handle duplicate targets.
    pub effects: Vec<EffectTemplate>,
    pub mana_produced: Option<ManaProduced>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum AuraTriggerCondition {
    ManaAbilityActivated,
    DealsCombatDamageToOpponent,
}
