use std::collections::BTreeSet;

use permanent::permanent_type::creature_type::CreatureType;
use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Creature {
    pub creature_types: BTreeSet<CreatureType>,
    pub power: Power,
    pub toughness: Toughness,
}

impl Creature {
    pub fn new(
        creature_types: BTreeSet<CreatureType>,
        power: Power,
        toughness: Toughness,
    ) -> Creature {

        Creature {
            creature_types,
            power,
            toughness,
        }
    }
}

impl Creature {
    // "2/2 Elf Warrior"
    pub fn from_str(raw: &str) -> Result<Creature, String> {
        let mut segments: Vec<&str> = raw.split(' ').collect();
        let head = segments.remove(0);
        let power_toughness: Vec<&str> = head.split('/').collect();
        let power: usize = power_toughness[0].parse().expect("Bad power.");
        let toughness: usize = power_toughness[1].parse().expect("Bad toughness.");

        let mut creature_types = BTreeSet::new();
        for segment in segments {
            creature_types.insert(CreatureType::from_str(segment)?);
        }

        Ok(Creature {
            creature_types,
            power: Power(Count::Fixed(power)),
            toughness: Toughness(Count::Fixed(toughness)),
        })
    }

    pub fn creature_type_text(&self) -> String {
        self.creature_types.iter()
            .map(|ct| format!("{:?}", ct))
            .collect::<Vec<_>>()
            .join(" ")
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Power(pub Count);

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Toughness(pub Count);

#[cfg(test)]
mod test {
    use permanent::permanent_type::creature::Creature;
    use permanent::permanent_type::creature_type::CreatureType;

    #[test]
    fn creature_from_str() {
        let creature = Creature::from_str("1/2 Human Archer").expect("Couldn't parse creature string");
        assert_eq!(1, creature.power.0.to_isize());
        assert_eq!(2, creature.toughness.0.to_isize());

        assert_eq!(2, creature.creature_types.len());
        assert!(creature.creature_types.contains(&CreatureType::Human));
        assert!(creature.creature_types.contains(&CreatureType::Archer));
    }
}
