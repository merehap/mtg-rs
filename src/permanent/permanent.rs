use std::collections::{BTreeMap, BTreeSet};

use mana::color::Color;
use effect::effect_type::permanent_effect_type::CounterType;
use face::face::{Face, Name};
use permanent::face::effective_face::EffectiveFace;
use permanent::face::permanent_face::PermanentFace;
use permanent::flags::{Flags, CombatStatus};
use permanent::permanent_card;
use permanent::permanent_id::PermanentID;
use permanent::permanent_layout::PermanentLayout;
use permanent::permanent_type::land::{Land, LandType, BasicLandType};
use player::player_id::PlayerID;
use util::non_empty_vec_deque::NonEmptyVecDeque;
use util::positive_integer::PositiveInteger;

pub struct Permanent {
    id: PermanentID,
    owner_id: PlayerID,
    layout: PermanentLayout,
    effective_face: EffectiveFace,
    damage: Option<PositiveInteger>,
    counters: BTreeMap<CounterType, PositiveInteger>,
    flags: Flags,
}

impl Permanent {
    pub fn new(
        id: PermanentID,
        owner_id: PlayerID,
        layout: PermanentLayout,
        flags: Flags,
    ) -> Permanent {

        let effective_face = EffectiveFace::from_permanent_layout(&layout);
        Permanent {
            id,
            owner_id,
            layout,
            effective_face,
            damage: None,
            counters: BTreeMap::new(),
            flags,
        }
    }

    pub fn id(&self) -> PermanentID {
        self.id
    }

    pub fn owner_id(&self) -> PlayerID {
        self.owner_id
    }

    pub fn effective_face(&self) -> &EffectiveFace {
        &self.effective_face
    }

    pub fn is_artifact(&self) -> bool {
        self.effective_face.is_artifact()
    }

    pub fn is_creature(&self) -> bool {
        self.effective_face.is_creature()
    }

    pub fn is_enchantment(&self) -> bool {
        self.effective_face.is_enchantment()
    }

    pub fn is_land(&self) -> bool {
        self.effective_face.is_land()
    }

    pub fn is_planeswalker(&self) -> bool {
        self.effective_face.is_planeswalker()
    }

    pub fn basic_land_types(&self) -> BTreeSet<BasicLandType> {
        match self.effective_face.maybe_land {
            None => BTreeSet::new(),
            Some(Land {ref land_type}) => match land_type {
                LandType::Basic {basic_land_types} => basic_land_types.clone(),
                LandType::NonBasic {basic_land_types, ..} => basic_land_types.clone(),
            }
        }
    }

    pub fn permanent_layout(&self) -> &PermanentLayout {
        &self.layout
    }

    pub fn into_permanent_layout(self) -> PermanentLayout {
        self.layout
    }

    pub fn damage(&self) -> Option<PositiveInteger> {
        self.damage
    }

    pub fn add_damage(&mut self, damage: PositiveInteger) {
        self.damage = self.damage
            .map(|d| d + damage)
            .or(Some(damage));
    }

    pub fn clear_damage(&mut self) {
        self.damage = None;
    }

    pub fn add_counters(&mut self, count: PositiveInteger, counter_type: CounterType) {
        // +1/+1 and -1/-1 counters cancel each other, unlike any other counter type.
        use effect::effect_type::permanent_effect_type::CounterType::{PlusOnePlusOne, MinusOneMinusOne};
        match (counter_type,
               self.counters.contains_key(&PlusOnePlusOne),
               self.counters.contains_key(&MinusOneMinusOne)) {
            (PlusOnePlusOne, _, true) => {
                self.remove_counters(count, MinusOneMinusOne);
                return;
            },
            (MinusOneMinusOne, _, true) => {
                self.remove_counters(count, PlusOnePlusOne);
                return;
            },
            (_, true, true) =>
                panic!("Both +1/+1 and -1/-1 counters were stored at the same time."),
            (_, _, _) =>
                { /* Different type of counter or no existing one one counters. */ },
        }

        self.counters.entry(counter_type)
            .and_modify(|n| *n += count)
            .or_insert(count);
    }

    // Remove up to the specified number of counters,
    // removing them all if count is more than the existing total.
    pub fn remove_counters(&mut self, count: PositiveInteger, counter_type: CounterType) {
        let old_count = self.counters.get(&counter_type).cloned();
        if let Some(old_count) = old_count {
            if let Some(new_count) = old_count.subtract(count) {
                self.counters.insert(counter_type, new_count);
            } else {
                self.remove_all_counters(counter_type);
            }
        }
    }

    pub fn remove_all_counters(&mut self, counter_type: CounterType) -> Option<PositiveInteger> {
        self.counters.remove(&counter_type)
    }

    pub fn flags(&self) -> &Flags {
        &self.flags
    }

    pub fn flags_mut(&mut self) -> &mut Flags {
        &mut self.flags
    }

    pub fn is_untapped(&self) -> bool {
        !self.flags().tapped
    }

    pub fn untap(&mut self) {
        self.flags_mut().tapped = false;
    }

    pub fn is_tapped(&self) -> bool {
        self.flags().tapped
    }

    pub fn tap(&mut self) {
        self.flags_mut().tapped = true;
    }

    pub fn is_attacking(&self) -> bool {
        if let CombatStatus::Attacking {..} = self.flags.combat_status {
            true
        } else {
            false
        }
    }

    pub fn attack(&mut self) {
        self.flags.combat_status = CombatStatus::Attacking {blockers: Vec::new()};
    }

    pub fn is_blocked(&self) -> bool {
        match self.flags.combat_status {
            CombatStatus::Attacking {ref blockers} if !blockers.is_empty() => true,
            _ => false,
        }
    }

    pub fn blockers(&self) -> Vec<PermanentID> {
        match self.flags.combat_status {
            CombatStatus::Attacking {ref blockers} => blockers.clone(),
            _ => Vec::new(),
        }
    }

    pub fn blocked_by(&mut self, new_blockers: Vec<PermanentID>) {
        if let CombatStatus::Attacking {ref blockers} = self.flags.combat_status {
            assert!(blockers.is_empty());
        } else {
            panic!("Blockers can't be added to a creature that isn't attacking.");
        }

        self.flags.combat_status = CombatStatus::Attacking {blockers: new_blockers}
    }

    pub fn stop_attacking(&mut self) {
        self.flags.combat_status = CombatStatus::None;
    }

    pub fn blocking(&self) -> Vec<PermanentID> {
        if let CombatStatus::Blocking {ref blockees} = self.flags.combat_status {
            blockees.clone().into_vec()
        } else {
            Vec::new()
        }
    }

    pub fn is_blocking(&self) -> bool {
        if let CombatStatus::Blocking {..} = self.flags.combat_status {
            true
        } else {
            false
        }
    }

    pub fn block(&mut self, id: PermanentID) {
        match self.flags.combat_status {
            CombatStatus::Attacking {..} => panic!("Can't block with an attacking creature."),
            CombatStatus::None =>
                self.flags.combat_status = CombatStatus::Blocking {blockees: NonEmptyVecDeque::new(id, Vec::new())},
            CombatStatus::Blocking {ref mut blockees} => blockees.push_back(id),
        }
    }

    pub fn stop_blocking(&mut self) {
        self.flags.combat_status = CombatStatus::None;
    }

    // Ignores tapped status.
    pub fn can_block(&self, potential_attacker: &Permanent) -> bool {
        if potential_attacker.flags().flying &&
            !(self.flags().flying || self.flags().reach) {

            return false;
        }

        if potential_attacker.flags().fear &&
            !(self.effective_face().is_color(Color::Black) || self.is_artifact()) {

            return false;
        }

        true
    }

    pub fn converted_mana_cost(&self) -> usize {
        use self::PermanentLayout::*;
        match self.permanent_layout() {
            Regular(perm) => perm.casting_cost.mana().converted_mana_cost(),
            Land(_) => 0,
            DoubleFaced(permanent_card::DoubleFaced{front, ..}) =>
                front.casting_cost.mana().converted_mana_cost(),
            DoubleFacedLand(_) => 0,
            Flip(permanent_card::Flip{top, ..}) =>
                top.casting_cost.mana().converted_mana_cost(),
            Melded{top, bottom, ..} =>
                top.casting_cost.mana().converted_mana_cost() +
                    bottom.casting_cost.mana().converted_mana_cost(),
            FaceDown{..} => 0,
            Token(..) => 0,
            CostedToken(perm) => perm.casting_cost.mana().converted_mana_cost(),
        }
    }

    pub fn primary_face(&self) -> Option<Box<&PermanentFace>> {
        self.layout.primary_face()
    }

    pub fn primary_face_name(&self) -> Option<&Name> {
        if let Some(face) = self.primary_face() {
            (*face).maybe_name()
        } else {
            None
        }
    }
}
