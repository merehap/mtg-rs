use ability::static_ability::StaticAbility;
use effect::effect_type::permanent_effect_type::ContinuousPermanentEffectType;
use permanent::face::permanent_face::PermanentFace;
use permanent::permanent_id::PermanentID;
use util::non_empty_vec_deque::NonEmptyVecDeque;

pub struct Flags {
    pub summoning_sick: bool,
    pub tapped: bool,
    pub had_first_strike_at_beginning_of_this_combat: bool,

    pub first_strike: bool,
    pub double_strike: bool,
    pub shroud: bool,
    pub haste: bool,
    pub flying: bool,
    pub reach: bool,
    pub fear: bool,
    pub vigilance: bool,
    pub trample: bool,
    pub defender: bool,

    pub combat_status: CombatStatus,
}

impl Flags {
    pub fn initial() -> Flags {
        let mut flags = Flags::all_unset();
        flags.summoning_sick = true;
        flags
    }

    pub fn all_unset() -> Flags {
        Flags {
            summoning_sick: false,
            tapped: false,
            had_first_strike_at_beginning_of_this_combat: false,

            shroud: false,

            haste: false,
            first_strike: false,
            double_strike: false,
            defender: false,
            vigilance: false,
            trample: false,

            flying: false,
            reach: false,
            fear: false,

            combat_status: CombatStatus::None,
        }
    }

    pub fn from_permanent_face(face: Box<&PermanentFace>) -> Flags {
        let mut flags = Flags::initial();
        for ability in &face.permanent_abilities().static_abilities {
            if let StaticAbility::Permanent(ContinuousPermanentEffectType::Keyword(keyword)) = ability {
                use effect::effect_type::permanent_effect_type::Keyword::*;
                match keyword {
                    Shroud => flags.shroud = true,
                    Protection(_) => unimplemented!(),

                    // Creature Keywords
                    Flying => flags.flying = true,
                    Reach => flags.reach = true,
                    Fear => flags.fear = true,
                    FirstStrike => flags.first_strike = true,
                    DoubleStrike => flags.double_strike = true,
                    Vigilance => flags.vigilance = true,
                    Trample => flags.trample = true,
                    Defender => flags.defender = true,
                    Haste => flags.haste = true,
                    Flash => unimplemented!(),
                    LandWalk(_) => unimplemented!(),
                }
            }
        }

        flags
    }

    pub fn persistent_flags_to_strings(&self) -> Vec<String> {
        let mut names = Vec::new();
        // Pattern match like this to make the compiler tell us if we've missed anything.
        // Some fields have to be explicitly ignored since a UI might not want to display
        // them, or may want to display them in a non-default way.
        let Flags {
            summoning_sick,
            tapped,
            had_first_strike_at_beginning_of_this_combat,
            shroud,
            haste,
            first_strike,
            double_strike,
            defender,
            vigilance,
            trample,
            flying,
            reach,
            fear,
            combat_status,
            } = self;

        Flags::ignore(summoning_sick);
        Flags::ignore(tapped);
        Flags::ignore(had_first_strike_at_beginning_of_this_combat);
        Flags::ignore(haste);
        Flags::ignore(combat_status);

        Flags::push_name_if(&mut names, "shroud", shroud);
        Flags::push_name_if(&mut names, "first strike", first_strike);
        Flags::push_name_if(&mut names, "double strike", double_strike);
        Flags::push_name_if(&mut names, "defender", defender);
        Flags::push_name_if(&mut names, "vigilance", vigilance);
        Flags::push_name_if(&mut names, "trample", trample);
        Flags::push_name_if(&mut names, "flying", flying);
        Flags::push_name_if(&mut names, "reach", reach);
        Flags::push_name_if(&mut names, "fear", fear);

        names
    }

    fn push_name_if(vec: &mut Vec<String>, name: &str, condition: &bool) {
        if *condition {
            vec.push(name.to_string());
        }
    }

    fn ignore<T>(_: &T) {

    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Debug)]
pub enum CombatStatus {
    None,
    Attacking {blockers: Vec<PermanentID>},
    Blocking {blockees: NonEmptyVecDeque<PermanentID>},
}

