use std::cmp;

use game::Game;
use turn::sub_steps::util;
use util::positive_integer::PositiveInteger;

pub fn assign_attacker_damage(game: &mut Game) {
    let mut total_damage_to_non_active_player = 0;
    let active_player_id = game.active_player_id();
    for active_index in 0..game.battlefield().side(active_player_id).permanents().len() {
        let mut damage_to_assign;
        let trample;
        let blockers;
        {
            let is_first_strike_step = game.turns().current_step().is_first_strike_combat_damage_step();
            let perm = &mut game.battlefield_mut().side_mut(active_player_id).permanents_mut()[active_index];
            if !perm.is_attacking() ||
                !util::step_matches_strike_type(perm, is_first_strike_step) {
                continue;
            }

            if is_first_strike_step {
                perm.flags_mut().had_first_strike_at_beginning_of_this_combat = true;
            }

            let power = perm.effective_face().power()
                .expect("How was a non-creature selected to attack?");
            damage_to_assign = cmp::max(power as usize, 0);
            trample = perm.flags().trample;

            blockers = perm.blockers().clone();
        }

        if blockers.is_empty() {
            total_damage_to_non_active_player += damage_to_assign;
        } else {
            for blocker_id in blockers {
                let damage_to_assign_to_blocker;
                {
                    let blocker = game.battlefield()
                        .get(blocker_id)
                        .expect("Blocker should still exist");
                    let damage_to_deal_to_blocker = cmp::min(
                        blocker.effective_face().toughness()
                            .expect("How was a non-creature selected to block?"),
                        damage_to_assign as isize,
                    );
                    damage_to_assign_to_blocker = cmp::max(damage_to_deal_to_blocker, 0) as usize;
                }

                damage_to_assign -= damage_to_assign_to_blocker;
                if let Some(damage) = PositiveInteger::from_usize(damage_to_assign_to_blocker) {
                    game.assigned_combat_damage_mut().entry(blocker_id)
                        .and_modify(|d| *d += damage)
                        .or_insert(damage);
                }
            }

            // All of the left over damage for a trampler is assigned to the player.
            if trample {
                total_damage_to_non_active_player += damage_to_assign;
            }
        }
    }

    game.set_assigned_combat_damage_to_non_active_player(
        PositiveInteger::from_usize(total_damage_to_non_active_player));
}