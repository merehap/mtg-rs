use game::{Game, ContinueResult};
use permanent::permanent_id::PermanentID;

pub fn declare_blockers(game: &mut Game) -> ContinueResult {
    let attackers = game.battlefield().side(game.active_player_id())
        .matching_permanents(|perm| perm.is_attacking());
    let eligible_blockers = determine_eligible_blockers(game);
    let blocking_assignments =
        game.non_active_player().brain.declare_blockers(&attackers, &eligible_blockers);
    let active_player_id = game.active_player_id();
    game.battlefield_mut().foreach_selected_permanent_mut(
        active_player_id,
        blocking_assignments.keys().cloned().cloned().collect(),
        |perm| {
            let perm_id = perm.id().clone();
            perm.blocked_by(
                blocking_assignments[&perm_id].iter()
                    .cloned()
                    .cloned()
                    .collect())
        }
    );

    let non_active_player_id = game.non_active_player().id;
    let mut continue_result = ContinueResult::NoUpdate;
    for (attacker, blockers) in blocking_assignments {
        game.battlefield_mut().foreach_selected_permanent_mut(
            non_active_player_id,
            blockers.into_iter().cloned().collect(),
            |perm| perm.block(*attacker)
        );

        continue_result = ContinueResult::ZoneStateUpdated;
    }

    continue_result
}

fn determine_eligible_blockers(game: &Game) -> Vec<PermanentID> {
    game.battlefield().side(game.non_active_player_id()).permanents().iter()
        .filter(|perm| perm.is_creature() && !perm.flags().tapped)
        .map(|perm| perm.id())
        .collect()
}
