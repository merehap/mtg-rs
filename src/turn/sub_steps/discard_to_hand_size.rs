use game::{Game, ContinueResult};

pub fn discard_to_hand_size(game: &mut Game) -> ContinueResult {
    let hand_size = game.active_player().hand.size();
    let mut continue_result = ContinueResult::NoUpdate;
    if let Some(max_hand_size) = game.active_player().hand.maximum_size() {
        if hand_size >= max_hand_size {
            let discard_ids = game.active_player().brain.pick_discards(
                hand_size - max_hand_size,
                &game.active_player().hand,
            ).into_iter().cloned().collect();
            game.active_player_mut().discard(discard_ids);

            continue_result = ContinueResult::ZoneStateUpdated;
        }
    }

    continue_result
}