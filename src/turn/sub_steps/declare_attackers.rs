use game::{Game, ContinueResult};
use permanent::permanent_id::PermanentID;

pub fn declare_attackers(game: &mut Game) -> ContinueResult {
    let eligible_attackers = determine_eligible_attackers(game);
    let selected_attackers =
        game.active_player().brain.declare_attackers(&eligible_attackers);
    let current_player_id = game.active_player_id();

    let mut continue_result = ContinueResult::NoUpdate;
    game.battlefield_mut().foreach_selected_permanent_mut(
        current_player_id,
        selected_attackers.into_iter().cloned().collect(),
        |perm| {
            perm.attack();
            if !perm.flags().vigilance {
                perm.tap();
            }

            continue_result = ContinueResult::ZoneStateUpdated;
        },
    );

    continue_result
}

fn determine_eligible_attackers(game: &Game) -> Vec<PermanentID> {
    game.battlefield().side(game.active_player_id()).permanents().iter()
        .filter(|perm|
            perm.is_creature() &&
            perm.is_untapped() &&
            !perm.flags().defender &&
            (!perm.flags().summoning_sick || perm.flags().haste))
        .map(|perm| perm.id())
        .collect()
}
