use game::Game;
use turn::step::Step;

pub fn enable_needed_combat_damage_steps(game: &mut Game) {
    use script::condition::permanent_condition::PermanentCondition::*;
    let first_strikers_present = !game.battlefield()
        .matching_permanents(
            And(vec![
                Or(vec![Attacking, Blocking]),
                Or(vec![FirstStrike, DoubleStrike]),
            ])
        )
        .is_empty();

    if first_strikers_present {
        game.turns_mut().current_turn_mut().current_step_mut()
            .mark_as_first_strike_combat_damage_step();

        let mut regular_combat_damage_step = Step::new_combat_damage();
        // Remove the ability for the next step to add more combat steps.
        regular_combat_damage_step.remove_first_sub_step();

        game.turns_mut().current_turn_mut().current_phase_mut()
            .add_step_after_current_one(regular_combat_damage_step);
    }
}