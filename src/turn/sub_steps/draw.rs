use game::{Game, ContinueResult};
use util::positive_integer::PositiveInteger;

pub fn draw(game: &mut Game) -> ContinueResult {
    game.active_player_mut().draw(PositiveInteger::one());
    ContinueResult::ZoneStateUpdated
}