use game::Game;

pub fn cleanup(game: &mut Game) {
    game.battlefield_mut()
        .foreach_permanent_both_players_mut(|perm| perm.clear_damage())
}