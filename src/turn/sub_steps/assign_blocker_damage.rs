use std::cmp;

use game::Game;
use turn::sub_steps::util;
use util::positive_integer::PositiveInteger;

pub fn assign_blocker_damage(game: &mut Game) {
    let non_active_player_id = game.non_active_player_id();
    for non_active_index in 0..game.battlefield().side(non_active_player_id).permanents().len() {
        let mut damage_to_assign;
        let attackers;
        {
            let is_first_strike_step = game.turns().current_step().is_first_strike_combat_damage_step();
            let perm = &mut game.battlefield_mut().side_mut(non_active_player_id)
                .permanents_mut()[non_active_index];
            if !perm.is_blocking() ||
                !util::step_matches_strike_type(perm, is_first_strike_step) {
                continue;
            }

            if is_first_strike_step {
                perm.flags_mut().had_first_strike_at_beginning_of_this_combat = true;
            }

            let power = perm.effective_face().power()
                .expect("How was a non-creature selected to block?");
            damage_to_assign = cmp::max(power as usize, 0);
            attackers = perm.blocking().clone();
        }

        for attacker_id in attackers {
            let toughness = game.battlefield()
                .get(attacker_id)
                .expect("Attacker should have been present.")
                .effective_face()
                .toughness()
                .expect("How was a non-creature selected to attack?");
            let damage_to_deal_to_attacker = cmp::min(
                toughness,
                damage_to_assign as isize,
            );

            let damage_to_assign_to_attacker = cmp::max(damage_to_deal_to_attacker, 0) as usize;
            damage_to_assign -= damage_to_assign_to_attacker;
            if let Some(damage) = PositiveInteger::from_usize(damage_to_assign_to_attacker) {
                game.assigned_combat_damage_mut().entry(attacker_id)
                    .and_modify(|d| *d += damage)
                    .or_insert(damage);
            }
        }
    };
}