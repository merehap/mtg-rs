use permanent::permanent::Permanent;

pub fn step_matches_strike_type(perm: &Permanent, is_first_strike_step: bool) -> bool {
    perm.flags().double_strike ||
        (is_first_strike_step && perm.flags().first_strike) ||
        (!is_first_strike_step && !perm.flags().had_first_strike_at_beginning_of_this_combat)
}