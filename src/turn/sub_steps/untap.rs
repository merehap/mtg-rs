use game::{Game, ContinueResult};

pub fn untap(game: &mut Game) -> ContinueResult {
    let player_id = game.active_player_id();
    game.battlefield_mut().foreach_permanent_mut(player_id, |perm| perm.untap());
    ContinueResult::ZoneStateUpdated
}