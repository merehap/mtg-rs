use card::card::Card;
use cost::cost::Cost;
use game::{Game, AdvanceResult, ContinueResult, PriorityPeriodResult};
use mana::mana::Mana;
use player::player_brain::Decision;
use player::player_option::{PlayerOption, ValidatedPlayerChoice};
use script::condition::permanent_condition::PermanentCondition;
use util::positive_integer::PositiveInteger;
use zone::hand::HandCardID;
use zone::zoned_card_id::ZonedCardID;
use zone::zoned_face_id::ZonedFaceID;

pub fn priority_period(game: &mut Game) -> AdvanceResult {
    if let Some(advance_result) = state_based_actions(game) {
        return advance_result;
    }

    let mut current_options = Vec::new();
    let mut cost_options = Vec::new();
    get_current_options(&game, &mut current_options, &mut cost_options);
    println!("{} OPTIONS: {:?}", game.priority_player_id(), current_options);
    let mut selected_option = None;
    if let Decision::Choice(choice) =
            get_player_decision(&game, &current_options, cost_options) {

        selected_option = Some(choice.player_option().clone());
        execute_chosen_option(game, choice);
    }

    AdvanceResult::Continue(ContinueResult::PriorityPeriod(PriorityPeriodResult {
        selected_option,
        options: current_options,
    }))
}

fn state_based_actions(game: &mut Game) -> Option<AdvanceResult> {
    let dying_creatures = game.battlefield_mut().matching_permanents(
        PermanentCondition::DamageGreaterOrEqualToToughness);

    for id in dying_creatures {
        game.bury(id);
    }

    let active_player_loses = game.active_player().life_total.0 <= 0 ||
        game.active_player().attempted_to_draw_from_empty_deck;
    let non_active_player_loses = game.non_active_player().life_total.0 <= 0 ||
        game.non_active_player().attempted_to_draw_from_empty_deck;

    match (active_player_loses, non_active_player_loses) {
        (true, true) => Some(AdvanceResult::Tie),
        (true, false) => Some(AdvanceResult::PlayerLoses(game.active_player_id())),
        (false, true) => Some(AdvanceResult::PlayerWins(game.active_player_id())),
        /* No winners nor losers yet: the game goes on.*/
        (false, false) => None,
    }
}

fn get_current_options<'a>(
    game: &Game,
    current_options: &'a mut Vec<PlayerOption>,
    cost_options: &'a mut Vec<(Option<Cost>, PlayerOption)>) {

    game.battlefield().foreach_permanent(game.priority_player().id, |permanent| {
        if !permanent.flags().tapped
            && !permanent.basic_land_types().is_empty() {

            let option = PlayerOption::BasicLandTypeManaAbility(permanent.id());
            current_options.push(option.clone());
            cost_options.push((None, option));
        }
    });

    for (id, card) in game.priority_player().hand.cards().iter() {
        if card.is_instant() {
            get_options_for_playing_card_from_hand(game, **id, card, current_options, cost_options);
        } else if game.turns().current_turn().current_step_name().is_main() &&
                game.priority_player_id() == game.active_player_id() &&
                game.stack().is_empty() {

            get_options_for_playing_card_from_hand(game, **id, card, current_options, cost_options);
        }
    }
}

fn get_options_for_playing_card_from_hand<'a>(
    game: &Game,
    hand_card_id: HandCardID,
    card: &'a Card,
    current_options: &'a mut Vec<PlayerOption>,
    cost_options: &mut Vec<(Option<Cost>, PlayerOption)>) {

    let can_play_land =
        game.turns().current_turn().lands_played_this_turn() <
            game.priority_player().max_lands_per_turn;

    let card_id = ZonedCardID::Hand(hand_card_id);
    let default_face_id = ZonedFaceID::default_face(card_id);

    match card {
        Card::Permanent(permanent) if permanent.is_land() && can_play_land => {
            cost_options.push((None, PlayerOption::PlayCardAsNonSpell(card_id)));
        },
        Card::Permanent(permanent) => {
            if permanent.maybe_casting_cost().is_some() {
                cost_options.push((
                    permanent.maybe_casting_cost().cloned(),
                    PlayerOption::CastCard(default_face_id)
                ));
            }
        },
        Card::Ephemeral(ephemeral) => {
            for (split_id, face) in ephemeral.faces() {
                if face.maybe_casting_cost().is_some() {
                    let face_id = ZonedFaceID::new(card_id, split_id);
                    let option = PlayerOption::CastCard(face_id);

                    cost_options.push((
                        face.maybe_casting_cost().cloned(),
                        option
                    ));
                }
            }
        },
    };

    let player = game.priority_player();
    for (maybe_casting_cost, option) in cost_options {
        // If there's a casting cost but it can't be paid, don't add this option.
        if let Some(casting_cost) = maybe_casting_cost {
            if player.mana_pool.can_pay_cost(casting_cost.mana(), player.life_total) {
                current_options.push(option.clone());
            }
        } else {
            current_options.push(option.clone());
        }
    }
}

fn get_player_decision<'a>(
    game: &Game,
    current_options: &'a Vec<PlayerOption>,
    cost_options: Vec<(Option<Cost>, PlayerOption)>) -> Decision<'a> {

    let mut choice = None;
    if !current_options.is_empty() {
        println!();
        println!("{:?} has priority. ", game.priority_player().id);
        println!("Hand: {}", game.priority_player().hand);
        println!("Mana Pool: {}", game.priority_player().mana_pool);
        println!("Stack: {}", game.stack());
        choice = game.priority_player().brain.make_priority_period_decision(
            current_options, cost_options, game.priority_player(), &game);
    };

    let decision = choice
        .map(Decision::Choice)
        .unwrap_or(Decision::Pass);
    println!("Decision made by {}: {:?}", game.priority_player_id(), decision);
    decision
}

fn execute_chosen_option(game: &mut Game, player_choice: ValidatedPlayerChoice) {
    let priority_player_id = game.priority_player_id();
    let player_option = player_choice.player_option().clone();
    let ephemeral_effects = player_choice.ephemeral_effects();
    match player_option {
        PlayerOption::PlayCardAsNonSpell(zoned_card_id) => {
            let card = game.remove_card(zoned_card_id)
                .expect("Card should have been present.");
            game.play_card_as_non_spell(
                zoned_card_id.owner_id(), priority_player_id, card);
        },
        PlayerOption::CastCard(zoned_face_id) => {
            let card_view = game.remove_card_as_card_view(zoned_face_id)
                .expect("CardView should have been present.");
            game.cast_spell(
                zoned_face_id.zoned_card_id().owner_id(),
                priority_player_id,
                card_view,
                ephemeral_effects,
            );
        },
        PlayerOption::BasicLandTypeManaAbility(permanent_id) => {
            let first = {
                let permanent = game.battlefield_mut()
                    .get_mut_for_player(priority_player_id, permanent_id)
                    .expect("Permanent wasn't present?");
                permanent.tap();
                let basic_land_types = permanent.basic_land_types();
                // TODO: Give players the option.
                basic_land_types.into_iter().next().unwrap()
            };
            game.priority_player_mut().mana_pool.add_mana(
                Mana::Color(first.mana_color()),
                PositiveInteger::one());
        },
        _ => unimplemented!(),
    }
}
