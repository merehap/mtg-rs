use game::{Game, ContinueResult};

pub fn remove_from_combat(game: &mut Game) -> ContinueResult {
    let active_player_id = game.active_player_id();
    game.battlefield_mut().foreach_permanent_mut(active_player_id, |perm| {
        perm.stop_attacking();
        perm.flags_mut().had_first_strike_at_beginning_of_this_combat = false;
    });
    let non_active_player_id = game.non_active_player_id();
    game.battlefield_mut().foreach_permanent_mut(non_active_player_id, |perm| {
        perm.stop_blocking();
        perm.flags_mut().had_first_strike_at_beginning_of_this_combat = false;
    });

    ContinueResult::ZoneStateUpdated
}
