use std::collections::BTreeMap;

use game::{Game, ContinueResult};

pub fn deal_combat_damage(game: &mut Game) -> ContinueResult {
    for (perm_id, assigned_damage) in game.assigned_combat_damage().clone() {
        let perm = game.battlefield_mut().get_mut(perm_id)
            .expect("Permanent that had combat damage assigned to it no longer exists.");
        perm.add_damage(assigned_damage);
    }

    if let Some(damage) = game.assigned_combat_damage_to_non_active_player() {
        game.non_active_player().life_total.lose(damage);
    }

    // The damage has now been dealt so it is no longer assigned.
    *game.assigned_combat_damage_mut() = BTreeMap::new();
    game.set_assigned_combat_damage_to_non_active_player(None);

    ContinueResult::ZoneStateUpdated
}