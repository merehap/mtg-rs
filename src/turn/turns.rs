use std::mem;

use player::player_id::PlayerID;
use turn::ended_segment::EndedSegment;
use turn::phase::PhaseName;
use turn::step::{Step, StepName};
use turn::sub_step::SubStep;
use turn::turn::Turn;

pub struct Turns {
    current_turn: Turn,
    next_regular_turn: Turn,
    extra_turns: Vec<Turn>,
}

impl Turns {
    pub fn new(starter: PlayerID) -> Turns {
        let first_turn = Turn::new_with_no_draw_step(starter);
        Turns {
            current_turn: first_turn,
            next_regular_turn: Turn::new(starter.opponent()),
            extra_turns: Vec::new(),
        }
    }

    pub fn current_turn(&self) -> &Turn {
        &self.current_turn
    }

    pub fn current_turn_mut(&mut self) -> &mut Turn {
        &mut self.current_turn
    }

    pub fn current_phase_name(&self) -> PhaseName {
        self.current_turn.current_phase_name()
    }

    pub fn current_step(&self) -> &Step {
        self.current_turn.current_step()
    }

    pub fn current_step_name(&self) -> StepName {
        self.current_turn.current_step_name()
    }

    pub fn current_sub_step(&self) -> SubStep {
        self.current_turn().current_step().current_sub_step()
    }

    pub fn advance(&mut self) -> EndedSegment {
        let ended_segment = self.current_turn.advance();
        if ended_segment == EndedSegment::Turn {
            self.advance_turn();
        }

        ended_segment
    }

    fn advance_turn(&mut self) {
        if let Some(insertion) = self.extra_turns.pop() {
            self.current_turn = insertion;
        } else {
            let next_player_id = self.next_regular_turn.player_id().opponent();
            self.current_turn = mem::replace(
                &mut self.next_regular_turn,
                Turn::new(next_player_id));
        }
    }
}

#[cfg(test)]
mod test {
    use player::player_id::PlayerID;
    use turn::ended_segment::EndedSegment;
    use turn::step::StepName;
    use turn::turns::Turns;

    #[test]
    fn all_steps() {
        let mut step_names = Vec::new();

        let mut turns = Turns::new(PlayerID::Player0);
        let current_turn = turns.current_turn_mut();
        loop {
            step_names.push(current_turn.current_step_name());
            if current_turn.advance() == EndedSegment::Step {
                break;
            }
        }

        assert_eq!(
            step_names,
            vec![
                StepName::Untap,
                StepName::Upkeep,
                StepName::Draw,
                StepName::PreCombatMain,
                StepName::BeginningOfCombat,
                StepName::DeclareAttackers,
                StepName::DeclareBlockers,
                StepName::CombatDamage,
                StepName::EndOfCombat,
                StepName::PostCombatMain,
                StepName::End,
                StepName::Cleanup,
            ],
        );
    }
}
