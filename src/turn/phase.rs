use std::fmt;

use turn::ended_segment::EndedSegment;
use turn::step::{Step, StepName};
use util::non_empty_vec_deque::NonEmptyVecDeque;

#[derive(Clone)]
pub struct Phase {
    name: PhaseName,
    remaining_steps: NonEmptyVecDeque<Step>,
}

impl Phase {
    pub fn new_beginning_with_no_draw_step() -> Phase {
        Phase {
            name: PhaseName::Beginning,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_untap(),
                vec![
                    Step::new_upkeep(),
                ],
            )
        }
    }

    pub fn new_beginning() -> Phase {
        Phase {
            name: PhaseName::Beginning,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_untap(),
                vec![
                    Step::new_upkeep(),
                    Step::new_draw(),
                ],
            )
        }
    }

    pub fn new_pre_combat_main() -> Phase {
        Phase {
            name: PhaseName::PreCombatMain,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_pre_combat_main(),
                Vec::new(),
            )
        }
    }

    pub fn new_combat() -> Phase {
        Phase {
            name: PhaseName::PostCombatMain,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_beginning_of_combat(),
                vec![
                    Step::new_declare_attackers(),
                    Step::new_declare_blockers(),
                    Step::new_combat_damage(),
                    Step::new_end_of_combat(),
                ],
            )
        }
    }

    pub fn new_post_combat_main() -> Phase {
        Phase {
            name: PhaseName::PostCombatMain,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_post_combat_main(),
                Vec::new(),
            )
        }
    }

    pub fn new_ending() -> Phase {
        Phase {
            name: PhaseName::Ending,
            remaining_steps: NonEmptyVecDeque::new(
                Step::new_end(),
                vec![
                    Step::new_cleanup(),
                ],
            )
        }
    }

    pub fn name(&self) -> PhaseName {
        self.name
    }

    pub fn current_step(&self) -> &Step {
        self.remaining_steps.front()
    }

    pub fn current_step_mut(&mut self) -> &mut Step {
        self.remaining_steps.front_mut()
    }

    pub fn current_step_name(&self) -> StepName {
        self.remaining_steps.front().name()
    }

    pub fn add_step_after_current_one(&mut self, step: Step) {
        self.remaining_steps.insert(1, step);
    }

    pub fn advance(&mut self) -> EndedSegment {
        let mut step_result = self.remaining_steps.front_mut().advance();
        if step_result == EndedSegment::Step {
            if self.remaining_steps.len() <= 1 {
                step_result = EndedSegment::Phase;
            } else {
                self.remaining_steps.pop_front();
            }
        }

        step_result
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum PhaseName {
    Beginning,
    PreCombatMain,
    Combat,
    PostCombatMain,
    Ending,
}

impl fmt::Display for PhaseName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}
