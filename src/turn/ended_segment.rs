#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub enum EndedSegment {
    SubStep,
    Step,
    Phase,
    Turn,
}