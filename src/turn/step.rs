use std::fmt;

use turn::ended_segment::EndedSegment;
use turn::sub_step::SubStep;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Step {
    name: StepName,
    is_first_strike_combat_damage_step: bool,
    remaining_sub_steps: Vec<SubStep>,
}

impl Step {
    pub fn new_untap() -> Step {
        Step::new(
            StepName::Untap,
            vec![
                SubStep::Beginning,
                SubStep::Phasing,
                SubStep::Untap,
                SubStep::End,
            ],
        )
    }

    pub fn new_upkeep() -> Step {
        Step::new(
            StepName::Upkeep,
            vec![
                SubStep::Beginning,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_draw() -> Step {
        Step::new(
            StepName::Draw,
            vec![
                SubStep::Beginning,
                SubStep::Draw,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_pre_combat_main() -> Step {
        Step::new(
            StepName::PreCombatMain,
            vec![
                SubStep::Beginning,
                SubStep::Saga,
                SubStep::MainPriority,
                SubStep::End,
            ],
        )
    }

    pub fn new_beginning_of_combat() -> Step {
        Step::new(
            StepName::BeginningOfCombat,
            vec![
                SubStep::Beginning,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_declare_attackers() -> Step {
        Step::new(
            StepName::DeclareAttackers,
            vec![
                SubStep::Beginning,
                SubStep::DeclareAttackers,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_declare_blockers() -> Step {
        Step::new(
            StepName::DeclareBlockers,
            vec![
                SubStep::Beginning,
                SubStep::DeclareBlockers,
                SubStep::AssignAttackerOrder,
                SubStep::AssignBlockerOrder,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_combat_damage() -> Step {
        Step::new(
            StepName::CombatDamage,
            vec![
                SubStep::EnableNeededCombatDamageSteps,
                SubStep::Beginning,
                SubStep::AssignAttackerDamage,
                SubStep::AssignBlockerDamage,
                SubStep::DealCombatDamage,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_end_of_combat() -> Step {
        Step::new(
            StepName::EndOfCombat,
            vec![
                SubStep::Beginning,
                SubStep::Priority,
                SubStep::End,
                SubStep::RemoveFromCombat,
            ],
        )
    }

    pub fn new_post_combat_main() -> Step {
        Step::new(
            StepName::PostCombatMain,
            vec![
                SubStep::Beginning,
                SubStep::MainPriority,
                SubStep::End,
            ],
        )
    }

    pub fn new_end() -> Step {
        Step::new(
            StepName::End,
            vec![
                SubStep::Beginning,
                SubStep::Priority,
                SubStep::End,
            ],
        )
    }

    pub fn new_cleanup() -> Step {
        Step::new(
            StepName::Cleanup,
            vec![
                SubStep::Beginning,
                SubStep::DiscardToHandSize,
                SubStep::Cleanup,
                SubStep::EnableAdditionalCleanupStep,
                SubStep::End,
            ],
        )
    }

    fn new(name: StepName, sub_steps: Vec<SubStep>) -> Step {
        Step {
            name,
            is_first_strike_combat_damage_step: false,
            remaining_sub_steps: sub_steps,
        }
    }

    pub fn name(&self) -> StepName {
        self.name
    }

    pub fn is_first_strike_combat_damage_step(&self) -> bool {
        self.is_first_strike_combat_damage_step
    }

    pub fn mark_as_first_strike_combat_damage_step(&mut self) {
        self.is_first_strike_combat_damage_step = true;
    }

    pub fn current_sub_step(&self) -> SubStep {
        self.remaining_sub_steps[0]
    }

    pub fn remove_first_sub_step(&mut self) {
        assert!(!self.remaining_sub_steps.is_empty());
        self.remaining_sub_steps.remove(0);
    }

    pub fn advance(&mut self) -> EndedSegment {
        if self.remaining_sub_steps.len() <= 1 {
            EndedSegment::Step
        } else {
            self.remaining_sub_steps.remove(0);
            EndedSegment::SubStep
        }
    }
}

impl fmt::Display for Step {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum StepName {
    Untap,
    Upkeep,
    Draw,
    PreCombatMain,
    BeginningOfCombat,
    DeclareAttackers,
    DeclareBlockers,
    CombatDamage,
    EndOfCombat,
    PostCombatMain,
    End,
    Cleanup,
}

impl StepName {
    pub fn is_main(&self) -> bool {
        *self == StepName::PreCombatMain || *self == StepName::PostCombatMain
    }

}

impl fmt::Display for StepName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}
