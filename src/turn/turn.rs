use std::collections::VecDeque;

use player::player_id::PlayerID;
use turn::ended_segment::EndedSegment;
use turn::phase::{Phase, PhaseName};
use turn::step::{Step, StepName};

pub struct Turn {
    player_id: PlayerID,
    remaining_phases: VecDeque<Phase>,
    lands_played_this_turn: usize,
}

impl Turn {
    pub fn new(player_id: PlayerID) -> Turn {
        Turn {
            player_id,
            remaining_phases: VecDeque::from(vec![
                Phase::new_beginning(),
                Phase::new_pre_combat_main(),
                Phase::new_combat(),
                Phase::new_post_combat_main(),
                Phase::new_ending()
            ]),
            lands_played_this_turn: 0,
        }
    }

    pub fn new_with_no_draw_step(player_id: PlayerID) -> Turn {
        Turn {
            player_id,
            remaining_phases: VecDeque::from(vec![
                Phase::new_beginning_with_no_draw_step(),
                Phase::new_pre_combat_main(),
                Phase::new_combat(),
                Phase::new_post_combat_main(),
                Phase::new_ending()
            ]),
            lands_played_this_turn: 0,
        }
    }

    pub fn player_id(&self) -> PlayerID {
        self.player_id
    }

    pub fn current_phase_mut(&mut self) -> &mut Phase {
        &mut self.remaining_phases[0]
    }

    pub fn current_phase_name(&self) -> PhaseName {
        self.remaining_phases[0].name()
    }

    pub fn current_step(&self) -> &Step {
        self.remaining_phases[0].current_step()
    }

    pub fn current_step_mut(&mut self) -> &mut Step {
        self.remaining_phases[0].current_step_mut()
    }

    pub fn current_step_name(&self) -> StepName {
        self.remaining_phases[0].current_step_name()
    }

    pub fn lands_played_this_turn(&self) -> usize {
        self.lands_played_this_turn
    }

    pub fn increment_lands_played_count(&mut self) {
        self.lands_played_this_turn += 1;
    }

    pub fn advance(&mut self) -> EndedSegment {
        let mut phase_result = self.remaining_phases[0].advance();
        if phase_result == EndedSegment::Phase {
            if self.remaining_phases.len() <= 1 {
                phase_result = EndedSegment::Turn;
            } else {
                self.remaining_phases.remove(0);
            }
        }

        phase_result
    }
}
