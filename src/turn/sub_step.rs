use std::fmt;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum SubStep {
    Beginning,
    End,

    Priority,
    MainPriority,

    Phasing,
    Untap,
    Draw,
    Saga,

    DeclareAttackers,
    DeclareBlockers,
    EnableNeededCombatDamageSteps,
    AssignAttackerOrder,
    AssignBlockerOrder,
    AssignAttackerDamage,
    AssignBlockerDamage,
    DealCombatDamage,
    RemoveFromCombat,

    DiscardToHandSize,
    Cleanup,
    EnableAdditionalCleanupStep,
}

impl fmt::Display for SubStep {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}
