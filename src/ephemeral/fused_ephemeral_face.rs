use ability::ephemeral_kicker::EphemeralKicker;
use cost::cost::Cost;
use effect::effect_template::EffectTemplate;
use ephemeral::ephemeral_face::EphemeralFace;
use ephemeral::regular_ephemeral_face::RegularEphemeralFace;
use face::face::{Face, Name, FlavorText};

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct FusedEphemeralFace {
    name: Name,
    maybe_casting_cost: Option<Cost>,
    kickers: Vec<EphemeralKicker>,
    effect_templates: Vec<EffectTemplate>,

    left: RegularEphemeralFace,
    right: RegularEphemeralFace,
}

impl FusedEphemeralFace {
    pub fn new(left: RegularEphemeralFace, right: RegularEphemeralFace) -> FusedEphemeralFace {
        if !left.ephemeral_type().has_only_default_fields() ||
            !right.ephemeral_type().has_only_default_fields() {

            panic!("Can only fuse Ephemerals that have only default fields.");
        }

        let mut kickers = Vec::new();
        kickers.append(&mut left.kickers().clone());
        kickers.append(&mut right.kickers().clone());
        let mut effects = Vec::new();
        effects.append(&mut left.effect_templates().clone());
        effects.append(&mut right.effect_templates().clone());

        FusedEphemeralFace {
            name: Name(format!("{} // {}", left.name().0, right.name().0)),
            maybe_casting_cost: Cost::add_option_costs(
                left.maybe_casting_cost(),
                right.maybe_casting_cost(),
            ),
            kickers,
            effect_templates: effects,

            left,
            right,
        }
    }

    pub fn left(&self) -> &RegularEphemeralFace {
        &self.left
    }

    pub fn right(&self) -> &RegularEphemeralFace {
        &self.right
    }
}

impl Face for FusedEphemeralFace {
    fn maybe_name(&self) -> Option<&Name> {
        Some(self.name())
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        self.maybe_casting_cost.as_ref()
    }
}

impl EphemeralFace for FusedEphemeralFace {
    fn name(&self) -> &Name {
        &self.name
    }

    fn flavor_text(&self) -> Option<&FlavorText> {
        None
    }

    fn kickers(&self) -> &Vec<EphemeralKicker> {
        &self.kickers
    }

    fn effect_templates(&self) -> &Vec<EffectTemplate> {
        &self.effect_templates
    }

    fn is_instant(&self) -> bool {
        self.left.is_instant() || self.right.is_instant()
    }

    fn is_sorcery(&self) -> bool {
        self.left.is_sorcery() || self.right.is_sorcery()
    }
}
