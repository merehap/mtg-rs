use std::collections::BTreeMap;

use ephemeral::ephemeral_face::EphemeralFace;
use ephemeral::ephemeral_layout::{EphemeralLayout, SplitID};
use face::face::{Face, Expansion, CardNumber, Rarity, ArtistName};
use mana::color::Color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct EphemeralCard {
    pub expansion: Expansion,
    pub card_number: CardNumber,
    pub rarity: Rarity,
    pub artist_name: ArtistName,

    pub layout: EphemeralLayout,
}

impl EphemeralCard {
    pub fn is_sorcery(&self) -> bool {
        self.faces().iter()
            .find(|(_, face)| face.is_sorcery())
            .is_some()
    }

    pub fn is_instant(&self) -> bool {
        self.faces().iter()
            .find(|(_, face)| face.is_instant())
            .is_some()
    }

    pub fn is_color(&self, color: Color) -> bool {
        self.faces().iter()
            .find(|(_, face)| face.is_color(color))
            .is_some()
    }

    pub fn ephemeral_layout(&self) -> &EphemeralLayout {
        &self.layout
    }

    pub fn faces(&self) -> BTreeMap<Option<SplitID>, Box<&EphemeralFace>> {
        let mut faces: BTreeMap<Option<SplitID>, Box<&EphemeralFace>> = BTreeMap::new();

        use ephemeral::ephemeral_layout::EphemeralLayout::*;
        match self.layout {
            Regular(ref face) => {
                faces.insert(None, Box::new(face));
            },
            Split(ref left, ref right) => {
                faces.insert(Some(SplitID::Left), Box::new(left));
                faces.insert(Some(SplitID::Right), Box::new(right));
            },
            FiveSplit(ref one, ref two, ref three, ref four, ref five) => {
                // TODO: Actually have proper SplitIDs for 5.
                // These ones just override each other currently.
                faces.insert(None, Box::new(one));
                faces.insert(None, Box::new(two));
                faces.insert(None, Box::new(three));
                faces.insert(None, Box::new(four));
                faces.insert(None, Box::new(five));
            },
            Fuse(ref fused) => {
                faces.insert(Some(SplitID::Left), Box::new(fused.left()));
                faces.insert(Some(SplitID::Right), Box::new(fused.right()));
                faces.insert(None, Box::new(fused));
            },
        }

        faces
    }

    pub fn converted_mana_cost(&self) -> usize {
        use ephemeral::ephemeral_layout::EphemeralLayout::*;
        match self.layout {
            Regular(ref face) => face.converted_mana_cost(),
            Split(ref left, ref right) =>
                left.converted_mana_cost() + right.converted_mana_cost(),
            FiveSplit(ref first, ref second, ref third, ref fourth, ref fifth) =>
                *(&[first, second, third, fourth, fifth].iter()
                    .map(|face| face.converted_mana_cost())
                    .sum()),
            Fuse(ref fused) => fused.converted_mana_cost(),
        }
    }

    pub fn format_display_name(&self) -> String {
        use ephemeral::ephemeral_layout::EphemeralLayout::*;
        match self.layout {
            Regular(ref face) => face.name().0.clone(),
            Split(ref left, ref right) => format!("{} // {}", left.name().0.clone(), right.name().0.clone()),
            FiveSplit(ref first, ref second, ref third, ref fourth, ref fifth) =>
                format!("{} // {} // {} // {} // {}",
                        first.name().0.clone(),
                        second.name().0.clone(),
                        third.name().0.clone(),
                        fourth.name().0.clone(),
                        fifth.name().0.clone()),
            Fuse(ref fused) => fused.name().0.clone(),
        }
    }
}