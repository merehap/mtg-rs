use ability::ephemeral_kicker::EphemeralKicker;
use ability::stack_object_static_ability::StackObjectStaticAbility;
use cost::cost::Cost;
use cost::cost_builder::CostBuilder;
use effect::effect_template::EffectTemplate;
use ephemeral::ephemeral_card::EphemeralCard;
use ephemeral::ephemeral_layout::EphemeralLayout;
use ephemeral::ephemeral_face::CastingWindowConstraint;
use ephemeral::ephemeral_face_builder::EphemeralFaceBuilder;
use face::face::{Expansion, CardNumber, Rarity, ArtistName};
use util::positive_integer::PositiveInteger;

pub struct RegularEphemeralBuilder {
    expansion: Option<Expansion>,
    card_number: Option<CardNumber>,
    rarity: Option<Rarity>,
    artist_name: Option<ArtistName>,
    face_builder: EphemeralFaceBuilder,
}

impl RegularEphemeralBuilder {
    pub fn new() -> RegularEphemeralBuilder {
        RegularEphemeralBuilder {
            expansion: None,
            card_number: None,
            rarity: None,
            artist_name: None,
            face_builder: EphemeralFaceBuilder::new(),
        }
    }

    pub fn name(mut self, name: &str) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.name(name);
        self
    }

    pub fn expansion(mut self, expansion: Expansion) -> RegularEphemeralBuilder {
        self.expansion = Some(expansion);
        self
    }

    pub fn card_number(mut self, card_number: usize) -> RegularEphemeralBuilder {
        self.card_number = Some(CardNumber::new(
            PositiveInteger::from_usize(card_number).expect("Card number must be positive")));
        self
    }

    pub fn rarity(mut self, rarity: Rarity) -> RegularEphemeralBuilder {
        self.rarity = Some(rarity);
        self
    }

    pub fn artist_name(mut self, artist_name: &str) -> RegularEphemeralBuilder {
        self.artist_name = Some(ArtistName::from_str(artist_name).expect("Bad artist name."));
        self
    }

    pub fn flavor_text(mut self, flavor_text: &str) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.flavor_text(flavor_text);
        self
    }

    pub fn no_flavor_text(mut self) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.no_flavor_text();
        self
    }

    pub fn complex_casting_cost(mut self, casting_cost: Cost) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.complex_casting_cost(casting_cost);
        self
    }

    pub fn casting_cost(mut self, casting_cost: &str) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.casting_cost(casting_cost);
        self
    }

    pub fn sorcery(mut self) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.sorcery();
        self
    }

    pub fn sorcery_with_instant_speed_upgrade(
        mut self,
        instant_speed_cost: CostBuilder) -> RegularEphemeralBuilder {

        self.face_builder =
            self.face_builder.sorcery_with_instant_speed_upgrade(instant_speed_cost);
        self
    }

    pub fn instant(mut self) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.instant();
        self
    }

    pub fn instant_with_casting_window_constraint(
        mut self,
        constraint: CastingWindowConstraint
    ) -> RegularEphemeralBuilder {

        self.face_builder =
            self.face_builder.instant_with_casting_window_constraint(constraint);
        self
    }

    pub fn kicker(mut self, kicker: &str) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.add_kicker(EphemeralKicker::Regular(
            CostBuilder::new().mana(kicker).into_cost()
        ));
        self
    }

    pub fn add_static_ability(mut self, ability: StackObjectStaticAbility) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.add_static_ability(ability);
        self
    }

    pub fn add_effect(mut self, effect: EffectTemplate) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.add_effect(effect);
        self
    }

    pub fn add_kicker_effect(mut self, effect: EffectTemplate) -> RegularEphemeralBuilder {
        self.face_builder = self.face_builder.add_kicker_effect(effect);
        self
    }

    pub fn build(self) -> EphemeralCard {
        EphemeralCard {
            expansion: self.expansion.expect("Expansion must be set."),
            card_number: self.card_number.expect("Expansion must be set."),
            rarity: self.rarity.expect("Rarity must be set."),
            artist_name: self.artist_name.expect("ArtistName must be set."),
            layout: EphemeralLayout::Regular(self.face_builder.build()),
        }
    }
}
