use ability::ephemeral_kicker::EphemeralKicker;
use cost::cost::Cost;
use effect::effect_template::EffectTemplate;
use face::face::{Face, AsFace, Name, FlavorText};

///
/// An Ephemeral is any non-Permanent type of Card.
///
pub trait EphemeralFace : Face + AsFace {
    fn name(&self) -> &Name;
    fn flavor_text(&self) -> Option<&FlavorText>;
    fn kickers(&self) -> &Vec<EphemeralKicker>;
    fn effect_templates(&self) -> &Vec<EffectTemplate>;

    fn is_instant(&self) -> bool;
    fn is_sorcery(&self) -> bool;
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EphemeralType {
    Instant {
        subtype: EphemeralSubtype,
        casting_window_constraint: Option<CastingWindowConstraint>,
    },
    Sorcery {
        subtype: EphemeralSubtype,
        instant_speed_cost: Option<Cost>,
    },
}

impl EphemeralType {
    pub const REGULAR_INSTANT: EphemeralType = EphemeralType::Instant {
        subtype: EphemeralSubtype::Regular,
        casting_window_constraint: None,
    };

    pub const REGULAR_SORCERY: EphemeralType = EphemeralType::Sorcery {
        subtype: EphemeralSubtype::Regular,
        instant_speed_cost: None,
    };

    pub fn has_only_default_fields(&self) -> bool {
        *self == EphemeralType::REGULAR_INSTANT ||
            *self == EphemeralType::REGULAR_SORCERY
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EphemeralSubtype {
    Regular,
    Arcane,
    Trap,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CastingWindowConstraint {
    Combat,
    CombatAfterBlockersDeclared,
}
