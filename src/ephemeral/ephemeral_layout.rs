use std::fmt;

use ephemeral::fused_ephemeral_face::FusedEphemeralFace;
use ephemeral::regular_ephemeral_face::RegularEphemeralFace;

#[allow(clippy::large_enum_variant)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EphemeralLayout {
    /// A non-Permanent Card that only has a single Face.
    Regular(RegularEphemeralFace),
    /// A non-Permanent Card that has a left Face and a right Face.
    Split(RegularEphemeralFace, RegularEphemeralFace),
    /// A non-Permanent Card that has a five Faces.
    FiveSplit(
        RegularEphemeralFace,
        RegularEphemeralFace,
        RegularEphemeralFace,
        RegularEphemeralFace,
        RegularEphemeralFace,
    ),
    /// A non-Permanent Card that has a left Face and a right Face that
    /// can be played together as a single Spell.
    Fuse(FusedEphemeralFace),
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Copy, Debug)]
pub enum SplitID {
    Left,
    Right,
}

impl fmt::Display for SplitID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}
