use ability::ephemeral_kicker::EphemeralKicker;
use ability::stack_object_static_ability::StackObjectStaticAbility;
use cost::cost::Cost;
use cost::cost_builder::CostBuilder;
use effect::effect_template::EffectTemplate;
use ephemeral::ephemeral_face::{EphemeralType, EphemeralSubtype, CastingWindowConstraint};
use ephemeral::regular_ephemeral_face::RegularEphemeralFace;
use face::face::{Name, FlavorText};
use mana::mana_cost::ManaCost;

///
/// An Ephemeral is any non-Permanent type of Card.
///
pub struct EphemeralFaceBuilder {
    name: Option<Name>,
    flavor_text: Option<Option<FlavorText>>,
    casting_cost: Option<Option<Cost>>,
    ephemeral_type: Option<EphemeralType>,
    kickers: Vec<EphemeralKicker>,
    static_abilities: Vec<StackObjectStaticAbility>,
    effects: Vec<EffectTemplate>,
    kicker_effects: Vec<EffectTemplate>,
}

impl EphemeralFaceBuilder {
    pub fn new() -> EphemeralFaceBuilder {
        EphemeralFaceBuilder {
            name: None,
            flavor_text: None,
            casting_cost: None,
            ephemeral_type: None,
            kickers: Vec::new(),
            static_abilities: Vec::new(),
            effects: Vec::new(),
            kicker_effects: Vec::new(),
        }
    }

    pub fn name(mut self, name: &str) -> EphemeralFaceBuilder {
        self.name = Some(Name(name.to_string()));
        self
    }

    pub fn flavor_text(mut self, flavor_text: &str) -> EphemeralFaceBuilder  {
        self.flavor_text = Some(Some(FlavorText::new(flavor_text)));
        self
    }

    pub fn no_flavor_text(mut self) -> EphemeralFaceBuilder  {
        self.flavor_text = Some(None);
        self
    }

    pub fn complex_casting_cost(mut self, casting_cost: Cost) -> EphemeralFaceBuilder  {
        self.casting_cost = Some(Some(casting_cost));
        self
    }

    pub fn casting_cost(mut self, casting_cost: &str) -> EphemeralFaceBuilder  {
        self.casting_cost = Some(Some(
            Cost::mana_only(ManaCost::try_from_str(casting_cost).expect("Bad casting cost."))));
        self
    }

    pub fn sorcery(mut self) -> EphemeralFaceBuilder {
        self.ephemeral_type = Some(EphemeralType::REGULAR_SORCERY);
        self
    }

    pub fn sorcery_with_instant_speed_upgrade(
        mut self,
        instant_speed_additional_cost: CostBuilder) -> EphemeralFaceBuilder {

        self.ephemeral_type = Some(EphemeralType::Sorcery {
            subtype: EphemeralSubtype::Regular,
            instant_speed_cost: Some(instant_speed_additional_cost.into_cost()),
        });
        self
    }

    pub fn instant(mut self) -> EphemeralFaceBuilder {
        self.ephemeral_type = Some(EphemeralType::REGULAR_INSTANT);
        self
    }

    pub fn instant_with_casting_window_constraint(
        mut self,
        constraint: CastingWindowConstraint) -> EphemeralFaceBuilder {

        self.ephemeral_type = Some(EphemeralType::Instant {
            subtype: EphemeralSubtype::Regular,
            casting_window_constraint: Some(constraint),
        });
        self
    }

    pub fn add_kicker(mut self, kicker: EphemeralKicker) -> EphemeralFaceBuilder {
        self.kickers.push(kicker);
        self
    }

    pub fn add_static_ability(mut self, ability: StackObjectStaticAbility) -> EphemeralFaceBuilder {
        self.static_abilities.push(ability);
        self
    }

    pub fn add_effect(mut self, effect: EffectTemplate) -> EphemeralFaceBuilder {
        self.effects.push(effect);
        self
    }

    pub fn add_kicker_effect(mut self, effect: EffectTemplate) -> EphemeralFaceBuilder {
        self.kicker_effects.push(effect);
        self
    }

    pub fn build(self) -> RegularEphemeralFace {
        if self.effects.is_empty() {
            panic!("At least one effect must be specified for {:?}.", self.name);
        }

        RegularEphemeralFace {
            name: self.name.expect("Name is required for EphemeralFace"),
            flavor_text: self.flavor_text.expect("FlavorText is required for EphemeralFace"),
            casting_cost: self.casting_cost.expect("CastingCost is required for EphemeralFace"),
            ephemeral_type: self.ephemeral_type.expect("EphemeralType is required for EphemeralFace"),
            kickers: self.kickers,
            effect_templates: self.effects,
        }
    }
}