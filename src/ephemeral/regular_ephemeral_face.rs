use ability::ephemeral_kicker::EphemeralKicker;
use cost::cost::Cost;
use effect::effect_template::EffectTemplate;
use ephemeral::ephemeral_face::{EphemeralFace, EphemeralType};
use face::face::{Face, Name, FlavorText};

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct RegularEphemeralFace {
    pub name: Name,
    pub flavor_text: Option<FlavorText>,
    pub casting_cost: Option<Cost>,
    pub ephemeral_type: EphemeralType,
    pub kickers: Vec<EphemeralKicker>,
    pub effect_templates: Vec<EffectTemplate>,
}

impl RegularEphemeralFace {
    pub fn ephemeral_type(&self) -> &EphemeralType {
        &self.ephemeral_type
    }
}

impl Face for RegularEphemeralFace {
    fn maybe_name(&self) -> Option<&Name> {
        Some(&self.name())
    }

    fn maybe_casting_cost(&self) -> Option<&Cost> {
        self.casting_cost.as_ref()
    }
}

impl EphemeralFace for RegularEphemeralFace {
    fn name(&self) -> &Name {
        &self.name
    }

    fn flavor_text(&self) -> Option<&FlavorText> {
        self.flavor_text.as_ref()
    }

    fn kickers(&self) -> &Vec<EphemeralKicker> {
        &self.kickers
    }

    fn effect_templates(&self) -> &Vec<EffectTemplate> {
        &self.effect_templates
    }

    fn is_instant(&self) -> bool {
        match self.ephemeral_type {
            EphemeralType::Instant {..} => true,
            EphemeralType::Sorcery {..} => false,
        }
    }

    fn is_sorcery(&self) -> bool {
        match self.ephemeral_type {
            EphemeralType::Sorcery {..} => true,
            EphemeralType::Instant {..} => false,
        }
    }
}
