use std::collections::BTreeSet;
use std::fmt;

use cost::cost::Cost;
use mana::color::Color;
use util::positive_integer::PositiveInteger;

///
/// A Face is the collection of rules text and metadata that constitutes
/// a minimally playable Magic Object.
/// Cards and Permanents usually have a single Face, but some have more than one.
///
pub trait Face {
    fn maybe_name(&self) -> Option<&Name>;
    fn maybe_casting_cost(&self) -> Option<&Cost>;

    fn name_or_unnamed(&self) -> String {
        self.maybe_name()
            .map(|n| n.clone().0)
            .unwrap_or("(Unnamed)".to_string())
    }

    fn converted_mana_cost(&self) -> usize {
        self.maybe_casting_cost()
            .map(|cost| cost.mana().converted_mana_cost())
            .unwrap_or(0)
    }

    fn colors(&self) -> BTreeSet<Color> {
        self.maybe_casting_cost()
            .map(|cost| cost.mana().to_colors())
            .unwrap_or_default()
    }

    fn is_color(&self, color: Color) -> bool {
        self.colors().contains(&color)
    }

    ///
    /// Whether a card has no Colors.
    ///
    fn is_colorless(&self) -> bool {
        self.colors().is_empty()
    }
}

pub trait CostedFace: Face + AsFace {
    fn casting_cost(&self) -> &Cost;
}

// A workaround trait necessary for casting CostedFaces to Faces.
pub trait AsFace {
    fn as_face(&self) -> &Face;
}

impl<T: Face> AsFace for T {
    fn as_face(&self) -> &Face {
        self
    }
}

// TODO: Validate that there aren't spaces on the beginning nor end of the name.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Name(pub String);

impl fmt::Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Expansion {
    Invasion,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct CardNumber {
    number: PositiveInteger,
}

impl CardNumber {
    pub fn new(number: PositiveInteger) -> CardNumber {
        CardNumber{number}
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum Rarity {
    Land,
    Common,
    Uncommon,
    Rare,
    Mythic,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct ArtistName {
    name: String,
}

impl ArtistName {
    pub fn from_str(raw_name: &str) -> Result<ArtistName, String> {
        Ok(ArtistName{name: raw_name.to_string()})
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct FlavorText {
    text: String,
}

impl FlavorText {
    pub fn new(text: &str) -> FlavorText {
        FlavorText {text: text.to_string()}
    }
}
