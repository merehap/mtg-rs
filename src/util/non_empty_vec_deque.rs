use std::collections::VecDeque;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Debug)]
pub struct NonEmptyVecDeque<T> {
    values: VecDeque<T>,
}

impl <T> NonEmptyVecDeque<T> {
    pub fn new(first: T, rest: Vec<T>) -> NonEmptyVecDeque<T> {
        let mut values = VecDeque::new();
        values.push_back(first);
        values.append(&mut rest.into_iter().collect());
        NonEmptyVecDeque {values}
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn push_back(&mut self, t: T) {
        self.values.push_back(t);
    }

    pub fn front(&self) -> &T {
        self.values.front().expect("Zero elements in a non-empty Vec.")
    }

    pub fn front_mut(&mut self) -> &mut T {
        self.values.front_mut().expect("Zero elements in a non-empty Vec.")
    }

    pub fn pop_front(&mut self) -> Option<T> {
        if self.values.len() <= 1 {
            return None;
        }

        self.values.pop_front()
    }

    pub fn pop_back(&mut self) -> Option<T> {
        if self.values.len() <= 1 {
            return None;
        }

        self.values.pop_back()
    }

    pub fn insert(&mut self, index: usize, t: T) {
        self.values.insert(index, t);
    }

    pub fn into_vec(self) -> Vec<T> {
        self.values.into_iter().collect()
    }

    pub fn into_first_element(mut self) -> T {
        self.values.pop_front().expect("Zero elements in a non-empty Vec.")
    }
}
