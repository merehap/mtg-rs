use std::ops::{Add, AddAssign};

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct PositiveInteger(usize);

impl PositiveInteger {
    pub fn from_usize(n: usize) -> Option<PositiveInteger> {
        if n == 0 {
            None
        } else {
            Some(PositiveInteger(n))
        }
    }

    pub fn from_isize(n: isize) -> Option<PositiveInteger> {
        if n <= 0 {
            None
        } else {
            Some(PositiveInteger(n as usize))
        }
    }

    pub fn one() -> PositiveInteger {
        PositiveInteger(1)
    }

    pub fn to_usize(self) -> usize {
        self.0
    }

    pub fn to_isize(self) -> isize {
        self.0 as isize
    }

    pub fn subtract(self, rhs: PositiveInteger) -> Option<PositiveInteger> {
        if self > rhs {
            Some(PositiveInteger(self.0 - rhs.0))
        } else {
            None
        }
    }
}

impl Add for PositiveInteger {
    type Output = PositiveInteger;

    fn add(self, rhs: PositiveInteger) -> Self::Output {
        PositiveInteger(self.0 + rhs.0)
    }
}

impl AddAssign for PositiveInteger {
    fn add_assign(&mut self, rhs: PositiveInteger) {
        self.0 += rhs.0
    }
}
