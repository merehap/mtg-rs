use std::collections::BTreeMap;

pub fn concat_maps<K: Ord, V>(
    mut left: BTreeMap<K, V>,
    mut right: BTreeMap<K, V>,
) -> BTreeMap<K, V> {

    let mut map = BTreeMap::new();
    map.append(&mut left);
    map.append(&mut right);
    map
}
