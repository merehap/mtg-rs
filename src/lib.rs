#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![feature(half_open_range_patterns)]
#![feature(exclusive_range_pattern)]
// FIXME
#![allow(dead_code)]
// FIXME
#![allow(unused_variables)]
// FIXME
#![allow(bare_trait_objects)]
#![deny(clippy::all)]
#![allow(clippy::large_enum_variant)]
#![allow(clippy::module_inception)]
#![allow(clippy::option_option)]

extern crate rand;

mod ability;
pub mod card;
mod cost;
pub mod data;
pub mod deck;
mod effect;
mod ephemeral;
mod face;
pub mod game;
mod location;
pub mod mana;
pub mod permanent;
pub mod player;
mod script;
mod turn;
mod util;
mod zone;
