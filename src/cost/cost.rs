use std::collections::BTreeMap;
use std::ops::Add;

use mana::mana_cost::ManaCost;
use script::condition::permanent_condition::PermanentCondition;
use util::collections::concat_maps;
use util::positive_integer::PositiveInteger;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Cost {
    pub mana: ManaCost,
    pub life: usize,
    pub tap_counts: BTreeMap<PermanentCondition, PositiveInteger>,
    pub untap_counts: BTreeMap<PermanentCondition, PositiveInteger>,
    pub sacrifice_counts: BTreeMap<PermanentCondition, PositiveInteger>,
}

impl Cost {
    pub fn zero() -> Cost {
        Cost {
            mana: ManaCost::zero(),
            life: 0,
            tap_counts: BTreeMap::new(),
            untap_counts: BTreeMap::new(),
            sacrifice_counts: BTreeMap::new(),
        }
    }

    pub fn mana_only(mana: ManaCost) -> Cost {
        let mut cost = Cost::zero();
        cost.mana = mana;
        cost
    }

    pub fn mana(&self) -> &ManaCost {
        &self.mana
    }

    pub fn add_option_costs(left: Option<&Cost>, right: Option<&Cost>) -> Option<Cost> {
        if let (Some(l), Some(r)) = (left, right) {
            Some(l + r)
        } else {
            left.or(right).cloned()
        }
    }
}

impl Add for &Cost {
    type Output = Cost;

    fn add(self, rhs: &Cost) -> Self::Output {
        let lhs = self.clone();
        let rhs = rhs.clone();

        Cost {
            mana: lhs.mana + rhs.mana,
            life: lhs.life + rhs.life,
            tap_counts: concat_maps(lhs.tap_counts, rhs.tap_counts),
            untap_counts: concat_maps(lhs.untap_counts, rhs.untap_counts),
            sacrifice_counts: concat_maps(lhs.sacrifice_counts, rhs.sacrifice_counts),
        }
    }
}
