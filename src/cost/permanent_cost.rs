use cost::cost::Cost;
use mana::mana_cost::ManaCost;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct PermanentCost {
    pub regular_cost: Cost,
    pub self_tap_cost: Option<SelfTapCost>,
    pub self_remove_cost: Option<SelfRemoveCost>,
}

impl PermanentCost {
    pub fn zero() -> PermanentCost {
        PermanentCost {
            regular_cost: Cost::zero(),
            self_tap_cost: None,
            self_remove_cost: None,
        }
    }

    pub fn regular_cost_only(regular_cost: Cost) -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.regular_cost = regular_cost;
        cost
    }

    pub fn mana_only(mana: ManaCost) -> PermanentCost {
        PermanentCost::regular_cost_only(Cost::mana_only(mana))
    }

    pub fn self_tap() -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.self_tap_cost = Some(SelfTapCost::Tap);
        cost
    }

    pub fn self_untap() -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.self_tap_cost = Some(SelfTapCost::Untap);
        cost
    }

    pub fn self_bounce() -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.self_remove_cost = Some(SelfRemoveCost::Bounce);
        cost
    }

    pub fn self_sacrifice() -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.self_remove_cost = Some(SelfRemoveCost::Sacrifice);
        cost
    }

    pub fn self_exile() -> PermanentCost {
        let mut cost = PermanentCost::zero();
        cost.self_remove_cost = Some(SelfRemoveCost::Exile);
        cost
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum SelfTapCost {
    Tap,
    Untap,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum SelfRemoveCost {
    Bounce,
    Sacrifice,
    Exile,
}
