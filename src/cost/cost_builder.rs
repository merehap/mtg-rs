use std::collections::BTreeMap;

use cost::cost::Cost;
use cost::permanent_cost::{PermanentCost, SelfTapCost, SelfRemoveCost};
use mana::mana_cost::ManaCost;
use script::condition::permanent_condition::PermanentCondition;
use util::positive_integer::PositiveInteger;

pub struct CostBuilder {
    mana: ManaCost,
    life: usize,

    tap_counts: BTreeMap<PermanentCondition, PositiveInteger>,
    untap_counts: BTreeMap<PermanentCondition, PositiveInteger>,
    sacrifice_counts: BTreeMap<PermanentCondition, PositiveInteger>,
    discard_at_random_count: usize,

    self_tap_cost: Option<SelfTapCost>,
    self_remove_cost: Option<SelfRemoveCost>,
}

impl CostBuilder {
    pub fn new() -> CostBuilder {
        CostBuilder {
            mana: ManaCost::zero(),
            life: 0,
            tap_counts: BTreeMap::new(),
            untap_counts: BTreeMap::new(),
            sacrifice_counts: BTreeMap::new(),
            discard_at_random_count: 0,

            self_tap_cost: None,
            self_remove_cost: None,
        }
    }

    pub fn mana(mut self, value: &str) -> CostBuilder {
        self.mana = ManaCost::try_from_str(value)
            .expect("Bad mana cost.");
        self
    }

    pub fn life(mut self, value: usize) -> CostBuilder {
        self.life = value;
        self
    }

    pub fn add_tap_count(mut self, count: usize, condition: PermanentCondition) -> CostBuilder {
        self.tap_counts.insert(
            condition,
            PositiveInteger::from_usize(count).expect("Tap count must be positive."));
        self
    }

    pub fn add_untap_count(mut self, count: usize, condition: PermanentCondition) -> CostBuilder {
        self.untap_counts.insert(
            condition,
            PositiveInteger::from_usize(count).expect("Untap count must be positive."));
        self
    }

    pub fn sacrifice_count(mut self, count: usize, condition: PermanentCondition) -> CostBuilder {
        self.sacrifice_counts.insert(
            condition,
            PositiveInteger::from_usize(count).expect("Sacrifice count must be positive."));
        self
    }

    pub fn discard_at_random_count(mut self, count: usize) -> CostBuilder {
        self.discard_at_random_count = count;
        self
    }

    pub fn self_tap(mut self) -> CostBuilder {
        self.self_tap_cost = Some(SelfTapCost::Tap);
        self
    }

    pub fn self_untap(mut self) -> CostBuilder {
        self.self_tap_cost = Some(SelfTapCost::Untap);
        self
    }

    pub fn self_bounce(mut self) -> CostBuilder {
        self.self_remove_cost = Some(SelfRemoveCost::Bounce);
        self
    }

    pub fn self_sacrifice(mut self) -> CostBuilder {
        self.self_remove_cost = Some(SelfRemoveCost::Sacrifice);
        self
    }

    pub fn self_exile(mut self) -> CostBuilder {
        self.self_remove_cost = Some(SelfRemoveCost::Exile);
        self
    }

    pub fn into_cost(self) -> Cost {
        if self.self_tap_cost.is_some() {
            panic!("SelfTapCost cannot be set when creating a Cost.");
        }

        if self.self_remove_cost.is_some() {
            panic!("SelfRemoveCost cannot be set when creating a Cost.");
        }

        Cost {
            mana: self.mana,
            life: self.life,
            tap_counts: self.tap_counts,
            untap_counts: self.untap_counts,
            sacrifice_counts: self.sacrifice_counts,
        }
    }

    pub fn into_permanent_cost(self) -> PermanentCost {
        PermanentCost {
            regular_cost: Cost {
                mana: self.mana,
                life: self.life,
                tap_counts: self.tap_counts,
                untap_counts: self.untap_counts,
                sacrifice_counts: self.sacrifice_counts,
            },
            self_tap_cost: self.self_tap_cost,
            self_remove_cost: self.self_remove_cost,
        }
    }
}