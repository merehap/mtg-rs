use effect::effect_type::permanent_effect_type::CounterType;
use mana::color::Color;
use script::condition::card_condition::CardCondition;
use script::condition::color_condition::ColorCondition;
use script::condition::condition::Condition;
use script::condition::permanent_condition::PermanentCondition;
use script::variable::object_variable::ObjectVariable;
use script::variable::player_variable::PlayerVariable;
use util::positive_integer::PositiveInteger;
use game::Game;
use player::player_id::PlayerID;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Count {
    Fixed(usize),
    FixedSigned(isize),
    ManaVariable(ColorCondition),
    MatchingColorInCost(Color),
    NumberVariable,
    KickerManaVariable(ColorCondition),
    Permanent(Box<PermanentCondition>),
    SourceControllerBasicLandTypes,
    AuraControllerBasicLandTypes,
    BlockerCount,

    MatchingCardsInHand {
        player_variable: PlayerVariable,
        condition: Box<CardCondition>,
    },

    ConvertedManaCost(ObjectVariable),
    Power(ObjectVariable),
    Toughness(ObjectVariable),
    SelfCounters(CounterType),

    Negative(Box<Count>),
    Multiply(Box<Count>, Box<Count>),

    IfElse {
        condition: Box<Condition>,
        then: Box<Count>,
        otherwise: Box<Count>,
    }
}

impl Count {
    pub fn mana_variable() -> Count {
        Count::ManaVariable(ColorCondition::Any)
    }

    pub fn kicker_mana_variable() -> Count {
        Count::KickerManaVariable(ColorCondition::Any)
    }

    pub fn negative(count: Count) -> Count {
        Count::Negative(Box::new(count))
    }

    pub fn permanent(condition: PermanentCondition) -> Count {
        Count::Permanent(Box::new(condition))
    }

    pub fn to_isize(&self, stack_object_controller_id: PlayerID, game: &Game) -> isize {
        match *self {
            Count::Fixed(value) => value as isize,
            Count::FixedSigned(value) => value,
            Count::Permanent(condition) => game.battlefield().matching_permanents(condition.compile()).len(),
            _ => unimplemented!("Non-fixed size Counts aren't supported yet."),
        }
    }

    pub fn to_usize(&self) -> Option<usize> {
        let value = self.to_isize();
        if value >= 0 {
            Some(value as usize)
        } else {
            None
        }
    }

    pub fn to_positive_integer(&self) -> Option<PositiveInteger> {
        PositiveInteger::from_isize(self.to_isize())
    }
}