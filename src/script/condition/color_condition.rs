use mana::color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ColorCondition {
    Any,
    Color(color::Color),
    Or(Vec<color::Color>),
}

impl ColorCondition {
    pub fn white() -> ColorCondition {
        ColorCondition::Color(color::Color::White)
    }

    pub fn blue() -> ColorCondition {
        ColorCondition::Color(color::Color::Blue)
    }

    pub fn black() -> ColorCondition {
        ColorCondition::Color(color::Color::Black)
    }

    pub fn red() -> ColorCondition {
        ColorCondition::Color(color::Color::Red)
    }

    pub fn green() -> ColorCondition {
        ColorCondition::Color(color::Color::Green)
    }
}
