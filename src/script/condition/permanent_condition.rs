use effect::effect_type::permanent_effect_type::CounterType;
use permanent::permanent::Permanent;
use permanent::permanent_type::creature_type::CreatureType;
use script::condition::card_condition::CardCondition;
use script::condition::integer_condition::IntegerCondition;
use script::condition::player_condition::PlayerCondition;
use script::variable::color_variable::ColorVariable;
use script::variable::colors_variable::ColorsVariable;
use script::variable::creature_type_variable::CreatureTypeVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentCondition {
    Any,

    CardCondition(CardCondition),

    Itself,
    NotItself,

    ControlledBy(PlayerCondition),
    // TODO: Remove these six in favor of using PlayerCondition.
    ControlledByController,
    ControlledByOpponent,
    ControlledByTarget,
    ControllerControls(Box<PermanentCondition>),
    NonControllerControls(Box<PermanentCondition>),
    // Auras have two different meanings of "you" depending on if it is in quotes or not.
    AuraControllerControls(Box<PermanentCondition>),

    Untapped,
    Tapped,

    UniqueName,
    HasNonManaActivatedAbility,
    // TODO: Move WasKicked to an appropriate location.
    WasKicked,
    MostCommonPermanentColorOrTied,

    HasCounter(CounterType),

    // Creature conditions.
    Attacking,
    AttackingAlone,
    Blocking,
    Power(IntegerCondition),
    DamageGreaterOrEqualToToughness,
    Flying,
    FirstStrike,
    DoubleStrike,

    // TODO: Remove this. It should only exist in Conditional.
    CoinFlip,
    // TODO: Fix/remove this.
    CheckPreviousSelection(CardCondition),

    And(Vec<PermanentCondition>),
    Or(Vec<PermanentCondition>),
    Not(Box<PermanentCondition>),
}

impl PermanentCondition {
    pub fn not(permanent_condition: PermanentCondition) -> PermanentCondition {
        PermanentCondition::Not(Box::new(permanent_condition))
    }

    pub fn white() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::white())
    }

    pub fn blue() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::blue())
    }

    pub fn black() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::black())
    }

    pub fn red() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::red())
    }

    pub fn green() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::green())
    }

    pub fn color(color_variable: ColorVariable) -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Color(color_variable))
    }

    pub fn multicolored() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Multicolored)
    }

    pub fn any_color(colors: ColorsVariable) -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::AnyColor(colors))
    }

    pub fn plains() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::plains())
    }

    pub fn island() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::island())
    }

    pub fn swamp() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::swamp())
    }

    pub fn mountain() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::mountain())
    }

    pub fn forest() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::forest())
    }

    pub fn basic_land() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::BasicLand)
    }

    pub fn non_basic_land() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::NonBasicLand)
    }

    pub fn land() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Land)
    }

    pub fn creature() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Creature)
    }

    pub fn artifact() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Artifact)
    }

    pub fn enchantment() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Enchantment)
    }

    pub fn planeswalker() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Planeswalker)
    }

    pub fn ephemeral() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Ephemeral)
    }

    pub fn legendary() -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::Legendary)
    }

    pub fn creature_type(creature_type: CreatureType) -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::creature_type(creature_type))
    }

    pub fn creature_type_variable(creature_type_variable: CreatureTypeVariable) -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::CreatureType(creature_type_variable))
    }

    pub fn compare_converted_mana_cost(integer_condition: IntegerCondition) -> PermanentCondition {
        PermanentCondition::CardCondition(CardCondition::CompareConvertedManaCost(integer_condition))
    }

    pub fn controller_controls(permanent_condition: PermanentCondition) -> PermanentCondition {
        PermanentCondition::ControllerControls(Box::new(permanent_condition))
    }

    pub fn aura_controller_controls(permanent_condition: PermanentCondition) -> PermanentCondition {
        PermanentCondition::AuraControllerControls(Box::new(permanent_condition))
    }

    pub fn compile(&self) -> Box<Fn(&Permanent) -> bool> {
        let cond = self.clone();

        use self::PermanentCondition::*;
        Box::new(move |perm| {
            match cond.clone() {
                Any => true,

                CardCondition(card_condition) =>
                    card_condition.compile_for_permanent_face()(perm.effective_face()),

                Itself => panic!("This condition no longer makes sense. Remove it."),
                NotItself => panic!("This condition no longer makes sense. Remove it."),

                ControlledBy(_) => unimplemented!(),
                ControlledByController => unimplemented!(),
                ControlledByOpponent => unimplemented!(),
                ControlledByTarget => unimplemented!(),

                ControllerControls(_) => unimplemented!(),
                NonControllerControls(_) => unimplemented!(),
                AuraControllerControls(_) => unimplemented!(),

                Untapped => unimplemented!(),
                Tapped => unimplemented!(),

                UniqueName => unimplemented!(),
                HasNonManaActivatedAbility => unimplemented!(),
                // TODO: Move WasKicked to an appropriate location.
                WasKicked => unimplemented!(),
                MostCommonPermanentColorOrTied => unimplemented!(),

                HasCounter(_) => unimplemented!(),

                // Creature conditions.
                Attacking => perm.is_attacking(),
                AttackingAlone => unimplemented!(),
                Blocking => perm.is_blocking(),
                Power(_) => unimplemented!(),
                DamageGreaterOrEqualToToughness => {
                    if let Some(toughness) = perm.effective_face().toughness(perm.controller_id) {
                        let damage = perm.damage()
                            .map(|d| d.to_isize())
                            .unwrap_or(0);
                        damage >= toughness
                    } else {
                        false
                    }
                },
                Flying => unimplemented!(),
                FirstStrike => perm.flags().first_strike,
                DoubleStrike => perm.flags().double_strike,

                // TODO: Remove this. It should only exist in Conditional.
                CoinFlip => unimplemented!(),
                // TODO: Fix/remove this.
                CheckPreviousSelection(_) => unimplemented!(),

                And(permanent_conditions) => permanent_conditions.iter()
                    .all(|cond| cond.compile()(perm)),
                Or(permanent_conditions) => permanent_conditions.iter()
                    .any(|cond| cond.compile()(perm)),
                Not(permanent_condition) =>
                    !permanent_condition.compile()(perm),
            }
        })
    }

}
