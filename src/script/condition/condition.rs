use mana::color::Color;
use script::condition::card_condition::CardCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Condition {
    Always,
    Permanent(PermanentCondition),

    CoinFlip,

    MostCommonPermanentColorOrTied(Color),

    Equal(Count, Count),
    LessThanOrEqual(Count, Count),

    // Check whether the selection or target of the previous effect in the current chain
    // matches the specified CardCondition.
    // Necessary for complex effect dependencies.
    CheckPreviousSelection(CardCondition),

    Not(Box<Condition>),
}

impl Condition {
    pub fn not(condition: Condition) -> Condition {
        Condition::Not(Box::new(condition))
    }
}