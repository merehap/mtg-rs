#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentGroupCondition {
    Any,
    MaxOneOfEachBasicLandType,
}