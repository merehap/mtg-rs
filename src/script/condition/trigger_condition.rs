use script::condition::card_condition::CardCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::condition::player_condition::PlayerCondition;
use turn::step::StepName;

// TODO: Break TriggerCondition up into ObjectTriggerCondition and StateTriggerCondition.
// ObjectTriggers can refer to the trigger object, but this is a bug for StateTriggers.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum TriggerCondition {
    BeginningOfStep {
        step_name: StepName,
        player_turns: PlayerTurns,
    },
    SelfAttacks,
    SelfBlocks,
    SelfIsBlocked,
    SelfIsBlockedBy(PermanentCondition),
    SelfDealsCombatDamageToPlayer,
    SelfBecomesTapped,
    SpellCast {
        // TODO: This shouldn't be able to target.
        player_condition: PlayerCondition,
        card_condition: CardCondition,
    },
    EntersBattlefield(PermanentCondition),
    Dies(PermanentCondition),
    KickerPaid,
    TargetsChosen,
    ManaAbilityActivated {
        source_condition: PermanentCondition,
    },

    Or(Vec<TriggerCondition>),
}

// TODO: Replace this with PlayerCondition
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerTurns {
    All,
    EffectController,
    OpponentsOfEffectController,
}
