use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum IntegerCondition {
    EqualTo(Count),
    LessThanOrEqualTo(Count),
    GreaterThanOrEqualTo(Count),
}