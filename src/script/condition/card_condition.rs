use card::card::Card;
use ephemeral::ephemeral_card::EphemeralCard;
use mana::color::Color;
use permanent::face::permanent_face::PermanentFace;
use permanent::permanent_type::creature_type::CreatureType;
use permanent::permanent_type::land;
use script::condition::integer_condition::IntegerCondition;
use script::variable::basic_land_type_variable::BasicLandTypeVariable;
use script::variable::card_name_variable::CardNameVariable;
use script::variable::color_variable::ColorVariable;
use script::variable::colors_variable::ColorsVariable;
use script::variable::creature_type_variable::CreatureTypeVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardCondition {
    Any,

    Color(ColorVariable),
    AnyColor(ColorsVariable),
    Multicolored,

    Name(CardNameVariable),
    CompareConvertedManaCost(IntegerCondition),

    Legendary,

    Land,
    Creature,
    Artifact,
    Enchantment,
    Planeswalker,

    Ephemeral,

    BasicLand,
    NonBasicLand,
    BasicLandType(BasicLandTypeVariable),

    CreatureType(CreatureTypeVariable),

    And(Vec<CardCondition>),
    Or(Vec<CardCondition>),
    Not(Box<CardCondition>),
}

impl CardCondition {
    pub fn not(card_condition: CardCondition) -> CardCondition {
        CardCondition::Not(Box::new(card_condition))
    }

    pub fn white() -> CardCondition {
        CardCondition::Color(ColorVariable::Fixed(Color::White))
    }

    pub fn blue() -> CardCondition {
        CardCondition::Color(ColorVariable::Fixed(Color::Blue))
    }

    pub fn black() -> CardCondition {
        CardCondition::Color(ColorVariable::Fixed(Color::Black))
    }

    pub fn red() -> CardCondition {
        CardCondition::Color(ColorVariable::Fixed(Color::Red))
    }

    pub fn green() -> CardCondition {
        CardCondition::Color(ColorVariable::Fixed(Color::Green))
    }

    pub fn plains() -> CardCondition {
        CardCondition::BasicLandType(BasicLandTypeVariable::Fixed(land::BasicLandType::Plains))
    }

    pub fn island() -> CardCondition {
        CardCondition::BasicLandType(BasicLandTypeVariable::Fixed(land::BasicLandType::Island))
    }

    pub fn swamp() -> CardCondition {
        CardCondition::BasicLandType(BasicLandTypeVariable::Fixed(land::BasicLandType::Swamp))
    }

    pub fn mountain() -> CardCondition {
        CardCondition::BasicLandType(BasicLandTypeVariable::Fixed(land::BasicLandType::Mountain))
    }

    pub fn forest() -> CardCondition {
        CardCondition::BasicLandType(BasicLandTypeVariable::Fixed(land::BasicLandType::Forest))
    }

    pub fn creature_type(creature_type: CreatureType) -> CardCondition {
        CardCondition::CreatureType(CreatureTypeVariable::Fixed(creature_type))
    }

    pub fn compile(&self) -> Box<Fn(&Card) -> bool> {
        let cond = self.clone();
        Box::new(move |card| {
            match card {
                Card::Permanent(permanent) =>
                    cond.compile_for_permanent_face()(*permanent.primary_face()),
                Card::Ephemeral(ephemeral) =>
                    cond.compile_for_ephemeral_card()(ephemeral),
            }
        })
    }

    pub fn compile_for_permanent_face(&self) -> Box<Fn(&PermanentFace) -> bool> {
        let cond = self.clone();

        use script::condition::card_condition::CardCondition::*;
        Box::new(move |face| {
            match cond {
                Any => true,

                Color(ref color_variable) => match color_variable {
                    ColorVariable::Fixed(color) => face.is_color(*color),
                    _ => unimplemented!(),
                },
                AnyColor(_) => unimplemented!(),
                Multicolored => unimplemented!(),

                Name(_) => unimplemented!(),
                CompareConvertedManaCost(_) => unimplemented!(),

                Legendary => unimplemented!(),

                Land => face.is_land(),
                Creature => face.is_creature(),
                Artifact => face.is_artifact(),
                Enchantment => face.is_enchantment(),
                Planeswalker => face.is_planeswalker(),

                Ephemeral => false,

                BasicLand => unimplemented!(),
                NonBasicLand => unimplemented!(),
                BasicLandType(_) => unimplemented!(),

                CreatureType(_) => unimplemented!(),

                And(ref conditions) => conditions.iter()
                    .all(|cond| cond.compile_for_permanent_face()(face)),
                Or(ref conditions) => conditions.iter()
                    .any(|cond| cond.compile_for_permanent_face()(face)),
                Not(ref condition) =>
                    !condition.compile_for_permanent_face()(face),
            }
        })
    }

    fn compile_for_ephemeral_card(&self) -> Box<Fn(&EphemeralCard) -> bool> {
        let cond = self.clone();

        use script::condition::card_condition::CardCondition::*;
        Box::new(move |ephemeral| {
            match cond {
                Any => true,

                Color(ref color_variable) => match color_variable {
                    ColorVariable::Fixed(color) => ephemeral.is_color(*color),
                    _ => unimplemented!(),
                },
                AnyColor(_) => unimplemented!(),
                Multicolored => unimplemented!(),

                Name(_) => unimplemented!(),
                CompareConvertedManaCost(_) => unimplemented!(),

                Legendary => false,

                Land => false,
                Creature => false,
                Artifact => false,
                Enchantment => false,
                Planeswalker => false,

                Ephemeral => true,

                BasicLand => false,
                NonBasicLand => false,
                BasicLandType(_) => false,

                CreatureType(_) => false,

                And(ref conditions) => conditions.iter()
                    .all(|cond| cond.compile_for_ephemeral_card()(ephemeral)),
                Or(ref conditions) => conditions.iter()
                    .any(|cond| cond.compile_for_ephemeral_card()(ephemeral)),
                Not(ref condition) =>
                    !condition.compile_for_ephemeral_card()(ephemeral),
            }
        })
    }
}
