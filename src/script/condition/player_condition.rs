use script::variable::player_variable::PlayerVariable;
use game::Game;
use player::player_state::PlayerState;
use player::player_id::PlayerID;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerCondition {
    Any,
    Matches(PlayerVariable),
    OpponentOf(PlayerVariable),
}

impl PlayerCondition {
    pub fn compile(&self) -> Box<Fn(&PlayerState, PlayerID, &Game) -> bool> {
        let cond = self.clone();

        use self::PlayerCondition::*;
        Box::new(move |player_state, controller_id, game| {
            println!("Priority player: {}, player recipient: {}", game.priority_player_id(), player_state.id);
            match cond.clone() {
                Any => true,
                Matches(variable) => match variable {
                    PlayerVariable::Active => game.active_player_id() == player_state.id,
                    // FIXME: NonActivePlayer isn't the same as DefendingPlayer in multiplayer games.
                    PlayerVariable::Defending => game.non_active_player_id() == player_state.id,
                    // TODO: It seems this is wrong if the current effect is triggered rather than cast/activated.
                    // Probably need a new field in Game for this.
                    PlayerVariable::EffectController => player_state.id == controller_id,
                    PlayerVariable::EffectRecipientController => unimplemented!(),
                    PlayerVariable::EffectRecipientOwner => unimplemented!(),
                    PlayerVariable::PreviousSelectionController => unimplemented!(),
                    PlayerVariable::TriggerObjectController => unimplemented!(),
                },
                OpponentOf(variable) => unimplemented!(),
            }
        })
    }
}