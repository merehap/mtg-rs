use cost::cost_builder::CostBuilder;
use cost::permanent_cost::PermanentCost;
use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentCostVariable {
    Fixed(PermanentCost),
    GenericManaCount(Count),
    // The mana cost of the current permanent.
    SelfManaCost,
}

impl PermanentCostVariable {
    pub fn fixed_life(count: usize) -> PermanentCostVariable {
        PermanentCostVariable::Fixed(CostBuilder::new().life(count).into_permanent_cost())
    }
}
