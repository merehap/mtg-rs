// TODO: Add start-up panic for ObjectVariables that can't possibly be populated
// in the context that they are given in.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum ObjectVariable {
    Itself,
    // TODO: Panic on start-up for any cards that specify these without a previous effect.
    // (Having a previous effect that doesn't always yield the right type of
    // object is probably fine. Teferi's Response actually currently depends on this behavior.).
    PreviousSelection,
    PreviousDiscard,
    CurrentTriggeredAbilityObject,
    RecipientEffectSource,
    Blocker,
}
