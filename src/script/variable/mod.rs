pub mod basic_land_type_variable;
pub mod card_name_variable;
pub mod color_variable;
pub mod colors_variable;
pub mod creature_type_variable;
pub mod object_variable;
pub mod permanent_cost_variable;
pub mod player_variable;
