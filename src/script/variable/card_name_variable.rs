use script::variable::object_variable::ObjectVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardNameVariable {
    CurrentlySelectedName,
    FromObject(ObjectVariable),
    ChooseFromHandOfSelectedPlayer,
    NonBasicLandInput,
}
