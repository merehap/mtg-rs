use effect::interpreter::interpreter::ResolutionVariables;
use game::Game;
use player::player_id::PlayerID;
use turn::phase::PhaseName;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerVariable {
    // When a spell or ability refers to "you".
    EffectController,
    EffectRecipientOwner,
    EffectRecipientController,
    TriggerObjectController,
    PreviousSelectionController,
    Active,
    // TODO: NonActive statuses will need to clarified when 3+ player games are supported.
    Defending,
}

impl PlayerVariable {
    pub fn interpret(
        &self,
        recipient_owner_id: PlayerID,
        recipient_controller_id: PlayerID,
        resolution_variables: &ResolutionVariables,
        game: &Game,
    ) -> PlayerID {

        use script::variable::player_variable::PlayerVariable::*;
        match self {
            EffectController => resolution_variables.stack_object_controller_id,
            EffectRecipientOwner => recipient_owner_id,
            EffectRecipientController => recipient_controller_id,
            TriggerObjectController => {
                resolution_variables.trigger_object_controller_id
                    .expect("Can't refer to a trigger object for a non-object triggered ability.")
            },
            PreviousSelectionController => {
                resolution_variables.unique_previous_selection_controller_id
                    .expect("TODO: Figure out how to filter out this effect instead of panicking.")
            }
            Active => game.active_player_id(),
            Defending => {
                if game.turns().current_phase_name() != PhaseName::Combat {
                    panic!("SelectedPlayer::Defending must only be specified for combat situations.");
                }

                game.non_active_player_id()
            },
        }
    }
}