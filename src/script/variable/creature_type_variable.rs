use permanent::permanent_type::creature_type::CreatureType;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CreatureTypeVariable {
    Fixed(CreatureType),
    Input,
}
