use mana::color::Color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ColorVariable {
    Fixed(Color),
    // The player selects a Color when the current ability resolves.
    AbilityInput,
    // Take a color from the single generic costed ability cost.
    // Colorless mana should result in the ability having no effect.
    AbilityManaCostColor,
    // The player selects a Color as part of the current permanent entering the battlefield.
    EnterTheBattlefieldInput,
    // The player selects a color upon the current spell resolving.
    ResolvedInput,
    // Used for when two inputs are needed upon spell resolution.
    ResolvedInput2,
}