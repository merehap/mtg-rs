#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ColorsVariable {
    PreviousSpellCast,
    SelfColors,
    TriggerObject,
    CurrentColorsProduced,
}
