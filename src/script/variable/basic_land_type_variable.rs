use permanent::permanent_type::land::BasicLandType;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum BasicLandTypeVariable {
    Fixed(BasicLandType),
    AuraEntersTheBattlefield,
    ResolvedInput,
    ResolvedInput2,
}

impl BasicLandTypeVariable {
    pub fn plains() -> BasicLandTypeVariable {
        BasicLandTypeVariable::Fixed(BasicLandType::Plains)
    }

    pub fn island() -> BasicLandTypeVariable {
        BasicLandTypeVariable::Fixed(BasicLandType::Island)
    }

    pub fn swamp() -> BasicLandTypeVariable {
        BasicLandTypeVariable::Fixed(BasicLandType::Swamp)
    }

    pub fn mountain() -> BasicLandTypeVariable {
        BasicLandTypeVariable::Fixed(BasicLandType::Mountain)
    }

    pub fn green() -> BasicLandTypeVariable {
        BasicLandTypeVariable::Fixed(BasicLandType::Forest)
    }
}