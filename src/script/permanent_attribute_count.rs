#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentAttributeCount {
    ConvertedManaCost,
    Power,
}
