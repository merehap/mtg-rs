#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![feature(half_open_range_patterns)]
#![feature(exclusive_range_pattern)]
// FIXME
#![allow(dead_code)]
// FIXME
#![allow(unused_variables)]
// FIXME
#![allow(bare_trait_objects)]
#![deny(clippy::all)]
#![allow(clippy::large_enum_variant)]
#![allow(clippy::module_inception)]
#![allow(clippy::option_option)]

extern crate rand;

mod ability;
mod card;
mod cost;
mod data;
mod deck;
mod effect;
mod ephemeral;
mod face;
mod game;
mod location;
mod mana;
mod permanent;
mod player;
mod script;
mod turn;
mod util;
mod zone;

use data::expansion::invasion;
use data::query::card_by_name;
use deck::deck::Deck;
use game::{Game, AdvanceResult};
use player::player_brain::AlwaysPassingPlayerBrain;

fn main() {
    let cards = &invasion::invasion();

    let deck0 = Deck::new(vec![
        (card_by_name(cards, "Plains"), 17),
        (card_by_name(cards, "Benalish Trapper"), 4),
        (card_by_name(cards, "Angel of Mercy"), 4),
        (card_by_name(cards, "Blinding Light"), 4),
        (card_by_name(cards, "Benalish Emissary"), 4),
        (card_by_name(cards, "Atalya, Samite Master"), 4),
        (card_by_name(cards, "Ardent Soldier"), 3),
    ]);
    let deck1 = Deck::new(vec![
        (card_by_name(cards, "Forest"), 17),
        (card_by_name(cards, "Quirion Elves"), 4),
        (card_by_name(cards, "Pincer Spider"), 4),
        (card_by_name(cards, "Quirion Sentinel"), 4),
        (card_by_name(cards, "Blurred Mongoose"), 4),
        (card_by_name(cards, "Harrow"), 4),
        (card_by_name(cards, "Saproling Symbiosis"), 3),
    ]);

    let mut game = Game::new(
        Box::new(AlwaysPassingPlayerBrain::new()),
        deck0,
        Box::new(AlwaysPassingPlayerBrain::new()),
        deck1);

    while let AdvanceResult::Continue(_) = game.advance() {

    }
}
