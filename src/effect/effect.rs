use effect::effect_receivers::EffectReceivers;
use effect::effect_template::EffectTemplate;
use effect::effect_type::card_effect_type::CardEffectType;
use effect::effect_type::damage_effect_type::DamageEffectType;
use effect::effect_type::permanent_effect_type::PermanentEffectType;
use effect::effect_type::player_effect_type::PlayerEffectType;
use effect::effect_type::stack_object_effect_type::StackObjectEffectType;
use effect::interpreter::interpreter::ResolutionVariables;
use effect::receiver::card_receiver::CardReceiver;
use effect::receiver::permanent_receiver::PermanentReceiver;
use effect::receiver::stack_object_receiver::StackObjectReceiver;
use effect::receiver::player_receiver::PlayerReceiver;
use effect::receiver::damage_receiver::DamageReceiver;
use game::Game;
use player::player_id::PlayerID;

#[derive(Clone, Debug)]
pub struct Effect {
    pub template: EffectTemplate,

    pub permanent_effect_receivers:
        EffectReceivers<PermanentEffectType, PermanentReceiver>,
    pub card_effect_receivers:
        EffectReceivers<CardEffectType, CardReceiver>,
    pub stack_object_effect_receivers:
        EffectReceivers<StackObjectEffectType, StackObjectReceiver>,
    pub player_effect_receivers:
        EffectReceivers<PlayerEffectType, PlayerReceiver>,
    pub damage_effect_receivers:
        EffectReceivers<DamageEffectType, DamageReceiver>,
}

impl Effect {
    pub fn new(template: EffectTemplate) -> Effect {
        Effect {
            template,
            permanent_effect_receivers: EffectReceivers::new(),
            card_effect_receivers: EffectReceivers::new(),
            stack_object_effect_receivers: EffectReceivers::new(),
            player_effect_receivers: EffectReceivers::new(),
            damage_effect_receivers: EffectReceivers::new(),
        }
    }

    pub fn template(&self) -> &EffectTemplate {
        &self.template
    }

    pub fn has_targets(&self) -> bool {
        match self {
            Effect {
                template: _,
                permanent_effect_receivers,
                card_effect_receivers,
                stack_object_effect_receivers,
                player_effect_receivers,
                damage_effect_receivers,
            } => {
                permanent_effect_receivers.has_targets() ||
                    card_effect_receivers.has_targets() ||
                    stack_object_effect_receivers.has_targets() ||
                    player_effect_receivers.has_targets() ||
                    damage_effect_receivers.has_targets()
            }
        }
    }

    pub fn prune_targets(&mut self, stack_object_controller_id: PlayerID, game: &Game) {
        match self {
            Effect {
                template: _,
                ref mut permanent_effect_receivers,
                ref mut card_effect_receivers,
                ref mut stack_object_effect_receivers,
                ref mut player_effect_receivers,
                ref mut damage_effect_receivers,
            } => {
                permanent_effect_receivers.prune_targets(stack_object_controller_id, game);
                card_effect_receivers.prune_targets(stack_object_controller_id, game);
                stack_object_effect_receivers.prune_targets(stack_object_controller_id, game);
                player_effect_receivers.prune_targets(stack_object_controller_id, game);
                damage_effect_receivers.prune_targets(stack_object_controller_id, game);
            }
        }
    }

    pub fn interpret(
        &self,
        resolution_variables: &ResolutionVariables,
        game: &mut Game,
    ) -> Option<PlayerID> {

        for (effect_type, receivers) in self.permanent_effect_receivers.receivers_by_effect_type() {
            // TODO: Populate trigger object controller ID in this call.
            let compiled_effect = effect_type.compile(resolution_variables.clone());
            for receiver in receivers {
                let id = receiver.id()
                    .expect("Unresolved permanent receiver ID!");
                compiled_effect(id, game);
            }
        }

        for (effect_type, receivers) in self.card_effect_receivers.receivers_by_effect_type() {
            let compiled_effect = effect_type.compile(resolution_variables.clone());
            for receiver in receivers {
                let id = receiver.id()
                    .expect("Unresolved card receiver ID!");
                compiled_effect(id, game);
            }
        }

        for (effect_type, receivers) in self.player_effect_receivers.receivers_by_effect_type() {
            let compiled_effect = effect_type.compile(resolution_variables.clone());
            for receiver in receivers {
                let id = receiver.id()
                    .expect("Unresolved player receiver ID!");
                compiled_effect(id, game);
            }
        }

        /*
        match effect.template() {
            EffectTemplate::Global(global_effect) => {
                match global_effect {
                    GlobalEffectType::Permanent {condition, effect_types} => {
                        let permanent_ids =
                            game.battlefield().matching_permanents(condition.clone());
                        // TODO: Populate trigger object controller ID in this call.
                        let compiled_effects: Vec<_> = effect_types.iter()
                            .map(|effect_type| compile_permanent_effect_type(
                                effect_type,
                                resolution_variables.clone(),
                            ))
                            .collect();
                        for id in permanent_ids {
                            for compiled_effect in &compiled_effects {
                                compiled_effect(id, game);
                            }
                        }
                    },
                    _ => unimplemented!(),
                }
            },
        }
        */

        // TODO: Actually set this.
        None
    }
}
