use effect::receiver::receiver::{Receiver, ReceiverState, Validity};
use game::Game;
use player::player_id::PlayerID;

#[derive(Clone, Debug)]
pub struct EffectReceivers<ET, R: Receiver> {
    targets: Vec<(ET, Vec<R>)>,
    untargeted_receivers: Vec<(ET, Vec<R>)>,
}

impl <ET, R: Receiver> EffectReceivers<ET, R> {
    pub fn new() -> EffectReceivers<ET, R> {
        EffectReceivers {
            targets: Vec::new(),
            untargeted_receivers: Vec::new(),
        }
    }

    pub fn receivers_by_effect_type(&self) -> Vec<(&ET, &Vec<R>)> {
        self.targets.iter()
            .chain(self.untargeted_receivers.iter())
            .map(|(effect_type, targets)| (effect_type, targets))
            .collect()
    }

    pub fn targets(&self) -> Vec<&R> {
        self.targets.iter()
            .flat_map(|(_, targets)| targets)
            .collect()
    }

    pub fn has_targets(&self) -> bool {
        !self.targets().is_empty()
    }

    pub fn push_target(&mut self, effect_type: ET, receivers: Vec<R>) {
        self.targets.push((effect_type, receivers));
    }

    pub fn push_untargeted_receiver(&mut self, effect_type: ET, receivers: Vec<R>) {
        self.untargeted_receivers.push((effect_type, receivers));
    }

    pub fn prune_targets(&mut self, controller_id: PlayerID, game: &Game) {
        EffectReceivers::prune_specific_invalids(
            &mut self.targets, controller_id, game);
    }

    pub fn prune_untargeted_receivers(&mut self, controller_id: PlayerID, game: &Game) {
        EffectReceivers::prune_specific_invalids(
            &mut self.untargeted_receivers, controller_id, game);
    }

    fn prune_specific_invalids(receivers: &mut Vec<(ET, Vec<R>)>, controller_id: PlayerID, game: &Game) {
        for (_, receivers) in receivers {
            receivers.retain(|receiver| {
                println!("State: {:?}", receiver.state(controller_id, game));
                match receiver.state(controller_id, game) {
                    ReceiverState::Specified(Validity::Valid) => true,
                    ReceiverState::Specified(_) => false,
                    _ => panic!("Unresolved receiver when pruning!"),
                }
            })
        }
    }
}
