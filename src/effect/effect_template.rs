use std::collections::BTreeSet;

use cost::cost::Cost;
use effect::effect_type::card_effect_type::CardEffectType;
use effect::effect_type::damage_effect_type::*;
use effect::effect_type::expiration::Expiration;
use effect::effect_type::global_effect_type::GlobalEffectType;
use effect::effect_type::permanent_effect_type::*;
use effect::effect_type::player_effect_type::PlayerEffectType;
use effect::effect_type::selecting::*;
use effect::effect_type::stack_object_effect_type::{StackObjectEffectType, StackObjectType};
use location::card_zone_type::CardZoneType;
use mana::mana_produced::ManaProduced;
use script::condition::card_condition::CardCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::condition::player_condition::PlayerCondition;
use script::variable::player_variable::PlayerVariable;
use turn::step::StepName;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EffectTemplate {
    Permanent {
        effect_types: Vec<PermanentEffectType>,
        selecting: Selecting,
        selection_condition: PermanentCondition,
    },
    Card {
        effect_types: Vec<CardEffectType>,
        selecting: Selecting,
        selection_condition: CardCondition,
        selection_owner: PlayerVariable,
        card_zone: CardZoneType,
    },
    Player {
        selecting: SelectingPlayers,
        effect_types: Vec<PlayerEffectType>,
    },
    Damage {
        effect_types: Vec<DamageEffectType>,
        damage_recipients: DamageRecipients,
    },
    StackObject {
        effect_types: Vec<StackObjectEffectType>,
        selecting: Selecting,
        stack_object_types: BTreeSet<StackObjectType>,
        selection_owner_condition: PlayerCondition,
    },
    Mana {
        mana_produced: ManaProduced,
    },
    Global(GlobalEffectType),

    PayCostToPrevent {
        cost: Cost,
        effect_template: Box<EffectTemplate>,
    },

    Delay {
        until: StepName,
        effect_template: Box<EffectTemplate>,
    },

    // TODO: Use non-empty Vecs for these three.
    ChooseOne(Vec<EffectTemplate>),
    ChooseTwo(Vec<EffectTemplate>),
    ChooseThreeWithRepeats(Vec<EffectTemplate>),

    Sequence(Vec<EffectTemplate>),
}

impl EffectTemplate {
    pub fn self_effect(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::Permanent {
            effect_types: vec![effect_type],
            selecting: Selecting::Itself,
            selection_condition: PermanentCondition::Any,
        }
    }

    pub fn self_creature_effect(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::Permanent {
            effect_types: vec![effect_type],
            selecting: Selecting::Itself,
            selection_condition: PermanentCondition::creature(),
        }
    }

    pub fn target_permanent(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(effect_type, PermanentCondition::Any)
    }

    pub fn target_creature(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(effect_type, PermanentCondition::creature())
    }

    pub fn target_land(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(effect_type, PermanentCondition::creature())
    }

    pub fn target_artifact(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(effect_type, PermanentCondition::creature())
    }

    pub fn target_enchantment(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(effect_type, PermanentCondition::creature())
    }

    pub fn target_artifact_or_enchantment(effect_type: PermanentEffectType) -> EffectTemplate {
        EffectTemplate::target_typed_permanent(
            effect_type,
            PermanentCondition::And(vec![
                PermanentCondition::artifact(),
                PermanentCondition::enchantment(),
            ])
        )
    }

    fn target_typed_permanent(effect_type: PermanentEffectType, condition: PermanentCondition) -> EffectTemplate {
        EffectTemplate::Permanent {
            effect_types: vec![effect_type],
            selecting: Selecting::one_target(),
            selection_condition: condition,
        }
    }

    pub fn target_spell(effect_type: StackObjectEffectType) -> EffectTemplate {
        let mut stack_object_types = BTreeSet::new();
        stack_object_types.insert(StackObjectType::any_spell());

        EffectTemplate::StackObject {
            effect_types: vec![effect_type],
            selecting: Selecting::one_target(),
            stack_object_types,
            selection_owner_condition: PlayerCondition::Any,
        }
    }

    pub fn target_activated_ability(effect_type: StackObjectEffectType) -> EffectTemplate {
        let mut stack_object_types = BTreeSet::new();
        stack_object_types.insert(StackObjectType::any_activated_ability());

        EffectTemplate::StackObject {
            effect_types: vec![effect_type],
            selecting: Selecting::one_target(),
            stack_object_types,
            selection_owner_condition: PlayerCondition::Any,
        }
    }

    pub fn target_player(effect_type: PlayerEffectType) -> EffectTemplate {
        EffectTemplate::Player {
            selecting: SelectingPlayers::one_target(),
            effect_types: vec![effect_type],
        }
    }

    pub fn controller_effect(player_effect_type: PlayerEffectType) -> EffectTemplate {
        EffectTemplate::Player {
            selecting: SelectingPlayers::effect_controller(),
            effect_types: vec![player_effect_type],
        }
    }

    // TODO: Remove this helper function and inline its contents.
    pub fn prevent_all_combat_damage(expiration: Expiration) -> EffectTemplate {
        EffectTemplate::Global(GlobalEffectType::Permanent {
            effect_types: vec![
                PermanentEffectType::Continuous {
                    expiration,
                    continuous_type: ContinuousPermanentEffectType::CannotDealCombatDamage,
                }
            ],
            condition: PermanentCondition::creature(),
        })
    }
}
