use effect::effect::Effect;
use game::Game;
use player::player_id::PlayerID;

pub fn interpret_ephemeral_effects(
    mut effects: Vec<Effect>,
    stack_object_controller_id: PlayerID,
    trigger_object_controller_id: Option<PlayerID>,
    game: &mut Game,
) -> EphemeralEffectsResult {

    println!("Interpreting Ephemeral effects.");
    let had_targets = effects.iter().any(|effect| effect.has_targets());
    prune_invalid_targets(&mut effects, stack_object_controller_id, game);
    let has_targets = effects.iter().any(|effect| effect.has_targets());
    if had_targets && !has_targets {
        println!("Fizzled!");
        return EphemeralEffectsResult::Fizzle;
    }

    let mut resolution_variables = ResolutionVariables {
        stack_object_controller_id,
        trigger_object_controller_id,
        unique_previous_selection_controller_id: None,
    };
    // TODO: Does it matter if we loop over effect or permanents for the outer loop?
    for effect in effects {
        println!("Executing effect!");
        let id = effect.interpret(&resolution_variables, game);
        resolution_variables.unique_previous_selection_controller_id = id;
    }

    EphemeralEffectsResult::Resolve
}

#[derive(Clone)]
pub struct ResolutionVariables {
    pub stack_object_controller_id: PlayerID,
    pub trigger_object_controller_id: Option<PlayerID>,
    pub unique_previous_selection_controller_id: Option<PlayerID>,
}

pub enum EphemeralEffectsResult {
    Resolve,
    Fizzle,
}

fn prune_invalid_targets(effects: &mut Vec<Effect>, stack_object_controller_id: PlayerID, game: &Game) {
    for effect in effects {
        match effect {
            Effect {
                template: _,
                ref mut permanent_effect_receivers,
                ref mut card_effect_receivers,
                ref mut stack_object_effect_receivers,
                ref mut player_effect_receivers,
                ref mut damage_effect_receivers,
            } => {
                permanent_effect_receivers.prune_targets(stack_object_controller_id, &game);
                card_effect_receivers.prune_targets(stack_object_controller_id, &game);
                stack_object_effect_receivers.prune_targets(stack_object_controller_id, &game);
                player_effect_receivers.prune_targets(stack_object_controller_id, &game);
                damage_effect_receivers.prune_targets(stack_object_controller_id, &game);
            }
        }
    }
}
