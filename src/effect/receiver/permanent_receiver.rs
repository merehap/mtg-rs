use std::collections::BTreeMap;

use effect::effect_type::permanent_effect_type::CounterType;
use effect::receiver::receiver::{Receiver, ReceiverID};
use game::Game;
use permanent::permanent::Permanent;
use permanent::permanent_id::PermanentID;
use script::condition::permanent_condition::PermanentCondition;
use script::variable::object_variable::ObjectVariable;
use util::positive_integer::PositiveInteger;
use player::player_id::PlayerID;

#[derive(Clone, Debug)]
pub struct PermanentReceiver {
    id: ReceiverID<PermanentID>,
    condition: PermanentCondition,
    counters: BTreeMap<CounterType, PositiveInteger>,
}

impl PermanentReceiver {
    pub fn new(id: PermanentID, condition: PermanentCondition) -> PermanentReceiver {
        PermanentReceiver {
            id: ReceiverID::ID(id),
            condition,
            counters: BTreeMap::new(),
        }
    }

    pub fn multiple(ids: &[PermanentID], condition: PermanentCondition) -> Vec<PermanentReceiver> {
        ids.iter()
            .cloned()
            .map(|id| PermanentReceiver::new(id, condition.clone()))
            .collect()
    }

    pub fn id(&self) -> Option<PermanentID> {
        self.unvalidated_id()
    }
}

impl Receiver for PermanentReceiver {
    type ID = PermanentID;
    type Object = Permanent;

    fn receiver_id(&self) -> ReceiverID<PermanentID> {
        self.id
    }

    fn resolve_variable(
        &mut self,
        variable: ObjectVariable,
        game: &Game,
    ) -> Option<PermanentID> {

        use script::variable::object_variable::ObjectVariable::*;
        match variable {
            Itself => {},
            PreviousSelection => {},
            PreviousDiscard => {},
            CurrentTriggeredAbilityObject => {},
            RecipientEffectSource => {},
            Blocker => {},
        }

        None
    }

    fn unchecked_provide_id(&mut self, id: PermanentID) {
        self.id = ReceiverID::ID(id);
    }

    fn get_object(id: PermanentID, game: &Game) -> Option<&Permanent> {
        game.battlefield().get(id)
    }

    fn check_condition(&self, perm: &Permanent, controller_id: PlayerID, game: &Game) -> bool {
        self.condition.compile()(perm)
    }
}

