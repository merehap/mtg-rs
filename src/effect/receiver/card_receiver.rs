use std::collections::BTreeSet;

use card::card::Card;
use effect::receiver::receiver::{Receiver, ReceiverID};
use game::Game;
use script::condition::card_condition::CardCondition;
use script::variable::object_variable::ObjectVariable;
use zone::zoned_card_id::ZonedCardID;
use player::player_id::PlayerID;

// Magic's presumably never going to allow targeting cards within a library,
// so it's unfortunate that that is allowed for EffectReceivers.
// But it seems better to keep consistency with every other Receiver type than
// to eliminate this case.
#[derive(Clone, Debug)]
pub struct CardReceiver {
    id: ReceiverID<ZonedCardID>,
    condition: CardCondition,
}

impl CardReceiver {
    pub fn new(zoned_card_id: ZonedCardID, condition: CardCondition) -> CardReceiver {
        CardReceiver {id: ReceiverID::ID(zoned_card_id), condition}
    }

    pub fn multiple(
        ids: &BTreeSet<ZonedCardID>,
        condition: CardCondition,
    ) -> Vec<CardReceiver> {

        ids.iter()
            .cloned()
            .map(|id| CardReceiver::new(id, condition.clone()))
            .collect()
    }

    pub fn id(&self) -> Option<ZonedCardID> {
        self.unvalidated_id()
    }

    fn unchecked_provide_id(&mut self, id: ZonedCardID) {
        self.id = ReceiverID::ID(id);
    }
}

impl Receiver for CardReceiver {
    type ID = ZonedCardID;
    type Object = Card;

    fn receiver_id(&self) -> ReceiverID<ZonedCardID> {
        self.id
    }

    fn resolve_variable(
        &mut self,
        variable: ObjectVariable,
        game: &Game,
    ) -> Option<ZonedCardID> {

        use script::variable::object_variable::ObjectVariable::*;
        match variable {
            Itself => {},
            PreviousSelection => {},
            PreviousDiscard => {},
            CurrentTriggeredAbilityObject => {},
            RecipientEffectSource => {},
            Blocker => {},
        }

        None
    }

    fn unchecked_provide_id(&mut self, id: ZonedCardID) {
        self.id = ReceiverID::ID(id);
    }

    fn get_object(id: ZonedCardID, game: &Game) -> Option<&Card> {
        game.lookup_card(id)
    }

    fn check_condition(&self, card: &Card, controller_id: PlayerID, game: &Game) -> bool {
        self.condition.compile()(card)
    }
}

