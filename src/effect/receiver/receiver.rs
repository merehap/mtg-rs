use game::Game;
use script::variable::object_variable::ObjectVariable;
use player::player_id::PlayerID;

pub trait Receiver {
    type ID: Copy + std::fmt::Debug;
    type Object;

    fn receiver_id(&self) -> ReceiverID<Self::ID>;
    fn resolve_variable(&mut self, variable: ObjectVariable, game: &Game) -> Option<Self::ID>;
    fn unchecked_provide_id(&mut self, id: Self::ID);

    fn get_object(id: Self::ID, game: &Game) -> Option<&Self::Object>;
    fn check_condition(&self, object: &Self::Object, controller_id: PlayerID, game: &Game) -> bool;

    fn unvalidated_id(&self) -> Option<Self::ID> {
        if let ReceiverID::ID(id) = self.receiver_id() {
            Some(id)
        } else {
            None
        }
    }

    fn valid_candidate(&self, id: Self::ID, controller_id: PlayerID, game: &Game) -> Validity {
        if let Some(object) = Self::get_object(id, game) {
            if self.check_condition(object, controller_id, game) {
                Validity::Valid
            } else {
                Validity::ConditionFailed
            }
        } else {
            Validity::NonExistent
        }
    }

    fn state(&self, controller_id: PlayerID, game: &Game) -> ReceiverState {
        println!("ReceiverID? {:?}", self.receiver_id());
        match self.receiver_id() {
            ReceiverID::ID(id) => ReceiverState::Specified(self.valid_candidate(id, controller_id, game)),
            ReceiverID::Variable(_) => ReceiverState::VariableUnresolved,
            ReceiverID::Unspecified => ReceiverState::IDUnspecified,
        }
    }

    fn provide_id(&mut self, id: Self::ID, stack_object_controller_id: PlayerID, game: &mut Game) -> Validity {
        let validity = self.valid_candidate(id.clone(), stack_object_controller_id, game);
        if validity == Validity::Valid {
            self.unchecked_provide_id(id);
        }

        validity
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum Validity {
    Valid,
    ConditionFailed,
    NonExistent,
}

#[derive(Debug)]
pub enum ReceiverState {
    Specified(Validity),
    VariableUnresolved,
    IDUnspecified,
}

#[derive(Clone, Copy, Debug)]
pub enum ReceiverID<ID> {
    ID(ID),
    Variable(ObjectVariable),
    Unspecified,
}

