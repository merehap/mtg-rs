use effect::receiver::receiver::{Receiver, ReceiverID};
use game::Game;
use permanent::permanent_id::PermanentID;
use player::player_id::PlayerID;
use script::variable::object_variable::ObjectVariable;

#[derive(Clone, Debug)]
pub struct DamageReceiver {
    id: ReceiverID<DamageReceiverID>,
}

impl DamageReceiver {
    pub fn id(&self) -> Option<DamageReceiverID> {
        self.unvalidated_id()
    }
}

impl Receiver for DamageReceiver {
    type ID = DamageReceiverID;
    // TODO: Make a real wrapper type.
    type Object = u32;

    fn receiver_id(&self) -> ReceiverID<DamageReceiverID> {
        self.id
    }

    fn resolve_variable(
        &mut self,
        variable: ObjectVariable,
        game: &Game,
    ) -> Option<DamageReceiverID> {

        use script::variable::object_variable::ObjectVariable::*;
        match variable {
            Itself => {},
            PreviousSelection => {},
            PreviousDiscard => {},
            CurrentTriggeredAbilityObject => {},
            RecipientEffectSource => {},
            Blocker => {},
        }

        None
    }

    fn unchecked_provide_id(&mut self, id: DamageReceiverID) {
        self.id = ReceiverID::ID(id);
    }

    fn get_object(_: DamageReceiverID, _: &Game) -> Option<&Self::Object> {
        None
    }

    fn check_condition(&self, _: &Self::Object, _controller_id: PlayerID, _: &Game) -> bool {
        false
    }
}

#[derive(Clone, Copy, Debug)]
pub enum DamageReceiverID {
    Permanent(PermanentID),
    Player(PlayerID),
}
