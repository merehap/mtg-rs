use effect::receiver::receiver::{Receiver, ReceiverID};
use game::Game;
use player::player_id::PlayerID;
use player::player_state::PlayerState;
use script::variable::object_variable::ObjectVariable;
use script::condition::player_condition::PlayerCondition;

#[derive(Clone, Debug)]
pub struct PlayerReceiver {
    id: ReceiverID<PlayerID>,
    condition: PlayerCondition,
}

impl PlayerReceiver {
    pub fn id(&self) -> Option<PlayerID> {
        self.unvalidated_id()
    }

    pub fn multiple(ids: &[PlayerID], condition: PlayerCondition) -> Vec<PlayerReceiver> {
        ids.iter()
            .cloned()
            .map(|id| PlayerReceiver {id: ReceiverID::ID(id), condition: condition.clone()})
            .collect()
    }
}

impl Receiver for PlayerReceiver {
    type ID = PlayerID;
    type Object = PlayerState;

    fn receiver_id(&self) -> ReceiverID<PlayerID> {
        self.id
    }

    fn resolve_variable(
        &mut self,
        variable: ObjectVariable,
        game: &Game,
    ) -> Option<PlayerID> {

        use script::variable::object_variable::ObjectVariable::*;
        match variable {
            Itself => {},
            PreviousSelection => {},
            PreviousDiscard => {},
            CurrentTriggeredAbilityObject => {},
            RecipientEffectSource => {},
            Blocker => {},
        }

        None
    }

    fn unchecked_provide_id(&mut self, id: PlayerID) {
        self.id = ReceiverID::ID(id);
    }

    fn get_object(id: PlayerID, game: &Game) -> Option<&PlayerState> {
        Some(game.lookup_player(id))
    }

    fn check_condition(&self, player_state: &PlayerState, stack_object_controller_id: PlayerID, game: &Game) -> bool {
        self.condition.compile()(player_state, stack_object_controller_id, game)
    }
}

