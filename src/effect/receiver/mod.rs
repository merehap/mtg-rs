pub mod receiver;
pub mod card_receiver;
pub mod permanent_receiver;
pub mod stack_object_receiver;
pub mod player_receiver;
pub mod damage_receiver;