use effect::receiver::receiver::{Receiver, ReceiverID};
use game::Game;
use script::variable::object_variable::ObjectVariable;
use zone::stack::{StackObject, StackObjectID};
use player::player_id::PlayerID;

#[derive(Clone, Debug)]
pub struct StackObjectReceiver {
    id: ReceiverID<StackObjectID>,
}

impl StackObjectReceiver {
    pub fn id(&self) -> Option<StackObjectID> {
        self.unvalidated_id()
    }
}

impl Receiver for StackObjectReceiver {
    type ID = StackObjectID;
    type Object = StackObject;

    fn receiver_id(&self) -> ReceiverID<StackObjectID> {
        self.id
    }

    fn resolve_variable(
        &mut self,
        variable: ObjectVariable,
        game: &Game,
    ) -> Option<StackObjectID> {

        use script::variable::object_variable::ObjectVariable::*;
        match variable {
            Itself => {},
            PreviousSelection => {},
            PreviousDiscard => {},
            CurrentTriggeredAbilityObject => {},
            RecipientEffectSource => {},
            Blocker => {},
        }

        None
    }

    fn unchecked_provide_id(&mut self, id: StackObjectID) {
        self.id = ReceiverID::ID(id);
    }

    fn get_object(id: StackObjectID, game: &Game) -> Option<&StackObject> {
        None
    }

    fn check_condition(&self, perm: &StackObject, controller_id: PlayerID, game: &Game) -> bool {
        false
    }
}
