use effect::effect_type::card_effect_type::CardEffectType;
use effect::effect_type::damage_effect_type::*;
use effect::effect_type::permanent_effect_type::*;
use effect::effect_type::player_effect_type::*;
use effect::effect_type::expiration::Expiration;
use location::card_zone_type::CardZoneType;
use mana::mana_cost::ManaCost;
use mana::mana_produced::ManaProduced;
use script::condition::card_condition::CardCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::condition::player_condition::PlayerCondition;
use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum GlobalEffectType {
    Continuous {
        expiration: Expiration,
        effect_type: ContinuousGlobalEffectType,
    },

    Card{condition: CardCondition, card_zone: CardZoneType, effect_type: CardEffectType},
    Permanent{condition: PermanentCondition, effect_types: Vec<PermanentEffectType>},
    Player{condition: PlayerCondition, effect_type: PlayerEffectType},
    Damage {
        target_types: DamageRecipientTypes,
        effect_type: DamageEffectType,
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ContinuousGlobalEffectType {
    Permanent {
        condition: PermanentCondition,
        effect_types: Vec<ContinuousPermanentEffectType>,
    },
    PermanentCannot {
        condition: PermanentCondition,
        effect_types: Vec<PermanentCannotEffectType>,
    },
    Player {
        condition: PlayerCondition,
        effect_types: Vec<ContinuousPlayerEffectType>,
    },
    Damage {
        allowed_target_types: DamageRecipientTypes,
        effect_types: Vec<ContinuousDamageEffectType>,
    },
    SpellCostModifier {
        condition: CardCondition,
        modifier: CostModifier,
    },
    // Spells matching the condition cannot be cast.
    PreventCasting {
        condition: CardCondition,
    },
    DamageCap {
        cap: usize,
        allowed_target_types: DamageRecipientTypes,
    },
    // TODO: Represent this as a PermanentEffectType instead.
    ManaReplacementUponTap {
        source: PermanentCondition,
        replacement_per_unit: ManaProduced,
    },
    MaxAttackersPerTurn(Count),
    MaxBlockersPerTurn(Count),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CostModifier {
    Add(ManaCost),
    Subtract(ManaCost),
}
