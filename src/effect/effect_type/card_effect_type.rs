use effect::interpreter::interpreter::ResolutionVariables;
use game::Game;
use location::card_zone_type::CardZoneType;
use script::variable::player_variable::PlayerVariable;
use zone::zoned_card_id::ZonedCardID;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardEffectType {
    MoveTo(CardZoneType),
    MoveToBattlefield {
        new_controller: PlayerVariable,
    },
}

impl CardEffectType {
    pub fn compile(
        &self,
        resolution_variables: ResolutionVariables,
    ) -> Box<dyn Fn(ZonedCardID, &mut Game)> {

        let effect_type: CardEffectType = self.clone();

        use effect::effect_type::card_effect_type::CardEffectType::*;
        Box::new(move |id: ZonedCardID, game: &mut Game| {
            match effect_type {
                MoveToBattlefield {ref new_controller} => {
                    let card = game.remove_card(id)
                        .expect("Card should have been present.");
                    let controller_id = new_controller.interpret(
                        id.owner_id(),
                        id.owner_id(),
                        &resolution_variables,
                        game,
                    );
                    game.add_card_to_battlefield_if_permanent(
                        id.owner_id(), controller_id, card);
                },
                MoveTo(card_zone_type) => {
                    let card = game.remove_card(id)
                        .expect("Card should have been present.");
                },
            }
        })
    }
}