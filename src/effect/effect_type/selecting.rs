use script::condition::player_condition::PlayerCondition;
use script::count::Count;
use script::variable::object_variable::ObjectVariable;
use script::variable::player_variable::PlayerVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Selecting {
    Itself,
    ItselfUnconditionallyAndTargets(Count),
    Specific(ObjectVariable),
    Targets(Count),
    // TODO: Generalize.
    TargetsWithTheSameController(Count),
    NonTargets(Count),
    AnyNumberOfTargets,
}

impl Selecting {
    pub fn one_target() -> Selecting {
        Selecting::Targets(Count::Fixed(1))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum SelectingPlayers {
    Single(PlayerVariable),
    Targets {
        count: Count,
        condition: PlayerCondition,
    },
    NonTargets {
        count: Count,
        condition: PlayerCondition,
    },
    AllMatching(PlayerCondition),
}

impl SelectingPlayers {
    pub fn one_target() -> SelectingPlayers {
        SelectingPlayers::Targets {
            count: Count::Fixed(1),
            condition: PlayerCondition::Any,
        }
    }

    pub fn one_target_opponent_of_effect_controller() -> SelectingPlayers {
        SelectingPlayers::Targets {
            count: Count::Fixed(1),
            condition: PlayerCondition::OpponentOf(PlayerVariable::EffectController),
        }
    }

    pub fn one_non_target() -> SelectingPlayers {
        SelectingPlayers::NonTargets {
            count: Count::Fixed(1),
            condition: PlayerCondition::Any,
        }
    }

    pub fn all() -> SelectingPlayers {
        SelectingPlayers::AllMatching(PlayerCondition::Any)
    }

    pub fn all_opponents_of_effect_controller() -> SelectingPlayers {
        SelectingPlayers::AllMatching(
            PlayerCondition::OpponentOf(PlayerVariable::EffectController))
    }

    pub fn effect_controller() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::EffectController)
    }

    pub fn effect_recipient_owner() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::EffectRecipientOwner)
    }

    pub fn effect_recipient_controller() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::EffectRecipientController)
    }

    pub fn trigger_object_controller() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::TriggerObjectController)
    }

    pub fn previous_selection_controller() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::PreviousSelectionController)
    }

    pub fn active() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::Active)
    }

    pub fn defending() -> SelectingPlayers {
        SelectingPlayers::Single(PlayerVariable::Defending)
    }
}
