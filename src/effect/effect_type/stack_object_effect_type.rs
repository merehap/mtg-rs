use std::collections::BTreeSet;

use cost::cost::Cost;
use effect::effect_type::expiration::Expiration;
use script::condition::card_condition::CardCondition;
use script::variable::basic_land_type_variable::BasicLandTypeVariable;
use script::variable::color_variable::ColorVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum StackObjectEffectType {
    Continuous {
        expiration: Expiration,
        effect_type: ContinuousStackObjectEffectType,
    },
    Counter,
    Exile,

    ChangeTargets,

    ControllerOfTargetMayCancel {
        cancel_cost: Cost,
        effect_type: Box<StackObjectEffectType>,
    },

    ControllerAndTargetControllerBidLife {
        starting_bid: usize,
        winner_effect_type: Box<StackObjectEffectType>,
    },
    ScryBattle {
        winner_effect_type: Box<StackObjectEffectType>,
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum StackObjectType {
    Spell {
        target_condition: CardCondition,
        target_of_target_condition: CardCondition,
    },
    ActivatedAbility {
        target_of_target_condition: CardCondition,
    },
}

impl StackObjectType {
    pub fn any() -> BTreeSet<StackObjectType> {
        let mut result = BTreeSet::new();
        result.insert(StackObjectType::any_spell());
        result.insert(StackObjectType::any_activated_ability());
        result
    }

    pub fn any_type_with_target_of_target_condition(
        target_of_target_condition: CardCondition) -> BTreeSet<StackObjectType> {

        let mut result = BTreeSet::new();
        result.insert(StackObjectType::Spell {
            target_condition: CardCondition::Any,
            target_of_target_condition: target_of_target_condition.clone(),
        });
        result.insert(StackObjectType::ActivatedAbility {
            target_of_target_condition
        });
        result
    }

    pub fn any_spell() -> StackObjectType {
        StackObjectType::Spell {
            target_condition: CardCondition::Any,
            target_of_target_condition: CardCondition::Any,
        }
    }

    pub fn spell_matching_condition(target_condition: CardCondition) -> StackObjectType {
        StackObjectType::Spell {
            target_condition,
            target_of_target_condition: CardCondition::Any,
        }
    }

    pub fn any_activated_ability() -> StackObjectType {
        StackObjectType::ActivatedAbility {
            target_of_target_condition: CardCondition::Any,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ContinuousStackObjectEffectType {
    OverrideColors {
        new_colors: Vec<ColorVariable>,
    },
    ReplaceColorWords {
        old_color: ColorVariable,
        new_color: ColorVariable,
    },
    ReplaceBasicLandTypeWords {
        old_basic_land_type: BasicLandTypeVariable,
        new_basic_land_type: BasicLandTypeVariable,
    },
}
