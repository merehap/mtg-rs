use effect::effect_type::expiration::*;
use effect::effect_type::selecting::*;
use effect::interpreter::interpreter::ResolutionVariables;
use game::Game;
use location::location::CardLocation;
use location::card_zone_type::CardZoneType;
use mana::color::Color;
use permanent::permanent::Permanent;
use permanent::permanent_id::PermanentID;
use permanent::permanent_type::land::BasicLandType;
use script::condition::card_condition::CardCondition;
use script::condition::integer_condition::IntegerCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::count::Count;
use script::permanent_attribute_count::PermanentAttributeCount;
use script::variable::basic_land_type_variable::BasicLandTypeVariable;
use script::variable::color_variable::ColorVariable;
use script::variable::player_variable::PlayerVariable;
use turn::step::StepName;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentEffectType {
    Continuous {
        expiration: Expiration,
        continuous_type: ContinuousPermanentEffectType,
    },
    // Cannot effects are continuous effects too, but they have an extra field.
    Cannot {
        expiration: Expiration,
        ending: EffectEnding,
        cannot_type: PermanentCannotEffectType,
    },

    Tap,
    Untap,
    ToggleTapState,
    MoveTo(CardLocation),
    Sacrifice,
    DestroyWithNoRegeneration,
    ExileAndReturn {
        return_step: StepName,
        new_controller: PlayerVariable,
    },
    GainControl {
        new_controller: PlayerVariable,
    },
    // TODO: Fix this. This isn't a PermanentEffect since it affects two permanents.
    // This also means that it can't be made into a global permanent effect.
    ExchangeControl,

    // TODO: Remove this in favor of newer representations.
    DealDamageToControllerOfTarget(PermanentAttributeCount),
    TargetDealsDamageToItsController(Count),

    AddCounters(Count, CounterType),
    RemoveCounters(Count, CounterType),
    RemoveAllCounters(CounterType),

    // Regenerate is a continuous effect, but there is no way to set a custom expiration for it.
    Regenerate,

    ControllerOfTargetsAssignsEffects {
        first_effect_type: Box<PermanentEffectType>,
        other_effect_type: Box<PermanentEffectType>,
    },

    Conditional {
        condition: PermanentCondition,
        conditional_effect_type: Box<PermanentEffectType>,
    },
}

impl PermanentEffectType {
    pub fn destroy() -> PermanentEffectType {
        PermanentEffectType::MoveTo(CardLocation::Graveyard)
    }

    pub fn exile() -> PermanentEffectType {
        PermanentEffectType::MoveTo(CardLocation::Exile)
    }

    pub fn bounce() -> PermanentEffectType {
        PermanentEffectType::MoveTo(CardLocation::Hand)
    }

    pub fn add_counter(counter_type: CounterType) -> PermanentEffectType {
        PermanentEffectType::AddCounters(Count::Fixed(1), counter_type)
    }

    pub fn remove_counter(counter_type: CounterType) -> PermanentEffectType {
        PermanentEffectType::RemoveCounters(Count::Fixed(1), counter_type)
    }

    pub fn compile(
        &self,
        resolution_variables: ResolutionVariables,
    ) -> Box<dyn Fn(PermanentID, &mut Game)> {

        let effect_type: PermanentEffectType = self.clone();

        use self::PermanentEffectType::*;
        Box::new(move |id: PermanentID, game: &mut Game| {
            match effect_type {
                Continuous { .. } => unimplemented!(),
                // Cannot effects are continuous effects too, but they have an extra field.
                Cannot { .. } => unimplemented!(),

                Tap => get_perm_mut(game, id).tap(),
                Untap => get_perm_mut(game, id).untap(),
                ToggleTapState => {
                    let perm = get_perm_mut(game, id);
                    let tapped = perm.flags().tapped;
                    perm.flags_mut().tapped = !tapped;
                },
                MoveTo(ref card_location) => {
                    use location::location::CardLocation::*;
                    match card_location {
                        Hand => { game.bounce(id); },
                        Graveyard => { game.bury(id); },
                        Exile => { game.exile_from_battlefield(id); },
                        Library(library_location) => {
                            game.battlefield_to_library(*library_location, id);
                        }
                    };
                },
                Sacrifice => {game.bury(id);},
                DestroyWithNoRegeneration => unimplemented!(),
                ExileAndReturn{..} => unimplemented!(),
                GainControl {ref new_controller} => {
                    // TODO: Remove these unwraps once target pruning is consolidated here.
                    let recipient_owner_id =
                        game.battlefield().get(id).unwrap().owner_id();
                    let recipient_controller_id =
                        game.battlefield().lookup_controller_id(id).unwrap();
                    let new_controller_id = new_controller.interpret(
                        recipient_owner_id,
                        recipient_controller_id,
                        &resolution_variables,
                        &game,
                    );
                    game.battlefield_mut().change_controller(new_controller_id, id);
                },
                // TODO: Remove this.
                ExchangeControl => unimplemented!(),

                // TODO: Remove this in favor of newer representations.
                DealDamageToControllerOfTarget(ref _permanent_attribute_count) => unimplemented!(),
                TargetDealsDamageToItsController(ref _count) => unimplemented!(),

                AddCounters(ref count, ref counter_type) => {
                    if let Some(count) = count.to_positive_integer() {
                        get_perm_mut(game, id).add_counters(count, *counter_type);
                    }
                },
                RemoveCounters(ref count, ref counter_type) => {
                    if let Some(count) = count.to_positive_integer() {
                        get_perm_mut(game, id).remove_counters(count, *counter_type);
                    }
                },
                RemoveAllCounters(ref counter_type) => {
                    get_perm_mut(game, id).remove_all_counters(*counter_type);
                },

                // Regenerate is a continuous effect, but there is no way to set a custom expiration for it.
                Regenerate => unimplemented!(),

                ControllerOfTargetsAssignsEffects {..} => unimplemented!(),

                Conditional {ref condition, ref conditional_effect_type} => {
                    if condition.compile()(get_perm(game, id)) {
                        let compiled_effect = conditional_effect_type.compile(
                            resolution_variables.clone());
                        compiled_effect(id, game);
                    }
                },
            }
        })
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ContinuousPermanentEffectType {
    Keyword(Keyword),

    AddColor(ColorVariable),
    OverrideColors {
        new_colors: Vec<ColorVariable>,
    },
    OverrideBasicLandTypes {
        new_basic_land_types: Vec<BasicLandTypeVariable>,
    },
    ReplaceColorWords {
        old_color: ColorVariable,
        new_color: ColorVariable,
    },
    ReplaceBasicLandTypeWords {
        old_basic_land_type: BasicLandTypeVariable,
        new_basic_land_type: BasicLandTypeVariable,
    },
    ZoneRedirect {
        regular_zone: CardZoneType,
        override_zone: CardZoneType,
    },

    EntersTapped,

    // Creature effects
    AdjustPowerToughness {
        power_adjustment: Count,
        toughness_adjustment: Count,
    },
    GainLifeEqualToSelfDamageDealt,
    GainLifeEqualToSelfCombatDamageDealt,
    DeadlyCombatDamage,
    MustAttack,
    CannotAttack,
    CannotAttackUnlessGenericCostIsPaid(Count),
    CannotAttackWithoutOtherAttacker(CardCondition),
    CannotBlock,
    CannotDealCombatDamage,
    CannotBeDealtCombatDamage,
    IgnoreDamage(IntegerCondition),
    CannotRegenerate,
    UnblockableBy(PermanentCondition),
    MustBlock(Selecting),
    OverrideDefender,
}

impl ContinuousPermanentEffectType {
    pub fn color_protection(color: Color) -> ContinuousPermanentEffectType {
        ContinuousPermanentEffectType::Keyword(Keyword::Protection(
            CardCondition::Color(ColorVariable::Fixed(color))))
    }

    pub fn adjust_power_toughness_same_amount(count: Count) -> ContinuousPermanentEffectType {
        ContinuousPermanentEffectType::AdjustPowerToughness {
            power_adjustment: count.clone(),
            toughness_adjustment: count,
        }
    }

    pub fn adjust_power_toughness(
        power_adjustment: isize,
        toughness_adjustment: isize) -> ContinuousPermanentEffectType{

        ContinuousPermanentEffectType::AdjustPowerToughness {
            power_adjustment: Count::FixedSigned(power_adjustment),
            toughness_adjustment: Count::FixedSigned(toughness_adjustment),
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Keyword {
    Shroud,
    Protection(CardCondition),

    // Creature Keywords
    Flying,
    Reach,
    Fear,
    FirstStrike,
    DoubleStrike,
    Vigilance,
    Trample,
    Defender,
    Haste,
    Flash,
    LandWalk(BasicLandTypeVariable),
}

impl Keyword {
    pub fn plainswalk() -> Keyword {
        Keyword::LandWalk(BasicLandTypeVariable::Fixed(BasicLandType::Plains))
    }

    pub fn islandwalk() -> Keyword {
        Keyword::LandWalk(BasicLandTypeVariable::Fixed(BasicLandType::Island))
    }

    pub fn swampwalk() -> Keyword {
        Keyword::LandWalk(BasicLandTypeVariable::Fixed(BasicLandType::Swamp))
    }

    pub fn mountainwalk() -> Keyword {
        Keyword::LandWalk(BasicLandTypeVariable::Fixed(BasicLandType::Mountain))
    }

    pub fn forestwalk() -> Keyword {
        Keyword::LandWalk(BasicLandTypeVariable::Fixed(BasicLandType::Forest))
    }
}

// See Comprehensive Rules 614.17 for this normally unnamed effect type that is similar
// to Replacement effects.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentCannotEffectType {
    CannotUntapDuringControllersUntapStep,
    CannotBeRegenerated,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum CounterType {
    PlusOnePlusOne,
    MinusOneMinusOne,
    Hourglass,
    Feather,
}

fn get_perm<'a>(game: &'a Game, id: PermanentID) -> &'a Permanent {
    game.battlefield().get(id).expect(&format!("Missing permanent {:?}", id))
}

fn get_perm_mut<'a>(game: &'a mut Game, id: PermanentID) -> &'a mut Permanent {
    game.battlefield_mut().get_mut(id).expect(&format!("Missing permanent {:?}", id))
}
