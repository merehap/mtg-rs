use effect::effect_type::expiration::*;
use effect::effect_type::permanent_effect_type::PermanentEffectType;
use effect::effect_type::selecting::*;
use script::condition::card_condition::CardCondition;
use script::condition::condition::Condition;
use script::condition::permanent_condition::PermanentCondition;
use script::count::Count;
use script::variable::player_variable::PlayerVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum DamageEffectType {
    Continuous {
        expiration: Expiration,
        effect_type: ContinuousDamageEffectType,
    },

    Permanent(PermanentEffectType),

    Deal(Count),
    DealUnpreventable(Count),
    // Drain only up to the amount of life/toughness/loyalty the target has.
    CappedDrain{
        damage_count: Count,
        additional_cap: Option<Count>,
    },

    IfElse {
        condition: Condition,
        then: Box<DamageEffectType>,
        otherwise: Box<DamageEffectType>,
    },
    TargetConditional {
        target_condition: PermanentCondition,
        then: Box<DamageEffectType>,
        otherwise: Box<DamageEffectType>,
    }
}

impl DamageEffectType {
    pub fn deal(count: usize) -> DamageEffectType {
        DamageEffectType::Deal(Count::Fixed(count))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ContinuousDamageEffectType {
    Prevent(Count),
    PreventAllDamage,
    PreventAllDamageFromSources {
        source_count: Count,
        source_condition: PermanentCondition,
    },
    PreventAllDamageFromAllMatchingSources {
        source_condition: PermanentCondition,
    },
    PreventAllDamageFromSourcesAndGainLife {
        source_count: Count,
        source_condition: PermanentCondition,
        source_condition_for_life_gain: CardCondition,
    },
    RedirectDamageBackToSourceController {
        source_condition: CardCondition,
    },
}

impl ContinuousDamageEffectType {
    pub fn prevent(count: usize) -> ContinuousDamageEffectType {
        ContinuousDamageEffectType::Prevent(Count::Fixed(count))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum DamageRecipients {
    All,
    Itself,
    Player(PlayerVariable),
    ControllerChooses {
        count: Count,
        recipient_types: DamageRecipientTypes,
    },
}

impl DamageRecipients {
    pub fn controller_chooses_one_creature() -> DamageRecipients{
        DamageRecipients::ControllerChooses {
            count: Count::Fixed(1),
            recipient_types: DamageRecipientTypes::any_creature(),
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct DamageRecipientTypes {
    pub creatures: Option<PermanentCondition>,
    pub players: Option<SelectingPlayers>,
    pub planeswalkers: bool,
}

impl DamageRecipientTypes {
    pub fn any_recipient() -> DamageRecipientTypes {
        DamageRecipientTypes {
            creatures: Some(PermanentCondition::Any),
            players: Some(SelectingPlayers::all()),
            planeswalkers: true,
        }
    }

    pub fn any_creature() -> DamageRecipientTypes {
        DamageRecipientTypes {
            creatures: Some(PermanentCondition::Any),
            players: None,
            planeswalkers: false,
        }
    }

    pub fn any_player() -> DamageRecipientTypes {
        DamageRecipientTypes {
            creatures: None,
            players: Some(SelectingPlayers::all()),
            planeswalkers: true,
        }
    }

    pub fn controller() -> DamageRecipientTypes {
        DamageRecipientTypes {
            creatures: None,
            players: Some(SelectingPlayers::effect_controller()),
            planeswalkers: false,
        }
    }
}

