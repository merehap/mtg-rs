use std::collections::BTreeSet;

use ability::static_ability::StaticAbility;
use effect::effect_type::card_effect_type::CardEffectType;
use effect::effect_type::expiration::{Expiration, EffectEnding};
use effect::effect_type::permanent_effect_type::PermanentEffectType;
use effect::interpreter::interpreter::ResolutionVariables;
use game::Game;
use location::card_pile_source::CardPileSource;
use location::location::{CardLocation, PermanentCardLocation};
use location::card_zone_type::CardZoneType;
use mana::color::Color;
use permanent::permanent_type::creature_type::CreatureType;
use player::player_id::PlayerID;
use player::player_state::Ending;
use script::condition::card_condition::CardCondition;
use script::condition::condition::Condition;
use script::condition::permanent_condition::PermanentCondition;
use script::condition::permanent_group_condition::PermanentGroupCondition;
use script::condition::player_condition::PlayerCondition;
use script::count::Count;
use zone::card_zone::CardZone;
use zone::hand::HandCardID;
use zone::library::LibraryLocation;
use permanent::permanent_layout::PermanentLayout;
use permanent::face::costless_permanent_face::CostlessPermanentFace;
use face::face::Name;
use permanent::face::permanent_face::{IsSnow, IsLegendary, NonLandPermanentTypes};
use ability::permanent_abilities::PermanentAbilitiesBuilder;
use permanent::permanent_type::creature::{Creature, Power, Toughness};

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerEffectType {
    Continuous {
        expiration: Expiration,
        continuous_type: ContinuousPlayerEffectType,
    },
    Replacement {
        expiration: Expiration,
        ending: EffectEnding,
        replacement_type: PlayerReplacementEffectType,
    },

    Win,
    Lose,

    Sacrifice {
        count: Count,
        permanent_condition: PermanentCondition,
    },
    CreateCreatureTokens {
        count: Count,
        power: Power,
        toughness: Toughness,
        creature_types: BTreeSet<CreatureType>,
        colors: BTreeSet<Color>,
        static_abilities: Vec<StaticAbility>,
    },
    GainLife(Count),
    LoseLife(Count),
    Draw(Count),
    Scry(Count),
    Discard(Count),
    DiscardAtRandom(Count),
    DealDamageToEachCreature(Count),
    // Spell controller chooses what card(s) should be discarded.
    ChooseDiscard {
        count: Count,
        card_condition: CardCondition,
    },
    DiscardMatching {
        card_condition: CardCondition,
    },
    TutorCards {
        card_count: CardCount,
        card_condition: CardCondition,
        destination: CardLocation,
    },
    TutorCardsToBattlefield {
        card_count: CardCount,
        card_condition: CardCondition,
        enters_tapped: bool,
    },
    RevealFromTopOfLibraryUntil {
        ending_condition: CardCondition,
        ending_card_destination: PermanentCardLocation,
        other_cards_destination: PermanentCardLocation,
        should_shuffle_afterwards: bool,
    },
    RevealFromTopOfLibraryAndTakeMatching {
        count: Count,
        card_condition: CardCondition,
        matching_cards_destination: CardLocation,
        non_matching_cards_destination: CardLocation,
        should_shuffle_afterwards: bool,
    },
    LookAtCardsFromTopOfLibraryAndChooseOne {
        card_count: Count,
        chosen_card_destination: CardLocation,
        unchosen_cards_destination: CardLocation,
        should_shuffle_afterwards: bool,
        should_reveal: bool,
    },
    TwoPilesOfCards {
        pile_creator_condition: PlayerCondition,
        pile_selector_condition: PlayerCondition,
        zone: CardPileSource,
        selected_pile_effect_type: Option<CardEffectType>,
        unselected_pile_effect_type: Option<CardEffectType>,
    },
    TwoPilesOfPermanents {
        pile_creator_condition: PlayerCondition,
        pile_selector_condition: PlayerCondition,
        permanent_condition: PermanentCondition,
        selected_pile_condition: PermanentGroupCondition,
        selected_pile_effect_type: Option<PermanentEffectType>,
        unselected_pile_effect_type: Option<PermanentEffectType>,
    },

    ShuffleGraveyardIntoLibrary(CardCondition),

    ExileAllMatching {
        card_condition: CardCondition,
        card_zone_type: CardZoneType,
    },

    Conditional {
        condition: Condition,
        effect_type: Box<PlayerEffectType>,
    },
}

impl PlayerEffectType {
    pub fn draw(count: usize) -> PlayerEffectType {
        PlayerEffectType::Draw(Count::Fixed(count))
    }

    pub fn discard(count: usize) -> PlayerEffectType {
        PlayerEffectType::Discard(Count::Fixed(count))
    }

    pub fn gain_life(count: usize) -> PlayerEffectType {
        PlayerEffectType::GainLife(Count::Fixed(count))
    }

    pub fn lose_life(count: usize) -> PlayerEffectType {
        PlayerEffectType::LoseLife(Count::Fixed(count))
    }

    pub fn compile(
        &self,
        resolution_variables: ResolutionVariables,
    ) -> Box<dyn Fn(PlayerID, &mut Game)> {

        let effect_type: PlayerEffectType = self.clone();

        use self::PlayerEffectType::*;
        Box::new(move |id: PlayerID, game: &mut Game| {
            match effect_type.clone() {
                Continuous {..} => unimplemented!(),
                Replacement {..} => unimplemented!(),

                Win => game.lookup_player_mut(id).alternate_ending = Some(Ending::Won),
                Lose => game.lookup_player_mut(id).alternate_ending = Some(Ending::Lost),

                Sacrifice {count, permanent_condition} => unimplemented!(),
                CreateCreatureTokens {
                    count,
                    power,
                    toughness,
                    creature_types,
                    colors,
                    static_abilities,
                } => {
                    println!("Creating creature tokens!");
                    let creature = Creature {creature_types, power, toughness};
                    let token_layout = PermanentLayout::Token(CostlessPermanentFace {
                        name: Name(creature.creature_type_text()),
                        colors,
                        is_snow: IsSnow::NotSnow,
                        land: None,
                        non_land_permanent_types: NonLandPermanentTypes::creature_only(creature),
                        is_legendary: IsLegendary::NonLegendary,

                        permanent_abilities: PermanentAbilitiesBuilder::new()
                            .static_abilities(static_abilities)
                            .build(),
                        graveyard_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
                        kicker_permanent_abilities: PermanentAbilitiesBuilder::new().build(),
                    });

                    for _ in 0..count.to_usize().unwrap() {
                        println!("One created!");
                        game.battlefield_mut().add_permanent(id, id, token_layout.clone());
                    }
                },
                GainLife(count) => {
                    if let Some(life) = count.to_positive_integer() {
                        game.lookup_player_mut(id).life_total.gain(life);
                    }
                },
                LoseLife(count) => {
                    if let Some(life) = count.to_positive_integer() {
                        game.lookup_player_mut(id).life_total.lose(life);
                    }
                },
                Draw(count) => {
                    if let Some(count) = count.to_positive_integer() {
                        game.lookup_player_mut(id).draw(count);
                    }
                },
                Scry(count) => unimplemented!(),
                Discard(count) => unimplemented!(),
                DiscardAtRandom(count) => unimplemented!(),
                DealDamageToEachCreature(count) => unimplemented!(),
                // Spell controller chooses what card(s) should be discarded.
                ChooseDiscard {count, card_condition} => unimplemented!(),
                DiscardMatching {card_condition} => {
                    let check = card_condition.compile();
                    let player = game.lookup_player_mut(id);
                    let discard_ids: BTreeSet<HandCardID> =
                        (&player.hand).cards().iter()
                            .filter(|(_, card)| check(card))
                            .map(|(id, _)| **id)
                            .collect();
                    player.discard(discard_ids);
                },
                TutorCards {card_count, card_condition, destination} => unimplemented!(),
                TutorCardsToBattlefield{card_count, card_condition, enters_tapped} =>
                    unimplemented!(),
                RevealFromTopOfLibraryUntil {
                    ending_condition,
                    ending_card_destination,
                    other_cards_destination,
                    should_shuffle_afterwards,
                } => {
                    // TODO: How to represent cards being revealed?
                    let check = ending_condition.compile();
                    let mut other_removed_cards = Vec::new();
                    while let Some(card) =
                        game.lookup_player_mut(id).library.remove(LibraryLocation::Top) {

                        if check(&card) {
                            game.add_permanent_card(id, ending_card_destination, card);
                            break;
                        } else {
                            other_removed_cards.push(card);
                        }
                    }

                    // TODO: Allow player to order these.
                    for card in other_removed_cards {
                        game.add_permanent_card(
                            id, other_cards_destination.clone(), card);
                    }

                    if should_shuffle_afterwards {
                        game.lookup_player_mut(id).library.shuffle();
                    }
                },
                RevealFromTopOfLibraryAndTakeMatching {
                    count,
                    card_condition,
                    matching_cards_destination,
                    non_matching_cards_destination,
                    should_shuffle_afterwards,
                } => {
                    // TODO: How to represent cards being revealed?
                    let check = card_condition.compile();
                    let mut matching_cards = Vec::new();
                    let mut non_matching_cards = Vec::new();
                    while let Some(card) =
                    game.lookup_player_mut(id).library.remove(LibraryLocation::Top) {
                        if check(&card) {
                            matching_cards.push(card);
                        } else {
                            non_matching_cards.push(card);
                        }
                    }

                    // TODO: Allow player to order these.
                    for card in matching_cards {
                        game.add_card(
                            id, matching_cards_destination.clone(), card);
                    }

                    for card in non_matching_cards {
                        game.add_card(
                            id, non_matching_cards_destination.clone(), card);
                    }

                    if should_shuffle_afterwards {
                        game.lookup_player_mut(id).library.shuffle();
                    }
                },
                LookAtCardsFromTopOfLibraryAndChooseOne {
                    card_count,
                    chosen_card_destination,
                    unchosen_cards_destination,
                    should_shuffle_afterwards,
                    should_reveal,
                } => unimplemented!(),
                TwoPilesOfCards {
                    pile_creator_condition,
                    pile_selector_condition,
                    zone,
                    selected_pile_effect_type,
                    unselected_pile_effect_type,
                } => unimplemented!(),
                TwoPilesOfPermanents {
                    pile_creator_condition,
                    pile_selector_condition,
                    permanent_condition,
                    selected_pile_condition,
                    selected_pile_effect_type,
                    unselected_pile_effect_type,
                } => unimplemented!(),

                ShuffleGraveyardIntoLibrary(card_condition) => {
                    let player = game.lookup_player_mut(id);
                    let ids = player.graveyard.matching(card_condition);
                    player.graveyard_to_top_of_library(ids);
                },
                ExileAllMatching {card_condition, card_zone_type} => {
                    let zoned_card_ids = game.lookup_player_mut(id)
                        .matching_cards(card_zone_type, card_condition);
                    let cards = game.remove_cards(zoned_card_ids);
                    game.lookup_player_mut(id).exile.add_all(cards);
                },

                Conditional {condition, effect_type} => unimplemented!(),
            }
        })
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ContinuousPlayerEffectType {
    CannotPlayLandCards,
    PlayWithHandRevealed,
    PlayWithTopLibraryCardRevealed,
    MaxSpellsPerTurn(Count),
    CardsMayBePlayedFromGraveyard,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerReplacementEffectType {
    SkipDefaultDrawStep,
    ZoneRedirect {
        regular_zone: CardZoneType,
        override_zone: CardZoneType,
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardCount {
    Any,
    Predetermined(Count),
}
