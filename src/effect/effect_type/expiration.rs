#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Expiration {
    Never,
    EndOfTurn,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EffectEnding {
    OnFirstUse,
    OnExpiration,
}

