use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::cmp;

use cost::cost::Cost;
use effect::effect::Effect;
use effect::receiver::card_receiver::CardReceiver;
use effect::receiver::permanent_receiver::PermanentReceiver;
use effect::effect_template::EffectTemplate;
use effect::effect_type::selecting::{Selecting, SelectingPlayers};
use game::Game;
use permanent::permanent_id::PermanentID;
use player::player_option::{PlayerOption, ValidatedPlayerChoice};
use player::player_state::PlayerState;
use zone::hand::{Hand, HandCardID};
use zone::stack::FaceType;
use effect::receiver::player_receiver::PlayerReceiver;
use script::condition::player_condition::PlayerCondition;

pub trait PlayerBrain {
    fn make_priority_period_decision<'a>(
        &self,
        current_options: &'a Vec<PlayerOption>,
        cost_options: Vec<(Option<Cost>, PlayerOption)>,
        player_state: &PlayerState,
        game: &Game) -> Option<ValidatedPlayerChoice<'a>>;

    fn declare_attackers<'a>(&self, eligible_attackers: &'a Vec<PermanentID>)
        -> Vec<&'a PermanentID>;

    fn declare_blockers<'a, 'b>(
        &self,
        attackers: &'a Vec<PermanentID>,
        eligible_blockers: &'b Vec<PermanentID>)
        -> BTreeMap<&'a PermanentID, BTreeSet<&'b PermanentID>>;

    fn pick_discards<'a>(&self, mut count: usize, hand: &'a Hand)
        -> BTreeSet<&'a HandCardID> {

        count = cmp::min(count, hand.size());

        let discards = self.pick_discards_unchecked(count, hand);
        if discards.len() != count {
            panic!("Player brain was supposed to provide {} discards but provided {}.",
                count, discards.len(),
            );
        }

        discards
    }

    fn pick_discards_unchecked<'a>(&self, count: usize, hand: &'a Hand)
        -> BTreeSet<&'a HandCardID>;
}

#[derive(Clone, Debug)]
pub enum Decision<'a> {
    Pass,
    Choice(ValidatedPlayerChoice<'a>),
}

pub struct AlwaysPassingPlayerBrain {

}

impl AlwaysPassingPlayerBrain {
    pub fn new() -> AlwaysPassingPlayerBrain {
        AlwaysPassingPlayerBrain{}
    }
}

impl PlayerBrain for AlwaysPassingPlayerBrain {
    fn make_priority_period_decision<'a>(
        &self,
        _: &'a Vec<PlayerOption>,
        _: Vec<(Option<Cost>, PlayerOption)>,
        _: &PlayerState,
        _: &Game) -> Option<ValidatedPlayerChoice<'a>> {

        None
    }

    fn declare_attackers<'a>(&self, _: &'a Vec<PermanentID>) -> Vec<&'a PermanentID> {
        Vec::new()
    }

    fn declare_blockers<'a, 'b>(&self, _: &'a Vec<PermanentID>, _: &'b Vec<PermanentID>) -> BTreeMap<&'a PermanentID, BTreeSet<&'b PermanentID>> {
        BTreeMap::new()
    }

    fn pick_discards_unchecked<'a>(&self, count: usize, hand: &'a Hand) -> BTreeSet<&'a HandCardID> {
        let mut discards = BTreeSet::new();
        for i in 0..count {
            discards.insert(hand.cards()[i].0);
        }

        discards
    }
}

pub struct ExhaustivePlayerBrain {

}

impl ExhaustivePlayerBrain {
    pub fn new() -> ExhaustivePlayerBrain {
        ExhaustivePlayerBrain{}
    }
}

impl PlayerBrain for ExhaustivePlayerBrain {
    fn make_priority_period_decision<'a>(
        &self,
        current_options: &'a Vec<PlayerOption>,
        cost_options: Vec<(Option<Cost>, PlayerOption)>,
        player_state: &PlayerState,
        game: &Game) -> Option<ValidatedPlayerChoice<'a>> {

        let potential_mana_count = player_state.mana_pool.len() + current_options.iter()
            .filter(|option|
                if let PlayerOption::BasicLandTypeManaAbility(_) = option { true } else { false })
            .count();
        let maybe_minimum_cmc = cost_options.iter()
            .filter_map(|(maybe_cost, _)| maybe_cost.clone().map(|cost| cost.mana.converted_mana_cost()))
            .min();

        for option in current_options {
            if let PlayerOption::BasicLandTypeManaAbility(_) = option {
                // No point activating a mana ability if nothing can be paid for even with all your mana.
                if let Some(minimum_cmc) = maybe_minimum_cmc {
                    if potential_mana_count >= minimum_cmc {
                        return Some(ValidatedPlayerChoice::validate_effects(
                            option,
                            Vec::new(),
                        ));
                    }
                }
            } else {
                let mut effects = Vec::new();
                match option {
                    PlayerOption::CastCard(zoned_face_id) => {
                        let face = game.lookup_face(*zoned_face_id)
                            .expect("Card face should have been there.");
                        if let FaceType::Ephemeral(ephemeral_face) = face {
                            for effect_template in ephemeral_face.effect_templates() {
                                let mut effect = Effect::new(effect_template.clone());
                                match effect.template {
                                    EffectTemplate::Global(_) => {
                                        // No target selection can be done for global effects.
                                    },
                                    EffectTemplate::Permanent {
                                        ref effect_types,
                                        ref selecting,
                                        ref selection_condition,
                                    } => {
                                        let count = match selecting {
                                            Selecting::Targets(count) => count.to_usize()
                                                .expect("Can't have a negative number of targets."),
                                            _ => unimplemented!(),
                                        };

                                        let permanent_ids = &game.battlefield()
                                            .matching_permanents(selection_condition.clone())[0..count];
                                        for effect_type in effect_types {
                                            effect.permanent_effect_receivers.push_target(
                                                effect_type.clone(),
                                                PermanentReceiver::multiple(
                                                    permanent_ids,
                                                    selection_condition.clone(),
                                                ),
                                            );
                                        }
                                    },
                                    EffectTemplate::Card {
                                        ref effect_types,
                                        ref selecting,
                                        ref selection_condition,
                                        ref selection_owner,
                                        ref card_zone,
                                    } => {
                                        let count = match selecting {
                                            Selecting::Targets(count) => count.to_usize()
                                                .expect("Can't have a negative number of targets."),
                                            _ => unimplemented!(),
                                        };

                                        // TODO: Actually apply selection_owner here.
                                        let zoned_card_ids = player_state.matching_cards(
                                            *card_zone, selection_condition.clone());
                                        for effect_type in effect_types {
                                            effect.card_effect_receivers.push_target(
                                                effect_type.clone(),
                                                CardReceiver::multiple(
                                                    &zoned_card_ids, selection_condition.clone()),
                                            );
                                        }
                                    },
                                    EffectTemplate::Player {ref selecting, ref effect_types} => {
                                        // FIXME
                                        let condition = match selecting {
                                            SelectingPlayers::Single(variable) => PlayerCondition::Matches(variable.clone()),
                                            SelectingPlayers::AllMatching(condition) => condition.clone(),
                                            SelectingPlayers::Targets {condition, ..} => condition.clone(),
                                            SelectingPlayers::NonTargets {condition, ..} => condition.clone(),
                                        };

                                        // FIXME: This needs to use actual selection logic, not hardcode to ActivePlayer.
                                        let player_ids = &[game.active_player_id()];
                                        for effect_type in effect_types {
                                            effect.player_effect_receivers.push_target(
                                                effect_type.clone(),
                                                PlayerReceiver::multiple(player_ids, condition.clone()),
                                            );
                                        }
                                    },
                                    EffectTemplate::StackObject {
                                        ref effect_types,
                                        ref selecting,
                                        ref stack_object_types,
                                        ref selection_owner_condition,
                                    } => unimplemented!(),
                                    EffectTemplate::Damage {
                                        ..
                                    } => unimplemented!(),
                                    EffectTemplate::Mana {..} =>
                                        unimplemented!(),
                                    EffectTemplate::Sequence(_) =>
                                        unimplemented!(),
                                    EffectTemplate::ChooseOne(_) =>
                                        unimplemented!(),
                                    EffectTemplate::ChooseTwo(_) =>
                                        unimplemented!(),
                                    EffectTemplate::ChooseThreeWithRepeats(_) =>
                                        unimplemented!(),
                                    EffectTemplate::Delay {until, effect_template} =>
                                        unimplemented!(),
                                    EffectTemplate::PayCostToPrevent {cost, effect_template} =>
                                        unimplemented!(),
                                }

                                effects.push(effect);
                            }
                        }
                    },
                    _ => {},
                }

                return Some(ValidatedPlayerChoice::validate_effects(option, effects));
            }
        }

        None
    }

    fn declare_attackers<'a>(&self, eligible_attackers: &'a Vec<PermanentID>) -> Vec<&'a PermanentID> {
        let mut selected_attackers = Vec::new();
        for eligible_attacker in eligible_attackers {
            selected_attackers.push(eligible_attacker);
        }

        selected_attackers
    }

    fn declare_blockers<'a, 'b>(
        &self,
        attackers: &'a Vec<PermanentID>,
        eligible_blockers: &'b Vec<PermanentID>)
        -> BTreeMap<&'a PermanentID, BTreeSet<&'b PermanentID>> {

        let mut assignments = BTreeMap::new();
        for i in 0..cmp::min(attackers.len(), eligible_blockers.len()) {
            let mut current_blockers = BTreeSet::new();
            current_blockers.insert(
                eligible_blockers.get(i).expect("Ineligible blocker generated?"));
            assignments.insert(&attackers[i], current_blockers);
        }

        assignments
    }

    fn pick_discards_unchecked<'a>(&self, count: usize, hand: &'a Hand) -> BTreeSet<&'a HandCardID> {
        let mut discards = BTreeSet::new();
        for i in 0..count {
            discards.insert(hand.cards()[i].0);
        }

        discards
    }
}
