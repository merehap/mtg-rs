use effect::effect::Effect;
use permanent::permanent_id::PermanentID;
use zone::graveyard::GraveyardCardID;
use zone::hand::HandCardID;
use zone::zoned_card_id::ZonedCardID;
use zone::zoned_face_id::ZonedFaceID;

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Debug)]
pub enum PlayerOption {
    PlayCardAsNonSpell(ZonedCardID),
    CastCard(ZonedFaceID),

    HandAbility(HandCardID),
    BasicLandTypeManaAbility(PermanentID),
    BattlefieldManaAbility(PermanentID),
    BattlefieldAbility(PermanentID),
    GraveyardAbility(GraveyardCardID),
}

#[derive(Clone, Debug)]
pub struct ValidatedPlayerChoice<'a> {
    player_option: &'a PlayerOption,
    ephemeral_effects: Vec<Effect>,
}

impl <'a> ValidatedPlayerChoice<'a> {
    pub fn validate_effects(
        player_option: &'a PlayerOption,
        effects: Vec<Effect>,
    ) -> ValidatedPlayerChoice {

        ValidatedPlayerChoice {
            player_option,
            ephemeral_effects: effects,
        }
    }

    pub fn player_option(&self) -> PlayerOption {
        self.player_option.clone()
    }

    pub fn ephemeral_effects(self) -> Vec<Effect> {
        self.ephemeral_effects
    }
}
