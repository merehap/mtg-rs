use std::fmt;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash, Debug)]
pub enum PlayerID {
    Player0,
    Player1,
}

impl PlayerID {
    pub fn opponent(self) -> PlayerID {
        if self == PlayerID::Player0 {
            PlayerID::Player1
        } else {
            PlayerID::Player0
        }
    }
}

impl fmt::Display for PlayerID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub enum RelativePlayerID {
    Active,
    NonActive,
}
