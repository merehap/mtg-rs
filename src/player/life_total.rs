use util::positive_integer::PositiveInteger;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct LifeTotal(pub isize);

impl LifeTotal {
    pub fn gain(&mut self, life: PositiveInteger) {
        self.0 += life.to_isize();
    }

    pub fn lose(&mut self, life: PositiveInteger) {
        self.0 -= life.to_isize();
    }
}
