use std::collections::BTreeSet;

use card::card::Card;
use location::card_zone_type::CardZoneType;
use mana::mana_pool::ManaPool;
use player::player_brain::PlayerBrain;
use player::life_total::LifeTotal;
use player::player_id::{PlayerID, RelativePlayerID};
use script::condition::card_condition::CardCondition;
use util::positive_integer::PositiveInteger;
use zone::card_zone::CardZone;
use zone::exile::Exile;
use zone::graveyard::{Graveyard, GraveyardCardID};
use zone::hand::{Hand, HandCardID};
use zone::library::{Library, LibraryLocation};
use zone::zoned_card_id::ZonedCardID;

///
/// The complete game state of a single player in a regular game of MTG.
///
pub struct PlayerState {
    pub id: PlayerID,
    pub mana_pool: ManaPool,
    pub life_total: LifeTotal,

    pub library: Library,
    pub hand: Hand,
    pub graveyard: Graveyard,
    pub exile: Exile,
    // TODO: Add "Outside of Game" zone for evil cards like Living Wish?

    pub attempted_to_draw_from_empty_deck: bool,
    pub alternate_ending: Option<Ending>,

    pub max_lands_per_turn: usize,

    pub brain: Box<PlayerBrain>,
}

impl PlayerState {
    pub fn new(player_id: PlayerID, library: Library, brain: Box<PlayerBrain>) -> PlayerState {
        PlayerState {
            id: player_id,
            life_total: LifeTotal(20),
            mana_pool: ManaPool::new(),
            library,
            hand: Hand::new(player_id),
            graveyard: Graveyard::new(player_id),
            exile: Exile::new(player_id),

            attempted_to_draw_from_empty_deck: false,
            alternate_ending: None,

            max_lands_per_turn: 1,

            brain,
        }
    }

    pub fn draw(&mut self, count: PositiveInteger) -> Vec<&Card> {
        let initial_hand_size = self.hand.size();
        for _ in 0..count.to_usize() {
            if let Some(card) = self.library.remove(LibraryLocation::Top) {
                self.hand.add(card);
            } else {
                self.attempted_to_draw_from_empty_deck = true;
                break;
            }
        }

        self.hand.cards()[initial_hand_size..].iter()
            .map(|(_, card)| *card)
            .collect()
    }

    // Each HandCardID must be in the hand or else panic.
    pub fn discard(&mut self, hand_card_ids: BTreeSet<HandCardID>) -> Vec<&Card> {
        let initial_graveyard_size = self.graveyard.size();
        for discard_id in hand_card_ids {
            let card = self.hand.remove(discard_id)
                .expect("Specified HandCardIDs must be present in hand!");
            self.graveyard.add(card);
        }

        self.graveyard.cards()[initial_graveyard_size..].iter()
            .map(|(_, card)| *card)
            .collect()
    }

    pub fn graveyard_to_top_of_library(
        &mut self, graveyard_card_ids: BTreeSet<GraveyardCardID>) -> Vec<&Card> {

        let initial_library_size = self.library.size();
        for graveyard_card_id in graveyard_card_ids {
            let card = self.graveyard.remove(graveyard_card_id)
                .expect("Specified HandCardIDs must be present in hand!");
            self.library.add(LibraryLocation::Top, card);
        }

        self.library.cards()[initial_library_size..].iter()
            .map(|(_, card)| *card)
            .collect()
    }

    pub fn matching_cards(
        &self,
        card_zone_type: CardZoneType,
        condition: CardCondition,
    ) -> BTreeSet<ZonedCardID> {
        match card_zone_type {
            CardZoneType::Library =>
                self.library.matching(condition).iter()
                    .map(|id| ZonedCardID::Library(*id))
                    .collect(),
            CardZoneType::Hand =>
                self.hand.matching(condition).iter()
                    .map(|id| ZonedCardID::Hand(*id))
                    .collect(),
            CardZoneType::Graveyard =>
                self.graveyard.matching(condition).iter()
                    .map(|id| ZonedCardID::Graveyard(*id))
                    .collect(),
            CardZoneType::Exile =>
                self.exile.matching(condition).iter()
                    .map(|id| ZonedCardID::Exile(*id))
                    .collect(),
        }
    }

    pub fn current_ending_state(&self) -> Option<Ending> {
        if self.life_total.0 <= 0 || self.attempted_to_draw_from_empty_deck {
            Some(Ending::Lost)
        } else if let Some(ending) = &self.alternate_ending {
            Some(*ending)
        } else {
            None
        }
    }

    fn absolute_player_id(&self, relative_player_id: RelativePlayerID) -> PlayerID {
        match relative_player_id {
            RelativePlayerID::Active => self.id,
            RelativePlayerID::NonActive => self.id.opponent(),
        }
    }
}

#[derive(Clone, Copy)]
pub enum Ending {
    Won,
    Lost,
    // TODO: Divine Intervention is the only one.
    Drawn,
}