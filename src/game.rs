use std::collections::{BTreeSet, BTreeMap};

use card::card::Card;
use deck::deck::Deck;
use effect::effect::Effect;
use effect::interpreter::interpreter;
use location::location::{CardLocation, PermanentCardLocation};
use permanent::permanent::Permanent;
use permanent::permanent_card::PermanentCard;
use permanent::permanent_id::PermanentID;
use permanent::permanent_layout::PermanentLayout;
use player::player_brain::{PlayerBrain, AlwaysPassingPlayerBrain};
use player::player_id::PlayerID;
use player::player_option::PlayerOption;
use player::player_state::{PlayerState, Ending};
use turn::ended_segment::EndedSegment;
use turn::step::StepName;
use turn::sub_step::SubStep;
use turn::sub_steps::*;
use turn::turns::Turns;
use util::positive_integer::PositiveInteger;
use zone::battlefield::Battlefield;
use zone::library::LibraryLocation;
use zone::stack::{Stack, StackObject, StackObjectType, CardView, FaceType, IsSplitSecond};
use zone::zoned_card_id::ZonedCardID;
use zone::zoned_face_id::ZonedFaceID;

///
/// The complete game state of a regular two player game of MTG.
///
pub struct Game {
    player0: PlayerState,
    player1: PlayerState,
    battlefield: Battlefield,
    stack: Stack,
    turns: Turns,
    assigned_combat_damage: BTreeMap<PermanentID, PositiveInteger>,
    assigned_combat_damage_to_non_active_player: Option<PositiveInteger>,

    priority_player_id: PlayerID,
    previous_decision_was_pass: bool,
    game_over: bool,
}

impl Game {
    pub fn new(
        player0_brain: Box<PlayerBrain>,
        player0_deck: Deck,
        player1_brain: Box<PlayerBrain>,
        player1_deck: Deck,
    ) -> Game {

        let player0_library = player0_deck.to_random_order_library(PlayerID::Player0);
        let player1_library = player1_deck.to_random_order_library(PlayerID::Player1);

        let mut state = Game {
            player0: PlayerState::new(
                PlayerID::Player0, player0_library, player0_brain),
            player1: PlayerState::new(
                PlayerID::Player1, player1_library, player1_brain),
            battlefield: Battlefield::new(),
            stack: Stack::new(),
            turns: Turns::new(PlayerID::Player0),
            assigned_combat_damage: BTreeMap::new(),
            assigned_combat_damage_to_non_active_player: None,

            priority_player_id: PlayerID::Player0,
            previous_decision_was_pass: false,
            game_over: false,
        };

        state.player0.draw(PositiveInteger::from_usize(7).unwrap());
        state.player1.draw(PositiveInteger::from_usize(7).unwrap());

        state
    }

    pub fn empty_hand(player0_deck: Deck, player1_deck: Deck) -> Game {
        let player0_library = player0_deck.to_random_order_library(PlayerID::Player0);
        let player1_library = player1_deck.to_random_order_library(PlayerID::Player1);
        Game {
            player0: PlayerState::new(
                PlayerID::Player0, player0_library, Box::new(AlwaysPassingPlayerBrain::new())),
            player1: PlayerState::new(
                PlayerID::Player1, player1_library, Box::new(AlwaysPassingPlayerBrain::new())),
            battlefield: Battlefield::new(),
            stack: Stack::new(),
            turns: Turns::new(PlayerID::Player0),
            assigned_combat_damage: BTreeMap::new(),
            assigned_combat_damage_to_non_active_player: None,

            priority_player_id: PlayerID::Player0,
            previous_decision_was_pass: false,
            game_over: false,
        }
    }

    pub fn empty() -> Game {
        Game::empty_hand(
            Deck::new(Vec::new()),
            Deck::new(Vec::new()),
        )
    }

    pub fn player0(&self) -> &PlayerState {
        &self.player0
    }

    pub fn player1(&self) -> &PlayerState {
        &self.player1
    }

    pub fn battlefield(&self) -> &Battlefield {
        &self.battlefield
    }

    pub fn battlefield_mut(&mut self) -> &mut Battlefield {
        &mut self.battlefield
    }

    pub fn stack(&self) -> &Stack {
        &self.stack
    }

    pub fn turns(&self) -> &Turns {
        &self.turns
    }

    pub fn turns_mut(&mut self) -> &mut Turns {
        &mut self.turns
    }

    pub fn assigned_combat_damage(&self) -> &BTreeMap<PermanentID, PositiveInteger> {
        &self.assigned_combat_damage
    }

    pub fn assigned_combat_damage_mut(&mut self) -> &mut BTreeMap<PermanentID, PositiveInteger> {
        &mut self.assigned_combat_damage
    }

    pub fn assigned_combat_damage_to_non_active_player(&self) -> Option<PositiveInteger> {
        self.assigned_combat_damage_to_non_active_player
    }

    pub fn set_assigned_combat_damage_to_non_active_player(&mut self, value: Option<PositiveInteger>) {
        self.assigned_combat_damage_to_non_active_player = value;
    }

    pub fn advance_until_state_change_or_options(&mut self) -> AdvanceResult {
        loop {
            let result = {
                self.advance().clone()
            };

            match result.clone() {
                AdvanceResult::Continue(ContinueResult::ZoneStateUpdated) =>
                    return result.clone(),
                AdvanceResult::Continue(ContinueResult::PriorityPeriod(
                    PriorityPeriodResult {ref options, ..}
                )) if !options.is_empty() => return result.clone(),
                AdvanceResult::Continue(_) => {},
                _ => return result.clone(),
            }
        }
    }

    pub fn advance(&mut self) -> AdvanceResult {
        if self.game_over {
            return AdvanceResult::GameAlreadyOver;
        }

        let result = self.advance_with_no_status_update();
        match result {
            AdvanceResult::PlayerWins(..) => self.game_over = true,
            AdvanceResult::PlayerLoses(..) => self.game_over = true,
            AdvanceResult::Tie => self.game_over = true,
            _ => {},
        }

        result
    }

    fn advance_with_no_status_update(&mut self) -> AdvanceResult {
        if self.turns.current_step_name() == StepName::Untap &&
            self.turns.current_sub_step() == SubStep::Beginning {
            println!();
            println!("*** BEGINNING {:?} TURN.", self.active_player().id);
            let active_player_id = self.active_player_id();
            self.priority_player_id = active_player_id;

            // Summoning sickness doesn't seem to be described as part of the turn structure,
            // the removal of it just happens at the earliest part of the turn.
            // Since nothing can trigger yet, it seems safe to put clearing it here.
            self.battlefield.foreach_permanent_mut(active_player_id, |perm| {
                perm.flags_mut().summoning_sick = false;
            });
        }

        let first_strike_step_text =
            if self.turns().current_step().is_first_strike_combat_damage_step() {
                " (First Strike)"
            } else {
                ""
            };
        println!(
            "*** {}{} step, {} sub-step ({}'s turn)",
            self.turns.current_step_name(),
            first_strike_step_text,
            self.turns.current_sub_step(),
            self.active_player().id);

        let mut continue_result = ContinueResult::NoUpdate;
        match self.turns().current_sub_step() {
            SubStep::Beginning => {},
            SubStep::End => {},

            SubStep::Priority => {
                match priority_period::priority_period(self) {
                    AdvanceResult::Continue(cont_result) => continue_result = cont_result,
                    result => return result,
                }
            },
            SubStep::MainPriority => {
                match priority_period::priority_period(self) {
                    AdvanceResult::Continue(cont_result) => continue_result = cont_result,
                    result => return result,
                }
            },

            SubStep::Phasing => {},
            SubStep::Untap => {
                continue_result = untap::untap(self);
            },
            SubStep::Draw => {
                continue_result = draw::draw(self);
            },

            SubStep::Saga => {},

            SubStep::DeclareAttackers => {
                continue_result = declare_attackers::declare_attackers(self);
            },
            SubStep::DeclareBlockers => {
                continue_result = declare_blockers::declare_blockers(self);
            },
            SubStep::EnableNeededCombatDamageSteps => {
                enable_needed_combat_damage_steps::enable_needed_combat_damage_steps(self);
            },
            SubStep::AssignAttackerOrder => {},
            SubStep::AssignBlockerOrder => {},
            SubStep::AssignAttackerDamage => {
                assign_attacker_damage::assign_attacker_damage(self);
            },
            SubStep::AssignBlockerDamage => {
                assign_blocker_damage::assign_blocker_damage(self);
            },
            SubStep::DealCombatDamage => {
                deal_combat_damage::deal_combat_damage(self);
            },
            SubStep::RemoveFromCombat => {
                continue_result = remove_from_combat::remove_from_combat(self);
            },

            SubStep::DiscardToHandSize => {
                continue_result = discard_to_hand_size::discard_to_hand_size(self);
            },
            SubStep::Cleanup => {
                cleanup::cleanup(self);
            },
            SubStep::EnableAdditionalCleanupStep => {},
        }

        let mut maybe_ended_segment = None;
        use game::ContinueResult::*;
        match continue_result {
            // If there was a priority period and the player passed,
            // then either pass priority or advance to the next sub-step.
            PriorityPeriod(PriorityPeriodResult {selected_option: None, ..}) => {

                if self.previous_decision_was_pass {
                    if let Some(stack_object) = self.stack.pop() {
                        self.resolve_stack_object(stack_object);
                        match (&self.player0.alternate_ending, &self.player1.alternate_ending) {
                            (None, None) => {},
                            (Some(ending), None) => return match ending {
                                Ending::Won => AdvanceResult::PlayerWins(PlayerID::Player0),
                                Ending::Lost => AdvanceResult::PlayerLoses(PlayerID::Player0),
                                Ending::Drawn => AdvanceResult::Tie,
                            },
                            (None, Some(ending)) => return match ending {
                                Ending::Won => AdvanceResult::PlayerWins(PlayerID::Player1),
                                Ending::Lost => AdvanceResult::PlayerLoses(PlayerID::Player1),
                                Ending::Drawn => AdvanceResult::Tie,
                            },
                            (Some(ending0), Some(ending1)) => match (ending0, ending1) {
                                (_, _) => unimplemented!(),
                            },
                        }
                    } else {
                        maybe_ended_segment = Some(self.turns.advance());
                    }
                }

                // Restart the priority sequence after each resolved stack object.
                self.previous_decision_was_pass = !self.previous_decision_was_pass;
                self.priority_player_id = self.priority_player_id.opponent();
            },
            // If there was a priority period and the player didn't pass,
            // then they still have priority and neither the priority player nor the
            // current sub-step changes.
            PriorityPeriod(_) => {},
            NoUpdate | ZoneStateUpdated =>
                maybe_ended_segment = Some(self.turns.advance()),
        }

        if let Some(ended_segment) = maybe_ended_segment {
            if ended_segment > EndedSegment::SubStep {
                self.player0.mana_pool.clear();
                self.player1.mana_pool.clear();
            }
        }

        AdvanceResult::Continue(continue_result)
    }

    pub fn active_player_id(&self) -> PlayerID {
        self.turns.current_turn().player_id()
    }

    pub fn non_active_player_id(&self) -> PlayerID {
        self.turns.current_turn().player_id().opponent()
    }

    pub fn priority_player_id(&self) -> PlayerID {
        self.priority_player_id
    }

    pub fn lookup_card(&self, zoned_card_id: ZonedCardID) -> Option<&Card> {
        let owner = self.lookup_player(zoned_card_id.owner_id());
        match zoned_card_id {
            ZonedCardID::Library(library_id) =>
                owner.library.get(library_id.location()),
            ZonedCardID::Hand(hand_card_id) =>
                owner.hand.get(hand_card_id),
            ZonedCardID::Graveyard(graveyard_card_id) =>
                owner.graveyard.get(graveyard_card_id),
            ZonedCardID::Exile(exile_card_id) =>
                owner.exile.get(exile_card_id),
        }
    }

    pub fn add_permanent_card(
        &mut self,
        owner_id: PlayerID,
        destination: PermanentCardLocation,
        card: Card,
    ) {
        match destination {
            PermanentCardLocation::Battlefield => {
                self.add_card_to_battlefield_if_permanent(owner_id, owner_id, card);
            },
            PermanentCardLocation::CardLocation(card_location) => {
                self.add_card(owner_id, card_location, card);
            },
        }
    }

    pub fn remove_card(&mut self, zoned_card_id: ZonedCardID) -> Option<Card> {
        let owner = self.lookup_player_mut(zoned_card_id.owner_id());
        match zoned_card_id {
            ZonedCardID::Library(library_id) =>
                owner.library.remove(library_id.location()),
            ZonedCardID::Hand(hand_card_id) =>
                owner.hand.remove(hand_card_id),
            ZonedCardID::Graveyard(graveyard_card_id) =>
                owner.graveyard.remove(graveyard_card_id),
            ZonedCardID::Exile(exile_card_id) =>
                owner.exile.remove(exile_card_id),
        }
    }

    pub fn remove_cards(&mut self, zoned_card_ids: BTreeSet<ZonedCardID>) -> Vec<Card> {
        let mut cards = Vec::new();
        for zoned_card_id in zoned_card_ids {
            if let ZonedCardID::Library(_) = zoned_card_id {
                // TODO: Fix this.
                panic!("Can't use remove_cards on a Library.");
            }

            if let Some(card) = self.remove_card(zoned_card_id) {
                cards.push(card);
            }
        }

        cards
    }

    pub fn lookup_face(&self, zoned_face_id: ZonedFaceID) -> Option<FaceType> {
        let card = self.lookup_card(zoned_face_id.zoned_card_id())?;
        Some(CardView::lookup_face_type(card, zoned_face_id))
    }

    pub fn remove_card_as_card_view(
        &mut self, zoned_face_id: ZonedFaceID) -> Option<CardView> {

        let card = self.remove_card(zoned_face_id.zoned_card_id())?;
        Some(CardView::new(card, zoned_face_id))
    }

    pub fn lookup_multiple_cards(
        &self,
        meta_card_ids: BTreeSet<ZonedCardID>,
    ) -> Vec<(ZonedCardID, &Card)> {

        meta_card_ids.into_iter()
            .filter_map(|id| self.lookup_card(id).map(|card| (id, card)))
            .collect()
    }

    pub fn play_card_as_non_spell(
        &mut self, owner_id: PlayerID, controller_id: PlayerID, card: Card) {

        if card.is_land() {
            // TODO: What happens to this count if the land is redirected to a
            // different zone?
            self.turns.current_turn_mut().increment_lands_played_count();
        }

        self.add_card_to_battlefield_if_permanent(owner_id, controller_id, card);
    }

    // TODO: Support separate owner and controller IDs here.
    pub fn cast_spell(
        &mut self,
        owner_id: PlayerID,
        controller_id: PlayerID,
        card_view: CardView,
        ephemeral_effects: Vec<Effect>,
    ) {
        {
            let casting_cost = card_view.view_face().maybe_casting_cost()
                .expect("To cast a spell it must have a casting cost.");

            // TODO: Select modes.
            // TODO: Select targets.
            // TODO: Divide/distribute effects.
            // TODO: Determine total cost.
            // TODO: Pay additional and alternative costs too.
            self.lookup_player_mut(controller_id).mana_pool
                .pay_cost(casting_cost.mana())
                .expect("Not enough mana in the mana pool to pay the cost");
        }

        let _ = self.stack.push(
            owner_id,
            controller_id,
            StackObjectType::Spell {
                card_view: Box::new(card_view),
                overridden_success_destination: None,
                overridden_failure_destination: None,
                was_kicked: false,
            },
            ephemeral_effects,
            IsSplitSecond::NotSplitSecond,
        );

        // TODO: Apply spell modifiers that occur when a spell is cast.
        // TODO: Spell becomes cast.
        // TODO: Add spell cast triggers and put on stack triggers.
    }

    fn resolve_stack_object(&mut self, item: StackObject) {
        interpreter::interpret_ephemeral_effects(
            item.ephemeral_effects,
            item.controller_id,
            None,
            self,
        );

        match item.object_type {
            StackObjectType::Spell {card_view, overridden_success_destination, ..} => {

                println!("Resolving spell!");
                let card = card_view.into_card();
                if let Some(dest) = overridden_success_destination {
                    self.add_card(item.owner_id, dest, card);
                } else {
                    self.add_card_to_default_after_stack_zone(
                        item.owner_id,
                        item.controller_id,
                        card,
                    );
                }
            },
            StackObjectType::SpellCopy => unimplemented!(),
            StackObjectType::Ability => unimplemented!(),
            StackObjectType::ObjectTriggeredAbility {..} => unimplemented!(),
        }
    }

    pub fn add_card(
        &mut self,
        owner_id: PlayerID,
        destination: CardLocation,
        card: Card,
    ) -> &Card {

        let player = self.lookup_player_mut(owner_id);
        use location::location::CardLocation::*;
        match destination {
            Hand => player.hand.add(card),
            Graveyard => player.graveyard.add(card),
            Exile => player.exile.add(card),
            Library(library_location) => player.library.add(library_location, card),
        }
    }

    fn add_card_to_default_after_stack_zone(
        &mut self, owner_id: PlayerID, controller_id: PlayerID, card: Card) {

        match &card {
            Card::Ephemeral(_) => {
                self.lookup_player_mut(owner_id).graveyard.add(card);
            },
            Card::Permanent(_) => {
                self.add_card_to_battlefield_if_permanent(
                    owner_id,
                    controller_id,
                    card);
            }
        }
    }

    pub fn add_card_to_battlefield_if_permanent(
        &mut self,
        owner_id: PlayerID,
        controller_id: PlayerID,
        card: Card,
    ) {

        use permanent::permanent_card::PermanentCard::*;
        let permanent_layout = match card {
            Card::Ephemeral(..) => {
                // Ephemerals remain in their original zone.
                return;
            },
            Card::Permanent(permanent) => match permanent {
                RegularPermanent(perm) => PermanentLayout::Regular(perm),
                Land(perm) => PermanentLayout::Land(perm),
                DoubleFaced(perm) => PermanentLayout::DoubleFaced(perm),
                DoubleFacedLand(perm) => PermanentLayout::DoubleFacedLand(perm),
                Flip(perm) => PermanentLayout::Flip(perm),
            },
        };

        self.battlefield.add_permanent(owner_id, controller_id, permanent_layout);
    }

    pub fn active_player(&self) -> &PlayerState {
        let current = self.turns.current_turn().player_id();
        self.lookup_player(current)
    }

    pub fn active_player_mut(&mut self) -> &mut PlayerState {
        let current = self.turns.current_turn().player_id();
        self.lookup_player_mut(current)
    }

    pub fn non_active_player(&mut self) -> &mut PlayerState {
        let other = self.turns.current_turn().player_id().opponent();
        self.lookup_player_mut(other)
    }

    pub fn bounce(&mut self, permanent_id: PermanentID) -> Option<Vec<&Card>> {
        let permanent = self.battlefield.remove(permanent_id)?;
        let result = self.lookup_player_mut(permanent.owner_id()).hand
            .add_all(Game::permanent_to_cards(permanent));
        Some(result)
    }

    pub fn bury(&mut self, permanent_id: PermanentID) -> Option<Vec<&Card>> {
        let permanent = self.battlefield.remove(permanent_id)?;
        let result = self.lookup_player_mut(permanent.owner_id()).graveyard
            .add_all(Game::permanent_to_cards(permanent));
        Some(result)
    }

    pub fn exile_from_battlefield(&mut self, permanent_id: PermanentID) -> Option<Vec<&Card>> {
        let permanent = self.battlefield.remove(permanent_id)?;
        let result = self.lookup_player_mut(permanent.owner_id()).exile
            .add_all(Game::permanent_to_cards(permanent));
        Some(result)
    }

    pub fn battlefield_to_library(
        &mut self,
        library_location: LibraryLocation,
        permanent_id: PermanentID,
    ) -> Option<Vec<&Card>> {

        let permanent = self.battlefield.remove(permanent_id)?;
        let result = self.lookup_player_mut(permanent.owner_id()).library
            .add_all(library_location, Game::permanent_to_cards(permanent));
        Some(result)
    }

    pub fn priority_player(&self) -> &PlayerState {
        let id = self.priority_player_id;
        self.lookup_player(id)
    }

    pub fn priority_player_mut(&mut self) -> &mut PlayerState {
        let id = self.priority_player_id;
        self.lookup_player_mut(id)
    }

    pub fn lookup_player(&self, id: PlayerID) -> &PlayerState {
        if id == PlayerID::Player0 {
            &self.player0
        } else {
            &self.player1
        }
    }

    pub fn lookup_player_mut(&mut self, id: PlayerID) -> &mut PlayerState {
        if id == PlayerID::Player0 {
            &mut self.player0
        } else {
            &mut self.player1
        }
    }

    // A single permanent is zero cards in the case of a token or similar,
    // two cards in the case of a meld card,
    // and one card in every other case.
    fn permanent_to_cards(permanent: Permanent) -> Vec<Card> {
        let perm = permanent;
        use permanent::permanent_layout::PermanentLayout::*;
        let permanent_cards = match perm.into_permanent_layout() {
            Regular(regular) => vec![PermanentCard::RegularPermanent(regular)],
            Land(land) => { vec![PermanentCard::Land(land)] },
            DoubleFaced(double_faced) => vec![PermanentCard::DoubleFaced(double_faced)],
            DoubleFacedLand(double_faced) => vec![PermanentCard::DoubleFacedLand(double_faced)],
            Flip(flip) => vec![PermanentCard::Flip(flip)],
            Melded {top, bottom, ..} => {
                vec![PermanentCard::RegularPermanent(top), PermanentCard::RegularPermanent(bottom)]
            },
            FaceDown {front, ..} => vec![front],
            Token(_) => vec![],
            CostedToken(_) => vec![],
        };

        permanent_cards.into_iter()
            .map(Card::Permanent)
            .collect()
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum AdvanceResult {
    Continue(ContinueResult),
    PlayerWins(PlayerID),
    PlayerLoses(PlayerID),
    Tie,
    GameAlreadyOver,
}

impl AdvanceResult {
    pub fn is_continue(&self) -> bool {
        if let AdvanceResult::Continue{..} = self {
            true
        } else {
            false
        }
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum ContinueResult {
    NoUpdate,
    ZoneStateUpdated,
    PriorityPeriod(PriorityPeriodResult),
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct PriorityPeriodResult {
    pub selected_option: Option<PlayerOption>,
    pub options: Vec<PlayerOption>,
}

#[cfg(test)]
mod test {
    use card::card::Card;
    use deck::deck::Deck;
    use face::face::*;
    use game::{Game, AdvanceResult};
    use permanent::face::land_face::LandFace;
    use permanent::face::permanent_face_builder::PermanentFaceBuilder;
    use permanent::permanent_card::PermanentCard;
    use permanent::permanent_layout::PermanentLayout;
    use permanent::permanent_type::artifact::*;
    use player::player_id::PlayerID;
    use turn::step::*;
    use util::positive_integer::PositiveInteger;

    fn land_face() -> LandFace {
        let card = Card::forest(
            Expansion::Invasion,
            CardNumber::new(PositiveInteger::one()),
            ArtistName::from_str("Artist").unwrap(),
        );

        if let Card::Permanent(PermanentCard::Land(face)) = card {
            face
        } else {
            panic!()
        }
    }

    #[test]
    fn untap_step() {
        let mut game = Game::empty();

        let land_id = game.battlefield.add_permanent(
            PlayerID::Player0, PlayerID::Player0, PermanentLayout::Land(land_face())).id();

        let creature_id = game.battlefield.add_permanent(
            PlayerID::Player0,
            PlayerID::Player0,
            PermanentLayout::Regular(PermanentFaceBuilder::new()
                .name("Grizzly Bear")
                .expansion(Expansion::Invasion)
                .card_number(1)
                .artist_name("Artist")
                .no_flavor_text()
                .not_snow()
                .non_legendary()
                .rarity(Rarity::Common)
                .casting_cost("1G")
                .creature("2/2 Bear")
                .into_costed_permanent_face()
            ),
        ).id();

        let artifact_id = game.battlefield.add_permanent(
            PlayerID::Player0,
            PlayerID::Player0,
            PermanentLayout::Regular(PermanentFaceBuilder::new()
                .name("Golgari Signet")
                .expansion(Expansion::Invasion)
                .card_number(2)
                .rarity(Rarity::Common)
                .artist_name("Artist")
                .no_flavor_text()
                .casting_cost("3")
                .not_snow()
                .non_legendary()
                .artifact(ArtifactType::Regular)
                .into_costed_permanent_face()
            ),
        ).id();

        let enemy_artifact_id = game.battlefield.add_permanent(
            PlayerID::Player1,
            PlayerID::Player1,
            PermanentLayout::Regular(PermanentFaceBuilder::new()
                .name("Izzet Signet")
                .expansion(Expansion::Invasion)
                .card_number(3)
                .rarity(Rarity::Common)
                .artist_name("Artist")
                .no_flavor_text()
                .casting_cost("3")
                .not_snow()
                .non_legendary()
                .artifact(ArtifactType::Regular)
                .into_costed_permanent_face()
            ),
        ).id();

        assert_eq!(game.turns.current_turn().current_step_name(), StepName::Untap);
        assert!(game.battlefield.get(land_id).expect("Land missing").is_tapped());
        assert!(game.battlefield.get(creature_id).expect("Creature missing").is_tapped());
        assert!(game.battlefield.get(artifact_id).expect("Artifact missing").is_untapped());
        assert!(game.battlefield.get(enemy_artifact_id).expect("Enemy artifact missing").is_tapped());

        game.advance();

        assert_eq!(game.turns.current_turn().current_step_name(), StepName::Upkeep);
        // Tapped permanents should become untapped.
        assert!(game.battlefield.get(land_id).expect("Land missing").is_untapped());
        assert!(game.battlefield.get(creature_id).expect("Creature missing").is_untapped());
        // The state of already untapped permanents shouldn't change.
        assert!(game.battlefield.get(artifact_id).expect("Artifact missing").is_untapped());
        // Enemy permanents shouldn't be untapped.
        assert!(game.battlefield.get(enemy_artifact_id).expect("Enemy artifact missing").is_tapped());
    }

    #[test]
    fn draw_step() {
        let forest = Card::Permanent(PermanentCard::Land(land_face()));
        let deck0 = Deck::new(vec![(forest.clone(), 1)]);
        let mut game = Game::empty_hand(deck0, Deck::new(vec![]));

        assert_eq!(game.player0.hand.size(), 0, "Initial bad hand size");
        game.advance();
        game.advance();
        assert_eq!(game.turns.current_turn().current_step_name(), StepName::Draw);
        assert_eq!(game.player0.hand.size(), 0, "Bad hand size");
        assert_eq!(game.player0.library.size(), 1, "Bad library size");
        let step_outcome = game.advance();
        assert!(step_outcome.is_continue());
        assert_eq!(game.player0.hand.size(), 1, "Bad hand size");
        assert_eq!(game.player0.library.size(), 0, "Bad library size");
        assert_eq!(game.turns.current_turn().current_step_name(), StepName::PreCombatMain);
    }

    #[test]
    fn lose_when_drawing_last_card() {
        let mut game = Game::empty();
        game.advance();
        game.advance();
        assert_eq!(game.turns.current_turn().current_step_name(), StepName::Draw);
        let step_outcome = game.advance();
        assert_eq!(step_outcome, AdvanceResult::PlayerLoses(PlayerID::Player0));
        assert_eq!(game.turns.current_turn().current_step_name(), StepName::Draw);
    }
}
