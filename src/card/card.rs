use ephemeral::ephemeral_card::EphemeralCard;
use face::face::{Expansion, CardNumber, ArtistName};
use permanent::face::land_face::LandFace;
use permanent::permanent_card::PermanentCard;
use permanent::permanent_type::land::BasicLandType;
///
/// A Card represents any type of Object that is allowed within a Library
/// (Tokens aren't Cards).
///
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum Card {
    Permanent(PermanentCard),
    Ephemeral(EphemeralCard),
}

impl Card {
    pub fn plains(expansion: Expansion, card_number: CardNumber, artist_name: ArtistName) -> Card {
        Card::Permanent(PermanentCard::Land(LandFace::basic(
            "Plains", expansion, card_number, artist_name, BasicLandType::Plains)))
    }

    pub fn island(expansion: Expansion, card_number: CardNumber, artist_name: ArtistName) -> Card {
        Card::Permanent(PermanentCard::Land(LandFace::basic(
            "Island", expansion, card_number, artist_name, BasicLandType::Island)))
    }

    pub fn swamp(expansion: Expansion, card_number: CardNumber, artist_name: ArtistName) -> Card {
        Card::Permanent(PermanentCard::Land(LandFace::basic(
            "Swamp", expansion, card_number, artist_name, BasicLandType::Swamp)))
    }

    pub fn mountain(expansion: Expansion, card_number: CardNumber, artist_name: ArtistName) -> Card {
        Card::Permanent(PermanentCard::Land(LandFace::basic(
            "Mountain", expansion, card_number, artist_name, BasicLandType::Mountain)))
    }

    pub fn forest(expansion: Expansion, card_number: CardNumber, artist_name: ArtistName) -> Card {
        Card::Permanent(PermanentCard::Land(LandFace::basic(
            "Forest", expansion, card_number, artist_name, BasicLandType::Forest)))
    }

    pub fn is_permanent(&self) -> bool {
        self.is_matching_permanent(|_| true)
    }

    pub fn is_land(&self) -> bool {
        self.is_matching_permanent(PermanentCard::is_land)
    }

    pub fn is_artifact(&self) -> bool {
        self.is_matching_permanent(PermanentCard::is_artifact)
    }

    pub fn is_creature(&self) -> bool {
        self.is_matching_permanent(PermanentCard::is_creature)
    }

    pub fn is_enchantment(&self) -> bool {
        self.is_matching_permanent(PermanentCard::is_enchantment)
    }

    pub fn is_planeswalker(&self) -> bool {
        self.is_matching_permanent(PermanentCard::is_planeswalker)
    }

    pub fn is_ephemeral(&self) -> bool {
        self.is_matching_ephemeral(|_| true)
    }

    pub fn is_instant(&self) -> bool {
        self.is_matching_ephemeral(EphemeralCard::is_instant)
    }

    pub fn is_sorcery(&self) -> bool {
        self.is_matching_ephemeral(EphemeralCard::is_sorcery)
    }

    fn is_matching_permanent<F>(&self, f: F) -> bool
        where F: Fn(&PermanentCard) -> bool {

        if let Card::Permanent(perm) = self {
            f(perm)
        } else {
            false
        }
    }

    fn is_matching_ephemeral<F>(&self, f: F) -> bool
        where F: Fn(&EphemeralCard) -> bool {

        if let Card::Ephemeral(ephemeral) = self {
            f(ephemeral)
        } else {
            false
        }
    }

    pub fn converted_mana_cost(&self) -> usize {
        match self {
            Card::Permanent(permanent) => permanent.converted_mana_cost(),
            Card::Ephemeral(ephemeral) => ephemeral.converted_mana_cost(),
        }
    }

    pub fn format_display_name(&self) -> String {
        match self {
            Card::Permanent(permanent) => permanent.format_display_name(),
            Card::Ephemeral(ephemeral) => ephemeral.format_display_name(),
        }
    }
}
