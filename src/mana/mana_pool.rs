use std;
use std::fmt;
use std::collections::BTreeMap;

use mana::mana::Mana;
use mana::mana_cost::ManaCost;
use player::life_total::LifeTotal;
use util::positive_integer::PositiveInteger;

#[derive(Clone, Debug)]
pub struct ManaPool {
    regular_mana: BTreeMap<Mana, usize>,
    snow_sourced_mana: BTreeMap<Mana, usize>,
}

impl ManaPool {
    pub fn new() -> ManaPool {
        let mut pool = ManaPool {regular_mana: BTreeMap::new(), snow_sourced_mana: BTreeMap::new()};
        pool.clear();
        pool
    }

    pub fn add_mana(&mut self, mana: Mana, count: PositiveInteger) {
        self.regular_mana.entry(mana)
            .and_modify(|original_count| *original_count += count.to_usize())
            .or_insert(1);
    }

    pub fn add_snow_sourced_mana(&mut self, mana: Mana, count: PositiveInteger) {
        self.snow_sourced_mana.entry(mana)
            .and_modify(|original_count| *original_count += count.to_usize())
            .or_insert(1);
    }

    pub fn clear(&mut self) {
        self.regular_mana = empty_mana_map();
        self.snow_sourced_mana = empty_mana_map();
    }

    pub fn len(&self) -> usize {
        self.regular_mana.values().sum::<usize>() +
            self.snow_sourced_mana.values().sum::<usize>()
    }

    pub fn regular_mana_count(&self, mana: Mana) -> usize {
        *self.regular_mana.get(&mana).expect("Map set up incorrectly")
    }

    pub fn regular_mana_count_mut(&mut self, mana: Mana) -> &mut usize {
        self.regular_mana.get_mut(&mana).expect("Map set up incorrectly")
    }

    pub fn snow_sourced_mana_count(&self, mana: Mana) -> usize {
        *self.snow_sourced_mana.get(&mana).expect("Map set up incorrectly")
    }

    pub fn snow_sourced_mana_count_mut(&mut self, mana: Mana) -> &mut usize {
        self.snow_sourced_mana.get_mut(&mana).expect("Map set up incorrectly")
    }

    pub fn can_pay_cost(&self, mana_cost: &ManaCost, life_total: LifeTotal) -> bool {
        let base_payment = ManaCostPayment::from_non_hybrid_costs(mana_cost);
        if !self.is_payment_subset(&base_payment, life_total) {
            return false;
        }

        // TODO: Implement hybrid and phyrexian costs.

        true
    }

    pub fn pay_cost(&mut self, mana_cost: &ManaCost) -> Result<(), String> {
        // Pay non-generic costs first since they are the more specific constraint.
        for (mana, count) in mana_cost.regular_mana_counts() {
            self.pay(*mana, *count).expect("Failed to pay colored mana cost.");
        }

        let mut generic_count = mana_cost.generic_count();
        if generic_count == 0 {
            return Ok(());
        }

        for (mana, count) in self.regular_mana.clone() {
            let amount_to_pay = std::cmp::min(generic_count, count);
            if amount_to_pay == 0 {
                continue;
            }

            self.pay(mana, PositiveInteger::from_usize(amount_to_pay).unwrap())
                .expect("Failed to pay generic cost.");
            generic_count -= amount_to_pay;

            if generic_count == 0 {
                return Ok(());
            }
        }

        Err("Failed to pay mana cost!".to_string())
    }

    fn is_payment_subset(&self, payment: &ManaCostPayment, life_total: LifeTotal) -> bool {
        if payment.life as isize > life_total.0 || payment.snow > self.snow_sourced_mana.len() {
            return false;
        }

        let mut pool = self.clone();

        for (mana, count) in payment.mana.clone() {
            if pool.pay(mana, count).is_err() {
                return false;
            }
        }

        if pool.pay_snow(payment.snow).is_err() {
            return false;
        }

        pool.len() >= payment.generic
    }

    fn pay(&mut self, mana: Mana, count: PositiveInteger) -> Result<(), String> {
        if self.regular_mana_count(mana) + self.snow_sourced_mana_count(mana) < count.to_usize() {
            Err("Not enough mana to pay.".to_string())
        } else if self.regular_mana_count(mana) >= count.to_usize() {
            *self.regular_mana_count_mut(mana) -= count.to_usize();
            Ok(())
        } else {
            *self.snow_sourced_mana_count_mut(mana) -=
                count.to_usize() - self.regular_mana_count(mana);
            *self.regular_mana_count_mut(mana) = 0;
            Ok(())
        }
    }

    fn pay_snow(&mut self, snow_count: usize) -> Result<(), String> {
        let mut remaining_snow = snow_count;
        for (_, count) in self.snow_sourced_mana.iter_mut() {
            if remaining_snow <= *count {
                *count -= remaining_snow;
                return Ok(());
            }

            remaining_snow -= *count;
            *count = 0;
        }

        Err("Not enough snow remaining! Get back to work.".to_string())
    }
}

fn empty_mana_map() -> BTreeMap<Mana, usize> {
    let mut result = BTreeMap::new();
    for mana in Mana::manas() {
        result.insert(mana, 0);
    }

    result
}

impl fmt::Display for ManaPool {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (mana, count) in self.regular_mana.clone() {
            write!(f, "{}:{}, ", mana, count).expect("Bad fmt write.");
        }

        Ok(())
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
struct ManaCostPayment {
    mana: BTreeMap<Mana, PositiveInteger>,
    generic: usize,
    snow: usize,
    life: usize,
}

impl ManaCostPayment {
    fn from_non_hybrid_costs(mana_cost: &ManaCost) -> ManaCostPayment {
        ManaCostPayment {
            mana: mana_cost.regular_mana_counts().clone(),
            generic: mana_cost.generic_count(),
            snow: mana_cost.snow_count(),
            life: 0,
        }
    }
}

#[cfg(test)]
mod test {
    use std::collections::BTreeMap;

    use mana::color::Color;
    use mana::mana::Mana;
    use mana::mana_cost::ManaCost;
    use mana::mana_pool::ManaCostPayment;
    use mana::mana_pool::ManaPool;
    use player::life_total::LifeTotal;
    use util::positive_integer::PositiveInteger;

    #[test]
    fn payment_from_non_hybrid_costs() {
        let mut mana = BTreeMap::new();
        mana.insert(Mana::Colorless, PositiveInteger::from_usize(6).unwrap());
        mana.insert(Mana::Color(Color::White), PositiveInteger::from_usize(5).unwrap());
        mana.insert(Mana::Color(Color::Blue), PositiveInteger::from_usize(4).unwrap());
        mana.insert(Mana::Color(Color::Black), PositiveInteger::from_usize(3).unwrap());
        mana.insert(Mana::Color(Color::Red), PositiveInteger::from_usize(2).unwrap());
        mana.insert(Mana::Color(Color::Green), PositiveInteger::from_usize(1).unwrap());

        assert_eq!(
            ManaCostPayment {
                mana,
                generic: 3,
                snow: 2,
                life: 0,
            },
            ManaCostPayment::from_non_hybrid_costs(
                &ManaCost::try_from_str("3SSCCCCCCWWWWWUUUUBBBRRG").unwrap()),
        );
    }

    #[test]
    fn can_pay_with_exact_cost() {
        let pool = mana_pool_with_one_of_each_type();
        assert_eq!(6, pool.len());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("CWUBRG").unwrap(), LifeTotal(20)));
        assert_eq!(6, pool.len());
    }

    #[test]
    fn can_pay_with_exact_cost_with_generic() {
        let mut pool = mana_pool_with_one_of_each_type();
        pool.add_mana(Mana::Color(Color::Black), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("1CWUBRG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_with_exact_cost_with_only_generics() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("6").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_with_more_than_enough_mana() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("1WUB").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn cannot_pay_without_enough_mana() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(!pool.can_pay_cost(&ManaCost::try_from_str("CCWUBRG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn cannot_pay_with_wrong_mana() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(!pool.can_pay_cost(&ManaCost::try_from_str("CWWBRG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_zero_cost() {
        let pool = ManaPool::new();
        assert!(pool.can_pay_cost(&ManaCost::zero(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_zero_cost_with_non_empty_pool() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(pool.can_pay_cost(&ManaCost::zero(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_single_generic_cost() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Black), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("0").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_single_color_cost() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Black), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("B").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_mono_color_cost() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::from_usize(5).unwrap());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("GGGGG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn cannot_pay_wrong_mono_color_cost() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::from_usize(5).unwrap());
        assert!(!pool.can_pay_cost(&ManaCost::try_from_str("UUUUU").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_regular_cost_with_snow() {
        let pool = mana_pool_with_one_of_each_type_snow_sourced();
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("CWUBRG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_snow() {
        let mut pool = mana_pool_with_one_of_each_type_snow_sourced();
        pool.add_snow_sourced_mana(Mana::Color(Color::Green), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("SSSWBR").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_only_one_snow_symbol() {
        let mut pool = ManaPool::new();
        pool.add_snow_sourced_mana(Mana::Color(Color::White), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("S").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn cannot_pay_snow_with_regular() {
        let pool = mana_pool_with_one_of_each_type();
        assert!(!pool.can_pay_cost(&ManaCost::try_from_str("S").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_with_regular_and_snow() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Green), PositiveInteger::from_usize(2).unwrap());
        pool.add_snow_sourced_mana(Mana::Color(Color::Black), PositiveInteger::one());
        assert!(pool.can_pay_cost(&ManaCost::try_from_str("SGGG").unwrap(), LifeTotal(20)));
    }

    #[test]
    fn can_pay_with_regular_and_snow_complex() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Colorless, PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::White), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Blue), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Black), PositiveInteger::one());
        // No red here.
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::one());

        assert_eq!(5, pool.len());

        pool.add_snow_sourced_mana(Mana::Colorless, PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::White), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Blue), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Black), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Red), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Green), PositiveInteger::one());

        assert_eq!(11, pool.len());

        assert!(pool.can_pay_cost(&ManaCost::try_from_str("2SSSCCWWRB").unwrap(), LifeTotal(20)));

        assert_eq!(11, pool.len());
    }

    #[test]
    fn pay_green_cost() {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::one());
        pool.add_mana(Mana::Colorless, PositiveInteger::one());

        assert!(pool.pay_cost(&ManaCost::try_from_str("1G").unwrap()).is_ok());
    }

    fn mana_pool_with_one_of_each_type() -> ManaPool {
        let mut pool = ManaPool::new();
        pool.add_mana(Mana::Colorless, PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::White), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Blue), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Black), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Red), PositiveInteger::one());
        pool.add_mana(Mana::Color(Color::Green), PositiveInteger::one());
        pool
    }

    fn mana_pool_with_one_of_each_type_snow_sourced() -> ManaPool {
        let mut pool = ManaPool::new();
        pool.add_snow_sourced_mana(Mana::Colorless, PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::White), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Blue), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Black), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Red), PositiveInteger::one());
        pool.add_snow_sourced_mana(Mana::Color(Color::Green), PositiveInteger::one());
        pool
    }
}