use std::fmt;

use mana::color::Color;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum Mana {
    Colorless,
    Color(Color),
}

impl Mana {
    pub fn try_from_char(raw: char) -> Result<Mana, String> {
        match raw {
            'C' => Ok(Mana::Colorless),
            _   => Ok(Mana::Color(Color::try_from_char(raw)?)),
        }
    }

    pub fn manas() -> Vec<Mana> {
        use mana::mana::Mana::*;
        use mana::color::Color::*;
        vec![Colorless, Color(White), Color(Blue), Color(Black), Color(Red), Color(Green)]
    }

    pub fn to_color(self) -> Option<Color> {
        match self {
            Mana::Colorless => None,
            Mana::Color(color) => Some(color),
        }
    }
}

impl fmt::Display for Mana {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Mana::Colorless => write!(f, "C"),
            Mana::Color(color)=> write!(f, "{}", color),
        }
    }
}
