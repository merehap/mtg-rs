use mana::color::Color;
use mana::mana_group::ManaGroup;
use script::variable::color_variable::ColorVariable;
use script::variable::colors_variable::ColorsVariable;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum ManaProduced {
    Regular(ManaGroup),
    Variable(ColorVariable),
    ChooseOne(Vec<ManaGroup>),
    ChooseOneMatchingAvailable(ColorsVariable),
}

impl ManaProduced {
    pub fn choose_one_of_any_color() -> ManaProduced {
        ManaProduced::ChooseOne(
            Color::colors().into_iter().map(ManaGroup::from_color).collect()
        )
    }

    pub fn from_str(raw: &str) -> Result<ManaProduced, String> {
        let segments: Vec<&str> = raw.split(" or ").collect();

        if segments.is_empty() {
            return Err("No mana specified to preduce.".to_string());
        }

        if segments.len() == 1 {
            return Ok(ManaProduced::Regular(ManaGroup::try_from_str(segments[0].to_string())?));
        }

        let mut choices = Vec::new();
        for segment in segments {
            choices.push(ManaGroup::try_from_str(segment.to_string())?);
        }

        Ok(ManaProduced::ChooseOne(choices))
    }
}