use mana::color::Color;
use mana::hybrid::Hybrid;
use mana::mana::Mana;

// Note: It would be possible to use "Mana" instead of "Color" for all
// instances here, but in the absence of cards that use Colorless or Snow
// in compound symbols, it would be adding more complexity for nothing.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum ManaSymbol {
    Generic(usize),
    Single(Mana),
    Hybrid(Hybrid),
    GenericHybrid(Color),
    Phyrexian(Color),
    Snow,
}

impl ManaSymbol {
    pub fn try_from_str_head(raw: String) -> Result<(ManaSymbol, String), String> {
        let chars = raw.chars().collect::<Vec<char>>();
        match chars.as_slice() {
            [] => Err("Empty str.".to_string()),
            ['X', ..] | ['Y', ..] | ['Z', ..] =>
                Err("Mana variables are not part of the standard mana cost.".to_string()),
            ['S', rem @ ..] =>
                Ok((ManaSymbol::Snow, convert(rem))),
            ['{', color, '/', 'P', '}', rem @ ..] =>
                Ok((ManaSymbol::Phyrexian(Color::try_from_char(*color)?), convert(rem))),
            ['{', '2', '/', color, '}', rem @ ..] =>
                Ok((ManaSymbol::GenericHybrid(Color::try_from_char(*color)?), convert(rem))),
            ['{', color0, '/', color1, '}', rem @ ..] =>
                Ok((ManaSymbol::Hybrid(Hybrid::new(
                    Color::try_from_char(*color0)?,
                    Color::try_from_char(*color1)?)?),
                    convert(rem))),
            [mana, rem @ ..] if Mana::try_from_char(*mana).is_ok() =>
                Ok((ManaSymbol::Single(Mana::try_from_char(*mana)?), convert(rem))),
            [digit, ..] if digit.is_numeric() => {
                let digits = raw.chars()
                    .take_while(|d| d.is_numeric())
                    .collect::<String>();
                let count = digits.parse::<usize>().expect("Failed to parse Generic count.");
                Ok((ManaSymbol::Generic(count), raw.chars().skip(digits.len()).collect::<String>()))
            },
            _ => Err(format!("Invalid ManaCost pattern: {} .", raw)),
        }
    }

    pub fn converted_mana_cost(&self) -> usize {
        use self::ManaSymbol::*;
        match *self {
            Generic(ref count) => *count,
            Single(..) => 1,
            Hybrid(..) => 1,
            GenericHybrid(..) => 2,
            Phyrexian(..) => 1,
            Snow => 1,
        }
    }
}

fn convert(slice: &[char]) -> String {
    slice.iter().collect::<String>()
}
#[cfg(test)]
mod test {
    use mana::color::Color;
    use mana::hybrid::Hybrid;
    use mana::mana::Mana;
    use mana::mana_symbol::ManaSymbol;

    #[test]
    fn empty() {
        assert!(ManaSymbol::try_from_str_head("".to_string()).is_err())
    }

    #[test]
    fn no_variables_allowed() {
        assert!(ManaSymbol::try_from_str_head("X".to_string()).is_err())
    }

    #[test]
    fn bad_char() {
        assert!(ManaSymbol::try_from_str_head("/".to_string()).is_err())
    }

    #[test]
    fn lower_case() {
        assert!(ManaSymbol::try_from_str_head("w".to_string()).is_err())
    }

    #[test]
    fn singles() {
        assert_eq!(
            Ok((ManaSymbol::Single(Mana::Color(Color::White)), "".to_string())),
            ManaSymbol::try_from_str_head("W".to_string()));
        assert_eq!(
            Ok((ManaSymbol::Single(Mana::Color(Color::Blue)), "".to_string())),
            ManaSymbol::try_from_str_head("U".to_string()));
        assert_eq!(
            Ok((ManaSymbol::Single(Mana::Color(Color::Black)), "".to_string())),
            ManaSymbol::try_from_str_head("B".to_string()));
        assert_eq!(
            Ok((ManaSymbol::Single(Mana::Color(Color::Red)), "".to_string())),
            ManaSymbol::try_from_str_head("R".to_string()));
        assert_eq!(
            Ok((ManaSymbol::Single(Mana::Color(Color::Green)), "".to_string())),
            ManaSymbol::try_from_str_head("G".to_string()));
    }

    #[test]
    fn snow() {
        assert_eq!(
            Ok((ManaSymbol::Snow, "".to_string())),
            ManaSymbol::try_from_str_head("S".to_string()))
    }

    #[test]
    fn phyrexian() {
        assert_eq!(
            Ok((ManaSymbol::Phyrexian(Color::White), "".to_string())),
            ManaSymbol::try_from_str_head("{W/P}".to_string()))
    }

    #[test]
    fn generic_hybrid() {
        assert_eq!(
            Ok((ManaSymbol::GenericHybrid(Color::Green), "".to_string())),
            ManaSymbol::try_from_str_head("{2/G}".to_string()))
    }

    #[test]
    fn hybrid() {
        assert_eq!(
            Ok((ManaSymbol::Hybrid(Hybrid::new(Color::Blue, Color::Red).unwrap()), "".to_string())),
            ManaSymbol::try_from_str_head("{U/R}".to_string())
        )
    }

    #[test]
    fn generic() {
        assert_eq!(
            Ok((ManaSymbol::Generic(3), "".to_string())),
            ManaSymbol::try_from_str_head("3".to_string())
        );
    }

    #[test]
    fn long_generic() {
        assert_eq!(
            Ok((ManaSymbol::Generic(11), "".to_string())),
            ManaSymbol::try_from_str_head("11".to_string())
        );
    }

    #[test]
    fn just_the_tip() {
        assert_eq!(
            Ok((ManaSymbol::Phyrexian(Color::White), "5GRBCS".to_string())),
            ManaSymbol::try_from_str_head("{W/P}5GRBCS".to_string())
        );
    }

    #[test]
    fn bad_tail() {
        assert_eq!(
            Ok((ManaSymbol::Phyrexian(Color::White), "garbage".to_string())),
            ManaSymbol::try_from_str_head("{W/P}garbage".to_string())
        );
    }
}
