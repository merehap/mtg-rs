use std::cmp::Ordering;

use mana::color::Color;

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Hybrid {
    first_color: Color,
    second_color: Color,
}

impl Hybrid {
    pub fn new(first_color: Color, second_color: Color) -> Result<Hybrid, String> {
        match first_color.cmp(&second_color) {
            Ordering::Equal =>
                Err("Hybrid colors can't be the same.".to_string()),
            Ordering::Greater =>
                Err("Bad ordering of hybrid colors. Must adhere to WUBRG.".to_string()),
            Ordering::Less =>
                Ok(Hybrid {first_color, second_color})
        }
    }

    pub fn hybrids() -> Vec<Hybrid> {
        let mut hybrids = Vec::new();
        for first_color in Color::colors() {
            for second_color in Color::colors() {
                if first_color < second_color {
                    hybrids.push(
                        Hybrid::new(first_color, second_color).expect("Bug in hybrids()"));
                }
            }
        }

        hybrids
    }

    pub fn first_color(&self) -> Color {
        self.first_color
    }

    pub fn second_color(&self) -> Color {
        self.second_color
    }
}

#[cfg(test)]
mod test {
    use mana::color::Color;
    use mana::hybrid::Hybrid;

    #[test]
    fn correctly_ordered() {
        assert!(Hybrid::new(Color::White, Color::Blue).is_ok());
    }

    #[test]
    fn incorrectly_ordered() {
        assert!(Hybrid::new(Color::Green, Color::Red).is_err());
    }

    #[test]
    fn same() {
        assert!(Hybrid::new(Color::Black, Color::Black).is_err());
    }
}