use std::collections::{BTreeMap, BTreeSet};

use mana::color::Color;
use mana::hybrid::Hybrid;
use util::positive_integer::PositiveInteger;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct HybridManaMap {
    raw: BTreeMap<Color, BTreeMap<Color, PositiveInteger>>,
}

impl HybridManaMap {
    pub fn new() -> HybridManaMap {
        HybridManaMap {raw: BTreeMap::new()}
    }

    pub fn colors(&self) -> BTreeSet<Color> {
        let mut colors = BTreeSet::new();
        colors.extend(self.raw.keys());
        for value in self.raw.values() {
            colors.extend(value.keys());
        }

        colors.into_iter().cloned().collect()
    }

    pub fn counts(&self) -> BTreeMap<Hybrid, PositiveInteger> {
        let mut result = BTreeMap::new();

        for (first, second_counts) in &self.raw {
            for (second, count) in second_counts {
                result.insert(
                    Hybrid::new(*first, *second).expect("Invalid Hybrid stored."),
                    *count
                );
            }
        }

        result
    }

    // The total count of mana help in this collection.
    pub fn len(&self) -> usize {
        self.raw.values()
            .flat_map(|value| value.values())
            .fold(0, |acc, v| acc + v.to_usize())
    }

    pub fn insert(&mut self, count: PositiveInteger, hybrid: Hybrid) {
        self.raw.entry(hybrid.first_color()).or_insert_with(BTreeMap::new);

        self.raw.get_mut(&hybrid.first_color()).unwrap().entry(hybrid.second_color())
            .and_modify(|inner| *inner += count)
            .or_insert(count);
    }

    pub fn append(&mut self, other: HybridManaMap) {
        for (hybrid, count) in other.counts() {
            self.insert(count, hybrid);
        }
    }

    pub fn concat(lhs: HybridManaMap, rhs: HybridManaMap) -> HybridManaMap {
        let mut hybrid_mana_counts = HybridManaMap::new();
        hybrid_mana_counts.append(lhs);
        hybrid_mana_counts.append(rhs);
        hybrid_mana_counts
    }
}

#[cfg(test)]
mod test {
    use std::collections::BTreeSet;

    use mana::color::Color;
    use mana::hybrid::Hybrid;
    use mana::hybrid_mana_map::HybridManaMap;
    use util::positive_integer::PositiveInteger;

    #[test]
    fn insert_one() {
        let mut map = HybridManaMap::new();
        assert_eq!(0, map.len());
        map.insert(PositiveInteger::one(), Hybrid::new(Color::White, Color::Black).expect("Bad hybrid setup."));
        assert_eq!(1, map.len());
        map.raw
            .get(&Color::White).expect("First key missing.")
            .get(&Color::Black).expect("Second key missing.");
    }

    #[test]
    fn insert_all() {
        let mut map = HybridManaMap::new();
        for hybrid in Hybrid::hybrids() {
            map.insert(PositiveInteger::one(), hybrid);
        }

        // The 11th entry, and the first duplicate.
        map.insert(PositiveInteger::one(), Hybrid::new(Color::White, Color::Black).expect("Bad hybrid setup."));

        assert_eq!(11, map.len());
    }

    #[test]
    fn colors() {
        let mut map = HybridManaMap::new();
        map.insert(PositiveInteger::one(), Hybrid::new(Color::White, Color::Black).expect("Bad hybrid setup."));
        map.insert(PositiveInteger::one(), Hybrid::new(Color::Blue, Color::Black).expect("Bad hybrid setup."));
        map.insert(PositiveInteger::one(), Hybrid::new(Color::Blue, Color::Red).expect("Bad hybrid setup."));
        map.insert(PositiveInteger::one(), Hybrid::new(Color::White, Color::Black).expect("Bad hybrid setup."));

        let colors: BTreeSet<Color> =
            vec![Color::White, Color::Blue, Color::Red, Color::Black].into_iter().collect();
        assert_eq!(colors, map.colors());
    }
}
