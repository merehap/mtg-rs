use std::collections::BTreeMap;

use mana::color::Color;
use mana::mana::Mana;
use util::positive_integer::PositiveInteger;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct ManaGroup {
    mana_group: BTreeMap<Mana, PositiveInteger>,
}

impl ManaGroup {
    pub fn new(mana_vec: Vec<Mana>) -> ManaGroup {
        let mut mana_group = BTreeMap::new();
        for mana in mana_vec {
            mana_group.entry(mana)
                .and_modify(|count| *count += PositiveInteger::one())
                .or_insert_with(PositiveInteger::one);
        }

        ManaGroup {mana_group}
    }

    pub fn from_single(mana: Mana) -> ManaGroup {
        let mut mana_group = BTreeMap::new();
        mana_group.insert(mana, PositiveInteger::one());
        ManaGroup {mana_group}
    }

    pub fn from_color(color: Color) -> ManaGroup {
        ManaGroup::from_single(Mana::Color(color))
    }

    pub fn try_from_str(raw: String) -> Result<ManaGroup, String> {
        let mut mana_vec = Vec::new();
        for c in raw.chars() {
            mana_vec.push(Mana::try_from_char(c)?);
        }

        Ok(ManaGroup::new(mana_vec))
    }
}

#[cfg(test)]
mod test {
    use std::collections::BTreeMap;

    use mana::color::Color;
    use mana::mana::Mana;
    use mana::mana_group::ManaGroup;
    use util::positive_integer::PositiveInteger;

    #[test]
    fn from_color() {
        let mut mana_group = BTreeMap::new();
        mana_group.insert(Mana::Color(Color::White), PositiveInteger::one());
        assert_eq!(ManaGroup {mana_group}, ManaGroup::from_color(Color::White));
    }

    #[test]
    fn new_mana_group() {
        let manas = vec![
            Mana::Color(Color::White), Mana::Color(Color::White), Mana::Colorless,
            Mana::Color(Color::Blue), Mana::Color(Color::Black),
        ];

        let mana_group = ManaGroup::new(manas);

        let mut raw_mana_group = BTreeMap::new();
        raw_mana_group.insert(Mana::Color(Color::White), PositiveInteger::from_usize(2).unwrap());
        raw_mana_group.insert(Mana::Colorless, PositiveInteger::one());
        raw_mana_group.insert(Mana::Color(Color::Blue), PositiveInteger::one());
        raw_mana_group.insert(Mana::Color(Color::Black), PositiveInteger::one());
        assert_eq!(ManaGroup {mana_group: raw_mana_group}, mana_group);
    }

    #[test]
    fn try_from_str()  {
        let mut raw_mana_group = BTreeMap::new();
        raw_mana_group.insert(Mana::Color(Color::White), PositiveInteger::from_usize(2).unwrap());
        raw_mana_group.insert(Mana::Colorless, PositiveInteger::one());
        raw_mana_group.insert(Mana::Color(Color::Blue), PositiveInteger::one());
        raw_mana_group.insert(Mana::Color(Color::Black), PositiveInteger::one());
        assert_eq!(
            Ok(ManaGroup {mana_group: raw_mana_group}),
            ManaGroup::try_from_str("CWWUB".to_string()));
    }

    #[test]
    fn try_from_str_with_generic()  {
        assert!(ManaGroup::try_from_str("1CWWUB".to_string()).is_err());
    }
}