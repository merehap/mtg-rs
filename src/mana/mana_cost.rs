use std::collections::{BTreeMap, BTreeSet};
use std::ops::Add;

use mana::color::Color;
use mana::hybrid_mana_map::HybridManaMap;
use mana::mana::Mana;
use mana::mana_symbol::ManaSymbol;
use util::positive_integer::PositiveInteger;
use util::collections::concat_maps;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct ManaCost {
    generic_count: usize,
    snow_count: usize,
    regular_mana_counts: BTreeMap<Mana, PositiveInteger>,
    phyrexian_counts: BTreeMap<Color, PositiveInteger>,
    generic_hybrid_counts: BTreeMap<Color, PositiveInteger>,
    hybrid_mana_counts: HybridManaMap,
}

impl ManaCost {
    pub fn new(symbols: Vec<ManaSymbol>) -> Result<ManaCost, String> {
        let mut cost = ManaCost {
            generic_count: 0,
            snow_count: 0,
            regular_mana_counts: BTreeMap::new(),
            phyrexian_counts: BTreeMap::new(),
            generic_hybrid_counts: BTreeMap::new(),
            hybrid_mana_counts: HybridManaMap::new(),
        };

        let mut generic_is_set = false;
        // TODO: Instead of using a for loop, validate the order that the symbols are in.
        for symbol in symbols {
            use mana::mana_symbol::ManaSymbol::*;
            match symbol {
                Generic(count) => {
                    if generic_is_set {
                        return Err("Generic mana must only occur at most once.".to_string());
                    }

                    cost.generic_count = count;
                    generic_is_set = true;
                },
                Single(mana) => {
                    cost.regular_mana_counts.entry(mana)
                        .and_modify(|count| *count += PositiveInteger::one())
                        .or_insert_with(PositiveInteger::one);
                },
                Hybrid(hybrid) => {
                    cost.hybrid_mana_counts.insert(PositiveInteger::one(), hybrid);
                },
                GenericHybrid(color) => {
                    cost.generic_hybrid_counts.entry(color)
                        .and_modify(|count| *count += PositiveInteger::one())
                        .or_insert_with(PositiveInteger::one);
                },
                Phyrexian(color) => {
                    cost.phyrexian_counts.entry(color)
                        .and_modify(|count| *count += PositiveInteger::one())
                        .or_insert_with(PositiveInteger::one);
                },
                Snow => cost.snow_count += 1,
            }
        }

        Ok(cost)
    }

    pub fn zero() -> ManaCost {
        ManaCost::new(Vec::new()).expect("ManaCost::new is broken.")
    }

    pub fn try_from_str(raw: &str) -> Result<ManaCost, String> {
        let mut symbols = vec![];

        let mut remainder = raw.to_string();
        while !remainder.is_empty() {
            let (symbol, rem) = ManaSymbol::try_from_str_head(remainder)?;
            symbols.push(symbol);
            remainder = rem;
        }

        ManaCost::new(symbols)
    }

    pub fn regular_mana_counts(&self) -> &BTreeMap<Mana, PositiveInteger> {
        &self.regular_mana_counts
    }

    pub fn generic_count(&self) -> usize {
        self.generic_count
    }

    pub fn snow_count(&self) -> usize {
        self.snow_count
    }

    pub fn to_colors(&self) -> BTreeSet<Color> {
        let mut colors: BTreeSet<Color> = BTreeSet::new();
        colors.extend(self.regular_mana_counts.keys().cloned().filter_map(Mana::to_color));
        colors.extend(self.phyrexian_counts.keys().cloned());
        colors.extend(self.generic_hybrid_counts.keys().cloned());
        colors.extend(self.hybrid_mana_counts.colors());

        colors
    }

    pub fn converted_mana_cost(&self) -> usize {
        self.generic_count
            + self.snow_count
            + self.regular_mana_counts.values().fold(0, |sum, value| sum + value.to_usize())
            + self.phyrexian_counts.values().fold(0, |sum, value| sum + value.to_usize())
            + self.generic_hybrid_counts.values().fold(0, |sum, value| sum + value.to_usize())
            + self.hybrid_mana_counts.len()
    }
}

impl Add for ManaCost {
    type Output = ManaCost;

    fn add(self, rhs: ManaCost) -> Self::Output {
        let lhs = self.clone();
        let rhs = rhs.clone();

        ManaCost {
            generic_count: lhs.generic_count + rhs.generic_count,
            snow_count: lhs.snow_count + rhs.snow_count,
            regular_mana_counts:
                concat_maps(lhs.regular_mana_counts, rhs.regular_mana_counts),
            phyrexian_counts:
                concat_maps(lhs.phyrexian_counts, rhs.phyrexian_counts),
            generic_hybrid_counts:
                concat_maps(lhs.generic_hybrid_counts, rhs.generic_hybrid_counts),
            hybrid_mana_counts:
                HybridManaMap::concat(lhs.hybrid_mana_counts, rhs.hybrid_mana_counts),
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn zero() {

    }

    #[test]
    fn one_of_each() {

    }

    #[test]
    fn generic_only_once() {

    }
}