use std::fmt;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum Color {
    White,
    Blue,
    Black,
    Red,
    Green,
}

impl Color {
    pub fn try_from_char(raw: char) -> Result<Color, String> {
        match raw {
            'W' => Ok(Color::White),
            'U' => Ok(Color::Blue),
            'B' => Ok(Color::Black),
            'R' => Ok(Color::Red),
            'G' => Ok(Color::Green),
            _ => Err(format!("The character '{}' does not correspond to a color.", raw)),
        }
    }

    pub fn colors() -> Vec<Color> {
        use self::Color::*;
        vec![White, Blue, Black, Red, Green]
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Color::White => write!(f, "W"),
            Color::Blue => write!(f, "U"),
            Color::Black => write!(f, "B"),
            Color::Red => write!(f, "R"),
            Color::Green => write!(f, "G"),
        }
    }
}
