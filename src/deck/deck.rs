use std::collections::BTreeMap;

use card::card::Card;
use player::player_id::PlayerID;
use util::positive_integer::PositiveInteger;
use zone::library::Library;

pub struct Deck {
    cards: BTreeMap<Card, PositiveInteger>,
}

impl Deck {
    pub fn new(card_counts: Vec<(Card, usize)>) -> Deck {
        let mut cards = BTreeMap::new();
        for (card, count) in card_counts.into_iter() {
            let count = PositiveInteger::from_usize(count)
                .expect("Count of one type of card can't be zero!");
            let name = card.format_display_name();
            if cards.insert(card, count).is_some() {
                panic!("Attempted to double insert {:?}!", name);
            }
        }

        Deck {
            cards
        }
    }

    pub fn to_random_order_library(&self, owner_id: PlayerID) -> Library {
        let mut cards = Vec::new();
        for (card, count) in self.cards.clone() {
            for _ in 0..count.to_usize() {
                cards.push(card.clone());
            }
        }

        Library::randomized_order(owner_id, cards)
    }
}