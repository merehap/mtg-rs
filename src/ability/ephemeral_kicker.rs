use cost::cost::Cost;
use effect::effect_template::EffectTemplate;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum EphemeralKicker {
    Regular(Cost),
    Multi(Cost),
    Choice(Vec<EphemeralKickerChoice>),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct EphemeralKickerChoice {
    cost: Cost,
    effects: Vec<EffectTemplate>,
}
