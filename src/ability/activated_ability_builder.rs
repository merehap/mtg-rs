use ability::activated_ability::ActivatedAbility;
use ability::activated_mana_ability::ActivatedManaAbility;
use cost::cost_builder::CostBuilder;
use cost::permanent_cost::PermanentCost;
use effect::effect_template::EffectTemplate;
use mana::mana_produced::ManaProduced;
use script::variable::color_variable::ColorVariable;

pub struct ActivatedAbilityBuilder {
    cost: Option<PermanentCost>,
    per_turn_activation_limit: Option<usize>,
    mana_produced: Option<ManaProduced>,
    speed: AbilitySpeed,
    effects: Vec<EffectTemplate>,
}

impl ActivatedAbilityBuilder {
    pub fn new() -> ActivatedAbilityBuilder {
        ActivatedAbilityBuilder {
            cost: None,
            mana_produced: None,
            per_turn_activation_limit: None,
            speed: AbilitySpeed::Instant,
            effects: Vec::new(),
        }
    }

    pub fn cost(mut self, builder: CostBuilder) -> ActivatedAbilityBuilder {
        self.cost = Some(builder.into_permanent_cost());
        self
    }

    pub fn mana_produced(mut self, mana_produced: &str) -> ActivatedAbilityBuilder {
        self.mana_produced = Some(ManaProduced::from_str(mana_produced).expect("Bad mana produced text."));
        self
    }

    pub fn variable_mana_produced(mut self, color_variable: ColorVariable) -> ActivatedAbilityBuilder {
        self.mana_produced = Some(ManaProduced::Variable(color_variable));
        self
    }

    pub fn sorcery_speed(mut self) -> ActivatedAbilityBuilder {
        self.speed = AbilitySpeed::Sorcery;
        self
    }

    pub fn add_effect(mut self, effect: EffectTemplate) -> ActivatedAbilityBuilder {
        self.effects.push(effect);
        self
    }

    pub fn per_turn_activation_limit(mut self, count: usize) -> ActivatedAbilityBuilder {
        self.per_turn_activation_limit = Some(count);
        self
    }

    pub fn into_ability(self) -> ActivatedAbility {
        if self.mana_produced.is_some() {
            panic!("Normal abilities don't produce mana, use into_mana_ability() instead.");
        }

        ActivatedAbility {
            cost: self.cost.expect("ActivatedManaAbility must have a cost."),
            per_turn_activation_limit: self.per_turn_activation_limit,
            effects: self.effects,
        }
    }

    pub fn into_mana_ability(self) -> ActivatedManaAbility {
        ActivatedManaAbility {
            cost: self.cost.expect("ActivatedManaAbility must have a cost."),
            per_turn_activation_limit: self.per_turn_activation_limit,
            mana_produced: self.mana_produced.expect("ManaAbilities must produce mana."),
            other_effects: self.effects,
        }
    }
}

pub enum AbilitySpeed {
    Instant,
    Sorcery,
}