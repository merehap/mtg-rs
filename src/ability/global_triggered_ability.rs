use ability::triggered_ability::TriggeredAbility;
use script::condition::permanent_condition::PermanentCondition;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct GlobalTriggeredAbility {
    pub permanent_condition: PermanentCondition,
    pub ability: TriggeredAbility,
}