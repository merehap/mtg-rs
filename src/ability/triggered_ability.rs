use effect::effect_template::EffectTemplate;
use script::condition::condition::Condition;
use script::condition::trigger_condition::TriggerCondition;
use script::variable::permanent_cost_variable::PermanentCostVariable;

// TODO: Handle what happens if a "may" cost isn't paid. And maybe if a "must" cost can't be paid.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct TriggeredAbility {
    pub trigger_condition: TriggerCondition,
    pub execution_condition: Condition,
    pub player_option: PlayerOption,
    pub cost: Option<PermanentCostVariable>,
    // TODO: Handle duplicate targets.
    pub effects: Vec<EffectTemplate>,
    // Effects that occur if a "may" is declined.
    pub otherwise_effects: Vec<EffectTemplate>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PlayerOption {
    Must,
    May,
}