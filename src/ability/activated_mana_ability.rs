use cost::permanent_cost::PermanentCost;
use effect::effect_template::EffectTemplate;
use mana::mana_produced::ManaProduced;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct ActivatedManaAbility {
    pub cost: PermanentCost,
    pub per_turn_activation_limit: Option<usize>,
    pub mana_produced: ManaProduced,
    pub other_effects: Vec<EffectTemplate>,
}
