use ability::stack_object_static_ability::StackObjectStaticAbility;
use effect::effect_type::damage_effect_type::*;
use effect::effect_type::global_effect_type::ContinuousGlobalEffectType;
use effect::effect_type::permanent_effect_type::*;
use effect::effect_type::player_effect_type::*;
use effect::effect_type::selecting::SelectingPlayers;
use mana::color::Color;
use script::condition::condition::Condition;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum StaticAbility {
    Global(ContinuousGlobalEffectType),
    Permanent(ContinuousPermanentEffectType),
    PermanentCannot(PermanentCannotEffectType),
    Player {
        selecting: SelectingPlayers,
        effect_type: ContinuousPlayerEffectType,
    },
    PlayerReplacement {
        selecting: SelectingPlayers,
        replacement_type: PlayerReplacementEffectType,
    },
    StackObject(StackObjectStaticAbility),
    DamagePrevention {
        effect_type: ContinuousDamageEffectType,
        damage_prevention_recipient_types: DamageRecipientTypes,
    },

    Conditional {
        condition: Condition,
        effect_type: Box<StaticAbility>,
    }
}

impl StaticAbility {
    pub fn keyword(keyword: Keyword) -> StaticAbility {
        StaticAbility::Permanent(ContinuousPermanentEffectType::Keyword(keyword))
    }

    pub fn color_protection(color: Color) -> StaticAbility {
        StaticAbility::Permanent(ContinuousPermanentEffectType::color_protection(color))
    }
}

