use cost::permanent_cost::PermanentCost;
use effect::effect_template::EffectTemplate;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct ActivatedAbility {
    pub cost: PermanentCost,
    pub per_turn_activation_limit: Option<usize>,
    pub effects: Vec<EffectTemplate>,
}