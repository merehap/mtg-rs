use ability::permanent_abilities::PermanentAbilities;
use cost::cost::Cost;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub enum PermanentKicker {
    Regular(Cost),
    Multi(Cost),
    Choice(Vec<PermanentKickerChoice>),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct PermanentKickerChoice {
    cost: Cost,
    abilities: PermanentAbilities,
}
