use card::card::Card;
use location::location::CardLocation;
use permanent::permanent::Permanent;
use turn::step;

#[allow(dead_code)]
#[allow(clippy::large_enum_variant)]
pub enum Trigger {
    Step(step::Step),
    CardZoneTransfer {
        card: Card,
        source: CardLocation,
        destination: CardLocation,
    },
    ToBattlefieldTransfer {
        permanent: Permanent,
        source: Option<CardLocation>,
    },
    FromBattlefieldTransfer {
        permanent: Permanent,
        destination: CardLocation,
    },
    BlockedCreature(Permanent),
}