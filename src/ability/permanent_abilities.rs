use ability::activated_ability::ActivatedAbility;
use ability::activated_mana_ability::ActivatedManaAbility;
use ability::global_triggered_ability::GlobalTriggeredAbility;
use ability::static_ability::StaticAbility;
use ability::triggered_ability::TriggeredAbility;
use effect::effect_template::EffectTemplate;
use effect::effect_type::global_effect_type::ContinuousGlobalEffectType;
use effect::effect_type::permanent_effect_type::Keyword;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct PermanentAbilities {
    // TODO: Represent all zone transfer abilities in a single type.
    pub enter_the_battlefield_abilities: Vec<EffectTemplate>,
    pub enter_the_graveyard_abilities: Vec<EffectTemplate>,

    pub static_abilities: Vec<StaticAbility>,
    pub activated_abilities: Vec<ActivatedAbility>,
    pub global_static_abilities: Vec<ContinuousGlobalEffectType>,
    pub triggered_abilities: Vec<TriggeredAbility>,
    pub plus_one_plus_one_counter_count: usize,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct PermanentAbilitiesBuilder {
    enter_the_battlefield_abilities: Vec<EffectTemplate>,
    enter_the_graveyard_abilities: Vec<EffectTemplate>,
    static_abilities: Vec<StaticAbility>,
    activated_abilities: Vec<ActivatedAbility>,
    activated_mana_abilities: Vec<ActivatedManaAbility>,
    triggered_abilities: Vec<TriggeredAbility>,
    global_static_abilities: Vec<ContinuousGlobalEffectType>,
    global_triggered_abilities: Vec<GlobalTriggeredAbility>,
    plus_one_plus_one_counter_count: usize,
}

impl PermanentAbilitiesBuilder {
    pub fn new() -> PermanentAbilitiesBuilder {
        PermanentAbilitiesBuilder {
            enter_the_battlefield_abilities: Vec::new(),
            enter_the_graveyard_abilities: Vec::new(),
            static_abilities: Vec::new(),
            activated_abilities: Vec::new(),
            activated_mana_abilities: Vec::new(),
            triggered_abilities: Vec::new(),
            global_static_abilities: Vec::new(),
            global_triggered_abilities: Vec::new(),
            plus_one_plus_one_counter_count: 0,
        }
    }

    pub fn enter_the_battlefield_ability(mut self, value: EffectTemplate) -> PermanentAbilitiesBuilder {
        self.enter_the_battlefield_abilities.push(value);
        self
    }

    pub fn enter_the_graveyard_ability(mut self, value: EffectTemplate) -> PermanentAbilitiesBuilder {
        self.enter_the_graveyard_abilities.push(value);
        self
    }

    pub fn static_ability(mut self, value: StaticAbility) -> PermanentAbilitiesBuilder {
        self.static_abilities.push(value);
        self
    }

    pub fn static_abilities(mut self, value: Vec<StaticAbility>) -> PermanentAbilitiesBuilder {
        if !self.static_abilities.is_empty() {
            panic!("Attempting to set all static abilities at once, but there was one already set.");
        }

        self.static_abilities = value;
        self
    }

    pub fn keyword(mut self, value: Keyword) -> PermanentAbilitiesBuilder {
        self.static_abilities.push(StaticAbility::keyword(value));
        self
    }

    pub fn activated_ability(mut self, value: ActivatedAbility) -> PermanentAbilitiesBuilder {
        self.activated_abilities.push(value);
        self
    }

    pub fn activated_mana_ability(mut self, value: ActivatedManaAbility) -> PermanentAbilitiesBuilder {
        self.activated_mana_abilities.push(value);
        self
    }

    pub fn triggered_ability(mut self, value: TriggeredAbility) -> PermanentAbilitiesBuilder {
        self.triggered_abilities.push(value);
        self
    }

    pub fn global_static_ability(mut self, value: ContinuousGlobalEffectType) -> PermanentAbilitiesBuilder {
        self.global_static_abilities.push(value);
        self
    }

    pub fn global_triggered_ability(mut self, value: GlobalTriggeredAbility) -> PermanentAbilitiesBuilder {
        self.global_triggered_abilities.push(value);
        self
    }

    pub fn plus_one_plus_one_counter_count(mut self, value: usize) -> PermanentAbilitiesBuilder {
        self.plus_one_plus_one_counter_count = value;
        self
    }

    pub fn build(self) -> PermanentAbilities {
        PermanentAbilities {
            enter_the_battlefield_abilities: self.enter_the_battlefield_abilities,
            enter_the_graveyard_abilities: self.enter_the_graveyard_abilities,
            activated_abilities: self.activated_abilities,
            static_abilities: self.static_abilities,
            global_static_abilities: self.global_static_abilities,
            triggered_abilities: self.triggered_abilities,
            plus_one_plus_one_counter_count: self.plus_one_plus_one_counter_count,
        }
    }
}
