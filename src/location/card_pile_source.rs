use script::count::Count;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardPileSource {
    TopOfLibrary(Count),
    Hand,
    Graveyard,
}