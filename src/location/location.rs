use util::positive_integer::PositiveInteger;
use zone::library::LibraryLocation;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum CardLocation {
    Hand,
    Graveyard,
    Exile,
    Library(LibraryLocation),
}

impl CardLocation {
    pub fn under_top_of_library(depth: usize) -> CardLocation {
        if let Some(cards_down) = PositiveInteger::from_usize(depth) {
            CardLocation::Library(LibraryLocation::UnderTop(cards_down))
        } else {
            CardLocation::Library(LibraryLocation::Top)
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub enum PermanentCardLocation {
    Battlefield,
    CardLocation(CardLocation),
}