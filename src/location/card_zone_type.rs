#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub enum CardZoneType {
    Library,
    Hand,
    Graveyard,
    Exile,
}