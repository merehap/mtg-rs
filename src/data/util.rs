use std::collections::BTreeSet;
use std::hash::Hash;
use std::iter::FromIterator;

use cost::cost_builder::CostBuilder;
use face::face::{Name, CardNumber, ArtistName, FlavorText};
use mana::mana_cost::ManaCost;
use mana::mana_group::ManaGroup;
use mana::mana_produced::ManaProduced;
use permanent::permanent_type::creature_type::CreatureType;
use util::positive_integer::PositiveInteger;

pub fn name(raw: &str) -> Name {
    Name(raw.to_string())
}

pub fn card_number(raw: usize) -> CardNumber {
    let number = PositiveInteger::from_usize(raw).expect("Card number can't be zero!");
    CardNumber::new(number)
}

pub fn artist_name(raw: &str) -> ArtistName {
    ArtistName::from_str(raw).expect("Bad artist name!")
}

pub fn no_flavor_text() -> Option<FlavorText> {
    Option::None
}

pub fn flavor_text(text: &str) -> Option<FlavorText> {
    Some(FlavorText::new(text))
}

pub fn cost() -> CostBuilder {
    CostBuilder::new()
}

pub fn mana_cost(raw: &str) -> ManaCost {
    ManaCost::try_from_str(raw).expect("Bad mana cost text!")
}

pub fn mana_group(raw: &str) -> ManaGroup {
    ManaGroup::try_from_str(raw.to_string()).expect("Bad mana group text!")
}

pub fn choose_one_mana(raw: &str) -> ManaProduced {
    let result: Vec<ManaGroup> = raw.chars()
        .map(|raw_color| mana_group(&raw_color.to_string()))
        .collect();
    ManaProduced::ChooseOne(result)
}

pub fn creature_types(raw: Vec<CreatureType>) -> BTreeSet<CreatureType> {
    BTreeSet::from_iter(raw.iter().cloned())
}

pub fn pos(raw: usize) -> PositiveInteger {
    PositiveInteger::from_usize(raw).expect("A positive number must be specified!")
}

pub fn to_set<T: Ord + Hash + Clone>(raw: Vec<T>) -> BTreeSet<T> {
    let result = BTreeSet::from_iter(raw.iter().cloned());
    assert_eq!(raw.len(), result.len(), "Sets must be created from Vecs with no duplicate elements.");
    result
}
