use card::card::Card;

pub fn card_by_name(cards: &[Card], target_name: &str) -> Card {
    for card in cards.iter() {
        if card.format_display_name() == target_name.to_string() {
            return card.clone();
        }
    }

    panic!("Couldn't find card '{}'", target_name);
}
