use ability::activated_ability_builder::ActivatedAbilityBuilder;
use ability::global_triggered_ability::GlobalTriggeredAbility;
use ability::stack_object_static_ability::StackObjectStaticAbility;
use ability::permanent_abilities::PermanentAbilitiesBuilder;
use ability::static_ability::StaticAbility;
use ability::triggered_ability::{TriggeredAbility, PlayerOption};
use card::card::Card;
use data::util::*;
use effect::effect_template::EffectTemplate;
use effect::effect_type::card_effect_type::*;
use effect::effect_type::damage_effect_type::*;
use effect::effect_type::expiration::*;
use effect::effect_type::global_effect_type::*;
use effect::effect_type::permanent_effect_type::*;
use effect::effect_type::player_effect_type::*;
use effect::effect_type::selecting::*;
use effect::effect_type::stack_object_effect_type::*;
use ephemeral::ephemeral_face_builder::EphemeralFaceBuilder;
use ephemeral::regular_ephemeral_builder::RegularEphemeralBuilder;
use ephemeral::ephemeral_card::EphemeralCard;
use ephemeral::ephemeral_face::CastingWindowConstraint;
use ephemeral::ephemeral_layout::EphemeralLayout;
use face::face::{Expansion, Rarity};
use location::card_pile_source::CardPileSource;
use location::location::{CardLocation, PermanentCardLocation};
use location::card_zone_type::CardZoneType;
use mana::color::Color;
use mana::mana_produced::ManaProduced;
use permanent::face::permanent_face_builder::PermanentFaceBuilder;
use permanent::permanent_type::artifact::ArtifactType;
use permanent::permanent_type::creature::{Creature, Power, Toughness};
use permanent::permanent_type::creature_type::CreatureType;
use permanent::permanent_type::enchantment::{EnchantmentType, AuraTriggeredAbility, AuraTriggerCondition};
use permanent::permanent_type::land::{Land, BasicLandType};
use script::variable::basic_land_type_variable::BasicLandTypeVariable;
use script::variable::card_name_variable::CardNameVariable;
use script::condition::card_condition::CardCondition;
use script::condition::color_condition::ColorCondition;
use script::condition::condition::Condition;
use script::condition::integer_condition::IntegerCondition;
use script::condition::permanent_condition::PermanentCondition;
use script::condition::permanent_group_condition::PermanentGroupCondition;
use script::condition::player_condition::PlayerCondition;
use script::condition::trigger_condition::{TriggerCondition, PlayerTurns};
use script::count::Count;
use script::permanent_attribute_count::PermanentAttributeCount;
use script::variable::color_variable::ColorVariable;
use script::variable::colors_variable::ColorsVariable;
use script::variable::creature_type_variable::CreatureTypeVariable;
use script::variable::object_variable::ObjectVariable;
use script::variable::permanent_cost_variable::PermanentCostVariable;
use script::variable::player_variable::PlayerVariable;
use turn::step::*;
use zone::library::LibraryLocation;

pub fn invasion() -> Vec<Card> {
    vec![
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Alabaster Leech")
            .expansion(Expansion::Invasion)
            .card_number(1)
            .rarity(Rarity::Rare)
            .artist_name("Edward P. Beard, Jr.")
            .flavor_text(
r#"Its stones seem to serve a healing function, but removing them intact is an exhausting process."
—Tolarian research notes"#)
            .casting_cost("W")
            .not_snow()
            .non_legendary()
            .creature("1/3 Leech")
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::white(),
                modifier: CostModifier::Add(mana_cost("W")),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Angel of Mercy")
            .expansion(Expansion::Invasion)
            .card_number(2)
            .rarity(Rarity::Uncommon)
            .artist_name("Mark Tedin")
            .flavor_text(
r#""In times like these, people need to be reminded of compassion."
—Sisay"#)
            .casting_cost("4W")
            .not_snow()
            .non_legendary()
            .creature("3/3 Angel")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::gain_life(3)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ardent Soldier")
            .expansion(Expansion::Invasion)
            .card_number(3)
            .rarity(Rarity::Common)
            .artist_name("Paolo Parente")
            .no_flavor_text()
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/2 Human")
            .kicker("2")
            .static_ability(StaticAbility::keyword(Keyword::Vigilance))
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(1)
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Atalya, Samite Master")
            .expansion(Expansion::Invasion)
            .card_number(4)
            .rarity(Rarity::Rare)
            .artist_name("Rebecca Guay")
            .flavor_text(
                r#""Healing is a gift the divine ones have shared with us. It is a sacred trust.""#)
            .casting_cost("3WW")
            .not_snow()
            .legendary()
            .creature("2/3 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::ChooseOne(vec![
                    EffectTemplate::Damage {
                        effect_types: vec![
                            DamageEffectType::Continuous {
                                expiration: Expiration::EndOfTurn,
                                effect_type: ContinuousDamageEffectType::Prevent(
                                    Count::ManaVariable(ColorCondition::white()))
                            }
                        ],
                        damage_recipients: DamageRecipients::controller_chooses_one_creature(),
                    },
                    EffectTemplate::controller_effect(PlayerEffectType::GainLife(
                        Count::ManaVariable(ColorCondition::white()))),
                ]))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Benalish Emissary")
            .expansion(Expansion::Invasion)
            .card_number(5)
            .rarity(Rarity::Uncommon)
            .artist_name("Randy Gallegos")
            .no_flavor_text()
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .creature("1/4 Human")
            .kicker("1G")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(EffectTemplate::target_land(PermanentEffectType::destroy()))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Benalish Heralds")
            .expansion(Expansion::Invasion)
            .card_number(6)
            .rarity(Rarity::Uncommon)
            .artist_name("Don Hazeltine")
            .flavor_text(r#"The detailed dispatch could be summarized in four words: "Time is running out.""#)
            .casting_cost("3W")
            .not_snow()
            .non_legendary()
            .creature("2/4 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("3U").self_tap())
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Benalish Lancer")
            .expansion(Expansion::Invasion)
            .card_number(7)
            .rarity(Rarity::Common)
            .artist_name("Paolo Parente")
            .no_flavor_text()
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .kicker("2W")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .static_ability(StaticAbility::keyword(Keyword::FirstStrike))
                .plus_one_plus_one_counter_count(2)
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Benalish Trapper")
            .expansion(Expansion::Invasion)
            .card_number(8)
            .rarity(Rarity::Common)
            .artist_name("Ken Meyer, Jr.")
            .flavor_text(r#""I'm up here. You're down there. Now who's the lower life form?""#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Tap)),
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Blinding Light")
            .expansion(Expansion::Invasion)
            .card_number(9)
            .rarity(Rarity::Uncommon)
            .artist_name("Marc Fishman")
            .flavor_text(
                r#"An angel's sword impales only the body; her righteousness penetrates the soul."#)
            .casting_cost("2W")
            .sorcery()
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                effect_types: vec![PermanentEffectType::Tap],
                condition: PermanentCondition::not(PermanentCondition::white()),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Capashen Unicorn")
            .expansion(Expansion::Invasion)
            .card_number(10)
            .rarity(Rarity::Common)
            .artist_name("Jerry Tiritilli")
            .flavor_text(r#"Capashen riders were stern and humorless even before their ancestral home was reduced to rubble."#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/2 Unicorn")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1W").self_tap().self_sacrifice())
                .add_effect(EffectTemplate::target_artifact_or_enchantment(PermanentEffectType::destroy()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crimson Acolyte")
            .expansion(Expansion::Invasion)
            .card_number(11)
            .rarity(Rarity::Common)
            .artist_name("Orizio Daniele")
            .flavor_text(
r#"The faithful will walk through streams of fire and emerge unscathed.
—Crimson acolyte creed"#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .keyword(Keyword::Protection(CardCondition::red()))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W"))
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::color_protection(Color::Red),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crusading Knight")
            .expansion(Expansion::Invasion)
            .card_number(12)
            .rarity(Rarity::Rare)
            .artist_name("Edward P. Beard, Jr.")
            .flavor_text(
                r#""My only dream is to destroy the nightmares of others.""#)
            .casting_cost("2WW")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .keyword(Keyword::Protection(CardCondition::black()))
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                    Count::permanent(PermanentCondition::And(vec![
                        PermanentCondition::swamp(),
                        PermanentCondition::ControlledByOpponent,
                    ])),
                )
            ))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Death or Glory")
            .expansion(Expansion::Invasion)
            .card_number(13)
            .rarity(Rarity::Rare)
            .artist_name("Jeff Easley")
            .no_flavor_text()
            .casting_cost("4W")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![
                    PlayerEffectType::TwoPilesOfCards {
                        pile_creator_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectController),
                        pile_selector_condition: PlayerCondition::OpponentOf(
                            PlayerVariable::EffectController),
                        zone: CardPileSource::Graveyard,
                        selected_pile_effect_type:
                            Some(CardEffectType::MoveTo(CardZoneType::Exile)),
                        unselected_pile_effect_type:
                            Some(CardEffectType::MoveToBattlefield {
                                new_controller: PlayerVariable::EffectController
                            }),
                    }
                ],
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Dismantling Blow")
            .expansion(Expansion::Invasion)
            .card_number(14)
            .rarity(Rarity::Common)
            .artist_name("Mark Tedin")
            .no_flavor_text()
            .casting_cost("2W")
            .instant()
            .kicker("2U")
            .add_kicker_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(2)))
            .add_effect(EffectTemplate::target_artifact_or_enchantment(PermanentEffectType::destroy()))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Divine Presence")
            .expansion(Expansion::Invasion)
            .card_number(15)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spears")
            .flavor_text(
r#""Serra's light isn't easily extinguished."
—Reya Dawnbringer""#)
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .static_ability(StaticAbility::Global(ContinuousGlobalEffectType::DamageCap {
                cap: 3,
                allowed_target_types: DamageRecipientTypes::any_recipient(),
            }))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Fight or Flight")
            .expansion(Expansion::Invasion)
            .card_number(16)
            .rarity(Rarity::Rare)
            .artist_name("Randy Gallegos")
            .no_flavor_text()
            .casting_cost("3W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::BeginningOfCombat,
                    player_turns: PlayerTurns::OpponentsOfEffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::active(),
                        effect_types: vec![
                            PlayerEffectType::TwoPilesOfPermanents {
                                pile_creator_condition: PlayerCondition::Matches(
                                    PlayerVariable::EffectController),
                                pile_selector_condition: PlayerCondition::Matches(
                                    PlayerVariable::Active),
                                permanent_condition: PermanentCondition::creature(),
                                selected_pile_condition: PermanentGroupCondition::Any,
                                selected_pile_effect_type: None,
                                unselected_pile_effect_type: Some(PermanentEffectType::Continuous {
                                    expiration: Expiration::EndOfTurn,
                                    continuous_type: ContinuousPermanentEffectType::CannotAttack,
                                })
                            }
                        ]
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Glimmering Angel")
            .expansion(Expansion::Invasion)
            .card_number(17)
            .rarity(Rarity::Common)
            .artist_name("Ciruelo")
            .flavor_text(
r#""We turned to see where the blow came from and saw only a distant light."
—Capashen lord"#)
            .casting_cost("3W")
            .not_snow()
            .non_legendary()
            .creature("2/2 Angel")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Shroud),
                 }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Global Ruin")
            .expansion(Expansion::Invasion)
            .card_number(18)
            .rarity(Rarity::Rare)
            .artist_name("Greg Staples")
            .flavor_text(
r#""The earth shook, the sky rained fire, and it seemed the world was ending."
—Benalish refugee"#)
            .casting_cost("4W")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::all(),
                effect_types: vec![
                    PlayerEffectType::TwoPilesOfPermanents {
                        pile_creator_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectRecipientController),
                        pile_selector_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectRecipientController),
                        permanent_condition: PermanentCondition::land(),
                        selected_pile_condition: PermanentGroupCondition::MaxOneOfEachBasicLandType,
                        selected_pile_effect_type: None,
                        unselected_pile_effect_type: Some(PermanentEffectType::destroy()),
                    }
                ]
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Harsh Judgment")
            .expansion(Expansion::Invasion)
            .card_number(19)
            .rarity(Rarity::Common)
            .artist_name("Carl Critchlow")
            .flavor_text(
r#""At least they remember me."
—Gerrard"#)
            .casting_cost("2WW")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .static_ability(StaticAbility::DamagePrevention {
                damage_prevention_recipient_types: DamageRecipientTypes::controller(),
                effect_type: ContinuousDamageEffectType::RedirectDamageBackToSourceController {
                    source_condition: CardCondition::And(vec![
                        CardCondition::Ephemeral,
                        CardCondition::Color(ColorVariable::EnterTheBattlefieldInput),
                    ]),
                },
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Holy Day")
            .expansion(Expansion::Invasion)
            .card_number(20)
            .rarity(Rarity::Common)
            .artist_name("Pete Venters")
            .flavor_text(
r#""Will history remember this as the day demons arrived or the day saviors arrived?"
—Barrin"#)
            .casting_cost("W")
            .instant()
            .add_effect(EffectTemplate::prevent_all_combat_damage(Expiration::EndOfTurn))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Liberate")
            .expansion(Expansion::Invasion)
            .card_number(21)
            .rarity(Rarity::Uncommon)
            .artist_name("Alan Pollack")
            .flavor_text(
r#""Not everyone gets a second chance."
—Hanna"#)
            .casting_cost("1W")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::ControlledByController,
                ]),
                effect_types: vec![
                    PermanentEffectType::ExileAndReturn {
                        return_step: StepName::End,
                        new_controller: PlayerVariable::EffectRecipientOwner,
                    }
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Obsidian Acolyte")
            .expansion(Expansion::Invasion)
            .card_number(22)
            .rarity(Rarity::Common)
            .artist_name("Matthew D. Wilson")
            .flavor_text(
r#"The faithful will walk through streams of fire and emerge unscathed.
—Crimson acolyte creed"#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .keyword(Keyword::Protection(CardCondition::black()))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W"))
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::color_protection(Color::Black),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Orim's Touch")
            .expansion(Expansion::Invasion)
            .card_number(23)
            .rarity(Rarity::Uncommon)
            .artist_name("Roger Raupp")
            .no_flavor_text()
            .casting_cost("W")
            .instant()
            .kicker("1")
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![
                    DamageEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        effect_type: ContinuousDamageEffectType::Prevent(
                            Count::IfElse {
                                condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                                then: Box::new(Count::Fixed(4)),
                                otherwise: Box::new(Count::Fixed(2)),
                            }
                        ),
                    }
                ],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pledge of Loyalty")
            .expansion(Expansion::Invasion)
            .card_number(25)
            .rarity(Rarity::Uncommon)
            .artist_name("Franz Vohwinkel")
            .flavor_text(r#"Urza convinced many Dominarians not only to set aside their differences, but to embrace them."#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Conditional {
                        condition: Condition::Permanent(
                            PermanentCondition::aura_controller_controls(PermanentCondition::white())),
                        effect_type: Box::new(StaticAbility::color_protection(Color::White))
                    })
                    .static_ability(StaticAbility::Conditional {
                        condition: Condition::Permanent(
                            PermanentCondition::aura_controller_controls(PermanentCondition::blue())),
                        effect_type: Box::new(StaticAbility::color_protection(Color::Blue))
                    })
                    .static_ability(StaticAbility::Conditional {
                        condition: Condition::Permanent(
                            PermanentCondition::aura_controller_controls(PermanentCondition::black())),
                        effect_type: Box::new(StaticAbility::color_protection(Color::Black))
                    })
                    .static_ability(StaticAbility::Conditional {
                        condition: Condition::Permanent(
                            PermanentCondition::aura_controller_controls(PermanentCondition::red())),
                        effect_type: Box::new(StaticAbility::color_protection(Color::Red))
                    })
                    .static_ability(StaticAbility::Conditional {
                        condition: Condition::Permanent(
                            PermanentCondition::aura_controller_controls(PermanentCondition::green())),
                        effect_type: Box::new(StaticAbility::color_protection(Color::Green))
                    })
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Prison Barricade")
            .expansion(Expansion::Invasion)
            .card_number(25)
            .rarity(Rarity::Common)
            .artist_name("Thomas Gianni")
            .no_flavor_text()
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("1/3 Wall")
            .kicker("1W")
            .static_ability(StaticAbility::keyword(Keyword::Defender))
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(1)
                .static_ability(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::OverrideDefender))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Protective Sphere")
            .expansion(Expansion::Invasion)
            .card_number(26)
            .rarity(Rarity::Common)
            .artist_name("Rebecca Guay")
            .no_flavor_text()
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").life(1))
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![
                        DamageEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            effect_type: ContinuousDamageEffectType::PreventAllDamageFromSources {
                                source_count: Count::Fixed(1),
                                source_condition: PermanentCondition::color(ColorVariable::AbilityManaCostColor),
                            },
                        }
                    ],
                    damage_recipients: DamageRecipients::Player(PlayerVariable::EffectController),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pure Reflection")
            .expansion(Expansion::Invasion)
            .card_number(27)
            .rarity(Rarity::Rare)
            .artist_name("Scott M. Fischer")
            .no_flavor_text()
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SpellCast {
                    player_condition: PlayerCondition::Any,
                    card_condition: CardCondition::Creature
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Permanent {
                        condition: PermanentCondition::creature_type(CreatureType::Reflection),
                        effect_types: vec![PermanentEffectType::destroy()],
                    }),
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::trigger_object_controller(),
                        effect_types: vec![
                            PlayerEffectType::CreateCreatureTokens {
                                count: Count::Fixed(1),
                                power: Power(Count::ConvertedManaCost(ObjectVariable::CurrentTriggeredAbilityObject)),
                                toughness: Toughness(Count::ConvertedManaCost(ObjectVariable::CurrentTriggeredAbilityObject)),
                                colors: to_set(vec![Color::White]),
                                creature_types: to_set(vec![CreatureType::Reflection]),
                                static_abilities: Vec::new(),
                            },
                        ],
                    },
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rampant Elephant")
            .expansion(Expansion::Invasion)
            .card_number(28)
            .rarity(Rarity::Common)
            .artist_name("Alan Pollack")
            .flavor_text(r#"No matter how righteous the cause, it helps to bring along some muscle."#)
            .casting_cost("3W")
            .not_snow()
            .non_legendary()
            .creature("2/2 Elephant")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G"))
                .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    // Itself here refers to Rampant Elephant not the target,
                    // since Targeting is evaluated before the StaticAbility is given to the target.
                    continuous_type: ContinuousPermanentEffectType::MustBlock(Selecting::Itself),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Razorfoot Griffin")
            .expansion(Expansion::Invasion)
            .card_number(29)
            .rarity(Rarity::Common)
            .artist_name("Ben Thompson")
            .flavor_text(
r#""Do griffins fight to defend their homes or purely for sport?"
—Sisay"#)
            .casting_cost("4W")
            .not_snow()
            .non_legendary()
            .creature("2/2 Griffin")
            .keyword(Keyword::Flying)
            .keyword(Keyword::FirstStrike)
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Restrain")
            .expansion(Expansion::Invasion)
            .card_number(30)
            .rarity(Rarity::Common)
            .artist_name("Dave Dorman")
            .flavor_text(
r#""Hanna would give up her own life before she'd abandon the Weatherlight."
—Sisay"#)
            .casting_cost("2W")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::Attacking,
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::CannotDealCombatDamage,
                    }
                ]
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Reviving Dose")
            .expansion(Expansion::Invasion)
            .card_number(31)
            .rarity(Rarity::Common)
            .artist_name("D. Alexander Gregory")
            .flavor_text(
                r#"As healers battled each plague, they learned more about the next."#)
            .casting_cost("2W")
            .instant()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::gain_life(3)))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rewards of Diversity")
            .expansion(Expansion::Invasion)
            .card_number(32)
            .rarity(Rarity::Uncommon)
            .artist_name("Darrell Riche")
            .flavor_text(
r#""Everything is in place. Nothing can happen that isn't part of my plan."
—Urza"#)
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SpellCast {
                    player_condition: PlayerCondition::Any,
                    card_condition: CardCondition::Multicolored,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![EffectTemplate::controller_effect(PlayerEffectType::gain_life(4))],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Reya Dawnbringer")
            .expansion(Expansion::Invasion)
            .card_number(33)
            .rarity(Rarity::Rare)
            .artist_name("Matthew D. Wilson")
            .flavor_text(r#"A beacon of hope for a battered army."#)
            .casting_cost("6WWW")
            .not_snow()
            .legendary()
            .creature("4/6 Angel")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: None,
                effects: vec![
                    EffectTemplate::Card {
                        effect_types: vec![
                            CardEffectType::MoveToBattlefield {
                                new_controller: PlayerVariable::EffectController,
                            }
                        ],
                        selecting: Selecting::one_target(),
                        selection_condition: CardCondition::Any,
                        selection_owner: PlayerVariable::EffectController,
                        card_zone: CardZoneType::Graveyard,
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Rout")
            .expansion(Expansion::Invasion)
            .card_number(31)
            .rarity(Rarity::Common)
            .artist_name("D. Alexander Gregory")
            .flavor_text(
                r#"As healers battled each plague, they learned more about the next."#)
            .casting_cost("3WW")
            .sorcery_with_instant_speed_upgrade(
                cost().mana("2"),
            )
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                condition: PermanentCondition::creature(),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ruham Djinn")
            .expansion(Expansion::Invasion)
            .card_number(35)
            .rarity(Rarity::Uncommon)
            .artist_name("Jeff Easley")
            .no_flavor_text()
            .casting_cost("5W")
            .not_snow()
            .non_legendary()
            .creature("5/5 Djinn")
            .static_ability(StaticAbility::keyword(Keyword::FirstStrike))
            .static_ability(StaticAbility::Conditional {
                condition: Condition::MostCommonPermanentColorOrTied(Color::White),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(-2, -2),
                ))
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Samite Ministration")
            .expansion(Expansion::Invasion)
            .card_number(31)
            .rarity(Rarity::Uncommon)
            .artist_name("Darrell Riche")
            .no_flavor_text()
            .casting_cost("1W")
            .instant()
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![
                    DamageEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        effect_type: ContinuousDamageEffectType::PreventAllDamageFromSourcesAndGainLife {
                            source_count: Count::Fixed(1),
                            source_condition: PermanentCondition::Any,
                            source_condition_for_life_gain: CardCondition::Or(vec![
                                CardCondition::black(),
                                CardCondition::red(),
                            ]),
                        }
                    }
                ],
                damage_recipients: DamageRecipients::All,
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shackles")
            .expansion(Expansion::Invasion)
            .card_number(37)
            .rarity(Rarity::Common)
            .artist_name("Greg Staples")
            .flavor_text(r#""It could be worse," said Gerrard, looking around. "Well, maybe not.""#)
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::PermanentCannot(
                        PermanentCannotEffectType::CannotUntapDuringControllersUntapStep))
                    .build(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::bounce()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Spirit of Resistance")
            .expansion(Expansion::Invasion)
            .card_number(38)
            .rarity(Rarity::Rare)
            .artist_name("John Avon")
            .flavor_text(
r#""Our victory must come from all of Dominaria, or it will not come."
—Urza"#)
            .casting_cost("2W")
            .enchantment(EnchantmentType::Regular)
            .not_snow()
            .non_legendary()
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::And(vec![
                    PermanentCondition::controller_controls(PermanentCondition::white()),
                    PermanentCondition::controller_controls(PermanentCondition::blue()),
                    PermanentCondition::controller_controls(PermanentCondition::black()),
                    PermanentCondition::controller_controls(PermanentCondition::red()),
                    PermanentCondition::controller_controls(PermanentCondition::green()),
                ])),
                effect_type: Box::new(StaticAbility::DamagePrevention {
                    effect_type: ContinuousDamageEffectType::PreventAllDamage,
                    damage_prevention_recipient_types: DamageRecipientTypes::controller(),
                }),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Spirit Weaver")
            .expansion(Expansion::Invasion)
            .card_number(39)
            .rarity(Rarity::Uncommon)
            .artist_name("Matthew D. Wilson")
            .flavor_text(r#""Let my hope be your shield.""#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("2/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::adjust_power_toughness(0, 1),
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Or(vec![
                        PermanentCondition::green(),
                        PermanentCondition::blue(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Strength of Unity")
            .expansion(Expansion::Invasion)
            .card_number(40)
            .rarity(Rarity::Common)
            .artist_name("Andrew Goldhawk")
            .flavor_text(r#"All Dominarians agreed on one thing: they would not go down without a fight."#)
            .casting_cost("3W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                            Count::SourceControllerBasicLandTypes)))
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sunscape Apprentice")
            .expansion(Expansion::Invasion)
            .card_number(41)
            .rarity(Rarity::Common)
            .artist_name("Stephanie Law")
            .no_flavor_text()
            .casting_cost("W")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G").self_tap())
                .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                }))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::And(vec![
                        PermanentCondition::creature(),
                        PermanentCondition::ControlledByController,
                    ]),
                    effect_types: vec![
                        PermanentEffectType::MoveTo(
                            CardLocation::Library(LibraryLocation::Top)),
                    ],
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sunscape Master")
            .expansion(Expansion::Invasion)
            .card_number(42)
            .rarity(Rarity::Rare)
            .artist_name("Alan Rabinowitz")
            .no_flavor_text()
            .casting_cost("2WW")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("GG").self_tap())
                .add_effect(EffectTemplate::Global(GlobalEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    effect_type: ContinuousGlobalEffectType::Permanent {
                        condition: PermanentCondition::creature(),
                        effect_types: vec![
                            ContinuousPermanentEffectType::adjust_power_toughness(2, 2),
                        ]
                    }
                }))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("UU").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::bounce()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Teferi's Care")
            .expansion(Expansion::Invasion)
            .card_number(43)
            .rarity(Rarity::Uncommon)
            .artist_name("Scott Bailey")
            .flavor_text(
r#""If I do nothing else, I will protect my people."
—Teferi"#)
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost()
                    .mana("W")
                    .sacrifice_count(1, PermanentCondition::enchantment())
                )
                .add_effect(EffectTemplate::Permanent {
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::enchantment(),
                    effect_types: vec![PermanentEffectType::destroy()],
                })
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("3UU"))
                .add_effect(EffectTemplate::StackObject {
                    effect_types: vec![StackObjectEffectType::Counter],
                    selecting: Selecting::one_target(),
                    stack_object_types: to_set(vec![
                        StackObjectType::spell_matching_condition(CardCondition::Enchantment),
                    ]),
                    selection_owner_condition: PlayerCondition::Any,
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Wayfaring Giant")
            .expansion(Expansion::Invasion)
            .card_number(43)
            .rarity(Rarity::Uncommon)
            .artist_name("Christopher Moeller")
            .no_flavor_text()
            .casting_cost("2W")
            .not_snow()
            .non_legendary()
            .creature("1/3 Giant")
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                    Count::SourceControllerBasicLandTypes,
                )
            ))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Winnow")
            .expansion(Expansion::Invasion)
            .card_number(45)
            .rarity(Rarity::Common)
            .artist_name("D. Alexander Gregory")
            .flavor_text(
r#""Strength in numbers? I think not."
—Gerrard"#)
            .casting_cost("2W")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::Not(Box::new(PermanentCondition::land())),
                    PermanentCondition::Not(Box::new(PermanentCondition::UniqueName)),
                ]),
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Barrin's Unmaking")
            .expansion(Expansion::Invasion)
            .card_number(46)
            .rarity(Rarity::Common)
            .artist_name("Luca Zontini")
            .no_flavor_text()
            .casting_cost("1U")
            .instant()
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Conditional {
                condition: PermanentCondition::MostCommonPermanentColorOrTied,
                conditional_effect_type: Box::new(PermanentEffectType::bounce()),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Blind Seer")
            .expansion(Expansion::Invasion)
            .card_number(47)
            .rarity(Rarity::Rare)
            .artist_name("Dave Dorman")
            .flavor_text(
r#""I think he sees more than he lets on."
—Gerrard"#)
            .casting_cost("2UU")
            .not_snow()
            .non_legendary()
            .creature("3/3 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1U"))
                .add_effect(EffectTemplate::ChooseOne(vec![
                    EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::OverrideColors {
                            new_colors: vec![ColorVariable::AbilityInput],
                        },
                    }),
                    EffectTemplate::target_spell(StackObjectEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        effect_type: ContinuousStackObjectEffectType::OverrideColors {
                            new_colors: vec![ColorVariable::AbilityInput],
                        }
                    }),
                ]))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Breaking Wave")
            .expansion(Expansion::Invasion)
            .card_number(48)
            .rarity(Rarity::Uncommon)
            .artist_name("Carl Critchlow")
            .no_flavor_text()
            .casting_cost("2UU")
            .sorcery_with_instant_speed_upgrade(
                cost().mana("2"),
            )
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::creature(),
                effect_types: vec![PermanentEffectType::ToggleTapState],
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Collective Restraint")
            .expansion(Expansion::Invasion)
            .card_number(49)
            .rarity(Rarity::Rare)
            .artist_name("Dave Dorman")
            .flavor_text(
r#""I think he sees more than he lets on."
—Gerrard"#)
            .casting_cost("2UU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::ControlledByOpponent,
                ]),
                effect_types: vec![
                    ContinuousPermanentEffectType::CannotAttackUnlessGenericCostIsPaid(
                        Count::SourceControllerBasicLandTypes)
                ],
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Crystal Spray")
            .expansion(Expansion::Invasion)
            .card_number(50)
            .rarity(Rarity::Uncommon)
            .artist_name("Jeff Miracola")
            .no_flavor_text()
            .casting_cost("2U")
            .instant()
            .add_effect(EffectTemplate::ChooseOne(vec![
                EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::ReplaceColorWords {
                        old_color: ColorVariable::ResolvedInput,
                        new_color: ColorVariable::ResolvedInput2,
                    }
                }),
                EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::ReplaceBasicLandTypeWords {
                        old_basic_land_type: BasicLandTypeVariable::ResolvedInput,
                        new_basic_land_type: BasicLandTypeVariable::ResolvedInput2,
                    }
                }),
                EffectTemplate::target_spell(StackObjectEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    effect_type: ContinuousStackObjectEffectType::ReplaceColorWords {
                        old_color: ColorVariable::ResolvedInput,
                        new_color: ColorVariable::ResolvedInput2,
                    }
                }),
                EffectTemplate::target_spell(StackObjectEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    effect_type: ContinuousStackObjectEffectType::ReplaceBasicLandTypeWords {
                        old_basic_land_type: BasicLandTypeVariable::ResolvedInput,
                        new_basic_land_type: BasicLandTypeVariable::ResolvedInput2,
                    }
                }),
            ]))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Disrupt")
            .expansion(Expansion::Invasion)
            .card_number(51)
            .rarity(Rarity::Uncommon)
            .artist_name("Paolo Parente")
            .flavor_text("Teferi wanted no part of Urza's battle plans, but both agreed the Phyrexian portal must be closed.")
            .casting_cost("U")
            .instant()
            .add_effect(EffectTemplate::StackObject {
                effect_types: vec![
                    StackObjectEffectType::ControllerOfTargetMayCancel {
                        cancel_cost: cost().mana("1").into_cost(),
                        effect_type: Box::new(StackObjectEffectType::Counter),
                    },
                ],
                selecting: Selecting::one_target(),
                stack_object_types: to_set(vec![
                    StackObjectType::spell_matching_condition(CardCondition::Ephemeral),
                ]),
                selection_owner_condition: PlayerCondition::Any,
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Distorting Wake")
            .expansion(Expansion::Invasion)
            .card_number(52)
            .rarity(Rarity::Rare)
            .artist_name("Arnie Swekel")
            .flavor_text("Gerrard savored a grim smile as the Phyrexian portals disappeared behind the Weatherlight.")
            .casting_cost("UUU")
            .sorcery()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::bounce()],
                selecting: Selecting::Targets(Count::mana_variable()),
                selection_condition: PermanentCondition::not(PermanentCondition::land()),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Dream Thrush")
            .expansion(Expansion::Invasion)
            .card_number(53)
            .rarity(Rarity::Common)
            .artist_name("D. J. Cleland-Hura")
            .flavor_text(r#"Whether for good or ill, their arrival always means change."#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("1/1 Bird")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::target_land(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideBasicLandTypes {
                        new_basic_land_types: vec![BasicLandTypeVariable::ResolvedInput],
                    },
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Empress Galina")
            .expansion(Expansion::Invasion)
            .card_number(54)
            .rarity(Rarity::Rare)
            .artist_name("Matt Cavotta")
            .flavor_text(r#""Above the waves you may be mighty indeed, but down here you belong to me.""#)
            .casting_cost("3UU")
            .not_snow()
            .non_legendary()
            .creature("1/3 Merfolk")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("UU").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::GainControl {
                            new_controller: PlayerVariable::EffectController,
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::legendary(),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Essence Leak")
            .expansion(Expansion::Invasion)
            .card_number(55)
            .rarity(Rarity::Uncommon)
            .artist_name("Adam Rex")
            .flavor_text(
r#""We define the boundaries of reality; they don't define us."
—Teferi"#)
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::Any,
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .triggered_ability(TriggeredAbility {
                        trigger_condition: TriggerCondition::BeginningOfStep {
                            step_name: StepName::Upkeep,
                            player_turns: PlayerTurns::EffectController,
                        },
                        execution_condition: Condition::Permanent(PermanentCondition::Or(vec![
                            PermanentCondition::red(),
                            PermanentCondition::green(),
                        ])),
                        player_option: PlayerOption::May,
                        cost: Some(PermanentCostVariable::SelfManaCost),
                        effects: Vec::new(),
                        otherwise_effects: vec![EffectTemplate::self_effect(PermanentEffectType::Sacrifice)],
                    })
                    .build(),
            })
            .into_regular_permanent()
        ),

        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Exclude")
            .expansion(Expansion::Invasion)
            .card_number(56)
            .rarity(Rarity::Common)
            .artist_name("Mark Romanoski")
            .flavor_text(r#"“I don’t have time for you right now.” —Teferi"#)
            .casting_cost("2U")
            .instant()
            .add_effect(EffectTemplate::StackObject {
                effect_types: vec![StackObjectEffectType::Counter],
                selecting: Selecting::one_target(),
                stack_object_types: to_set(vec![
                    StackObjectType::spell_matching_condition(CardCondition::Creature),
                ]),
                selection_owner_condition: PlayerCondition::Any,
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Fact or Fiction")
            .expansion(Expansion::Invasion)
            .card_number(57)
            .rarity(Rarity::Uncommon)
            .artist_name("Terese Nielsen")
            .no_flavor_text()
            .casting_cost("3U")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![
                    PlayerEffectType::TwoPilesOfCards {
                        pile_creator_condition: PlayerCondition::OpponentOf(
                            PlayerVariable::EffectController),
                        pile_selector_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectController),
                        zone: CardPileSource::TopOfLibrary(Count::Fixed(5)),
                        selected_pile_effect_type: Some(CardEffectType::MoveTo(CardZoneType::Hand)),
                        unselected_pile_effect_type: Some(CardEffectType::MoveTo(CardZoneType::Graveyard)),
                    }
                ]
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Faerie Squadron")
            .expansion(Expansion::Invasion)
            .card_number(58)
            .rarity(Rarity::Common)
            .artist_name("rk post")
            .flavor_text(
r#""The stone whispers to me of dragon's fire and darkness. I wish I'd never pried it from the figurehead of that sunken Keldon longship."
—Isel, master carver"#)
            .casting_cost("U")
            .not_snow()
            .non_legendary()
            .creature("1/1 Faerie")
            .kicker("3U")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(2)
                .static_ability(StaticAbility::keyword(Keyword::Flying))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Mana Maze")
            .expansion(Expansion::Invasion)
            .card_number(59)
            .rarity(Rarity::Rare)
            .artist_name("Rebecca Guay")
            .flavor_text(
r#""Those who know only one path to victory can never hope to triumph."
—The Blind Seer"#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::PreventCasting {
                condition: CardCondition::AnyColor(ColorsVariable::PreviousSpellCast),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Manipulate Fate")
            .expansion(Expansion::Invasion)
            .card_number(60)
            .rarity(Rarity::Uncommon)
            .artist_name("John Matson")
            .no_flavor_text()
            .casting_cost("3U")
            .instant()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                card_count: CardCount::Predetermined(Count::Fixed(3)),
                card_condition: CardCondition::Any,
                destination: CardLocation::Exile,
            }))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Metathran Aerostat")
            .expansion(Expansion::Invasion)
            .card_number(61)
            .rarity(Rarity::Rare)
            .artist_name("Greg Staples")
            .no_flavor_text()
            .casting_cost("2UU")
            .not_snow()
            .non_legendary()
            .creature("2/2 Metathran")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U"))
                .add_effect(EffectTemplate::Sequence(vec![
                    EffectTemplate::Card {
                        effect_types: vec![
                            CardEffectType::MoveToBattlefield {
                                new_controller: PlayerVariable::EffectController,
                            }
                        ],
                        selecting: Selecting::one_target(),
                        selection_condition: CardCondition::CompareConvertedManaCost(
                            IntegerCondition::EqualTo(Count::ManaVariable(ColorCondition::Any))),
                        selection_owner: PlayerVariable::EffectController,
                        card_zone: CardZoneType::Hand,
                    },
                    EffectTemplate::self_effect(PermanentEffectType::bounce()),
                ]))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Metathran Transport")
            .expansion(Expansion::Invasion)
            .card_number(62)
            .rarity(Rarity::Uncommon)
            .artist_name("Glen Angus")
            .no_flavor_text()
            .casting_cost("1UU")
            .not_snow()
            .non_legendary()
            .creature("1/3 Metathran")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::UnblockableBy(PermanentCondition::blue())))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U"))
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideColors {
                        new_colors: vec![ColorVariable::Fixed(Color::Blue)],
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Metathran Zombie")
            .expansion(Expansion::Invasion)
            .card_number(63)
            .rarity(Rarity::Common)
            .artist_name("Arnie Swekel")
            .flavor_text(
r#""Rise, metathran. You shall serve a nobler cause in death."
—Tsabo Tavoc, Phyrexian general"#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("1/1 Metathran")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Regenerate))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Opt")
            .expansion(Expansion::Invasion)
            .card_number(64)
            .rarity(Rarity::Common)
            .artist_name("John Howe")
            .no_flavor_text()
            .casting_cost("U")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![PlayerEffectType::Scry(Count::Fixed(1))],
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phantasmal Terrain")
            .expansion(Expansion::Invasion)
            .card_number(63)
            .rarity(Rarity::Common)
            .artist_name("Dana Knutson")
            .no_flavor_text()
            .casting_cost("UU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::land(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::OverrideBasicLandTypes {
                            new_basic_land_types: vec![BasicLandTypeVariable::AuraEntersTheBattlefield],
                        }))
                    .build(),
            })
            .into_regular_permanent()
        ),

        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Probe")
            .expansion(Expansion::Invasion)
            .card_number(66)
            .rarity(Rarity::Common)
            .artist_name("Eric Peterson")
            .no_flavor_text()
            .casting_cost("2U")
            .sorcery()
            .kicker("1B")
            .add_kicker_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![PlayerEffectType::discard(2)],
            })
            .add_effect(EffectTemplate::Sequence(vec![
                EffectTemplate::controller_effect(PlayerEffectType::draw(3)),
                EffectTemplate::controller_effect(PlayerEffectType::discard(2)),
            ]))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Prohibit")
            .expansion(Expansion::Invasion)
            .card_number(67)
            .rarity(Rarity::Common)
            .artist_name("Eric Peterson")
            .no_flavor_text()
            .casting_cost("1U")
            .instant()
            .kicker("2")
            .add_effect(EffectTemplate::Sequence(vec![
                EffectTemplate::StackObject {
                    effect_types: vec![StackObjectEffectType::Counter],
                    selecting: Selecting::one_target(),
                    stack_object_types: to_set(vec![
                        StackObjectType::spell_matching_condition(
                            CardCondition::CompareConvertedManaCost(
                                IntegerCondition::LessThanOrEqualTo(
                                    Count::IfElse {
                                        condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                                        then: Box::new(Count::Fixed(4)),
                                        otherwise: Box::new(Count::Fixed(2)),
                                    },
                                )
                            ),
                        )
                    ]),
                    selection_owner_condition: PlayerCondition::Any,
                }
            ]))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Psychic Battle")
            .expansion(Expansion::Invasion)
            .card_number(68)
            .rarity(Rarity::Rare)
            .artist_name("Ray Lago")
            .no_flavor_text()
            .casting_cost("3UU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::TargetsChosen,
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::StackObject {
                        effect_types: vec![
                            StackObjectEffectType::ScryBattle {
                                winner_effect_type: Box::new(StackObjectEffectType::ChangeTargets),
                            }
                        ],
                        selecting: Selecting::one_target(),
                        stack_object_types: StackObjectType::any(),
                        selection_owner_condition: PlayerCondition::Any,
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rainbow Crow")
            .expansion(Expansion::Invasion)
            .card_number(69)
            .rarity(Rarity::Uncommon)
            .artist_name("Edward P. Beard, Jr.")
            .flavor_text(
                r#"Children claim no two feathers are exactly the same color, then eagerly gather them for proof."#)
            .casting_cost("3U")
            .not_snow()
            .non_legendary()
            .creature("2/2 Bird")
            .keyword(Keyword::Flying)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideColors {
                        new_colors: vec![ColorVariable::AbilityInput],
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Repulse")
            .expansion(Expansion::Invasion)
            .card_number(70)
            .rarity(Rarity::Common)
            .artist_name("Eric Peterson")
            .flavor_text(r#""You aren't invited.""#)
            .casting_cost("2U")
            .instant()
            .add_effect(EffectTemplate::target_creature(PermanentEffectType::bounce()))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sapphire Leech")
            .expansion(Expansion::Invasion)
            .card_number(71)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spencer")
            .flavor_text(
r#""The subject's wings are clearly vestigial. We suspect the gems somehow keep it aloft."
—Tolarian research notes"#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("2/2 Leech")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::blue(),
                modifier: CostModifier::Add(mana_cost("U")),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shimmering Wings")
            .expansion(Expansion::Invasion)
            .card_number(72)
            .rarity(Rarity::Common)
            .artist_name("Carl Critchlow")
            .flavor_text(
r#""Wings are but fins for swimming the sky."
—Empress Galina"#)
            .casting_cost("U")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .keyword(Keyword::Flying)
                    .build(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::bounce()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shoreline Raider")
            .expansion(Expansion::Invasion)
            .card_number(73)
            .rarity(Rarity::Common)
            .artist_name("Nelson DeCastro")
            .flavor_text(
r#""This strange new beast makes for an excellent meal. Get me more."
—Empress Galina"#)
            .casting_cost("2U")
            .not_snow()
            .non_legendary()
            .creature("2/2 Merfolk")
            .keyword(Keyword::Protection(CardCondition::creature_type(CreatureType::Kavu)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sky Weaver")
            .expansion(Expansion::Invasion)
            .card_number(74)
            .rarity(Rarity::Uncommon)
            .artist_name("Christopher Moeller")
            .flavor_text(r#""Let my wisdom give you wings.""#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("2/1 Metathran")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::Permanent {
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Or(vec![
                        PermanentCondition::white(),
                        PermanentCondition::black(),
                    ]),
                    effect_types: vec![
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Flying),
                        }
                    ]
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Stormscape Master")
            .expansion(Expansion::Invasion)
            .card_number(76)
            .rarity(Rarity::Rare)
            .artist_name("Hannibal King")
            .no_flavor_text()
            .casting_cost("2UU")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("WW").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(
                        Keyword::Protection(CardCondition::Color(ColorVariable::AbilityInput))),
                }))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("BB").self_tap())
                .add_effect(EffectTemplate::Player {
                    selecting: SelectingPlayers::one_target(),
                    effect_types: vec![PlayerEffectType::lose_life(2)],
                })
                .add_effect(EffectTemplate::Player {
                    selecting: SelectingPlayers::effect_controller(),
                    effect_types: vec![PlayerEffectType::gain_life(2)],
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Sway of Illusion")
            .expansion(Expansion::Invasion)
            .card_number(77)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg & Tim Hildebrandt")
            .flavor_text(
r#""I suggest you take a closer look."
—Tidal visionary"#)
            .casting_cost("1U")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::AnyNumberOfTargets,
                selection_condition: PermanentCondition::creature(),
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::OverrideColors {
                            new_colors: vec![ColorVariable::ResolvedInput],
                        },
                    }
                ]
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        // TODO: Good unit test target.
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Teferi's Response")
            .expansion(Expansion::Invasion)
            .card_number(78)
            .rarity(Rarity::Rare)
            .artist_name("Scott Bailey")
            .no_flavor_text()
            .casting_cost("1U")
            .instant()
            .add_effect(EffectTemplate::StackObject {
                effect_types: vec![StackObjectEffectType::Counter],
                selecting: Selecting::one_target(),
                stack_object_types: StackObjectType::any_type_with_target_of_target_condition(
                    CardCondition::Land,
                ),
                selection_owner_condition: PlayerCondition::OpponentOf(
                    PlayerVariable::EffectController),
            })
            // Since this is a PermanentEffect, it won't happen unless a permanent was the source
            // of the countered stack object.
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::Specific(ObjectVariable::RecipientEffectSource),
                selection_condition: PermanentCondition::Any,
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::Draw(Count::Fixed(2))))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Temporal Distortion")
            .expansion(Expansion::Invasion)
            .card_number(79)
            .rarity(Rarity::Rare)
            .artist_name("Stephanie Law")
            .no_flavor_text()
            .casting_cost("3UU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_triggered_ability(GlobalTriggeredAbility {
                permanent_condition: PermanentCondition::Or(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::land(),
                ]),
                ability: TriggeredAbility {
                    trigger_condition: TriggerCondition::SelfBecomesTapped,
                    execution_condition: Condition::Always,
                    player_option: PlayerOption::Must,
                    cost: None,
                    effects: vec![
                        EffectTemplate::self_effect(PermanentEffectType::AddCounters(
                            Count::Fixed(1),
                            CounterType::Hourglass,
                        ))
                    ],
                    otherwise_effects: Vec::new(),
                }
            })
            .global_static_ability(ContinuousGlobalEffectType::PermanentCannot {
                condition: PermanentCondition::HasCounter(CounterType::Hourglass),
                effect_types: vec![PermanentCannotEffectType::CannotUntapDuringControllersUntapStep],
            })
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::All,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Permanent {
                        condition: PermanentCondition::ControlledBy(
                            PlayerCondition::Matches(PlayerVariable::Active)),
                        effect_types: vec![PermanentEffectType::RemoveAllCounters(CounterType::Hourglass)],
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tidal Visionary")
            .expansion(Expansion::Invasion)
            .card_number(80)
            .rarity(Rarity::Common)
            .artist_name("Glen Angus")
            .no_flavor_text()
            .casting_cost("U")
            .not_snow()
            .non_legendary()
            .creature("1/1 Merfolk")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideColors {
                        new_colors: vec![ColorVariable::AbilityInput],
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tolarian Emissary")
            .expansion(Expansion::Invasion)
            .card_number(81)
            .rarity(Rarity::Uncommon)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("2U")
            .not_snow()
            .non_legendary()
            .creature("1/2 Human")
            .kicker("1W")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(
                    EffectTemplate::target_enchantment(PermanentEffectType::destroy()))
                .build()
            )
            .keyword(Keyword::Flying)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tower Drake")
            .expansion(Expansion::Invasion)
            .card_number(82)
            .rarity(Rarity::Uncommon)
            .artist_name("Ron Spencer")
            .flavor_text("Young tower drakes quickly learn to maneuver among Benalia's many spires.")
            .casting_cost("2U")
            .not_snow()
            .non_legendary()
            .creature("2/1 Human")
            .keyword(Keyword::Flying)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::adjust_power_toughness(0, 1),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Traveler's Cloak")
            .expansion(Expansion::Invasion)
            .card_number(83)
            .rarity(Rarity::Common)
            .artist_name("Rebecca Guay")
            .no_flavor_text()
            .casting_cost("2U")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .keyword(Keyword::LandWalk(BasicLandTypeVariable::AuraEntersTheBattlefield))
                    .build(),
            })
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vodalian Hypnotist")
            .expansion(Expansion::Invasion)
            .card_number(84)
            .rarity(Rarity::Uncommon)
            .artist_name("Rebecca Guay")
            .flavor_text(r#""Deceit is the heart of war.""#)
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("1/1 Merfolk")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2B").self_tap())
                .sorcery_speed()
                .add_effect(EffectTemplate::target_player(PlayerEffectType::discard(1)))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vodalian Merchant")
            .expansion(Expansion::Invasion)
            .card_number(85)
            .rarity(Rarity::Common)
            .artist_name("Scott M. Fischer")
            .flavor_text("Not choosing sides in a war can be dangerous—but lucrative.")
            .casting_cost("1U")
            .not_snow()
            .non_legendary()
            .creature("1/2 Merfolk")
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::discard(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vodalian Serpent")
            .expansion(Expansion::Invasion)
            .card_number(86)
            .rarity(Rarity::Common)
            .artist_name("Christopher Moeller")
            .no_flavor_text()
            .casting_cost("3U")
            .not_snow()
            .non_legendary()
            .creature("2/2 Serpent")
            .kicker("2")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(4)
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Wash Out")
            .expansion(Expansion::Invasion)
            .card_number(87)
            .rarity(Rarity::Uncommon)
            .artist_name("Matthew D. Wilson")
            .flavor_text(
r#""Rest now. You've neither won nor lost, but this battle is over for you just the same."
—Teferi"#)
            .casting_cost("3U")
            .sorcery()
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::color(ColorVariable::ResolvedInput),
                effect_types: vec![PermanentEffectType::bounce()],
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Well-Laid Plans")
            .expansion(Expansion::Invasion)
            .card_number(88)
            .rarity(Rarity::Rare)
            .artist_name("Kev Walker")
            .flavor_text(r#""I knew this day would come," said Urza. Looking at the destruction, Barrin sighed, "You don't have to revel in it.""#)
            .casting_cost("2U")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Damage {
                effect_types: vec![
                    ContinuousDamageEffectType::PreventAllDamageFromAllMatchingSources {
                        source_condition: PermanentCondition::And(vec![
                            PermanentCondition::creature(),
                            // SelfColors refers to the colors of the creature being protected,
                            // not the colors of Well-Laid Plans.
                            PermanentCondition::any_color(ColorsVariable::SelfColors),
                        ]),
                    }
                ],
                allowed_target_types: DamageRecipientTypes::any_creature(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Worldly Counsel")
            .expansion(Expansion::Invasion)
            .card_number(89)
            .rarity(Rarity::Common)
            .artist_name("Gary Ruddell")
            .no_flavor_text()
            .casting_cost("1U")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![
                    PlayerEffectType::LookAtCardsFromTopOfLibraryAndChooseOne {
                        card_count: Count::SourceControllerBasicLandTypes,
                        chosen_card_destination: CardLocation::Hand,
                        unchosen_cards_destination: CardLocation::Library(LibraryLocation::Bottom),
                        should_shuffle_afterwards: false,
                        should_reveal: false,
                    }
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Zanam Djinn")
            .expansion(Expansion::Invasion)
            .card_number(90)
            .rarity(Rarity::Uncommon)
            .artist_name("Eric Peterson")
            .no_flavor_text()
            .casting_cost("5U")
            .not_snow()
            .non_legendary()
            .creature("5/6 Djinn")
            .keyword(Keyword::Flying)
            .static_ability(StaticAbility::Conditional {
                condition: Condition::MostCommonPermanentColorOrTied(Color::Blue),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(-2, -2),
                ))
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Addle")
            .expansion(Expansion::Invasion)
            .card_number(91)
            .rarity(Rarity::Uncommon)
            .artist_name("Ron Spears")
            .flavor_text(
r#""I'll wring out your tiny mind like a sponge."
—Urborg witch"#)
            .casting_cost("1B")
            .sorcery()
            .add_effect(EffectTemplate::target_player(PlayerEffectType::ChooseDiscard {
                count: Count::Fixed(1),
                card_condition: CardCondition::Color(ColorVariable::AbilityInput)
            }))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Agonizing Demise")
            .expansion(Expansion::Invasion)
            .card_number(92)
            .rarity(Rarity::Common)
            .artist_name("Mark Brill")
            .no_flavor_text()
            .casting_cost("3B")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::DestroyWithNoRegeneration,
                    PermanentEffectType::DealDamageToControllerOfTarget(PermanentAttributeCount::Power),
                ],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::not(PermanentCondition::black()),
                    PermanentCondition::creature(),
                ]),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Andralite Leech")
            .expansion(Expansion::Invasion)
            .card_number(93)
            .rarity(Rarity::Rare)
            .artist_name("Wayne England")
            .flavor_text(
r#""Older specimens are completely encrusted with gems, which serve as both armor and weapons."
—Tolarian research notes"#)
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .creature("2/2 Leech")
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::black(),
                modifier: CostModifier::Add(mana_cost("B")),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Annihilate")
            .expansion(Expansion::Invasion)
            .card_number(94)
            .rarity(Rarity::Uncommon)
            .artist_name("Kev Walker")
            .flavor_text(
r#""Whatever Yawgmoth marks, dies. There is no escape."
—Tsabo Tavoc, Phyrexian general"#)
            .casting_cost("3BB")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::not(PermanentCondition::black()),
                    PermanentCondition::creature(),
                ]),
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Bog Initiate")
            .expansion(Expansion::Invasion)
            .card_number(95)
            .rarity(Rarity::Common)
            .artist_name("rk post")
            .flavor_text(
r#""Urborg's magic is strong. Did Urza send you to protect us or to protect against us?"
—Urborg witch, to Barrin"#)
            .casting_cost("1B")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1"))
                .mana_produced("B")
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Cremate")
            .expansion(Expansion::Invasion)
            .card_number(96)
            .rarity(Rarity::Uncommon)
            .artist_name("Andrew Goldhawk")
            .flavor_text(r#"Death's embrace need not be cold."#)
            .casting_cost("B")
            .instant()
            .add_effect(EffectTemplate::Card {
                selecting: Selecting::one_target(),
                selection_condition: CardCondition::Any,
                card_zone: CardZoneType::Graveyard,
                selection_owner: PlayerVariable::EffectController,
                effect_types: vec![CardEffectType::MoveTo(CardZoneType::Exile)],
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crypt Angel")
            .expansion(Expansion::Invasion)
            .card_number(97)
            .rarity(Rarity::Rare)
            .artist_name("Todd Lockwood")
            .flavor_text(r#"Once an angel, now an abomination."#)
            .casting_cost("4B")
            .not_snow()
            .non_legendary()
            .creature("3/3 Angel")
            .keyword(Keyword::Flying)
            .keyword(Keyword::Protection(CardCondition::white()))
            .enter_the_battlefield_ability(EffectTemplate::Card {
                effect_types: vec![CardEffectType::MoveTo(CardZoneType::Hand)],
                selecting: Selecting::one_target(),
                selection_condition: CardCondition::Or(vec![
                    CardCondition::blue(),
                    CardCondition::red(),
                ]),
                selection_owner: PlayerVariable::EffectController,
                card_zone: CardZoneType::Graveyard,
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Cursed Flesh")
            .expansion(Expansion::Invasion)
            .card_number(98)
            .rarity(Rarity::Common)
            .artist_name("Chippy")
            .flavor_text(r#"Misery, filth, disease—all things that bring suffering to the world—only make Phyrexians more dangerous."#)
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(-1, -1)))
                    .keyword(Keyword::Fear)
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Defiling Tears")
            .expansion(Expansion::Invasion)
            .card_number(99)
            .rarity(Rarity::Uncommon)
            .artist_name("rk post")
            .flavor_text(r#"Serra's living warriors looked on in horror. "Where is our Lady now?" they cried."#)
            .casting_cost("2B")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::Any,
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::OverrideColors {
                            new_colors: vec![ColorVariable::Fixed(Color::Black)]
                        }
                    },
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type:
                            ContinuousPermanentEffectType::adjust_power_toughness(1, -1),
                    },
                ]
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Desperate Research")
            .expansion(Expansion::Invasion)
            .card_number(100)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("1B")
            .sorcery()
            .add_effect(EffectTemplate::controller_effect(
                PlayerEffectType::RevealFromTopOfLibraryAndTakeMatching {
                    count: Count::Fixed(7),
                    card_condition: CardCondition::Name(CardNameVariable::NonBasicLandInput),
                    matching_cards_destination: CardLocation::Hand,
                    non_matching_cards_destination: CardLocation::Exile,
                    should_shuffle_afterwards: false,
                }
            ))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Devouring Strossus")
            .expansion(Expansion::Invasion)
            .card_number(101)
            .rarity(Rarity::Rare)
            .artist_name("D. Alexander Gregory")
            .no_flavor_text()
            .casting_cost("5BBB")
            .not_snow()
            .non_legendary()
            .creature("9/9 Horror")
            .keyword(Keyword::Flying)
            .keyword(Keyword::Trample)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::effect_controller(),
                        effect_types: vec![
                            PlayerEffectType::Sacrifice{
                                count: Count::Fixed(1),
                                permanent_condition: PermanentCondition::creature(),
                            }
                        ],
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().sacrifice_count(1, PermanentCondition::creature()))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Regenerate))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Do or Die")
            .expansion(Expansion::Invasion)
            .card_number(102)
            .rarity(Rarity::Rare)
            .artist_name("Christopher Moeller")
            .no_flavor_text()
            .casting_cost("1B")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::TwoPilesOfPermanents {
                        pile_creator_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectController),
                        pile_selector_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectRecipientController),
                        permanent_condition: PermanentCondition::creature(),
                        selected_pile_condition: PermanentGroupCondition::Any,
                        selected_pile_effect_type: Some(PermanentEffectType::DestroyWithNoRegeneration),
                        unselected_pile_effect_type: None,
                    }
                ]
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Dredge")
            .expansion(Expansion::Invasion)
            .card_number(103)
            .rarity(Rarity::Uncommon)
            .artist_name("Donato Giancola")
            .flavor_text(
r#""I'd strip away the world to see what's under it, but I'd never just leave it to these Phyrexian parasites and their false god."
—Urborg witch"#)
            .casting_cost("B")
            .instant()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::Sacrifice {
                count: Count::Fixed(1),
                permanent_condition: PermanentCondition::Or(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::land(),
                ])
            }))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Duskwalker")
            .expansion(Expansion::Invasion)
            .card_number(104)
            .rarity(Rarity::Common)
            .artist_name("David Martin")
            .no_flavor_text()
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .kicker("3B")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(2)
                .static_ability(StaticAbility::keyword(Keyword::Fear))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Exotic Curse")
            .expansion(Expansion::Invasion)
            .card_number(105)
            .rarity(Rarity::Common)
            .artist_name("Orizio Daniele")
            .flavor_text(
r#"Fouler than a necromancer's kiss.
—Jamuraan expression"#)
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                            Count::negative(Count::AuraControllerBasicLandTypes))))
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Firescreamer")
            .expansion(Expansion::Invasion)
            .card_number(106)
            .rarity(Rarity::Common)
            .artist_name("Alan Pollack")
            .flavor_text("In the dark, it's nearly invisible—until it exhales.")
            .casting_cost("3B")
            .not_snow()
            .non_legendary()
            .creature("2/2 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::adjust_power_toughness(1, 0),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Goham Djinn")
            .expansion(Expansion::Invasion)
            .card_number(107)
            .rarity(Rarity::Uncommon)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("5B")
            .not_snow()
            .non_legendary()
            .creature("5/5 Djinn")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1B"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Regenerate))
            )
            .static_ability(StaticAbility::Conditional {
                condition: Condition::MostCommonPermanentColorOrTied(Color::Black),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(-2, -2),
                ))
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Hate Weaver")
            .expansion(Expansion::Invasion)
            .card_number(108)
            .rarity(Rarity::Uncommon)
            .artist_name("Roger Raupp")
            .flavor_text(r#""Let my hate fuel your fury.""#)
            .casting_cost("1W")
            .not_snow()
            .non_legendary()
            .creature("2/1 Zombie")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::adjust_power_toughness(1, 0)
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Or(vec![
                        PermanentCondition::blue(),
                        PermanentCondition::red(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Hypnotic Cloud")
            .expansion(Expansion::Invasion)
            .card_number(109)
            .rarity(Rarity::Common)
            .artist_name("Randy Gallegos")
            .no_flavor_text()
            .casting_cost("1B")
            .sorcery()
            .kicker("4")
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::Discard(
                        Count::IfElse {
                            condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                            then: Box::new(Count::Fixed(3)),
                            otherwise: Box::new(Count::Fixed(1)),
                        }
                    )
                ]
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Marauding Knight")
            .expansion(Expansion::Invasion)
            .card_number(110)
            .rarity(Rarity::Rare)
            .artist_name("Daren Bader")
            .flavor_text(
r#""They ride our trails as if they were designed to," said the guard.
"They were," replied Gerrard."#)
            .casting_cost("2BB")
            .not_snow()
            .non_legendary()
            .creature("2/2 Zombie")
            .keyword(Keyword::Protection(CardCondition::white()))
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                    Count::permanent(PermanentCondition::And(vec![
                        PermanentCondition::plains(),
                        PermanentCondition::ControlledByOpponent,
                    ])),
                )
            ))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Mourning")
            .expansion(Expansion::Invasion)
            .card_number(111)
            .rarity(Rarity::Common)
            .artist_name("Terese Nielsen")
            .flavor_text(
                r#"Barrin waited for the nausea to pass or for Urza to say something else. He waited in vain."#)
            .casting_cost("1B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(-2, 0)))
                    .build()
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::bounce())))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Nightscape Apprentice")
            .expansion(Expansion::Invasion)
            .card_number(112)
            .rarity(Rarity::Common)
            .artist_name("Andrew Goldhawk")
            .no_flavor_text()
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .creature("1/1 Zombie")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::MoveTo(CardLocation::Library(LibraryLocation::Top))
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::And(vec![
                        PermanentCondition::creature(),
                        PermanentCondition::ControlledByController,
                    ]),
                })
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Any,
                    effect_types: vec![PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::FirstStrike)
                    }],
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Nightscape Master")
            .expansion(Expansion::Invasion)
            .card_number(113)
            .rarity(Rarity::Rare)
            .artist_name("Andrew Goldhawk")
            .no_flavor_text()
            .casting_cost("2BB")
            .not_snow()
            .non_legendary()
            .creature("2/2 Zombie")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("UU").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::bounce()))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("RR").self_tap())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(2)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_creature(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Battleflies")
            .expansion(Expansion::Invasion)
            .card_number(114)
            .rarity(Rarity::Common)
            .artist_name("Dan Frazier")
            .flavor_text(r#"After encountering them, Squee finally lost his appetite for bugs."#)
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .creature("0/1 Insect")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .per_turn_activation_limit(2)
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::adjust_power_toughness(1, 0),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Delver")
            .expansion(Expansion::Invasion)
            .card_number(115)
            .rarity(Rarity::Rare)
            .artist_name("Dana Knutson")
            .no_flavor_text()
            .casting_cost("3BB")
            .not_snow()
            .non_legendary()
            .creature("3/2 Zombie")
            .enter_the_battlefield_ability(EffectTemplate::Sequence(vec![
                EffectTemplate::Card {
                    effect_types: vec![
                        CardEffectType::MoveToBattlefield {
                            new_controller: PlayerVariable::EffectController,
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: CardCondition::Creature,
                    selection_owner: PlayerVariable::EffectController,
                    card_zone: CardZoneType::Graveyard,
                },
                EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::DealDamageToControllerOfTarget(
                            PermanentAttributeCount::ConvertedManaCost),
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::creature(),
                },
                EffectTemplate::Damage {
                    effect_types: vec![
                        DamageEffectType::Deal(Count::ConvertedManaCost(ObjectVariable::PreviousSelection))
                    ],
                    damage_recipients: DamageRecipients::Player(PlayerVariable::EffectController),
                }
            ]))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Infiltrator")
            .expansion(Expansion::Invasion)
            .card_number(116)
            .rarity(Rarity::Rare)
            .artist_name("Darrell Riche")
            .no_flavor_text()
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .creature("2/2 Minion")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2UU"))
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::ExchangeControl))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Reaper")
            .expansion(Expansion::Invasion)
            .card_number(116)
            .rarity(Rarity::Common)
            .artist_name("Sam Wood")
            .flavor_text(r#"It desires only to help others shed the itchy wet skin of life."#)
            .casting_cost("4B")
            .not_snow()
            .non_legendary()
            .creature("3/3 Zombie")
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfIsBlockedBy(PermanentCondition::green()),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Permanent {
                        effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                        selecting: Selecting::Specific(ObjectVariable::Blocker),
                        selection_condition: PermanentCondition::Any,
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Slayer")
            .expansion(Expansion::Invasion)
            .card_number(118)
            .rarity(Rarity::Common)
            .artist_name("Sam Wood")
            .flavor_text(r#"Benalia was only the first nation victimized by Tsabo Tavoc's specialized killers."#)
            .casting_cost("3B")
            .not_snow()
            .non_legendary()
            .creature("2/2 Minion")
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfIsBlockedBy(PermanentCondition::white()),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Permanent {
                        effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                        selecting: Selecting::Specific(ObjectVariable::Blocker),
                        selection_condition: PermanentCondition::Any,
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Plague Spitter")
            .expansion(Expansion::Invasion)
            .card_number(119)
            .rarity(Rarity::Rare)
            .artist_name("Chippy")
            .no_flavor_text()
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .creature("2/2 Horror")
            .enter_the_graveyard_ability(EffectTemplate::Global(GlobalEffectType::Damage {
                effect_type: DamageEffectType::deal(1),
                target_types: DamageRecipientTypes {
                    creatures: Some(PermanentCondition::Any),
                    players: Some(SelectingPlayers::all()),
                    planeswalkers: false,
                },
            }))
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Damage {
                        effect_type: DamageEffectType::deal(1),
                        target_types: DamageRecipientTypes {
                            creatures: Some(PermanentCondition::Any),
                            players: Some(SelectingPlayers::all()),
                            planeswalkers: false,
                        },
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ravenous Rats")
            .expansion(Expansion::Invasion)
            .card_number(120)
            .rarity(Rarity::Common)
            .artist_name("Tom Wänerstrand")
            .flavor_text(
                r#"Nothing is sacred to the rats. Everything is simply another meal."#)
            .casting_cost("1B")
            .not_snow()
            .non_legendary()
            .creature("1/1 Rat")
            .enter_the_battlefield_ability(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target_opponent_of_effect_controller(),
                effect_types: vec![PlayerEffectType::discard(1)],
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Reckless Spite")
            .expansion(Expansion::Invasion)
            .card_number(121)
            .rarity(Rarity::Uncommon)
            .artist_name("Chippy")
            .no_flavor_text()
            .casting_cost("1BB")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::Targets(Count::Fixed(2)),
                selection_condition: PermanentCondition::creature(),
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::lose_life(5)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Recover")
            .expansion(Expansion::Invasion)
            .card_number(122)
            .rarity(Rarity::Common)
            .artist_name("Nelson DeCastro")
            .flavor_text(
                r#"As Barrin exhumed his daughter's body, he finally realized the full price of his faith in Urza."#)
            .casting_cost("2B")
            .sorcery()
            .add_effect(EffectTemplate::Card {
                effect_types: vec![CardEffectType::MoveTo(CardZoneType::Hand)],
                selecting: Selecting::one_target(),
                selection_condition: CardCondition::Creature,
                selection_owner: PlayerVariable::EffectController,
                card_zone: CardZoneType::Graveyard,
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Scavenged Weaponry")
            .expansion(Expansion::Invasion)
            .card_number(123)
            .rarity(Rarity::Common)
            .artist_name("Alan Pollack")
            .flavor_text(
                r#""The Phyrexians have progressed," admired Urza. "Their parts are interchangeable.""#)
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(1, 1)))
                    .build(),
            })
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Soul Burn")
            .expansion(Expansion::Invasion)
            .card_number(124)
            .rarity(Rarity::Common)
            .artist_name("Andrew Goldhawk")
            .no_flavor_text()
            .casting_cost("2B")
            .sorcery()
            .add_effect(EffectTemplate::Damage {
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
                effect_types: vec![
                    DamageEffectType::CappedDrain {
                        damage_count: Count::ManaVariable(ColorCondition::Or(vec![
                            Color::Black,
                            Color::Red,
                        ])),
                        additional_cap: Some(Count::MatchingColorInCost(Color::Black)),
                    },
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Spreading Plague")
            .expansion(Expansion::Invasion)
            .card_number(125)
            .rarity(Rarity::Rare)
            .artist_name("Scott Bailey")
            .flavor_text(r#"The cruelest strain of plague keeps its hosts alive long enough to return home and infect their families."#)
            .casting_cost("4B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::EntersBattlefield(PermanentCondition::creature()),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Permanent {
                        condition: PermanentCondition::any_color(ColorsVariable::TriggerObject),
                        effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tainted Well")
            .expansion(Expansion::Invasion)
            .card_number(126)
            .rarity(Rarity::Common)
            .artist_name("Val Mayerik")
            .flavor_text(
r#""No, you get the water. I ain't thirsty."
—Squee"#)
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::land(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::OverrideBasicLandTypes {
                            new_basic_land_types: vec![BasicLandTypeVariable::swamp()],
                        }))
                    .build(),
            })
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Trench Wurm")
            .expansion(Expansion::Invasion)
            .card_number(127)
            .rarity(Rarity::Uncommon)
            .artist_name("Wayne England")
            .flavor_text(
r#""Their arrival marks the end of Yawgmoth's subtlety."
—Urza"#)
            .casting_cost("3B")
            .not_snow()
            .non_legendary()
            .creature("3/3 Wurm")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2R").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![PermanentEffectType::destroy()],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::non_basic_land(),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tsabo's Assassin")
            .expansion(Expansion::Invasion)
            .card_number(128)
            .rarity(Rarity::Rare)
            .artist_name("Glen Angus")
            .no_flavor_text()
            .casting_cost("2BB")
            .not_snow()
            .non_legendary()
            .creature("1/1 Zombie")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2R").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::And(vec![
                        PermanentCondition::creature(),
                        PermanentCondition::MostCommonPermanentColorOrTied,
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Tsabo's Decree")
            .expansion(Expansion::Invasion)
            .card_number(129)
            .rarity(Rarity::Rare)
            .artist_name("Thomas M. Baxa")
            .no_flavor_text()
            .casting_cost("5B")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::DiscardMatching {
                        card_condition: CardCondition::CreatureType(CreatureTypeVariable::Input)
                    }
                ],
            })
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::creature_type_variable(CreatureTypeVariable::Input),
                ]),
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
            }))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Twilight's Call")
            .expansion(Expansion::Invasion)
            .card_number(130)
            .rarity(Rarity::Rare)
            .artist_name("Mark Romanoski")
            .no_flavor_text()
            .casting_cost("4BB")
            .sorcery_with_instant_speed_upgrade(
                cost().mana("2"),
            )
            .add_effect(EffectTemplate::Global(GlobalEffectType::Card {
                effect_type: CardEffectType::MoveToBattlefield {
                    new_controller: PlayerVariable::EffectRecipientController,
                },
                condition: CardCondition::Creature,
                card_zone: CardZoneType::Graveyard,
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Emissary")
            .expansion(Expansion::Invasion)
            .card_number(131)
            .rarity(Rarity::Uncommon)
            .artist_name("Eric Peterson")
            .no_flavor_text()
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .creature("3/1 Human")
            .kicker("1U")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(EffectTemplate::target_permanent(PermanentEffectType::bounce()))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Phantom")
            .expansion(Expansion::Invasion)
            .card_number(132)
            .rarity(Rarity::Common)
            .artist_name("Daren Bader")
            .flavor_text(r#"A chilling fog with teeth of ice."#)
            .casting_cost("2B")
            .not_snow()
            .non_legendary()
            .creature("3/1 Spirit")
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::CannotBlock))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("U"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::CannotDealCombatDamage,
                }))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::CannotBeDealtCombatDamage,
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Shambler")
            .expansion(Expansion::Invasion)
            .card_number(133)
            .rarity(Rarity::Uncommon)
            .artist_name("Pete Venters")
            .flavor_text(r#"A writhing mass of rot, with sharp claws and a vicious spirit."#)
            .casting_cost("2BB")
            .not_snow()
            .non_legendary()
            .creature("4/3 Horror")
            .static_ability(StaticAbility::Global(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::NotItself,
                ]),
                effect_types: vec![
                    ContinuousPermanentEffectType::adjust_power_toughness(-1, -1),
                ]
            }))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Skeleton")
            .expansion(Expansion::Invasion)
            .card_number(134)
            .rarity(Rarity::Common)
            .artist_name("Alan Pollack")
            .no_flavor_text()
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .creature("0/1 Skeleton")
            .kicker("3")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(1)
                .build()
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Regenerate))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Yawgmoth's Agenda")
            .expansion(Expansion::Invasion)
            .card_number(135)
            .rarity(Rarity::Rare)
            .artist_name("Arnie Swekel")
            .no_flavor_text()
            .casting_cost("3BB")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .static_ability(StaticAbility::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_type: ContinuousPlayerEffectType::MaxSpellsPerTurn(Count::Fixed(1)),
            })
            .static_ability(StaticAbility::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_type: ContinuousPlayerEffectType::CardsMayBePlayedFromGraveyard,
            })
            .static_ability(StaticAbility::PlayerReplacement {
                selecting: SelectingPlayers::effect_controller(),
                replacement_type: PlayerReplacementEffectType::ZoneRedirect {
                    regular_zone: CardZoneType::Graveyard,
                    override_zone: CardZoneType::Exile,
                },
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ancient Kavu")
            .expansion(Expansion::Invasion)
            .card_number(136)
            .rarity(Rarity::Common)
            .artist_name("Glen Angus")
            .flavor_text(
                r#"Those with the ability to change their nature survived Phyrexia's biological attacks. Everything else died."#)
            .casting_cost("B")
            .not_snow()
            .non_legendary()
            .creature("3/3 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideColors {
                        new_colors: Vec::new(),
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Bend or Break")
            .expansion(Expansion::Invasion)
            .card_number(137)
            .rarity(Rarity::Rare)
            .artist_name("Mark Romanoski")
            .no_flavor_text()
            .casting_cost("3R")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::all(),
                effect_types: vec![
                    PlayerEffectType::TwoPilesOfPermanents {
                        pile_creator_condition: PlayerCondition::Matches(
                            PlayerVariable::EffectRecipientController),
                        pile_selector_condition: PlayerCondition::OpponentOf(
                            PlayerVariable::EffectRecipientController),
                        permanent_condition: PermanentCondition::land(),
                        selected_pile_condition: PermanentGroupCondition::Any,
                        selected_pile_effect_type: Some(PermanentEffectType::destroy()),
                        unselected_pile_effect_type: Some(PermanentEffectType::Tap),
                    }
                ],
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Breath of Darigaaz")
            .expansion(Expansion::Invasion)
            .card_number(138)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg & Tim Hildebrandt")
            .no_flavor_text()
            .casting_cost("1R")
            .sorcery()
            .kicker("2")
            .add_effect(EffectTemplate::Global(GlobalEffectType::Damage {
                effect_type: DamageEffectType::Deal(
                    Count::IfElse {
                        condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                        then: Box::new(Count::Fixed(4)),
                        otherwise: Box::new(Count::Fixed(2)),
                    }
                ),
                target_types: DamageRecipientTypes {
                    creatures: Some(PermanentCondition::not(PermanentCondition::Flying)),
                    players: Some(SelectingPlayers::all()),
                    planeswalkers: false,
                },
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Callous Giant")
            .expansion(Expansion::Invasion)
            .card_number(139)
            .rarity(Rarity::Rare)
            .artist_name("Mark Brill")
            .flavor_text(
                r#""The invasion has awoken slumbering mountains," noted Urza. "Good. We can use all the help we can get.""#)
            .casting_cost("4RR")
            .not_snow()
            .non_legendary()
            .creature("4/4 Giant")
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::IgnoreDamage(
                    IntegerCondition::LessThanOrEqualTo(Count::Fixed(3)))
            ))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Chaotic Strike")
            .expansion(Expansion::Invasion)
            .card_number(140)
            .rarity(Rarity::Uncommon)
            .artist_name("Massimilano Frezzato")
            .no_flavor_text()
            .casting_cost("1R")
            .instant_with_casting_window_constraint(
                CastingWindowConstraint::CombatAfterBlockersDeclared,
            )
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Conditional {
                condition: PermanentCondition::CoinFlip,
                conditional_effect_type: Box::new(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                }),
            }))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Collapsing Borders")
            .expansion(Expansion::Invasion)
            .card_number(141)
            .rarity(Rarity::Rare)
            .artist_name("Glen Angus")
            .no_flavor_text()
            .casting_cost("3R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::All,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::active(),
                        effect_types: vec![
                            PlayerEffectType::GainLife(Count::SourceControllerBasicLandTypes),
                            PlayerEffectType::lose_life(3),
                        ],
                    },
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crown of Flames")
            .expansion(Expansion::Invasion)
            .card_number(142)
            .rarity(Rarity::Common)
            .artist_name("Christopher Moeller")
            .flavor_text(
r#""All my life I've fought these monsters. Today I finish that fight."
—Barrin"#)
            .casting_cost("R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .activated_ability(ActivatedAbilityBuilder::new()
                        .cost(cost().mana("R"))
                        .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(1, 0),
                        }))
                        .into_ability()
                    )
                    .build()
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::bounce()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Firebrand Ranger")
            .expansion(Expansion::Invasion)
            .card_number(143)
            .rarity(Rarity::Uncommon)
            .artist_name("Quinton Hoover")
            .flavor_text(
                r#"A skilled ranger can glance at the mud on your boots and tell where you last camped."#)
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("2/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G").self_tap())
                .add_effect(EffectTemplate::Card {
                    effect_types: vec![
                        CardEffectType::MoveToBattlefield {
                            new_controller: PlayerVariable::EffectController,
                        }
                    ],
                    selecting: Selecting::NonTargets(Count::Fixed(1)),
                    selection_condition: CardCondition::BasicLand,
                    selection_owner: PlayerVariable::EffectController,
                    card_zone: CardZoneType::Hand,
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Ghitu Fire")
            .expansion(Expansion::Invasion)
            .card_number(144)
            .rarity(Rarity::Rare)
            .artist_name("Glen Angus")
            .no_flavor_text()
            .casting_cost("R")
            .sorcery_with_instant_speed_upgrade(
                cost().mana("2"),
            )
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![DamageEffectType::Deal(Count::ManaVariable(ColorCondition::Any))],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Goblin Spy")
            .expansion(Expansion::Invasion)
            .card_number(145)
            .rarity(Rarity::Uncommon)
            .artist_name("Scott M. Fischer")
            .flavor_text(
r#""Isn't he on our side?"
"Yep."
"Why's he spyin' on us?"
"Don't ask.""#)
            .casting_cost("R")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("4/4 Golem")
            .static_ability(StaticAbility::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_type: ContinuousPlayerEffectType::PlayWithTopLibraryCardRevealed,
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Halam Djinn")
            .expansion(Expansion::Invasion)
            .card_number(146)
            .rarity(Rarity::Uncommon)
            .artist_name("Adam Rex")
            .no_flavor_text()
            .casting_cost("5R")
            .not_snow()
            .non_legendary()
            .creature("6/5 Djinn")
            .static_ability(StaticAbility::keyword(Keyword::Haste))
            .static_ability(StaticAbility::Conditional {
                condition: Condition::MostCommonPermanentColorOrTied(Color::Red),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(-2, -2)
                ))
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Hooded Kavu")
            .expansion(Expansion::Invasion)
            .card_number(147)
            .rarity(Rarity::Common)
            .artist_name("John Howe")
            .flavor_text(r#"The spread of its hood eclipses all hope."#)
            .casting_cost("5R")
            .not_snow()
            .non_legendary()
            .creature("2/2 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Fear),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Aggressor")
            .expansion(Expansion::Invasion)
            .card_number(148)
            .rarity(Rarity::Common)
            .artist_name("Christopher Moeller")
            .no_flavor_text()
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("3/2 Kavu")
            .kicker("4")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(1)
                .build()
            )
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::CannotBlock))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Monarch")
            .expansion(Expansion::Invasion)
            .card_number(149)
            .rarity(Rarity::Rare)
            .artist_name("Terese Nielsen")
            .flavor_text(
                r#"Kavu emerged all across Dominaria, but the first and the strongest came from Yavimaya."#)
            .casting_cost("2RR")
            .not_snow()
            .non_legendary()
            .creature("3/3 Kavu")
            .static_ability(StaticAbility::Global(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::creature_type(CreatureType::Kavu),
                effect_types: vec![
                    ContinuousPermanentEffectType::Keyword(Keyword::Trample)
                ]
            }))
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::EntersBattlefield(
                    PermanentCondition::creature_type(CreatureType::Kavu)
                ),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::self_effect(PermanentEffectType::add_counter(CounterType::PlusOnePlusOne))
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Runner")
            .expansion(Expansion::Invasion)
            .card_number(150)
            .rarity(Rarity::Uncommon)
            .artist_name("Douglas Shuler")
            .no_flavor_text()
            .casting_cost("3R")
            .not_snow()
            .non_legendary()
            .creature("3/3 Kavu")
            .static_ability(StaticAbility::Conditional {
                condition: Condition::not(
                    Condition::Permanent(PermanentCondition::NonControllerControls(Box::new(
                        PermanentCondition::And(vec![
                            PermanentCondition::creature(),
                            PermanentCondition::Or(vec![
                                PermanentCondition::white(),
                                PermanentCondition::blue(),
                            ]),
                        ])
                    )))
                ),
                effect_type: Box::new(StaticAbility::keyword(Keyword::Haste)),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Scout")
            .expansion(Expansion::Invasion)
            .card_number(151)
            .rarity(Rarity::Common)
            .artist_name("DiTerlizzi")
            .no_flavor_text()
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("0/2 Kavu")
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::AdjustPowerToughness {
                power_adjustment: Count::SourceControllerBasicLandTypes,
                toughness_adjustment: Count::Fixed(0),
            }))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Lightning Dart")
            .expansion(Expansion::Invasion)
            .card_number(152)
            .rarity(Rarity::Uncommon)
            .artist_name("Arnie Swekel")
            .flavor_text(
                r#"Broken and punctured by dozens of Phyrexian portals, the angry sky began to spit lightning back at the world."#)
            .casting_cost("1R")
            .instant()
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![
                    DamageEffectType::TargetConditional {
                        target_condition: PermanentCondition::Or(vec![
                            PermanentCondition::white(),
                            PermanentCondition::blue(),
                        ]),
                        then: Box::new(DamageEffectType::deal(4)),
                        otherwise: Box::new(DamageEffectType::deal(1)),
                    }
                ],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Loafing Giant")
            .expansion(Expansion::Invasion)
            .card_number(153)
            .rarity(Rarity::Rare)
            .artist_name("Greg & Tim Hildebrandt")
            .no_flavor_text()
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("4/6 Giant")
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::Or(vec![
                    TriggerCondition::SelfAttacks,
                    TriggerCondition::SelfBlocks,
                ]),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Card {
                        effect_types: vec![CardEffectType::MoveTo(CardZoneType::Graveyard)],
                        selecting: Selecting::one_target(),
                        selection_condition: CardCondition::Any,
                        selection_owner: PlayerVariable::EffectController,
                        card_zone: CardZoneType::Library,
                    },
                    EffectTemplate::self_creature_effect(PermanentEffectType::Conditional {
                        condition: PermanentCondition::CheckPreviousSelection(CardCondition::Land),
                        conditional_effect_type: Box::new(PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type: ContinuousPermanentEffectType::CannotDealCombatDamage,
                        })
                    }),
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Mages' Contest")
            .expansion(Expansion::Invasion)
            .card_number(154)
            .rarity(Rarity::Rare)
            .artist_name("Bradley Williams")
            .no_flavor_text()
            .casting_cost("1RR")
            .sorcery()
            .add_effect(EffectTemplate::target_spell(
                StackObjectEffectType::ControllerAndTargetControllerBidLife {
                    starting_bid: 1,
                    winner_effect_type: Box::new(StackObjectEffectType::Counter),
                }
            ))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Maniacal Rage")
            .expansion(Expansion::Invasion)
            .card_number(155)
            .rarity(Rarity::Common)
            .artist_name("Matt Cavotta")
            .flavor_text(r#""Remember me?" Tahngarth roared. "Now I'll repay you for my stay on Rath""#)
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(2, 2)))
                    .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::CannotBlock))
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Obliterate")
            .expansion(Expansion::Invasion)
            .card_number(156)
            .rarity(Rarity::Rare)
            .artist_name("Kev Walker")
            .flavor_text(
                r#"For his family, Barrin made a funeral pyre of Tolaria."#)
            .casting_cost("6RR")
            .sorcery()
            .add_static_ability(StackObjectStaticAbility::Uncounterable)
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                condition: PermanentCondition::Or(vec![
                    PermanentCondition::artifact(),
                    PermanentCondition::creature(),
                    PermanentCondition::land(),
                ]),
            }))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Overload")
            .expansion(Expansion::Invasion)
            .card_number(157)
            .rarity(Rarity::Common)
            .artist_name("Gary Ruddell")
            .no_flavor_text()
            .casting_cost("R")
            .instant()
            .kicker("2")
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::artifact(),
                    PermanentCondition::compare_converted_mana_cost(
                        IntegerCondition::LessThanOrEqualTo(
                            Count::IfElse {
                                condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                                then: Box::new(Count::Fixed(5)),
                                otherwise: Box::new(Count::Fixed(2)),
                            }
                        )
                    ),
                ]),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pouncing Kavu")
            .expansion(Expansion::Invasion)
            .card_number(158)
            .rarity(Rarity::Common)
            .artist_name("Adam Rex")
            .no_flavor_text()
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("1/1 Kavu")
            .kicker("2R")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(2)
                .keyword(Keyword::Haste)
                .build()
            )
            .keyword(Keyword::FirstStrike)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rage Weaver")
            .expansion(Expansion::Invasion)
            .card_number(159)
            .rarity(Rarity::Uncommon)
            .artist_name("John Matson")
            .flavor_text(r#""Let my passion spur your victory.""#)
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("2/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::Keyword(Keyword::Haste)
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Or(vec![
                        PermanentCondition::black(),
                        PermanentCondition::green(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rogue Kavu")
            .expansion(Expansion::Invasion)
            .card_number(160)
            .rarity(Rarity::Common)
            .artist_name("Darrell Riche")
            .flavor_text(
                r#""I know how this one feels," said Urza. "It's not like others of its kind and has been shunned for its differences.""#)
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("1/1 Kavu")
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::AttackingAlone),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(2, 0)
                )),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ruby Leech")
            .expansion(Expansion::Invasion)
            .card_number(161)
            .rarity(Rarity::Rare)
            .artist_name("Jacques Bredy")
            .flavor_text(
r#""Its gems didn't stop pulsating until they were completely removed."
—Tolarian research notes"#)
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("2/2 Leech")
            .static_ability(StaticAbility::keyword(Keyword::FirstStrike))
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::red(),
                modifier: CostModifier::Add(mana_cost("R")),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Savage Offensive")
            .expansion(Expansion::Invasion)
            .card_number(162)
            .rarity(Rarity::Common)
            .artist_name("Greg & Tim Hildebrandt")
            .no_flavor_text()
            .casting_cost("R")
            .sorcery()
            .kicker("G")
            .add_kicker_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::ControlledByController,
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type:
                            ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                    }
                ]
            }))
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::ControlledByController,
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::Keyword(
                            Keyword::FirstStrike
                        )
                    }
                ]
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Scarred Puma")
            .expansion(Expansion::Invasion)
            .card_number(161)
            .rarity(Rarity::Common)
            .artist_name("Aaron Boyd")
            .flavor_text(r#"It's not eager to lose the other eye."#)
            .casting_cost("R")
            .not_snow()
            .non_legendary()
            .creature("2/1 Cat")
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::CannotAttackWithoutOtherAttacker(
                    CardCondition::Or(vec![
                        CardCondition::black(),
                        CardCondition::green(),
                    ])
                )
            ))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Scorching Lava")
            .expansion(Expansion::Invasion)
            .card_number(164)
            .rarity(Rarity::Common)
            .artist_name("Mark Tedin")
            .no_flavor_text()
            .casting_cost("1R")
            .instant()
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![
                    DamageEffectType::deal(2),
                    DamageEffectType::Permanent(PermanentEffectType::Conditional {
                        condition: PermanentCondition::WasKicked,
                        conditional_effect_type: Box::new(PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type: ContinuousPermanentEffectType::CannotRegenerate,
                        }),
                    }),
                    DamageEffectType::Permanent(PermanentEffectType::Conditional {
                        condition: PermanentCondition::WasKicked,
                        conditional_effect_type: Box::new(PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type: ContinuousPermanentEffectType::ZoneRedirect {
                                regular_zone: CardZoneType::Graveyard,
                                override_zone: CardZoneType::Exile,
                            },
                        })
                    }),
                ],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        // TODO: Good unit test target.
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Searing Rays")
            .expansion(Expansion::Invasion)
            .card_number(165)
            .rarity(Rarity::Uncommon)
            .artist_name("Doug Chaffee")
            .flavor_text(
r#""This is a battle of pawns, for the highest stakes imaginable."
—Urza"#)
            .casting_cost("R")
            .sorcery()
            .add_effect(EffectTemplate::Global(GlobalEffectType::Damage {
                effect_type: DamageEffectType::Deal(Count::permanent(
                    PermanentCondition::And(vec![
                        PermanentCondition::ControlledByTarget,
                        PermanentCondition::And(vec![
                            PermanentCondition::creature(),
                            PermanentCondition::color(ColorVariable::ResolvedInput),
                        ]),
                    ])
                )),
                target_types: DamageRecipientTypes::any_player(),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shivan Emissary")
            .expansion(Expansion::Invasion)
            .card_number(166)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg Staples")
            .no_flavor_text()
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .enter_the_battlefield_ability(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::not(PermanentCondition::black()),
                    PermanentCondition::creature(),
                ]),
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shivan Harvest")
            .expansion(Expansion::Invasion)
            .card_number(167)
            .rarity(Rarity::Uncommon)
            .artist_name("Daren Bader")
            .no_flavor_text()
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1R").sacrifice_count(1, PermanentCondition::creature()))
                .add_effect(EffectTemplate::target_land(PermanentEffectType::destroy()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Skittish Kavu")
            .expansion(Expansion::Invasion)
            .card_number(168)
            .rarity(Rarity::Uncommon)
            .artist_name("Pete Venters")
            .flavor_text(
                "Fierce when roused, this breed of kavu is easily distracted by a strong voice or melodic tune.")
            .casting_cost("1R")
            .not_snow()
            .non_legendary()
            .creature("1/1 Kavu")
            .static_ability(StaticAbility::Conditional {
                condition: Condition::not(Condition::Permanent(
                    PermanentCondition::NonControllerControls(Box::new(
                        PermanentCondition::And(vec![
                            PermanentCondition::Or(vec![
                                PermanentCondition::white(),
                                PermanentCondition::blue()
                            ]),
                            PermanentCondition::creature(),
                        ])
                    ))
                )),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(1, 1)
                )),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Skizzik")
            .expansion(Expansion::Invasion)
            .card_number(169)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("3R")
            .not_snow()
            .non_legendary()
            .creature("5/3 Elemental")
            .kicker("R")
            .static_ability(StaticAbility::keyword(Keyword::Trample))
            .static_ability(StaticAbility::keyword(Keyword::Haste))
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::End,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Permanent(
                    PermanentCondition::Not(Box::new(PermanentCondition::WasKicked))
                ),
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![EffectTemplate::self_effect(PermanentEffectType::Sacrifice)],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Slimy Kavu")
            .expansion(Expansion::Invasion)
            .card_number(170)
            .rarity(Rarity::Common)
            .artist_name("Randy Gallegos")
            .flavor_text(r#"Its slime liquefies the ground as efficiently as its fangs shred prey."#)
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("2/2 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::target_land(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideBasicLandTypes {
                        new_basic_land_types: vec![
                            BasicLandTypeVariable::Fixed(BasicLandType::Swamp)
                        ],
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Stand or Fall")
            .expansion(Expansion::Invasion)
            .card_number(171)
            .rarity(Rarity::Rare)
            .artist_name("Matt Cavotta")
            .no_flavor_text()
            .casting_cost("3R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::BeginningOfCombat,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::defending(),
                        effect_types: vec![
                            PlayerEffectType::TwoPilesOfPermanents {
                                pile_creator_condition: PlayerCondition::Matches(
                                    PlayerVariable::EffectController),
                                pile_selector_condition: PlayerCondition::Matches(
                                    PlayerVariable::EffectRecipientController),
                                permanent_condition: PermanentCondition::creature(),
                                selected_pile_condition: PermanentGroupCondition::Any,
                                selected_pile_effect_type: None,
                                unselected_pile_effect_type: Some(PermanentEffectType::Continuous {
                                    expiration: Expiration::EndOfTurn,
                                    continuous_type: ContinuousPermanentEffectType::CannotBlock,
                                }),
                            }
                        ],
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Stun")
            .expansion(Expansion::Invasion)
            .card_number(172)
            .rarity(Rarity::Common)
            .artist_name("Mike Ploog")
            .flavor_text(
                r#""They certainly are well protected. Too bad for them they're not well balanced."
—Sisay"#)
            .casting_cost("1R")
            .instant()
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                expiration: Expiration::EndOfTurn,
                continuous_type: ContinuousPermanentEffectType::CannotBlock,
            }))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        // TODO: Good unit test target (Are the proper lands tapped when a fetch land is used off-turn?)
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tectonic Instability")
            .expansion(Expansion::Invasion)
            .card_number(173)
            .rarity(Rarity::Rare)
            .artist_name("Rob Alexander")
            .flavor_text(
r#""Think of the problems we'll face if you uproot entire continents," said Urza.
"I have," replied Teferi, "but the alternative is worse.""#)
            .casting_cost("3R")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::EntersBattlefield(PermanentCondition::land()),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Permanent {
                        effect_types: vec![PermanentEffectType::Tap],
                        condition: PermanentCondition::ControlledBy(
                            PlayerCondition::Matches(PlayerVariable::TriggerObjectController)),
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Thunderscape Apprentice")
            .expansion(Expansion::Invasion)
            .card_number(174)
            .rarity(Rarity::Common)
            .artist_name("D. Alexander Gregory")
            .no_flavor_text()
            .casting_cost("R")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B").self_tap())
                .add_effect(EffectTemplate::target_player(PlayerEffectType::lose_life(1)))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G").self_tap())
                .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Thunderscape Master")
            .expansion(Expansion::Invasion)
            .card_number(175)
            .rarity(Rarity::Rare)
            .artist_name("Scott M. Fischer")
            .no_flavor_text()
            .casting_cost("2RR")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("BB").self_tap())
                .add_effect(EffectTemplate::target_player(PlayerEffectType::lose_life(2)))
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::gain_life(2)))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("GG").self_tap())
                .add_effect(EffectTemplate::Global(GlobalEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    effect_type: ContinuousGlobalEffectType::Permanent {
                        condition: PermanentCondition::Any,
                        effect_types: vec![
                            ContinuousPermanentEffectType::adjust_power_toughness(2, 2)
                        ]
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Tribal Flames")
            .expansion(Expansion::Invasion)
            .card_number(176)
            .rarity(Rarity::Common)
            .artist_name("Tony Szczudlo")
            .flavor_text(
r#""Fire is the universal language."
—Jhoira, master artificer"#)
            .casting_cost("1R")
            .sorcery()
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![DamageEffectType::Deal(Count::SourceControllerBasicLandTypes)],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Turf Wound")
            .expansion(Expansion::Invasion)
            .card_number(177)
            .rarity(Rarity::Common)
            .artist_name("Thomas Gianni")
            .flavor_text(
r#""I can't imagine how anyone lives in Shiv, let alone why they choose to stay."
—Sisay"#)
            .casting_cost("2R")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPlayerEffectType::CannotPlayLandCards,
                    }
                ],
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Urza's Rage")
            .expansion(Expansion::Invasion)
            .card_number(178)
            .rarity(Rarity::Rare)
            .artist_name("Matthew D. Wilson")
            .no_flavor_text()
            .casting_cost("2R")
            .instant()
            .add_static_ability(StackObjectStaticAbility::Uncounterable)
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![
                    DamageEffectType::IfElse {
                        condition: Condition::Permanent(PermanentCondition::WasKicked),
                        then: Box::new(DamageEffectType::DealUnpreventable(Count::Fixed(10))),
                        otherwise: Box::new(DamageEffectType::deal(3)),
                    }
                ],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Viashino Grappler")
            .expansion(Expansion::Invasion)
            .card_number(179)
            .rarity(Rarity::Common)
            .artist_name("Mark Romanoski")
            .flavor_text(
r#""They've returned for the mana rig, but it no longer belongs to them."
—Viashino bey"#)
            .casting_cost("2R")
            .not_snow()
            .non_legendary()
            .creature("3/1 Viashino")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Trample),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Zap")
            .expansion(Expansion::Invasion)
            .card_number(180)
            .rarity(Rarity::Common)
            .artist_name("John Matson")
            .flavor_text(
r#""All this time I thought Squee was useless," chuckled Sisay. "Who knew he'd be such a good shot?""#)
            .casting_cost("2R")
            .instant()
            .add_effect(EffectTemplate::Damage {
                effect_types: vec![DamageEffectType::deal(1)],
                damage_recipients: DamageRecipients::ControllerChooses {
                    count: Count::Fixed(1),
                    recipient_types: DamageRecipientTypes::any_recipient(),
                },
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Aggressive Urge")
            .expansion(Expansion::Invasion)
            .card_number(181)
            .rarity(Rarity::Common)
            .artist_name("Christopher Moeller")
            .flavor_text(r#"The power of the wild, concentrated in a single charge."#)
            .casting_cost("1G")
            .instant()
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                expiration:  Expiration::EndOfTurn,
                continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
            }))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Bind")
            .expansion(Expansion::Invasion)
            .card_number(182)
            .rarity(Rarity::Rare)
            .artist_name("Mark Zug")
            .flavor_text(
r#""The battlefield is cluttered enough. Be still."
—Multani, maro-sorcerer"#)
            .casting_cost("1G")
            .instant()
            .add_effect(EffectTemplate::target_activated_ability(StackObjectEffectType::Counter))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Blurred Mongoose")
            .expansion(Expansion::Invasion)
            .card_number(183)
            .rarity(Rarity::Rare)
            .artist_name("Heather Hudson")
            .flavor_text(
r#""The mongoose blew out its candle and was asleep in bed before the room went dark."
—Llanowar fable"#)
            .casting_cost("6")
            .not_snow()
            .non_legendary()
            .creature("2/1 Mongoose")
            .static_ability(StaticAbility::StackObject(StackObjectStaticAbility::Uncounterable))
            .keyword(Keyword::Shroud)
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Canopy Surge")
            .expansion(Expansion::Invasion)
            .card_number(184)
            .rarity(Rarity::Uncommon)
            .artist_name("Matt Cavotta")
            .no_flavor_text()
            .casting_cost("1G")
            .sorcery()
            .kicker("2")
            .add_effect(EffectTemplate::Global(GlobalEffectType::Damage {
                target_types: DamageRecipientTypes {
                    creatures: Some(PermanentCondition::Flying),
                    players: Some(SelectingPlayers::all()),
                    planeswalkers: false,
                },
                effect_type: DamageEffectType::IfElse {
                    condition: Condition::Permanent(PermanentCondition::WasKicked),
                    then: Box::new(DamageEffectType::deal(4)),
                    otherwise: Box::new(DamageEffectType::deal(1)),
                }
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Elfhame Sanctuary")
            .expansion(Expansion::Invasion)
            .card_number(185)
            .rarity(Rarity::Uncommon)
            .artist_name("Alan Rabinowitz")
            .no_flavor_text()
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: None,
                effects: vec![
                    EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                        card_count: CardCount::Predetermined(Count::Fixed(1)),
                        card_condition: CardCondition::BasicLand,
                        destination: CardLocation::Hand,
                    }),
                    EffectTemplate::controller_effect(PlayerEffectType::Replacement {
                        expiration: Expiration::EndOfTurn,
                        ending: EffectEnding::OnFirstUse,
                        replacement_type: PlayerReplacementEffectType::SkipDefaultDrawStep,
                    }),
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Elvish Champion")
            .expansion(Expansion::Invasion)
            .card_number(186)
            .rarity(Rarity::Rare)
            .artist_name("D. Alexander Gregory")
            .flavor_text(
r#""For what are leaves but countless blades
To fight a countless foe on high."
—Skyshroud hymn"#)
            .casting_cost("1GG")
            .not_snow()
            .non_legendary()
            .creature("2/2 Elf")
            .static_ability(StaticAbility::Global(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::NotItself,
                    PermanentCondition::creature_type(CreatureType::Elf),
                ]),
                effect_types: vec![
                    ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                    ContinuousPermanentEffectType::Keyword(Keyword::forestwalk()),
                ],
            }))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Explosive Growth")
            .expansion(Expansion::Invasion)
            .card_number(187)
            .rarity(Rarity::Common)
            .artist_name("Arnie Swekel")
            .no_flavor_text()
            .casting_cost("G")
            .instant()
            .kicker("5")
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                expiration: Expiration::EndOfTurn,
                continuous_type: ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                    Count::IfElse {
                        condition: Box::new(Condition::Permanent(PermanentCondition::WasKicked)),
                        then: Box::new(Count::Fixed(5)),
                        otherwise: Box::new(Count::Fixed(2)),
                    }
                )
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Fertile Ground")
            .expansion(Expansion::Invasion)
            .card_number(188)
            .rarity(Rarity::Common)
            .artist_name("Carl Critchlow")
            .flavor_text(
                r#"As Phyrexians descended, Multani paused to reflect on the beauty that might never be seen again."#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::land(),
                aura_triggered_abilities: vec![
                    AuraTriggeredAbility {
                        trigger_condition: AuraTriggerCondition::ManaAbilityActivated,
                        effects: Vec::new(),
                        mana_produced: Some(ManaProduced::choose_one_of_any_color()),
                    }
                ],
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new().build()
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Harrow")
            .expansion(Expansion::Invasion)
            .card_number(189)
            .rarity(Rarity::Common)
            .artist_name("Rob Alexander")
            .no_flavor_text()
            .complex_casting_cost(cost()
                .mana("2G")
                .sacrifice_count(1, PermanentCondition::land())
                .into_cost())
            .instant()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCardsToBattlefield {
                card_count: CardCount::Predetermined(Count::Fixed(2)),
                card_condition: CardCondition::BasicLand,
                enters_tapped: false,
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Jade Leech")
            .expansion(Expansion::Invasion)
            .card_number(190)
            .rarity(Rarity::Rare)
            .artist_name("John Howe")
            .flavor_text(
                r#""It took magic to extract one of the stones and five people to carry it."
—Tolarian research notes"#)
            .casting_cost("2GG")
            .not_snow()
            .non_legendary()
            .creature("5/5 Leech")
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::green(),
                modifier: CostModifier::Add(mana_cost("G")),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Chameleon")
            .expansion(Expansion::Invasion)
            .card_number(191)
            .rarity(Rarity::Uncommon)
            .artist_name("John Howe")
            .flavor_text(r#""They weren't part of my plan, but Gaea insisted.""#)
            .casting_cost("2GG")
            .not_snow()
            .non_legendary()
            .creature("4/4 Kavu")
            .static_ability(StaticAbility::StackObject(StackObjectStaticAbility::Uncounterable))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::OverrideColors {
                        new_colors: vec![ColorVariable::AbilityInput],
                    }
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Climber")
            .expansion(Expansion::Invasion)
            .card_number(192)
            .rarity(Rarity::Common)
            .artist_name("Rob Alexander")
            .flavor_text(r#"The appearance of the first kavu surprised Multani. As they continued to emerge, he no longer had any doubts about Yavimaya's ability to defend herself."#)
            .casting_cost("3GG")
            .not_snow()
            .non_legendary()
            .creature("3/3 Kavu")
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Lair")
            .expansion(Expansion::Invasion)
            .card_number(193)
            .rarity(Rarity::Rare)
            .artist_name("Chippy")
            .flavor_text(r#"As a dark rain of Phyrexians fell from the sky, fountains of kavu erupted from the ground."#)
            .casting_cost("2G")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::EntersBattlefield(
                    PermanentCondition::Power(IntegerCondition::GreaterThanOrEqualTo(Count::Fixed(4)))),
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![EffectTemplate::Player {
                    selecting: SelectingPlayers::trigger_object_controller(),
                    effect_types: vec![PlayerEffectType::draw(1)],
                }],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kavu Titan")
            .expansion(Expansion::Invasion)
            .card_number(194)
            .rarity(Rarity::Rare)
            .artist_name("Todd Lockwood")
            .no_flavor_text()
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("2/2 Kavu")
            .kicker("2G")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(3)
                .keyword(Keyword::Trample)
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Llanowar Cavalry")
            .expansion(Expansion::Invasion)
            .card_number(195)
            .rarity(Rarity::Common)
            .artist_name("Eric Peterson")
            .flavor_text(r#"For the first time, elves welcomed Benalish soldiers into the forest with something other than arrows."#)
            .casting_cost("2G")
            .not_snow()
            .non_legendary()
            .creature("1/4 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::Keyword(Keyword::Vigilance),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Llanowar Elite")
            .expansion(Expansion::Invasion)
            .card_number(196)
            .rarity(Rarity::Common)
            .artist_name("Kev Walker")
            .no_flavor_text()
            .casting_cost("G")
            .not_snow()
            .non_legendary()
            .creature("1/1 Elf")
            .kicker("8")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(5)
                .build()
            )
            .keyword(Keyword::Trample)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Llanowar Vanguard")
            .expansion(Expansion::Invasion)
            .card_number(197)
            .rarity(Rarity::Common)
            .artist_name("Greg & Tim Hildebrandt")
            .no_flavor_text()
            .casting_cost("2G")
            .not_snow()
            .non_legendary()
            .creature("1/1 Dryad")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(0, 4),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Might Weaver")
            .expansion(Expansion::Invasion)
            .card_number(198)
            .rarity(Rarity::Uncommon)
            .artist_name("Larry Elmore")
            .flavor_text(r#""Let my strength harden your resolve.""#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("2/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2"))
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::Keyword(Keyword::Trample),
                        }
                    ],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::Or(vec![
                        PermanentCondition::red(),
                        PermanentCondition::white(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Molimo, Maro-Sorcerer")
            .expansion(Expansion::Invasion)
            .card_number(199)
            .rarity(Rarity::Rare)
            .artist_name("Mark Zug")
            .flavor_text(r#""The world calls and I answer.""#)
            .casting_cost("4GGG")
            .not_snow()
            .non_legendary()
            .complex_creature(Creature::new(
                to_set(vec![CreatureType::Elemental]),
                Power(Count::permanent(PermanentCondition::And(vec![
                    PermanentCondition::land(),
                    PermanentCondition::ControlledByController,
                ]))),
                Toughness(Count::permanent(PermanentCondition::And(vec![
                    PermanentCondition::land(),
                    PermanentCondition::ControlledByController,
                ]))),
            ))
            .keyword(Keyword::Trample)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Nomadic Elf")
            .expansion(Expansion::Invasion)
            .card_number(200)
            .rarity(Rarity::Common)
            .artist_name("D. J. Cleland-Hura")
            .flavor_text(r#""I've journeyed across Dominaria. Phyrexians are everywhere. Plague is everywhere. There's no place left to hide.""#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("2/2 Elf")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1G"))
                .mana_produced("W or U or B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pincer Spider")
            .expansion(Expansion::Invasion)
            .card_number(201)
            .rarity(Rarity::Common)
            .artist_name("Dan Frazier")
            .no_flavor_text()
            .casting_cost("2G")
            .not_snow()
            .non_legendary()
            .creature("2/3 Spider")
            .kicker("3")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .plus_one_plus_one_counter_count(1)
                .build()
            )
            .keyword(Keyword::Reach)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pulse of Llanowar")
            .expansion(Expansion::Invasion)
            .card_number(202)
            .rarity(Rarity::Common)
            .artist_name("Rebecca Guay")
            .flavor_text(
r#""Gaea's memory is eternal, and she exists in all things."
—Molimo, maro-sorcerer"#)
            .casting_cost("3G")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::ManaReplacementUponTap {
                source: PermanentCondition::And(vec![
                    PermanentCondition::ControlledBy(
                        PlayerCondition::Matches(PlayerVariable::EffectController)),
                    PermanentCondition::basic_land(),
                ]),
                replacement_per_unit: ManaProduced::choose_one_of_any_color(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Quirion Elves")
            .expansion(Expansion::Invasion)
            .card_number(203)
            .rarity(Rarity::Common)
            .artist_name("Douglas Shuler")
            .no_flavor_text()
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("1/1 Elf")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("G")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .variable_mana_produced(ColorVariable::ResolvedInput)
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Quirion Sentinel")
            .expansion(Expansion::Invasion)
            .card_number(204)
            .rarity(Rarity::Common)
            .artist_name("Heather Hudson")
            .flavor_text(r#"All elvenkind stood against Phyrexia. The Quirion nation deployed its most spiritual adepts, who wielded the power of their native soil."#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("2/1 Elf")
            .enter_the_battlefield_ability(EffectTemplate::Mana {
                mana_produced: ManaProduced::choose_one_of_any_color(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Quirion Trailblazer")
            .expansion(Expansion::Invasion)
            .card_number(204)
            .rarity(Rarity::Common)
            .artist_name("Heather Hudson")
            .flavor_text(r#""All that matters is the path ahead.""#)
            .casting_cost("3G")
            .not_snow()
            .non_legendary()
            .creature("1/2 Elf")
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(
                PlayerEffectType::TutorCardsToBattlefield {
                    card_count: CardCount::Predetermined(Count::Fixed(1)),
                    card_condition: CardCondition::BasicLand,
                    enters_tapped: true,
                }
            ))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Restock")
            .expansion(Expansion::Invasion)
            .card_number(206)
            .rarity(Rarity::Rare)
            .artist_name("Daren Bader")
            .flavor_text(
r#""We hid such stockpiles all over Rath. We should continue that practice here."
—Lin Sivvi"#)
            .casting_cost("3GG")
            .sorcery()
            .add_effect(EffectTemplate::Card {
                effect_types: vec![CardEffectType::MoveTo(CardZoneType::Hand)],
                selecting: Selecting::Targets(Count::Fixed(2)),
                selection_condition: CardCondition::Any,
                selection_owner: PlayerVariable::EffectController,
                card_zone: CardZoneType::Graveyard,
            })
            .add_effect(EffectTemplate::StackObject {
                effect_types: vec![StackObjectEffectType::Exile],
                selecting: Selecting::Itself,
                stack_object_types: to_set(vec![StackObjectType::any_spell()]),
                selection_owner_condition: PlayerCondition::Any,
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rooting Kavu")
            .expansion(Expansion::Invasion)
            .card_number(207)
            .rarity(Rarity::Uncommon)
            .artist_name("Heather Hudson")
            .no_flavor_text()
            .casting_cost("2GG")
            .not_snow()
            .non_legendary()
            .creature("4/3 Kavu")
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::Dies(PermanentCondition::Itself),
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().self_exile().into_permanent_cost())),
                effects: vec![
                    EffectTemplate::controller_effect(
                        PlayerEffectType::ShuffleGraveyardIntoLibrary(CardCondition::Creature))
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Saproling Infestation")
            .expansion(Expansion::Invasion)
            .card_number(208)
            .rarity(Rarity::Rare)
            .artist_name("Heather Hudson")
            .flavor_text(r#""My army took centuries to gather," remarked Urza. "Yavimaya seems to conjure hers out of thin air.""#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::KickerPaid,
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                        count: Count::Fixed(1),
                        power: Power(Count::Fixed(1)),
                        toughness: Toughness(Count::Fixed(1)),
                        colors: to_set(vec![Color::Green]),
                        creature_types: to_set(vec![CreatureType::Saproling]),
                        static_abilities: Vec::new(),
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Saproling Symbiosis")
            .expansion(Expansion::Invasion)
            .card_number(209)
            .rarity(Rarity::Rare)
            .artist_name("Ciruelo")
            .no_flavor_text()
            .casting_cost("3G")
            .sorcery_with_instant_speed_upgrade(
                cost().mana("2"),
            )
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                count: Count::permanent(PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::ControlledByController,
                ])),
                power: Power(Count::Fixed(1)),
                toughness: Toughness(Count::Fixed(1)),
                colors: to_set(vec![Color::Green]),
                creature_types: to_set(vec![CreatureType::Saproling]),
                static_abilities: Vec::new(),
            }))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Scouting Trek")
            .expansion(Expansion::Invasion)
            .card_number(210)
            .rarity(Rarity::Uncommon)
            .artist_name("Stephanie Law")
            .flavor_text(
r#""I have chosen my path. Who will walk it with me?"
—Eladamri"#)
            .casting_cost("G")
            .sorcery()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                card_count: CardCount::Any,
                card_condition: CardCondition::BasicLand,
                destination: CardLocation::Library(LibraryLocation::Top),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Serpentine Kavu")
            .expansion(Expansion::Invasion)
            .card_number(297)
            .rarity(Rarity::Uncommon)
            .artist_name("Heather Hudson")
            .flavor_text(
                r#"Under Yavimaya's peaceful facade beat the hearts of many savage beasts."#)
            .casting_cost("4G")
            .not_snow()
            .non_legendary()
            .creature("4/4 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Haste),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sulam Djinn")
            .expansion(Expansion::Invasion)
            .card_number(212)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg & Tim Hildebrandt")
            .no_flavor_text()
            .casting_cost("5G")
            .not_snow()
            .non_legendary()
            .creature("6/6 Djinn")
            .keyword(Keyword::Trample)
            .static_ability(StaticAbility::Conditional {
                condition: Condition::MostCommonPermanentColorOrTied(Color::Green),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(-2, -2)
                ))
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Tangle")
            .expansion(Expansion::Invasion)
            .card_number(213)
            .rarity(Rarity::Uncommon)
            .artist_name("John Avon")
            .flavor_text(r#"Gaea battles rampant death with abundant life."#)
            .casting_cost("1G")
            .instant()
            .add_effect(EffectTemplate::prevent_all_combat_damage(Expiration::EndOfTurn))
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                condition: PermanentCondition::Attacking,
                effect_types: vec![
                    PermanentEffectType::Cannot {
                        expiration: Expiration::Never,
                        ending: EffectEnding::OnFirstUse,
                        cannot_type: PermanentCannotEffectType::CannotUntapDuringControllersUntapStep,
                    }
                ],
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Thicket Elemental")
            .expansion(Expansion::Invasion)
            .card_number(214)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("3GG")
            .not_snow()
            .non_legendary()
            .creature("4/4 Elemental")
            .kicker("1G")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(EffectTemplate::controller_effect(
                    PlayerEffectType::RevealFromTopOfLibraryUntil {
                        ending_condition: CardCondition::Creature,
                        ending_card_destination: PermanentCardLocation::Battlefield,
                        other_cards_destination: PermanentCardLocation::CardLocation(
                            CardLocation::Library(LibraryLocation::Top)),
                        should_shuffle_afterwards: true,
                    }
                ))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Thornscape Apprentice")
            .expansion(Expansion::Invasion)
            .card_number(215)
            .rarity(Rarity::Common)
            .artist_name("Randy Gallegos")
            .no_flavor_text()
            .casting_cost("G")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("W").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Tap))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R").self_tap())
                .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::FirstStrike),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Thornscape Master")
            .expansion(Expansion::Invasion)
            .card_number(216)
            .rarity(Rarity::Rare)
            .artist_name("Larry Elmore")
            .no_flavor_text()
            .casting_cost("2GG")
            .not_snow()
            .non_legendary()
            .creature("2/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("RR").self_tap())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(2)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_creature(),
                    },
                })
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("WW").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(
                        Keyword::Protection(
                            CardCondition::Color(ColorVariable::AbilityInput))
                    )
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Tranquility")
            .expansion(Expansion::Invasion)
            .card_number(217)
            .rarity(Rarity::Common)
            .artist_name("Rob Alexander")
            .flavor_text(r#"The plagues robbed Dominaria of all but its dreams. Eladamri hoped dreams were enough."#)
            .casting_cost("2G")
            .sorcery()
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                condition: PermanentCondition::enchantment(),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Treefolk Healer")
            .expansion(Expansion::Invasion)
            .card_number(218)
            .rarity(Rarity::Uncommon)
            .artist_name("Matt Cavotta")
            .flavor_text(
                r#"Protected by druids for untold centuries, the forest began to return the favor."#)
            .casting_cost("4G")
            .not_snow()
            .non_legendary()
            .creature("2/3 Treefolk")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2W").self_tap())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![
                        DamageEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            effect_type: ContinuousDamageEffectType::prevent(2),
                        }
                    ],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Utopia Tree")
            .expansion(Expansion::Invasion)
            .card_number(219)
            .rarity(Rarity::Rare)
            .artist_name("Gary Ruddell")
            .flavor_text(
                r#"The fruit of this fabled tree takes on the flavor of whatever food you love most."#)
            .casting_cost("1G")
            .not_snow()
            .non_legendary()
            .creature("0/2 Plant")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("W or U or B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Verdeloth the Ancient")
            .expansion(Expansion::Invasion)
            .card_number(220)
            .rarity(Rarity::Rare)
            .artist_name("Daren Bader")
            .no_flavor_text()
            .casting_cost("4GG")
            .not_snow()
            .legendary()
            .creature("4/4 Treefolk")
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::NotItself,
                    PermanentCondition::creature_type(CreatureType::Treefolk),
                ]),
                effect_types: vec![ContinuousPermanentEffectType::adjust_power_toughness(1, 1)],
            })
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::creature_type(CreatureType::Saproling),
                effect_types: vec![ContinuousPermanentEffectType::adjust_power_toughness(1, 1)],
            })
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                count: Count::kicker_mana_variable(),
                colors: to_set(vec![Color::Green]),
                creature_types: to_set(vec![CreatureType::Saproling]),
                power: Power(Count::Fixed(1)),
                toughness: Toughness(Count::Fixed(1)),
                static_abilities: Vec::new(),
            }))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Verduran Emissary")
            .expansion(Expansion::Invasion)
            .card_number(221)
            .rarity(Rarity::Uncommon)
            .artist_name("Alton Lawson")
            .no_flavor_text()
            .casting_cost("4GG")
            .not_snow()
            .non_legendary()
            .creature("2/3 Human")
            .kicker("1R")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(
                    EffectTemplate::target_artifact(PermanentEffectType::DestroyWithNoRegeneration))
                .build()
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Vigorous Charge")
            .expansion(Expansion::Invasion)
            .card_number(222)
            .rarity(Rarity::Common)
            .artist_name("Scott M. Fischer")
            .no_flavor_text()
            .casting_cost("G")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::Any,
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type:
                            ContinuousPermanentEffectType::Keyword(Keyword::Trample),
                    },
                    PermanentEffectType::Conditional {
                        condition: PermanentCondition::WasKicked,
                        conditional_effect_type: Box::new(PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::GainLifeEqualToSelfCombatDamageDealt,
                        })
                    }
                ],
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Wallop")
            .expansion(Expansion::Invasion)
            .card_number(223)
            .rarity(Rarity::Uncommon)
            .artist_name("Mike Ploog")
            .flavor_text(r#"In Yavimaya, flying low to join a battle can be a costly mistake."#)
            .casting_cost("1G")
            .sorcery()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::Or(vec![PermanentCondition::blue(), PermanentCondition::black()]),
                    PermanentCondition::creature(),
                ]),
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Wandering Stream")
            .expansion(Expansion::Invasion)
            .card_number(224)
            .rarity(Rarity::Common)
            .artist_name("Quinton Hoover")
            .flavor_text(
r#""Dominaria touches us all."
—Molimo, maro-sorcerer"#)
            .casting_cost("2G")
            .sorcery()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::GainLife(
                Count::Multiply(
                    Box::new(Count::Fixed(2)),
                    Box::new(Count::SourceControllerBasicLandTypes)))))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Whip Silk")
            .expansion(Expansion::Invasion)
            .card_number(221)
            .rarity(Rarity::Common)
            .artist_name("Dave Dorman")
            .flavor_text(r#"Llanowar forges weapons of steel. Yavimaya grows her weapons within."#)
            .casting_cost("4GG")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::keyword(Keyword::Reach))
                    .build(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G"))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::bounce())))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Absorb")
            .expansion(Expansion::Invasion)
            .card_number(224)
            .rarity(Rarity::Rare)
            .artist_name("Andrew Goldhawk")
            .flavor_text(
                r#""Your presumption is your downfall.""#)
            .casting_cost("WUU")
            .instant()
            .add_effect(EffectTemplate::target_spell(StackObjectEffectType::Counter))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::gain_life(3)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Aether Rift")
            .expansion(Expansion::Invasion)
            .card_number(227)
            .rarity(Rarity::Rare)
            .artist_name("Heather Hudson")
            .no_flavor_text()
            .casting_cost("WU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::controller_effect(PlayerEffectType::DiscardAtRandom(Count::Fixed(1))),
                    EffectTemplate::PayCostToPrevent {
                        cost: cost().life(5).into_cost(),
                        effect_template: Box::new(EffectTemplate::Card {
                            effect_types: vec![
                                CardEffectType::MoveToBattlefield {
                                    new_controller: PlayerVariable::EffectController,
                                }
                            ],
                            selecting: Selecting::Specific(ObjectVariable::PreviousDiscard),
                            selection_condition: CardCondition::Creature,
                            selection_owner: PlayerVariable::EffectController,
                            card_zone: CardZoneType::Graveyard,
                        }),
                    },
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Angelic Shield")
            .expansion(Expansion::Invasion)
            .card_number(228)
            .rarity(Rarity::Uncommon)
            .artist_name("Adam Rex")
            .flavor_text(
r#""If only an angel's wings could shelter us all."
—Barrin"#)
            .casting_cost("WU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .static_ability(StaticAbility::Global(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::ControlledByController,
                effect_types: vec![ContinuousPermanentEffectType::adjust_power_toughness(0, 1)],
            }))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_sacrifice())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::bounce()))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Armadillo Cloak")
            .expansion(Expansion::Invasion)
            .card_number(229)
            .rarity(Rarity::Common)
            .artist_name("Paolo Parente")
            .flavor_text(
r#""Don't laugh. It works."
—Yavimaya ranger"#)
            .casting_cost("1GW")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(2, 2)))
                    .keyword(Keyword::Trample)
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::GainLifeEqualToSelfDamageDealt))
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Armored Guardian")
            .expansion(Expansion::Invasion)
            .card_number(230)
            .rarity(Rarity::Uncommon)
            .artist_name("Arnie Swekel")
            .no_flavor_text()
            .casting_cost("3WU")
            .not_snow()
            .non_legendary()
            .creature("2/5 Cat Soldier")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1WW"))
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(
                        Keyword::Protection(CardCondition::Color(ColorVariable::AbilityInput))),
                }))
                .add_effect(EffectTemplate::self_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Shroud),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Artifact Mutation")
            .expansion(Expansion::Invasion)
            .card_number(231)
            .rarity(Rarity::Rare)
            .artist_name("Greg Staples")
            .flavor_text(
r#""From shards and splinters I call forth my living horde."
—Molimo, maro-sorcerer"#)
            .casting_cost("RG")
            .instant()
            .add_effect(EffectTemplate::target_artifact(PermanentEffectType::DestroyWithNoRegeneration))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                count: Count::ConvertedManaCost(ObjectVariable::PreviousSelection),
                power: Power(Count::Fixed(1)),
                toughness: Toughness(Count::Fixed(1)),
                colors: to_set(vec![Color::Green]),
                creature_types: to_set(vec![CreatureType::Saproling]),
                static_abilities: Vec::new(),
            }))
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Aura Mutation")
            .expansion(Expansion::Invasion)
            .card_number(232)
            .rarity(Rarity::Rare)
            .artist_name("Pete Venters")
            .flavor_text(
r#""Life can be found in all things, even things unnatural."
—Multani, maro-sorcerer"#)
            .casting_cost("GW")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::enchantment(),
            })
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                count: Count::ConvertedManaCost(ObjectVariable::PreviousSelection),
                power: Power(Count::Fixed(1)),
                toughness: Toughness(Count::Fixed(1)),
                colors: to_set(vec![Color::Green]),
                creature_types: to_set(vec![CreatureType::Saproling]),
                static_abilities: Vec::new(),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Aura Shards")
            .expansion(Expansion::Invasion)
            .card_number(233)
            .rarity(Rarity::Uncommon)
            .artist_name("Ron Spencer")
            .no_flavor_text()
            .casting_cost("3GW")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::EntersBattlefield(PermanentCondition::creature()),
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: None,
                effects: vec![EffectTemplate::target_artifact_or_enchantment(PermanentEffectType::destroy())],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Backlash")
            .expansion(Expansion::Invasion)
            .card_number(234)
            .rarity(Rarity::Rare)
            .artist_name("Chippy")
            .flavor_text(r#"Darigaaz decided his foe would be more useful as a weapon."#)
            .casting_cost("1BR")
            .instant()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::Tap,
                    PermanentEffectType::TargetDealsDamageToItsController(
                        Count::Power(ObjectVariable::PreviousSelection)),
                ],
                selecting: Selecting::Targets(Count::Fixed(2)),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::Untapped,
                    PermanentCondition::creature(),
                ]),
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Barrin's Spite")
            .expansion(Expansion::Invasion)
            .card_number(235)
            .rarity(Rarity::Rare)
            .artist_name("Andrew Goldhawk")
            .flavor_text(
r#""Only vengeance matters now."
—Barrin"#)
            .casting_cost("2UB")
            .sorcery()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::ControllerOfTargetsAssignsEffects {
                        first_effect_type: Box::new(PermanentEffectType::Sacrifice),
                        other_effect_type: Box::new(PermanentEffectType::bounce()),
                    }
                ],
                selecting: Selecting::TargetsWithTheSameController(Count::Fixed(2)),
                selection_condition: PermanentCondition::creature(),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Blazing Specter")
            .expansion(Expansion::Invasion)
            .card_number(236)
            .rarity(Rarity::Rare)
            .artist_name("Marc Fishman")
            .no_flavor_text()
            .casting_cost("2BR")
            .not_snow()
            .non_legendary()
            .creature("2/2 Specter")
            .keyword(Keyword::Flying)
            .keyword(Keyword::Haste)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::defending(),
                        effect_types: vec![PlayerEffectType::discard(1)],
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Captain Sisay")
            .expansion(Expansion::Invasion)
            .card_number(237)
            .rarity(Rarity::Rare)
            .artist_name("Ray Lago")
            .flavor_text(r#"Her leadership forged the Weatherlight's finest crew."#)
            .casting_cost("2GW")
            .not_snow()
            .legendary()
            .creature("2/2 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                    card_count: CardCount::Predetermined(Count::Fixed(1)),
                    card_condition: CardCondition::Legendary,
                    destination: CardLocation::Hand,
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Cauldron Dance")
            .expansion(Expansion::Invasion)
            .card_number(238)
            .rarity(Rarity::Uncommon)
            .artist_name("Donato Giancola")
            .no_flavor_text()
            .casting_cost("4BR")
            .instant_with_casting_window_constraint(
                CastingWindowConstraint::Combat,
            )
            .add_effect(EffectTemplate::Card {
                effect_types: vec![
                    CardEffectType::MoveToBattlefield {
                        new_controller: PlayerVariable::EffectController,
                    }
                ],
                selecting: Selecting::one_target(),
                selection_condition: CardCondition::Creature,
                selection_owner: PlayerVariable::EffectController,
                card_zone: CardZoneType::Graveyard,
            })
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Haste),
                    },
                    PermanentEffectType::bounce(),
                ],
                selecting: Selecting::Specific(ObjectVariable::PreviousSelection),
                selection_condition: PermanentCondition::Any,
            })
            .add_effect(EffectTemplate::Card {
                effect_types: vec![
                    CardEffectType::MoveToBattlefield {
                        new_controller: PlayerVariable::EffectController,
                    }
                ],
                selecting: Selecting::NonTargets(Count::Fixed(1)),
                selection_condition: CardCondition::Creature,
                selection_owner: PlayerVariable::EffectController,
                card_zone: CardZoneType::Hand,
            })
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Haste),
                    },
                    PermanentEffectType::Sacrifice,
                ],
                selecting: Selecting::Specific(ObjectVariable::PreviousSelection),
                selection_condition: PermanentCondition::Any,
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Charging Troll")
            .expansion(Expansion::Invasion)
            .card_number(239)
            .rarity(Rarity::Uncommon)
            .artist_name("Dave Dorman")
            .flavor_text(r#"They stop for nothing, not even the end of a battle."#)
            .casting_cost("2GW")
            .not_snow()
            .non_legendary()
            .creature("3/3 Troll")
            .keyword(Keyword::Vigilance)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("G"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Regenerate))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Cinder Shade")
            .expansion(Expansion::Invasion)
            .card_number(240)
            .rarity(Rarity::Uncommon)
            .artist_name("Nelson DeCastro")
            .no_flavor_text()
            .casting_cost("1BR")
            .not_snow()
            .non_legendary()
            .creature("1/1 Shade")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("B"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(1, 1),
                }))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("R").self_sacrifice())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::Deal(Count::Power(ObjectVariable::Itself))],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_creature(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Coalition Victory")
            .expansion(Expansion::Invasion)
            .card_number(241)
            .rarity(Rarity::Rare)
            .artist_name("Eric Peterson")
            .flavor_text(
r#""You can build a perfect machine out of imperfect parts."
—Urza"#)
            .casting_cost("3WUBRG")
            .sorcery()
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::Conditional {
                condition: Condition::Permanent(PermanentCondition::And(vec![
                    PermanentCondition::controller_controls(PermanentCondition::plains()),
                    PermanentCondition::controller_controls(PermanentCondition::island()),
                    PermanentCondition::controller_controls(PermanentCondition::swamp()),
                    PermanentCondition::controller_controls(PermanentCondition::mountain()),
                    PermanentCondition::controller_controls(PermanentCondition::forest()),

                    PermanentCondition::controller_controls(PermanentCondition::white()),
                    PermanentCondition::controller_controls(PermanentCondition::blue()),
                    PermanentCondition::controller_controls(PermanentCondition::black()),
                    PermanentCondition::controller_controls(PermanentCondition::red()),
                    PermanentCondition::controller_controls(PermanentCondition::green()),
                ])),
                effect_type: Box::new(PlayerEffectType::Win),
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crosis, the Purger")
            .expansion(Expansion::Invasion)
            .card_number(242)
            .rarity(Rarity::Rare)
            .artist_name("Pete Venters")
            .no_flavor_text()
            .casting_cost("3UBR")
            .not_snow()
            .legendary()
            .creature("6/6 Dragon")
            .keyword(Keyword::Flying)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().mana("2B").into_permanent_cost())),
                effects: vec![
                    EffectTemplate::Player {
                        selecting: SelectingPlayers::defending(),
                        effect_types: vec![
                            PlayerEffectType::DiscardMatching {
                                card_condition: CardCondition::Color(ColorVariable::AbilityInput)
                            }
                        ],
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Darigaaz, the Igniter")
            .expansion(Expansion::Invasion)
            .card_number(243)
            .rarity(Rarity::Rare)
            .artist_name("Mark Zug")
            .no_flavor_text()
            .casting_cost("3BRG")
            .not_snow()
            .legendary()
            .creature("6/6 Dragon")
            .keyword(Keyword::Flying)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().mana("2U").into_permanent_cost())),
                effects: vec![
                    EffectTemplate::Damage {
                        effect_types: vec![
                            DamageEffectType::Deal(Count::MatchingCardsInHand {
                                // TODO: Make sure this SelectedPlayer actually makes sense for 3+ player games.
                                player_variable: PlayerVariable::Defending,
                                condition: Box::new(CardCondition::Color(ColorVariable::ResolvedInput)),
                            })
                        ],
                        damage_recipients: DamageRecipients::Player(PlayerVariable::Defending),
                    }
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Dromar, the Banisher")
            .expansion(Expansion::Invasion)
            .card_number(244)
            .rarity(Rarity::Rare)
            .artist_name("Dave Dorman")
            .no_flavor_text()
            .casting_cost("3WUB")
            .not_snow()
            .legendary()
            .creature("6/6 Dragon")
            .keyword(Keyword::Flying)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().mana("2U").into_permanent_cost())),
                effects: vec![
                    EffectTemplate::Global(GlobalEffectType::Permanent {
                        condition: PermanentCondition::color(ColorVariable::AbilityInput),
                        effect_types: vec![PermanentEffectType::bounce()],
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Dueling Grounds")
            .expansion(Expansion::Invasion)
            .card_number(245)
            .rarity(Rarity::Rare)
            .artist_name("Pete Venters")
            .flavor_text(
r#""You think you can stop me?" hissed Tsabo.
"I think I can kill you," replied Gerrard."#)
            .casting_cost("1GW")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::MaxAttackersPerTurn(Count::Fixed(1)))
            .global_static_ability(ContinuousGlobalEffectType::MaxBlockersPerTurn(Count::Fixed(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Fires of Yavimaya")
            .expansion(Expansion::Invasion)
            .card_number(246)
            .rarity(Rarity::Uncommon)
            .artist_name("Val Mayerik")
            .no_flavor_text()
            .casting_cost("1RG")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::ControlledByController,
                effect_types: vec![ContinuousPermanentEffectType::Keyword(Keyword::Haste)],
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_sacrifice())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(2, 2),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Frenzied Tilling")
            .expansion(Expansion::Invasion)
            .card_number(247)
            .rarity(Rarity::Common)
            .artist_name("Mike Raabe")
            .flavor_text(
r#""Beneath her scars, Dominaria's beauty yet shines."
—Multani, maro-sorcerer"#)
            .casting_cost("3RG")
            .sorcery()
            .add_effect(EffectTemplate::target_land(PermanentEffectType::destroy()))
            .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCardsToBattlefield {
                card_count: CardCount::Predetermined(Count::Fixed(1)),
                card_condition: CardCondition::BasicLand,
                enters_tapped: true,
            }))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Galina's Knight")
            .expansion(Expansion::Invasion)
            .card_number(248)
            .rarity(Rarity::Common)
            .artist_name("David Martin")
            .flavor_text(
r#""Are they on our side?" asked Sisay.
"If they kill Phyrexians they are," replied Gerrard."#)
            .casting_cost("UB")
            .not_snow()
            .non_legendary()
            .creature("2/2 Merfolk Knight")
            .keyword(Keyword::Protection(CardCondition::red()))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Hanna, Ship's Navigator")
            .expansion(Expansion::Invasion)
            .card_number(248)
            .rarity(Rarity::Rare)
            .artist_name("David Martin")
            .flavor_text(
                r#""I never thought I'd spend my life fighting. I'm a maker, not a destroyer.""#)
            .casting_cost("1WU")
            .not_snow()
            .legendary()
            .creature("1/2 Human Artificer")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1WU").self_tap())
                .add_effect(EffectTemplate::Card {
                    effect_types: vec![CardEffectType::MoveTo(CardZoneType::Hand)],
                    selecting: Selecting::one_target(),
                    selection_condition: CardCondition::Or(vec![
                        CardCondition::Artifact,
                        CardCondition::Enchantment,
                    ]),
                    selection_owner: PlayerVariable::EffectController,
                    card_zone: CardZoneType::Graveyard,
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Heroes' Reunion")
            .expansion(Expansion::Invasion)
            .card_number(250)
            .rarity(Rarity::Uncommon)
            .artist_name("Terese Nielsen")
            .flavor_text(
r#""You helped save my people from a Phyrexian fate. Did you think I wouldn't return the favor?"
—Eladamri, to Gerrard"#)
            .casting_cost("GW")
            .instant()
            .add_effect(EffectTemplate::target_player(PlayerEffectType::gain_life(7)))
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Horned Cheetah")
            .expansion(Expansion::Invasion)
            .card_number(251)
            .rarity(Rarity::Uncommon)
            .artist_name("John Matson")
            .flavor_text(r#""I think she wants us to eat it," said Squee, staring at the oily carcass the cheetah had dragged to their fire."#)
            .casting_cost("1WU")
            .not_snow()
            .non_legendary()
            .creature("2/2 Cat")
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::GainLifeEqualToSelfCombatDamageDealt))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Hunting Kavu")
            .expansion(Expansion::Invasion)
            .card_number(252)
            .rarity(Rarity::Uncommon)
            .artist_name("Scott M. Fischer")
            .flavor_text(r#"Dominarians gladly fought alongside kavu . . . until the kavu figured out some Dominarians were quite tasty."#)
            .casting_cost("1RG")
            .not_snow()
            .non_legendary()
            .creature("2/3 Kavu")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1RG").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![PermanentEffectType::exile()],
                    selecting: Selecting::ItselfUnconditionallyAndTargets(Count::Fixed(1)),
                    selection_condition: PermanentCondition::Flying,
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Kangee, Aerie Keeper")
            .expansion(Expansion::Invasion)
            .card_number(253)
            .rarity(Rarity::Rare)
            .artist_name("Mark Romanoski")
            .no_flavor_text()
            .casting_cost("2WU")
            .not_snow()
            .non_legendary()
            .creature("2/2 Bird Wizard")
            .kicker("2")
            .kicker_abilities(PermanentAbilitiesBuilder::new()
                .enter_the_battlefield_ability(EffectTemplate::self_effect(PermanentEffectType::AddCounters(
                    Count::KickerManaVariable(ColorCondition::Any),
                    CounterType::Feather,
                )))
                .build())
            .static_ability(StaticAbility::keyword(Keyword::Flying))
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::NotItself,
                    PermanentCondition::creature_type(CreatureType::Bird),
                ]),
                effect_types: vec![
                    ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                        Count::SelfCounters(CounterType::Feather))
                ],
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Llanowar Knight")
            .expansion(Expansion::Invasion)
            .card_number(254)
            .rarity(Rarity::Common)
            .artist_name("Heather Hudson")
            .flavor_text(r#"Her armor and steed were borrowed, but her courage was hers alone."#)
            .casting_cost("GW")
            .not_snow()
            .non_legendary()
            .creature("2/2 Elf Knight")
            .keyword(Keyword::Protection(CardCondition::black()))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Lobotomy")
            .expansion(Expansion::Invasion)
            .card_number(256)
            .rarity(Rarity::Uncommon)
            .artist_name("Heather Hudson")
            .no_flavor_text()
            .casting_cost("2UB")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::ExileAllMatching {
                        card_condition: CardCondition::Name(
                            CardNameVariable::ChooseFromHandOfSelectedPlayer),
                        card_zone_type: CardZoneType::Graveyard,
                    },
                    PlayerEffectType::ExileAllMatching {
                        card_condition: CardCondition::Name(
                            CardNameVariable::CurrentlySelectedName),
                        card_zone_type: CardZoneType::Hand,
                    },
                    PlayerEffectType::ExileAllMatching {
                        card_condition: CardCondition::Name(
                            CardNameVariable::CurrentlySelectedName),
                        card_zone_type: CardZoneType::Library,
                    },
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Meteor Storm")
            .expansion(Expansion::Invasion)
            .card_number(256)
            .rarity(Rarity::Rare)
            .artist_name("John Avon")
            .flavor_text(r#"The storm wasn't an omen. It was the fulfillment of one."#)
            .casting_cost("RG")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("2RG").discard_at_random_count(2))
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(4)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Noble Panther")
            .expansion(Expansion::Invasion)
            .card_number(257)
            .rarity(Rarity::Rare)
            .artist_name("Matt Cavotta")
            .flavor_text(r#"Unlike many hunters, these panthers have no need for camouflage. They're fast enough to catch any prey."#)
            .casting_cost("2GW")
            .not_snow()
            .non_legendary()
            .creature("3/3 Cat")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1"))
                .add_effect(EffectTemplate::self_creature_effect(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::FirstStrike),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Ordered Migration")
            .expansion(Expansion::Invasion)
            .card_number(258)
            .rarity(Rarity::Uncommon)
            .artist_name("Heather Hudson")
            .flavor_text(
                r#""Birds reach all parts of the world," said Barrin. "They will make excellent scouts.""#)
            .casting_cost("3WU")
            .sorcery()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![
                    PlayerEffectType::CreateCreatureTokens {
                        count: Count::SourceControllerBasicLandTypes,
                        colors: to_set(vec![Color::Blue]),
                        power: Power(Count::Fixed(1)),
                        toughness: Toughness(Count::Fixed(1)),
                        creature_types: to_set(vec![CreatureType::Bird]),
                        static_abilities: vec![StaticAbility::keyword(Keyword::Flying)],
                    }
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Overabundance")
            .expansion(Expansion::Invasion)
            .card_number(259)
            .rarity(Rarity::Rare)
            .artist_name("Ben Thompson")
            .no_flavor_text()
            .casting_cost("1RG")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::ManaAbilityActivated {
                    source_condition: PermanentCondition::land(),
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Mana {
                        mana_produced: ManaProduced::ChooseOneMatchingAvailable(
                            ColorsVariable::CurrentColorsProduced),
                    },
                    EffectTemplate::Damage {
                        effect_types: vec![DamageEffectType::deal(1)],
                        damage_recipients: DamageRecipients::Player(
                            PlayerVariable::TriggerObjectController),
                    },
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Plague Spores")
            .expansion(Expansion::Invasion)
            .card_number(260)
            .rarity(Rarity::Common)
            .artist_name("Randy Gallegos")
            .flavor_text(
r#""Breathe deep, Dominaria. Breathe deep and die."
—Tsabo Tavoc, Phyrexian general"#)
            .casting_cost("4BR")
            .sorcery()
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::not(PermanentCondition::black()),
                    PermanentCondition::creature(),
                ]),
            })
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::land(),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Pyre Zombie")
            .expansion(Expansion::Invasion)
            .card_number(262)
            .rarity(Rarity::Uncommon)
            .artist_name("Arnie Swekel")
            .no_flavor_text()
            .casting_cost("1BR")
            .not_snow()
            .non_legendary()
            .creature("2/1 Zombie")
            .graveyard_abilities(PermanentAbilitiesBuilder::new()
                .triggered_ability(TriggeredAbility {
                    trigger_condition: TriggerCondition::BeginningOfStep {
                        step_name: StepName::Upkeep,
                        player_turns: PlayerTurns::EffectController,
                    },
                    execution_condition: Condition::Always,
                    player_option: PlayerOption::May,
                    cost: Some(PermanentCostVariable::Fixed(cost().mana("1BB").into_permanent_cost())),
                    effects: vec![EffectTemplate::Card {
                        effect_types: vec![CardEffectType::MoveTo(CardZoneType::Hand)],
                        selecting: Selecting::Itself,
                        selection_condition: CardCondition::Any,
                        selection_owner: PlayerVariable::EffectController,
                        card_zone: CardZoneType::Graveyard,
                    }],
                    otherwise_effects: Vec::new(),
                })
                .build()
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1RR").self_sacrifice())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(2)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Raging Kavu")
            .expansion(Expansion::Invasion)
            .card_number(262)
            .rarity(Rarity::Uncommon)
            .artist_name("Arnie Swekel")
            .flavor_text(r#"It took Yavimaya a thousand years to breed them, but it took only seconds for them to prove their worth."#)
            .casting_cost("1RG")
            .not_snow()
            .non_legendary()
            .creature("3/1 Kavu")
            .keyword(Keyword::Flash)
            .keyword(Keyword::Haste)
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Reckless Assault")
            .expansion(Expansion::Invasion)
            .card_number(263)
            .rarity(Rarity::Rare)
            .artist_name("Jeff Easley")
            .flavor_text(
r#""How will you fight an enemy that cares nothing for itself?"
—The Blind Seer"#)
            .casting_cost("2BR")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").life(2))
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(1)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Recoil")
            .expansion(Expansion::Invasion)
            .card_number(260)
            .rarity(Rarity::Common)
            .artist_name("Alan Pollack")
            .flavor_text(r#"Anything sent into a plagued world is bound to come back infected."#)
            .casting_cost("1UB")
            .sorcery()
            .add_effect(EffectTemplate::target_permanent(PermanentEffectType::bounce()))
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::previous_selection_controller(),
                effect_types: vec![PlayerEffectType::discard(1)],
            })
            .build()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Reviving Vapors")
            .expansion(Expansion::Invasion)
            .card_number(265)
            .rarity(Rarity::Uncommon)
            .artist_name("Pete Venters")
            .no_flavor_text()
            .casting_cost("2WU")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::effect_controller(),
                effect_types: vec![
                    PlayerEffectType::LookAtCardsFromTopOfLibraryAndChooseOne {
                        card_count: Count::Fixed(3),
                        chosen_card_destination: CardLocation::Hand,
                        unchosen_cards_destination: CardLocation::Graveyard,
                        should_shuffle_afterwards: false,
                        should_reveal: true,
                    },
                    PlayerEffectType::GainLife(Count::ConvertedManaCost(ObjectVariable::PreviousSelection)),
                ],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Riptide Crab")
            .expansion(Expansion::Invasion)
            .card_number(266)
            .rarity(Rarity::Uncommon)
            .artist_name("David Martin")
            .flavor_text(r#"It sleeps with its claws open."#)
            .casting_cost("1WU")
            .not_snow()
            .non_legendary()
            .creature("1/3 Crab")
            .static_ability(StaticAbility::keyword(Keyword::Vigilance))
            .enter_the_graveyard_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rith, the Awakener")
            .expansion(Expansion::Invasion)
            .card_number(267)
            .rarity(Rarity::Rare)
            .artist_name("Carl Critchlow")
            .no_flavor_text()
            .casting_cost("3RGW")
            .not_snow()
            .legendary()
            .creature("6/6 Dragon")
            .keyword(Keyword::Flying)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().mana("2G").into_permanent_cost())),
                effects: vec![
                    EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                        count: Count::permanent(
                            PermanentCondition::color(ColorVariable::AbilityInput)),
                        colors: to_set(vec![Color::Green]),
                        power: Power(Count::Fixed(1)),
                        toughness: Toughness(Count::Fixed(1)),
                        creature_types: to_set(vec![CreatureType::Saproling]),
                        static_abilities: Vec::new(),
                    })
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sabertooth Nishoba")
            .expansion(Expansion::Invasion)
            .card_number(268)
            .rarity(Rarity::Rare)
            .artist_name("Gary Ruddell")
            .no_flavor_text()
            .casting_cost("4GW")
            .not_snow()
            .non_legendary()
            .creature("5/5 Cat")
            .keyword(Keyword::Trample)
            .keyword(Keyword::Protection(CardCondition::blue()))
            .keyword(Keyword::Protection(CardCondition::red()))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Samite Archer")
            .expansion(Expansion::Invasion)
            .card_number(269)
            .rarity(Rarity::Uncommon)
            .artist_name("Scott M. Fischer")
            .flavor_text(r#""I can preserve ten lives by taking one.""#)
            .casting_cost("1WU")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![
                        DamageEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            effect_type: ContinuousDamageEffectType::prevent(1),
                        }
                    ],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(1)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_recipient(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Seer's Vision")
            .expansion(Expansion::Invasion)
            .card_number(270)
            .rarity(Rarity::Uncommon)
            .artist_name("Rebecca Guay")
            .no_flavor_text()
            .casting_cost("2UB")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Player {
                condition: PlayerCondition::OpponentOf(PlayerVariable::EffectController),
                effect_types: vec![ContinuousPlayerEffectType::PlayWithHandRevealed],
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_sacrifice())
                .add_effect(EffectTemplate::Player {
                    selecting: SelectingPlayers::all_opponents_of_effect_controller(),
                    effect_types: vec![
                        PlayerEffectType::ChooseDiscard {
                            count: Count::Fixed(1),
                            card_condition: CardCondition::Any,
                        }
                    ]
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shivan Zombie")
            .expansion(Expansion::Invasion)
            .card_number(271)
            .rarity(Rarity::Common)
            .artist_name("Tony Szczudlo")
            .flavor_text(r#"Barbarians long for a glorious death in battle. Phyrexia was eager to grant that wish."#)
            .casting_cost("BR")
            .not_snow()
            .non_legendary()
            .creature("2/2 Barbarian Zombie")
            .keyword(Keyword::Protection(CardCondition::white()))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Simoon")
            .expansion(Expansion::Invasion)
            .card_number(272)
            .rarity(Rarity::Uncommon)
            .artist_name("Tony Szczudlo")
            .flavor_text(r#"Hot wind whipped up as if the weather itself wanted to fight Phyrexians."#)
            .casting_cost("RG")
            .instant()
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target_opponent_of_effect_controller(),
                effect_types: vec![PlayerEffectType::DealDamageToEachCreature(Count::Fixed(1))]
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sleeper's Robe")
            .expansion(Expansion::Invasion)
            .card_number(273)
            .rarity(Rarity::Uncommon)
            .artist_name("Alan Pollack")
            .no_flavor_text()
            .casting_cost("UB")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: vec![
                    AuraTriggeredAbility {
                        trigger_condition: AuraTriggerCondition::DealsCombatDamageToOpponent,
                        mana_produced: None,
                        effects: vec![EffectTemplate::controller_effect(PlayerEffectType::draw(1))],
                    }
                ],
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .keyword(Keyword::Fear)
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Slinking Serpent")
            .expansion(Expansion::Invasion)
            .card_number(274)
            .rarity(Rarity::Uncommon)
            .artist_name("Wayne England")
            .flavor_text(r#"It winds its way through undergrowth as easily as it swims through shallows."#)
            .casting_cost("2UB")
            .not_snow()
            .non_legendary()
            .creature("2/3 Serpent")
            .keyword(Keyword::forestwalk())
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Smoldering Tar")
            .expansion(Expansion::Invasion)
            .card_number(275)
            .rarity(Rarity::Uncommon)
            .artist_name("David Day")
            .no_flavor_text()
            .casting_cost("2BR")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::BeginningOfStep {
                    step_name: StepName::Upkeep,
                    player_turns: PlayerTurns::EffectController,
                },
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::Damage {
                        effect_types: vec![DamageEffectType::deal(1)],
                        damage_recipients: DamageRecipients::ControllerChooses {
                            count: Count::Fixed(1),
                            recipient_types: DamageRecipientTypes::any_player(),
                        },
                    },
                ],
                otherwise_effects: Vec::new(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_sacrifice())
                .sorcery_speed()
                .add_effect(EffectTemplate::Damage {
                    effect_types: vec![DamageEffectType::deal(4)],
                    damage_recipients: DamageRecipients::ControllerChooses {
                        count: Count::Fixed(1),
                        recipient_types: DamageRecipientTypes::any_creature(),
                    },
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Spinal Embrace")
            .expansion(Expansion::Invasion)
            .card_number(276)
            .rarity(Rarity::Uncommon)
            .artist_name("Donato Giancola")
            .no_flavor_text()
            .casting_cost("3UUB")
            .instant_with_casting_window_constraint(
                CastingWindowConstraint::Combat,
            )
            .add_effect(EffectTemplate::Permanent {
                effect_types: vec![
                    PermanentEffectType::Untap,
                    PermanentEffectType::GainControl {
                        new_controller: PlayerVariable::EffectController,
                    },
                    PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::Keyword(Keyword::Haste),
                    },
                ],
                selecting: Selecting::one_target(),
                selection_condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::ControlledByOpponent,
                ]),
            })
            // TODO: Audit whether the effect controllers are correct for Delays.
            .add_effect(EffectTemplate::Delay {
                until: StepName::End,
                effect_template: Box::new(EffectTemplate::Sequence(vec![
                    EffectTemplate::Permanent {
                        effect_types: vec![PermanentEffectType::Sacrifice],
                        selecting: Selecting::Specific(ObjectVariable::PreviousSelection),
                        selection_condition: PermanentCondition::Any,
                    },
                    EffectTemplate::controller_effect(PlayerEffectType::GainLife(
                        Count::Toughness(ObjectVariable::PreviousSelection))),
                ])),
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Stalking Assassin")
            .expansion(Expansion::Invasion)
            .card_number(277)
            .rarity(Rarity::Rare)
            .artist_name("Dana Knutson")
            .no_flavor_text()
            .casting_cost("2BR")
            .not_snow()
            .non_legendary()
            .creature("1/1 Human Assassin")
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("3U").self_tap())
                .add_effect(EffectTemplate::target_creature(PermanentEffectType::Tap))
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("3B").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![PermanentEffectType::destroy()],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::And(vec![
                        PermanentCondition::Tapped,
                        PermanentCondition::creature(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sterling Grove")
            .expansion(Expansion::Invasion)
            .card_number(278)
            .rarity(Rarity::Uncommon)
            .artist_name("Jeff Miracola")
            .no_flavor_text()
            .casting_cost("GW")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                effect_types: vec![ContinuousPermanentEffectType::Keyword(Keyword::Shroud)],
                condition: PermanentCondition::enchantment(),
            })
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                    card_count: CardCount::Predetermined(Count::Fixed(1)),
                    card_condition: CardCondition::Enchantment,
                    destination: CardLocation::Library(LibraryLocation::Top),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Teferi's Moat")
            .expansion(Expansion::Invasion)
            .card_number(279)
            .rarity(Rarity::Rare)
            .artist_name("rk post")
            .flavor_text(
r#""Isolation is not the answer."
—Urza, to Teferi."#)
            .casting_cost("3WU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::Permanent {
                effect_types: vec![ContinuousPermanentEffectType::CannotAttack],
                condition: PermanentCondition::And(vec![
                    PermanentCondition::creature(),
                    PermanentCondition::color(ColorVariable::EnterTheBattlefieldInput),
                    PermanentCondition::Flying,
                    PermanentCondition::ControlledByOpponent,
                ])
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Treva, the Renewer")
            .expansion(Expansion::Invasion)
            .card_number(280)
            .rarity(Rarity::Rare)
            .artist_name("Ciruelo")
            .no_flavor_text()
            .casting_cost("3GWU")
            .not_snow()
            .legendary()
            .creature("6/6 Dragon")
            .keyword(Keyword::Flying)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfDealsCombatDamageToPlayer,
                execution_condition: Condition::Always,
                player_option: PlayerOption::May,
                cost: Some(PermanentCostVariable::Fixed(cost().mana("2W").into_permanent_cost())),
                effects: vec![
                    EffectTemplate::controller_effect(PlayerEffectType::GainLife(Count::permanent(
                        PermanentCondition::color(ColorVariable::AbilityInput))))
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tsabo Tavoc")
            .expansion(Expansion::Invasion)
            .card_number(281)
            .rarity(Rarity::Rare)
            .artist_name("Michael Sutfin")
            .flavor_text(r#""I might almost pity my enemies—if it wasn't so amusing to watch them die.""#)
            .casting_cost("5BR")
            .not_snow()
            .legendary()
            .creature("6/6 Horror")
            .keyword(Keyword::FirstStrike)
            .keyword(Keyword::Protection(CardCondition::Legendary))
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("BB").self_tap())
                .add_effect(EffectTemplate::Permanent {
                    effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                    selecting: Selecting::one_target(),
                    selection_condition: PermanentCondition::And(vec![
                        PermanentCondition::legendary(),
                        PermanentCondition::creature(),
                    ]),
                })
            )
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Undermine")
            .expansion(Expansion::Invasion)
            .card_number(282)
            .rarity(Rarity::Rare)
            .artist_name("Massimilano Frezzato")
            .flavor_text(r#""Which would you like first, the insult or the injury?""#)
            .casting_cost("UUB")
            .instant()
            .add_effect(EffectTemplate::target_spell(StackObjectEffectType::Counter))
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::previous_selection_controller(),
                effect_types: vec![PlayerEffectType::lose_life(3)],
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Drake")
            .expansion(Expansion::Invasion)
            .card_number(283)
            .rarity(Rarity::Uncommon)
            .artist_name("Sam Wood")
            .flavor_text(r#"Relentless as the sea, remorseless as death."#)
            .casting_cost("1UB")
            .not_snow()
            .non_legendary()
            .creature("2/3 Drake")
            .keyword(Keyword::Flying)
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::MustAttack))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vicious Kavu")
            .expansion(Expansion::Invasion)
            .card_number(284)
            .rarity(Rarity::Uncommon)
            .artist_name("Sam Wood")
            .flavor_text(
                r#"As battle raged in Shiv, a strange new ally appeared from below the ravaged ground."#)
            .casting_cost("1BR")
            .not_snow()
            .non_legendary()
            .creature("2/2 Kavu")
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::Attacking),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(2, 0))),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vile Consumption")
            .expansion(Expansion::Invasion)
            .card_number(285)
            .rarity(Rarity::Rare)
            .artist_name("Heather Hudson")
            .flavor_text(r#"The plague moved faster than an army and was far more deadly."#)
            .casting_cost("1UB")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Regular)
            .global_triggered_ability(GlobalTriggeredAbility {
                permanent_condition: PermanentCondition::creature(),
                ability: TriggeredAbility {
                    trigger_condition: TriggerCondition::BeginningOfStep {
                        step_name: StepName::Upkeep,
                        player_turns: PlayerTurns::All,
                    },
                    execution_condition: Condition::Always,
                    player_option: PlayerOption::May,
                    cost: Some(PermanentCostVariable::fixed_life(1)),
                    effects: Vec::new(),
                    otherwise_effects: vec![EffectTemplate::self_effect(PermanentEffectType::Sacrifice)],
                }
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Vodalian Zombie")
            .expansion(Expansion::Invasion)
            .card_number(286)
            .rarity(Rarity::Common)
            .artist_name("Greg & Tim Hildebrandt")
            .flavor_text(
r#""Every last one of you will become my servant. It's a shame you won't live to see the irony."
—Tsabo Tavoc, Phyrexian general"#)
            .casting_cost("UB")
            .not_snow()
            .non_legendary()
            .creature("2/2 Merfolk Zombie")
            .keyword(Keyword::Protection(CardCondition::green()))
            .into_regular_permanent()
        ),
        Card::Ephemeral(RegularEphemeralBuilder::new()
            .name("Void")
            .expansion(Expansion::Invasion)
            .card_number(287)
            .rarity(Rarity::Rare)
            .artist_name("Kev Walker")
            .no_flavor_text()
            .casting_cost("3BR")
            .sorcery()
            .add_effect(EffectTemplate::Global(GlobalEffectType::Permanent {
                effect_types: vec![PermanentEffectType::destroy()],
                condition: PermanentCondition::And(vec![
                    PermanentCondition::Or(vec![
                        PermanentCondition::artifact(),
                        PermanentCondition::creature(),
                    ]),
                    PermanentCondition::compare_converted_mana_cost(
                        IntegerCondition::EqualTo(Count::NumberVariable)),
                ]),
            }))
            .add_effect(EffectTemplate::Player {
                selecting: SelectingPlayers::one_target(),
                effect_types: vec![
                    PlayerEffectType::DiscardMatching {
                        card_condition: CardCondition::And(vec![
                            CardCondition::not(CardCondition::Land),
                            CardCondition::CompareConvertedManaCost(
                                IntegerCondition::EqualTo(Count::NumberVariable)),
                        ])
                    }
                ]
            })
            .build()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Voracious Cobra")
            .expansion(Expansion::Invasion)
            .card_number(288)
            .rarity(Rarity::Uncommon)
            .artist_name("Terese Nielsen")
            .flavor_text(r#"There's no known antidote for the cobra's venom . . . or its appetite."#)
            .casting_cost("2RG")
            .not_snow()
            .non_legendary()
            .creature("2/2 Snake")
            .keyword(Keyword::FirstStrike)
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::DeadlyCombatDamage))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Wings of Hope")
            .expansion(Expansion::Invasion)
            .card_number(290)
            .rarity(Rarity::Common)
            .artist_name("Wayne England")
            .flavor_text(r#"Urza knew Phyrexians would come through the air and sent his soldiers to greet them."#)
            .casting_cost("WU")
            .not_snow()
            .non_legendary()
            .enchantment(EnchantmentType::Aura {
                attachment_target_condition: PermanentCondition::creature(),
                aura_triggered_abilities: Vec::new(),
                enchanted_permanent_abilities: PermanentAbilitiesBuilder::new()
                    .keyword(Keyword::Flying)
                    .static_ability(StaticAbility::Permanent(
                        ContinuousPermanentEffectType::adjust_power_toughness(1, 3)))
                    .build(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Yavimaya Barbarian")
            .expansion(Expansion::Invasion)
            .card_number(290)
            .rarity(Rarity::Common)
            .artist_name("Don Hazeltine")
            .flavor_text(r#"Not all elves embrace the pastoral life. Some still roam the forest's edge, forever making war against their hated enemies."#)
            .casting_cost("GW")
            .not_snow()
            .non_legendary()
            .creature("2/2 Elf Barbarian")
            .keyword(Keyword::Protection(CardCondition::blue()))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Yavimaya Kavu")
            .expansion(Expansion::Invasion)
            .card_number(291)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg Staples")
            .flavor_text(r#"Kavu hunt prey in packs—one kavu hunts a pack of prey."#)
            .casting_cost("2RG")
            .not_snow()
            .non_legendary()
            .complex_creature(Creature {
                creature_types: creature_types(vec![CreatureType::Kavu]),
                power: Power(Count::permanent(PermanentCondition::red())),
                toughness: Toughness(Count::permanent(PermanentCondition::green())),
            })
            .into_regular_permanent()
        ),
        Card::Ephemeral(EphemeralCard {
            expansion: Expansion::Invasion,
            card_number: card_number(292),
            rarity: Rarity::Uncommon,
            artist_name: artist_name("David Martin"),
            layout: EphemeralLayout::Split(
                EphemeralFaceBuilder::new()
                    .name("Stand")
                    .no_flavor_text()
                    .casting_cost("W")
                    .instant()
                    .add_effect(EffectTemplate::Damage {
                        effect_types: vec![
                            DamageEffectType::Continuous {
                                expiration: Expiration::EndOfTurn,
                                effect_type: ContinuousDamageEffectType::Prevent(Count::Fixed(2)),
                            }
                        ],
                        damage_recipients: DamageRecipients::ControllerChooses {
                            count: Count::Fixed(1),
                            recipient_types: DamageRecipientTypes::any_creature(),
                        },
                    })
                    .build(),
                EphemeralFaceBuilder::new()
                    .name("Deliver")
                    .no_flavor_text()
                    .casting_cost("2U")
                    .instant()
                    .add_effect(EffectTemplate::target_permanent(PermanentEffectType::bounce()))
                    .build(),
            )
        }),
        Card::Ephemeral(EphemeralCard {
            expansion: Expansion::Invasion,
            card_number: card_number(293),
            rarity: Rarity::Uncommon,
            artist_name: artist_name("David Martin"),
            layout: EphemeralLayout::Split(
                EphemeralFaceBuilder::new()
                    .name("Spite")
                    .no_flavor_text()
                    .casting_cost("3U")
                    .instant()
                    .add_effect(EffectTemplate::StackObject {
                        effect_types: vec![StackObjectEffectType::Counter],
                        selecting: Selecting::one_target(),
                        stack_object_types: to_set(vec![
                            StackObjectType::spell_matching_condition(
                                CardCondition::not(CardCondition::Creature))
                        ]),
                        selection_owner_condition: PlayerCondition::Any,
                    })
                    .build(),
                EphemeralFaceBuilder::new()
                    .name("Malice")
                    .no_flavor_text()
                    .casting_cost("3B")
                    .instant()
                    .add_effect(EffectTemplate::Permanent {
                        effect_types: vec![PermanentEffectType::DestroyWithNoRegeneration],
                        selecting: Selecting::one_target(),
                        selection_condition: PermanentCondition::And(vec![
                            PermanentCondition::not(PermanentCondition::black()),
                            PermanentCondition::creature(),
                        ]),
                    })
                    .build(),
            )
        }),
        Card::Ephemeral(EphemeralCard {
            expansion: Expansion::Invasion,
            card_number: card_number(294),
            rarity: Rarity::Uncommon,
            artist_name: artist_name("David Martin"),
            layout: EphemeralLayout::Split(
                EphemeralFaceBuilder::new()
                    .name("Pain")
                    .no_flavor_text()
                    .casting_cost("B")
                    .instant()
                    .add_effect(EffectTemplate::target_player(PlayerEffectType::discard(1)))
                    .build(),
                EphemeralFaceBuilder::new()
                    .name("Suffering")
                    .no_flavor_text()
                    .casting_cost("3R")
                    .instant()
                    .add_effect(EffectTemplate::target_land(PermanentEffectType::destroy()))
                    .build(),
            )
        }),
        Card::Ephemeral(EphemeralCard {
            expansion: Expansion::Invasion,
            card_number: card_number(295),
            rarity: Rarity::Uncommon,
            artist_name: artist_name("Ben Thompson"),
            layout: EphemeralLayout::Split(
                EphemeralFaceBuilder::new()
                    .name("Assault")
                    .no_flavor_text()
                    .casting_cost("R")
                    .sorcery()
                    .add_effect(EffectTemplate::Damage {
                        effect_types: vec![DamageEffectType::deal(2)],
                        damage_recipients: DamageRecipients::ControllerChooses {
                            count: Count::Fixed(1),
                            recipient_types: DamageRecipientTypes::any_recipient(),
                        },
                    })
                    .build(),
                EphemeralFaceBuilder::new()
                    .name("Battery")
                    .no_flavor_text()
                    .casting_cost("3G")
                    .sorcery()
                    .add_effect(EffectTemplate::controller_effect(PlayerEffectType::CreateCreatureTokens {
                            count: Count::Fixed(1),
                            power: Power(Count::Fixed(1)),
                            toughness: Toughness(Count::Fixed(1)),
                            colors: to_set(vec![Color::Green]),
                            creature_types: to_set(vec![CreatureType::Elephant]),
                            static_abilities: Vec::new(),
                        }
                    ))
                    .build(),
            )
        }),
        Card::Ephemeral(EphemeralCard {
            expansion: Expansion::Invasion,
            card_number: card_number(296),
            rarity: Rarity::Uncommon,
            artist_name: artist_name("Ben Thompson"),
            layout: EphemeralLayout::Split(
                EphemeralFaceBuilder::new()
                    .name("Wax")
                    .no_flavor_text()
                    .casting_cost("G")
                    .instant()
                    .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                        expiration: Expiration::EndOfTurn,
                        continuous_type: ContinuousPermanentEffectType::adjust_power_toughness(2, 2),
                    }))
                    .build(),
                EphemeralFaceBuilder::new()
                    .name("Wane")
                    .no_flavor_text()
                    .casting_cost("W")
                    .sorcery()
                    .add_effect(EffectTemplate::target_enchantment(PermanentEffectType::destroy()))
                    .build(),
            )
        }),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Alloy Golem")
            .expansion(Expansion::Invasion)
            .card_number(297)
            .rarity(Rarity::Uncommon)
            .artist_name("Greg Staples")
            .flavor_text(
r#""We turned old weapons into new warriors."
—Jhoira, master artificer"#)
            .casting_cost("6")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("4/4 Golem")
            .static_ability(StaticAbility::Permanent(
                ContinuousPermanentEffectType::AddColor(ColorVariable::EnterTheBattlefieldInput)))
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Bloodstone Cameo")
            .expansion(Expansion::Invasion)
            .card_number(298)
            .rarity(Rarity::Uncommon)
            .artist_name("Tony Szczudlo")
            .flavor_text(
r#""The stone whispers to me of dragon's fire and darkness. I wish I'd never pried it from the figurehead of that sunken Keldon longship."
—Isel, master carver"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("B or R")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Chromatic Sphere")
            .expansion(Expansion::Invasion)
            .card_number(299)
            .rarity(Rarity::Uncommon)
            .artist_name("Luca Zontini")
            .flavor_text(
r#""Let insight and energy be your guides."
—The Blind Seer, to Gerrard"#)
            .casting_cost("1")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_tap().self_sacrifice())
                .mana_produced("W or U or B or R or G")
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Crosis's Attendant")
            .expansion(Expansion::Invasion)
            .card_number(300)
            .rarity(Rarity::Uncommon)
            .artist_name("Arnie Swekel")
            .flavor_text(
                r#""Crosis is the eye of the ur-dragon, piercing illusion and darkness.""#)
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("3/3 Golem")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .mana_produced("U or B or R")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Darigaaz's Attendant")
            .expansion(Expansion::Invasion)
            .card_number(301)
            .rarity(Rarity::Uncommon)
            .artist_name("Brom")
            .flavor_text(
                r#""Darigaaz is the breath of the ur-dragon, burning away the burdens of mortality.""#)
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("3/3 Golem")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .mana_produced("B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Drake-Skull Cameo")
            .expansion(Expansion::Invasion)
            .card_number(302)
            .rarity(Rarity::Uncommon)
            .artist_name("Dan Frazier")
            .flavor_text(
r#""A strange skull was turned up by an Ephran farmer's plow. I traded a copper ring for the ‘ox skull.' It resonates of the sea and danger."
—Isel, master carver"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("U or B")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Dromar's Attendant")
            .expansion(Expansion::Invasion)
            .card_number(303)
            .rarity(Rarity::Uncommon)
            .artist_name("Carl Critchlow")
            .flavor_text(
                r#""Dromar is the wings of the ur-dragon, sweeping away all opposition.""#)
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("3/3 Golem")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .mana_produced("W or U or B")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Juntu Stakes")
            .expansion(Expansion::Invasion)
            .card_number(304)
            .rarity(Rarity::Rare)
            .artist_name("Mark Brill")
            .flavor_text(
r#""Those who can't survive are of no consequence."
—Tsabo Tavoc"#)
            .casting_cost("2")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::PermanentCannot {
                condition: PermanentCondition::Power(
                    IntegerCondition::LessThanOrEqualTo(Count::Fixed(1))),
                effect_types: vec![
                    PermanentCannotEffectType::CannotUntapDuringControllersUntapStep,
                ],
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Lotus Guardian")
            .expansion(Expansion::Invasion)
            .card_number(305)
            .rarity(Rarity::Rare)
            .artist_name("Dana Knutson")
            .flavor_text(r#"Lotus fields are too valuable to leave undefended."#)
            .casting_cost("7")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("4/4 Golem")
            .keyword(Keyword::Flying)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("W or U or B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Altar")
            .expansion(Expansion::Invasion)
            .card_number(306)
            .rarity(Rarity::Rare)
            .artist_name("Ron Spears")
            .flavor_text(
r#""Your life was meaningless, but your death will glorify Yawgmoth."
—Tsabo Tavoc"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().sacrifice_count(1, PermanentCondition::creature()))
                .mana_produced("W or U or B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Phyrexian Lens")
            .expansion(Expansion::Invasion)
            .card_number(307)
            .rarity(Rarity::Rare)
            .artist_name("Matt Cavotta")
            .flavor_text(
r#""There's nothing I wouldn't give to achieve victory. Can you say the same?"
—Tsabo Tavoc, to Gerrard"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().life(1))
                .mana_produced("W or U or B or R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Planar Portal")
            .expansion(Expansion::Invasion)
            .card_number(308)
            .rarity(Rarity::Rare)
            .artist_name("Mark Tedin")
            .flavor_text(
                r#"The sky split, and the air crackled and roiled. The Phyrexian invasion of Dominaria had finally begun."#)
            .casting_cost("6")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("6"))
                .add_effect(EffectTemplate::controller_effect(PlayerEffectType::TutorCards {
                    card_count: CardCount::Predetermined(Count::Fixed(1)),
                    card_condition: CardCondition::Any,
                    destination: CardLocation::Hand,
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Power Armor")
            .expansion(Expansion::Invasion)
            .card_number(309)
            .rarity(Rarity::Uncommon)
            .artist_name("Doug Chaffee")
            .flavor_text(
r#""Great peril demands formidable weaponry."
—Urza"#)
            .casting_cost("4")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("3"))
                .add_effect(EffectTemplate::target_permanent(PermanentEffectType::Continuous {
                    expiration: Expiration::EndOfTurn,
                    continuous_type:
                        ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                            Count::SourceControllerBasicLandTypes),
                }))
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Rith's Attendant")
            .expansion(Expansion::Invasion)
            .card_number(310)
            .rarity(Rarity::Uncommon)
            .artist_name("Adam Rex")
            .flavor_text(
                r#""Rith is the claw of the ur-dragon, scattering seeds of devastation.""#)
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("3/3 Golem")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .mana_produced("R or G or W")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Seashell Cameo")
            .expansion(Expansion::Invasion)
            .card_number(311)
            .rarity(Rarity::Uncommon)
            .artist_name("Tony Szczudlo")
            .flavor_text(
r#""Today a seashell fell from the empty sky here in Kinymu, a hundred leagues from the sea. I'm torn—shall I carve a woman or a bird?"
—Isel, master carver"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("W or U")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sparring Golem")
            .expansion(Expansion::Invasion)
            .card_number(312)
            .rarity(Rarity::Uncommon)
            .artist_name("Adam Rex")
            .flavor_text(
                r#""Part drill sergeant, part training dummy," thought Gerrard. "I hope it can stand up to a real war.""#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .triggered_ability(TriggeredAbility {
                trigger_condition: TriggerCondition::SelfIsBlocked,
                execution_condition: Condition::Always,
                player_option: PlayerOption::Must,
                cost: None,
                effects: vec![
                    EffectTemplate::self_creature_effect(
                        PermanentEffectType::Continuous {
                            expiration: Expiration::EndOfTurn,
                            continuous_type:
                                ContinuousPermanentEffectType::adjust_power_toughness_same_amount(
                                    Count::BlockerCount),
                        }
                    )
                ],
                otherwise_effects: Vec::new(),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tek")
            .expansion(Expansion::Invasion)
            .card_number(313)
            .rarity(Rarity::Rare)
            .artist_name("Chippy")
            .no_flavor_text()
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("2/2 Dragon")
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::ControllerControls(Box::new(
                    PermanentCondition::plains()))),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(0, 2),
                ))
            })
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::ControllerControls(Box::new(
                    PermanentCondition::island()))),
                effect_type: Box::new(StaticAbility::keyword(Keyword::Flying)),
            })
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::ControllerControls(Box::new(
                    PermanentCondition::swamp()))),
                effect_type: Box::new(StaticAbility::Permanent(
                    ContinuousPermanentEffectType::adjust_power_toughness(2, 0)
                ))
            })
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::ControllerControls(Box::new(
                    PermanentCondition::mountain()))),
                effect_type: Box::new(StaticAbility::keyword(Keyword::FirstStrike)),
            })
            .static_ability(StaticAbility::Conditional {
                condition: Condition::Permanent(PermanentCondition::ControllerControls(Box::new(
                    PermanentCondition::forest()))),
                effect_type: Box::new(StaticAbility::keyword(Keyword::Trample)),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tigereye Cameo")
            .expansion(Expansion::Invasion)
            .card_number(314)
            .rarity(Rarity::Uncommon)
            .artist_name("Donato Giancola")
            .flavor_text(
                r#""Today a seashell fell from the empty sky here in Kinymu, a hundred leagues from the sea. I'm torn—shall I carve a woman or a bird?"
—Isel, master carver"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("G or W")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Treva's Attendant")
            .expansion(Expansion::Invasion)
            .card_number(315)
            .rarity(Rarity::Uncommon)
            .artist_name("Christopher Moeller")
            .flavor_text(
                r#""Treva is the voice of the ur-dragon, demanding cries of worship.""#)
            .casting_cost("5")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .creature("3/3 Golem")
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("1").self_sacrifice())
                .mana_produced("G or W or U")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Troll-Horn Cameo")
            .expansion(Expansion::Invasion)
            .card_number(316)
            .rarity(Rarity::Uncommon)
            .artist_name("Donato Giancola")
            .flavor_text(
r#""I found a troll-horn fragment in the wooded foothills of Hurloon, and it keeps growing larger. I wonder, is the horn recreating itself or the troll?"
—Isel, master carver"#)
            .casting_cost("3")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("R or G")
            )
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tsabo's Web")
            .expansion(Expansion::Invasion)
            .card_number(317)
            .rarity(Rarity::Rare)
            .artist_name("Carl Critchlow")
            .no_flavor_text()
            .casting_cost("2")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .enter_the_battlefield_ability(EffectTemplate::controller_effect(PlayerEffectType::draw(1)))
            .global_static_ability(ContinuousGlobalEffectType::PermanentCannot {
                condition: PermanentCondition::And(vec![
                    PermanentCondition::land(),
                    PermanentCondition::HasNonManaActivatedAbility,
                ]),
                effect_types: vec![
                    PermanentCannotEffectType::CannotUntapDuringControllersUntapStep,
                ],
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urza's Filter")
            .expansion(Expansion::Invasion)
            .card_number(318)
            .rarity(Rarity::Uncommon)
            .artist_name("Dave Dorman")
            .flavor_text(
                r#""It's a tool for aiding my other tools," explained Urza. "Now they'll work in harmony.""#)
            .casting_cost("4")
            .not_snow()
            .non_legendary()
            .artifact(ArtifactType::Regular)
            .global_static_ability(ContinuousGlobalEffectType::SpellCostModifier {
                condition: CardCondition::Multicolored,
                modifier: CostModifier::Subtract(mana_cost("2")),
            })
            .into_regular_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Ancient Spring")
            .expansion(Expansion::Invasion)
            .card_number(319)
            .rarity(Rarity::Common)
            .artist_name("Don Hazeltine")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("U")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("WB")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Archaeological Dig")
            .expansion(Expansion::Invasion)
            .card_number(320)
            .rarity(Rarity::Uncommon)
            .artist_name("Don Hazeltine")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("C")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("W or U or B or R or G")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Coastal Tower")
            .expansion(Expansion::Invasion)
            .card_number(321)
            .rarity(Rarity::Uncommon)
            .artist_name("Don Hazeltine")
            .flavor_text(
                r#"The Capashen built the highest towers in Benalia to afford themselves the best view."#)
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("W or U")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Elfhame Palace")
            .expansion(Expansion::Invasion)
            .card_number(322)
            .rarity(Rarity::Uncommon)
            .artist_name("Jerry Tiritilli")
            .flavor_text(
                r#"Llanowar has seven elfhames, or kingdoms, each with its own ruler. Their palaces are objects of awe, wonder, and envy."#)
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("G or W")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Geothermal Crevice")
            .expansion(Expansion::Invasion)
            .card_number(323)
            .rarity(Rarity::Common)
            .artist_name("John Avon")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("R")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("BG")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Irrigation Ditch")
            .expansion(Expansion::Invasion)
            .card_number(324)
            .rarity(Rarity::Common)
            .artist_name("Rob Alexander")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("W")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("GU")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Keldon Necropolis")
            .expansion(Expansion::Invasion)
            .card_number(325)
            .rarity(Rarity::Rare)
            .artist_name("Franz Vohwinkel")
            .no_flavor_text()
            .not_snow()
            .legendary()
            .land(Land::regular_non_basic())
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("C")
            )
            .activated_ability(ActivatedAbilityBuilder::new()
                .cost(cost().mana("4R").self_tap().self_sacrifice())
                .add_effect(EffectTemplate::ChooseOne(vec![
                    EffectTemplate::Damage {
                        effect_types: vec![DamageEffectType::deal(2)],
                        damage_recipients: DamageRecipients::ControllerChooses {
                            count: Count::Fixed(1),
                            recipient_types: DamageRecipientTypes::any_recipient(),
                        },
                    },
                ]))
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Salt Marsh")
            .expansion(Expansion::Invasion)
            .card_number(326)
            .rarity(Rarity::Uncommon)
            .artist_name("Jerry Tiritilli")
            .flavor_text(
r#"Only death breeds in stagnant water.
—Urborg saying"#)
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("U or B")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Shivan Oasis")
            .expansion(Expansion::Invasion)
            .card_number(327)
            .rarity(Rarity::Uncommon)
            .artist_name("Rob Alexander")
            .flavor_text(r#"Only the hardiest explorers survive to eat the fruit."#)
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("R or G")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Sulfur Vent")
            .expansion(Expansion::Invasion)
            .card_number(328)
            .rarity(Rarity::Common)
            .artist_name("Edward P. Beard, Jr.")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("B")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("UR")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Tinder Farm")
            .expansion(Expansion::Invasion)
            .card_number(329)
            .rarity(Rarity::Common)
            .artist_name("Rob Alexander")
            .no_flavor_text()
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("G")
            )
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap().self_sacrifice())
                .mana_produced("RW")
            )
            .into_land_permanent()
        ),
        Card::Permanent(PermanentFaceBuilder::new()
            .name("Urborg Volcano")
            .expansion(Expansion::Invasion)
            .card_number(330)
            .rarity(Rarity::Uncommon)
            .artist_name("Tony Szczudlo")
            .flavor_text(
                r#"Deep in the heart of Urborg lie massive volcanoes whose thick black smoke covers the land with perpetual darkness."#)
            .not_snow()
            .non_legendary()
            .land(Land::regular_non_basic())
            .static_ability(StaticAbility::Permanent(ContinuousPermanentEffectType::EntersTapped))
            .activated_mana_ability(ActivatedAbilityBuilder::new()
                .cost(cost().self_tap())
                .mana_produced("B or R")
            )
            .into_land_permanent()
        ),

        Card::plains(Expansion::Invasion, card_number(331), artist_name("John Avon")),
        Card::plains(Expansion::Invasion, card_number(332), artist_name("Ben Thompson")),
        Card::plains(Expansion::Invasion, card_number(333), artist_name("D. J. Cleland-Hura")),
        Card::plains(Expansion::Invasion, card_number(334), artist_name("Scott Bailey")),

        Card::island(Expansion::Invasion, card_number(335), artist_name("Tony Szczudlo")),
        Card::island(Expansion::Invasion, card_number(336), artist_name("John Avon")),
        Card::island(Expansion::Invasion, card_number(337), artist_name("Terese Nielsen")),
        Card::island(Expansion::Invasion, card_number(338), artist_name("Darrell Riche")),

        Card::swamp(Expansion::Invasion, card_number(339), artist_name("Ron Spencer")),
        Card::swamp(Expansion::Invasion, card_number(340), artist_name("Rob Alexander")),
        Card::swamp(Expansion::Invasion, card_number(341), artist_name("Rob Alexander")),
        Card::swamp(Expansion::Invasion, card_number(342), artist_name("Ron Spencer")),

        Card::mountain(Expansion::Invasion, card_number(343), artist_name("Matt Cavotta")),
        Card::mountain(Expansion::Invasion, card_number(344), artist_name("Jeff Miracola")),
        Card::mountain(Expansion::Invasion, card_number(345), artist_name("Glen Angus")),
        Card::mountain(Expansion::Invasion, card_number(346), artist_name("Scott Bailey")),

        Card::forest(Expansion::Invasion, card_number(347), artist_name("John Avon")),
        Card::forest(Expansion::Invasion, card_number(348), artist_name("Alan Pollack")),
        Card::forest(Expansion::Invasion, card_number(349), artist_name("Alan Pollack")),
        Card::forest(Expansion::Invasion, card_number(350), artist_name("Glen Angus")),
    ]
}